<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CandidateEducationsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CandidateEducationsTable Test Case
 */
class CandidateEducationsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CandidateEducationsTable
     */
    public $CandidateEducations;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.candidate_educations',
        'app.candidate_completes',
        'app.candidates',
        'app.jobs',
        'app.users',
        'app.user_roles',
        'app.companies',
        'app.user_profiles',
        'app.company_users',
        'app.countries',
        'app.states',
        'app.cities',
        'app.job_types',
        'app.educations',
        'app.candidate_histories',
        'app.working_shifts',
        'app.shifts',
        'app.days',
        'app.candidate_militaries',
        'app.candidate_references'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('CandidateEducations') ? [] : ['className' => CandidateEducationsTable::class];
        $this->CandidateEducations = TableRegistry::get('CandidateEducations', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CandidateEducations);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
