<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CandidateHistoriesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CandidateHistoriesTable Test Case
 */
class CandidateHistoriesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CandidateHistoriesTable
     */
    public $CandidateHistories;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.candidate_histories',
        'app.candidates',
        'app.jobs',
        'app.users',
        'app.user_roles',
        'app.companies',
        'app.user_profiles',
        'app.company_users',
        'app.countries',
        'app.states',
        'app.cities',
        'app.job_types',
        'app.educations',
        'app.working_shifts',
        'app.shifts',
        'app.days'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('CandidateHistories') ? [] : ['className' => CandidateHistoriesTable::class];
        $this->CandidateHistories = TableRegistry::get('CandidateHistories', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CandidateHistories);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
