<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CompanyDeleteQuestionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CompanyDeleteQuestionsTable Test Case
 */
class CompanyDeleteQuestionsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CompanyDeleteQuestionsTable
     */
    public $CompanyDeleteQuestions;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.company_delete_questions',
        'app.companies',
        'app.users',
        'app.user_roles',
        'app.user_profiles',
        'app.divisions',
        'app.company_users',
        'app.interview_questions'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('CompanyDeleteQuestions') ? [] : ['className' => CompanyDeleteQuestionsTable::class];
        $this->CompanyDeleteQuestions = TableRegistry::get('CompanyDeleteQuestions', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CompanyDeleteQuestions);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
