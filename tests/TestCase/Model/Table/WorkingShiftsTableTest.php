<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\WorkingShiftsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\WorkingShiftsTable Test Case
 */
class WorkingShiftsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\WorkingShiftsTable
     */
    public $WorkingShifts;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.working_shifts',
        'app.shifts',
        'app.candidates',
        'app.jobs',
        'app.users',
        'app.user_roles',
        'app.companies',
        'app.user_profiles',
        'app.company_users',
        'app.countries',
        'app.states',
        'app.cities',
        'app.job_types',
        'app.educations',
        'app.candidate_histories',
        'app.days'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('WorkingShifts') ? [] : ['className' => WorkingShiftsTable::class];
        $this->WorkingShifts = TableRegistry::get('WorkingShifts', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->WorkingShifts);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
