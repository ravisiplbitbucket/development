<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\EducationsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\EducationsTable Test Case
 */
class EducationsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\EducationsTable
     */
    public $Educations;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.educations',
        'app.jobs',
        'app.users',
        'app.user_roles',
        'app.companies',
        'app.countries',
        'app.states',
        'app.cities',
        'app.job_types'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Educations') ? [] : ['className' => EducationsTable::class];
        $this->Educations = TableRegistry::get('Educations', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Educations);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
