<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\EmployeeEquipmentsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\EmployeeEquipmentsTable Test Case
 */
class EmployeeEquipmentsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\EmployeeEquipmentsTable
     */
    public $EmployeeEquipments;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.employee_equipments',
        'app.candidates',
        'app.jobs',
        'app.users',
        'app.user_roles',
        'app.companies',
        'app.user_profiles',
        'app.divisions',
        'app.company_users',
        'app.countries',
        'app.states',
        'app.cities',
        'app.job_types',
        'app.educations',
        'app.candidate_histories',
        'app.working_shifts',
        'app.shifts',
        'app.days',
        'app.candidate_completes',
        'app.candidate_educations',
        'app.candidate_militaries',
        'app.candidate_references',
        'app.equipment'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('EmployeeEquipments') ? [] : ['className' => EmployeeEquipmentsTable::class];
        $this->EmployeeEquipments = TableRegistry::get('EmployeeEquipments', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->EmployeeEquipments);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
