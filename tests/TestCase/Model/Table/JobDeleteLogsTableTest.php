<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\JobDeleteLogsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\JobDeleteLogsTable Test Case
 */
class JobDeleteLogsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\JobDeleteLogsTable
     */
    public $JobDeleteLogs;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.job_delete_logs',
        'app.jobs',
        'app.users',
        'app.user_roles',
        'app.companies',
        'app.countries',
        'app.states',
        'app.cities',
        'app.user_profiles',
        'app.divisions',
        'app.company_users',
        'app.job_types',
        'app.educations',
        'app.candidates',
        'app.candidate_histories',
        'app.working_shifts',
        'app.shifts',
        'app.days',
        'app.candidate_completes',
        'app.candidate_educations',
        'app.candidate_militaries',
        'app.candidate_references'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('JobDeleteLogs') ? [] : ['className' => JobDeleteLogsTable::class];
        $this->JobDeleteLogs = TableRegistry::get('JobDeleteLogs', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->JobDeleteLogs);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
