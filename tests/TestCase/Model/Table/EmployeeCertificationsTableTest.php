<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\EmployeeCertificationsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\EmployeeCertificationsTable Test Case
 */
class EmployeeCertificationsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\EmployeeCertificationsTable
     */
    public $EmployeeCertifications;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.employee_certifications',
        'app.candidates',
        'app.jobs',
        'app.users',
        'app.user_roles',
        'app.companies',
        'app.user_profiles',
        'app.divisions',
        'app.company_users',
        'app.countries',
        'app.states',
        'app.cities',
        'app.job_types',
        'app.educations',
        'app.candidate_histories',
        'app.working_shifts',
        'app.shifts',
        'app.days',
        'app.candidate_completes',
        'app.candidate_educations',
        'app.candidate_militaries',
        'app.candidate_references',
        'app.certifications'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('EmployeeCertifications') ? [] : ['className' => EmployeeCertificationsTable::class];
        $this->EmployeeCertifications = TableRegistry::get('EmployeeCertifications', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->EmployeeCertifications);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
