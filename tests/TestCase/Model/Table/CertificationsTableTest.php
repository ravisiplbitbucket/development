<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CertificationsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CertificationsTable Test Case
 */
class CertificationsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CertificationsTable
     */
    public $Certifications;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.certifications',
        'app.users',
        'app.user_roles',
        'app.companies'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Certifications') ? [] : ['className' => CertificationsTable::class];
        $this->Certifications = TableRegistry::get('Certifications', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Certifications);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
