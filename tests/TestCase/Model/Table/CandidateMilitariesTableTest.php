<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CandidateMilitariesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CandidateMilitariesTable Test Case
 */
class CandidateMilitariesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CandidateMilitariesTable
     */
    public $CandidateMilitaries;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.candidate_militaries',
        'app.candidate_completes',
        'app.candidates',
        'app.jobs',
        'app.users',
        'app.user_roles',
        'app.companies',
        'app.user_profiles',
        'app.company_users',
        'app.countries',
        'app.states',
        'app.cities',
        'app.job_types',
        'app.educations',
        'app.candidate_histories',
        'app.working_shifts',
        'app.shifts',
        'app.days',
        'app.candidate_educations',
        'app.candidate_references'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('CandidateMilitaries') ? [] : ['className' => CandidateMilitariesTable::class];
        $this->CandidateMilitaries = TableRegistry::get('CandidateMilitaries', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CandidateMilitaries);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
