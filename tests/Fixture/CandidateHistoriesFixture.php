<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * CandidateHistoriesFixture
 *
 */
class CandidateHistoriesFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'candidate_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'company_name' => ['type' => 'string', 'length' => 150, 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'from_work' => ['type' => 'string', 'length' => null, 'null' => false, 'default' => null, 'collate' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'to_work' => ['type' => 'string', 'length' => null, 'null' => false, 'default' => null, 'collate' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'where_employ' => ['type' => 'text', 'length' => null, 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null],
        'designation' => ['type' => 'string', 'length' => 100, 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'security_company' => ['type' => 'integer', 'length' => 4, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '0->no,1->yes', 'precision' => null, 'autoIncrement' => null],
        'report_client' => ['type' => 'integer', 'length' => 4, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '0->no,1->yes', 'precision' => null, 'autoIncrement' => null],
        'client_name' => ['type' => 'string', 'length' => 150, 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'may_we_contact' => ['type' => 'integer', 'length' => 4, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '0->no,1->yes', 'precision' => null, 'autoIncrement' => null],
        'kind_site' => ['type' => 'string', 'length' => 50, 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'rate_paid' => ['type' => 'decimal', 'length' => 10, 'precision' => 2, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => ''],
        'foot_patrol' => ['type' => 'integer', 'length' => 4, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '0->no,1->yes', 'precision' => null, 'autoIncrement' => null],
        'hours_per_shift' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'perform_patrols' => ['type' => 'integer', 'length' => 4, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '0->no,1->yes', 'precision' => null, 'autoIncrement' => null],
        'rounds_verified' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'required_submit_report' => ['type' => 'integer', 'length' => 4, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '0->no,1->yes', 'precision' => null, 'autoIncrement' => null],
        'way_report_submitted' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'shift_work' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'is_site_armed' => ['type' => 'integer', 'length' => 4, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '0->no,1->yes', 'precision' => null, 'autoIncrement' => null],
        'equipment_issued' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'used_equipment' => ['type' => 'integer', 'length' => 4, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '0->no,1->yes', 'precision' => null, 'autoIncrement' => null],
        'contact_emergency' => ['type' => 'integer', 'length' => 4, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '0->no,1->yes', 'precision' => null, 'autoIncrement' => null],
        'ever_injured' => ['type' => 'integer', 'length' => 4, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '0->no,1->yes', 'precision' => null, 'autoIncrement' => null],
        'operate_vehicle' => ['type' => 'integer', 'length' => 4, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '0->no,1->yes', 'precision' => null, 'autoIncrement' => null],
        'vechicle_type' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'candidate_id' => 1,
            'company_name' => 'Lorem ipsum dolor sit amet',
            'from_work' => 'Lorem ipsum dolor sit amet',
            'to_work' => 'Lorem ipsum dolor sit amet',
            'where_employ' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
            'designation' => 'Lorem ipsum dolor sit amet',
            'security_company' => 1,
            'report_client' => 1,
            'client_name' => 'Lorem ipsum dolor sit amet',
            'may_we_contact' => 1,
            'kind_site' => 'Lorem ipsum dolor sit amet',
            'rate_paid' => 1.5,
            'foot_patrol' => 1,
            'hours_per_shift' => 1,
            'perform_patrols' => 1,
            'rounds_verified' => 'Lorem ipsum dolor sit amet',
            'required_submit_report' => 1,
            'way_report_submitted' => 'Lorem ipsum dolor sit amet',
            'shift_work' => 'Lorem ipsum dolor sit amet',
            'is_site_armed' => 1,
            'equipment_issued' => 'Lorem ipsum dolor sit amet',
            'used_equipment' => 1,
            'contact_emergency' => 1,
            'ever_injured' => 1,
            'operate_vehicle' => 1,
            'vechicle_type' => 'Lorem ipsum dolor sit amet'
        ],
    ];
}
