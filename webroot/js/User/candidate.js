(function () {
    function disableEnable(elementclass, state) {
        $(elementclass).find('input, select, button').each(
            function () {                    
                $(this).attr('disabled', state).removeClass('disabled');
            }
        );
    }
    jQuery.validator.addMethod("validate_email",function(value, element)
    {       
        if(/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test( value ))
        {           
            return true;        
        } else {         
             return false;        
        } 
    },"Please enter a valid Email.");

    function hideLoader() {
        $(document).find('.loader').hide();
    }
    function showLoader() {
        $(document).find('.loader').show();
    }

    function nextTab(elem) {
        $(elem).next().find('a[data-toggle="tab"]').click();
    }
    function prevTab(elem) {
        $(elem).prev().find('a[data-toggle="tab"]').click();
    }

    $('#datetimepicker1').datetimepicker({
        viewMode: 'years',
        format: 'YYYY'
    });

    $('#when_worked').datetimepicker({
        viewMode: 'years',
        format: 'YYYY'
    });

    $('#from-work').datetimepicker({
        viewMode: 'years',
        format: 'MM/DD/YYYY'
    });
    $('#to-work').datetimepicker({
        viewMode: 'years',
        format: 'MM/DD/YYYY'
    });

    
    $('select[name="country_id"]').on('change', function() {
    url = base_url+'jobs/get-states/'+$(this).val();
    $.ajax({
        url: url,
    }).done(function(html) {
            $(".state-container").html(html);
        });
    });

    $(document).on('change', 'select[name="state_id"]', function() {
        url = base_url+'jobs/get-cities/'+$(this).val();
        $.ajax({
            url: url,
        }).done(function(html) {
            $(".city-container").html(html);
        });
    });

    $('select[name="country_id"]').on('change', function() {
    url = base_url+'jobs/get-insurances/'+$(this).val();
    $.ajax({
        url: url,
    }).done(function(html) {
            $(".insurance-container").html(html);
        });
    });

    $('#candidate-apply-first-step').submit(function(event){
        if($('#candidate-apply-first-step').valid() ){
            event.preventDefault();
            showLoader();
            $.ajax({
                url: $('#candidate-apply-first-step').attr('action')+'.json',
                type: 'POST',
                data: $('#candidate-apply-first-step').serialize() ,
                dataType: 'json'
            }).done(function(data) {
                // $('#step2').html(data);
                var $active = $('.wizard .nav-tabs li.active');
                $active.next().removeClass('disabled');
                nextTab($active);
                setStepTwo();
                hideLoader();
                validatesecond();
                $(".prev-step").click(function (e) {            
                    var $active = $('.wizard .nav-tabs li.active');
                    prevTab($active);
                
                });

            });
        }
    })

    function setStepTwo() {
        $(document).find('#candidate-apply-second-step').submit(function(event){
            if($('#candidate-apply-second-step').valid() ){
                event.preventDefault()
                showLoader();
                $.ajax({
                    url: $('#candidate-apply-second-step').attr('action')+'.json',
                    type: 'POST',
                    data: $('#candidate-apply-second-step').serialize() ,
                }).done(function(data) {
                    // $('#step3').html(data);
                    var $active = $('.wizard .nav-tabs li.active');
                    $active.next().removeClass('disabled');
                    nextTab($active);
                    validatethird();
                    $(".prev-step").click(function (e) {            
                        var $active = $('.wizard .nav-tabs li.active');
                        prevTab($active);
                    
                    });
                    $(document).find('#candidate-history').unbind('submit');
                    history();
                    getHistoryDetail()
                    commonScript();
                    hideLoader();
                });
            }
        })
    }

    $('#candidate-apply-fourth-step').submit(function(event){
        if($('#candidate-apply-fourth-step').valid() ){
            event.preventDefault();
            showLoader();
            $.ajax({
                url: $('#candidate-apply-fourth-step').attr('action')+'.json',
                type: 'POST',
                data: $('#candidate-apply-fourth-step').serialize() ,
                dataType: 'json'
            }).done(function(data) {
                // $('#step2').html(data);
                var $active = $('.wizard .nav-tabs li.active');
                $active.next().removeClass('disabled');
                nextTab($active);
                setStepTwo();
                hideLoader();
                validatesecond();
                $(".prev-step").click(function (e) {            
                    var $active = $('.wizard .nav-tabs li.active');
                    prevTab($active);
                
                });

            });
        }
    })

    function history(){
         $(document).find('#candidate-history').submit(function(event){
            event.preventDefault();
            if ($(this).valid()) {
                showLoader();
                $.ajax({
                    url: $('#candidate-history').attr('action'),
                    type: 'POST',
                    data: $('#candidate-history').serialize() ,
                    // dataType:'json'
                }).done(function(data) {
                    hideLoader();
                    $('#history-list').append(data);
                    $('#employment-history').modal('hide');
                    getHistoryDetail();
                    commonScript();
                    
                    $('#candidate-history').find('input[type="text"]').val('');
                    $('input[name="security_company"][value="0"]').click();
                    $('#candidate-history').find('select').val('Residential');
                     $("#candidate-history #dates-field2").multiselect('deselectAll', false);
                    $("#candidate-history #dates-field2").multiselect('refresh');
                });
            }
        });
    }

    function edit() {        
    $(document).find('#edit-candidate-history').submit(function(event){
            event.preventDefault();
            if ($(this).valid()) {
                showLoader();
                $.ajax({
                    url: $('#edit-candidate-history').attr('action'),
                    type: 'POST',
                    data: $('#edit-candidate-history').serialize() ,
                    // dataType:'json'
                }).done(function(data) {
                    hideLoader();
                    // $('#history-list').append(data);
                    $('#edit-history-modal').modal('hide');
                    getHistoryDetail();
                    // commonScript(); 
                });
            }
        });
    }

    $(document).on('click','.edit-history', function(){
        var url= $(this).attr('data-url');
        var ele = this;
        showLoader();
        $.ajax({
            url: url,
            type: 'GET',
            // dataType:'json'
        }).done(function(data) {
            hideLoader();
            $('#edit-history-modal').find('.modal-body').html(data);
            $('#edit-history-modal').modal('show');
            edit();

            $('.multiselect-ui').multiselect({
                 includeSelectAllOption: true
            });
        
            $('.report-submit').multiselect({
                includeSelectAllOption: true
            });

            $('.languages-speak').multiselect({
                includeSelectAllOption: true
            });

        });
    });

    function getHistoryDetail() {
        $('.get-history-detail').on('click', function(){
            showLoader();
            var url = $(this).attr('data-url');
            $.ajax({
                url: url,
                type: 'get',
                // dataType:'json'
            }).done(function(data) {
                $('#history-detail-modal').find('.modal-body').html(data);
                $('#history-detail-modal').modal('show');
                hideLoader();
            });
        });
    }
    
    //Wizard
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
    
    var $target = $(e.target);
    
    if ($target.parent().hasClass('disabled')) {
        return false;
    }
    });
            
    $(".prev-step").click(function (e) {            
        var $active = $('.wizard .nav-tabs li.active');
        prevTab($active);
    
    });

    function commonScript() {
        addDisabledField();
        $('.guard').on('click', function(){
            if($(this).val()=="1"){
                $('.security-guard').removeClass('hide');
            }
            else{
                 $('.security-guard').addClass('hide');
            }
            addDisabledField();
        })
        
        $('.patrols').on('click', function(){
            if($(this).val()=="1"){
                $('.foot-patrols').removeClass('hide');
            }
            else{
                 $('.foot-patrols').addClass('hide');
            }
            addDisabledField();
        })
        
         $('.reports').on('click', function(){
            if($(this).val()=="1"){
                $('.submit-reports').removeClass('hide');
            }
            else{
                 $('.submit-reports').addClass('hide');
            }
            addDisabledField();
        })
        
           
        $('.vehicle').on('click', function(){
            if($(this).val()=="1"){
                $('.vechicle-required').removeClass('hide');
            }
            else{
                 $('.vechicle-required').addClass('hide');
            }
            addDisabledField();
        })
        
        
        $(function() {
            $('.multiselect-ui').multiselect({
                 includeSelectAllOption: true
            });
        
            $('.report-submit').multiselect({
             includeSelectAllOption: true
            });
        });
    }
    commonScript()

    function validatefirst(){
        $('#candidate-apply-first-step').validate({
            rules: {
                'first_name': {
                    required: true
                },
                'last_name': {
                    required: true
                },
                'home_phone' :{
                    required : true,
                    maxlength : 10
                },
                'mobile' : {
                    maxlength : 10
                },
                'email' : {
                    required : true,
                    validate_email : true
                },
                'address1' : {
                    required : true
                },
                'country_id' : {
                    required : true
                },
                'state_id' : {
                    required : true
                },
                'city_id' : {
                    required : true
                },
                'zipcode' : {
                    required : true,
                    maxlength : 6
                },
               'job_type_id' : {
                    required : true
                }
            },
            messages: {
                'first_name': {
                    required: "First name must not be empty"
                },
                'last_name': {
                    required: "Last name must not be empty"
                },
                'home_phone' :{
                    required : "Home phone number must not be empty",
                    maxlength : "Home phone number must not be greater than 10 digits"
                },
                'mobile' : {
                    maxlength : "Mobile Phone number must not be greater than 10 digits"
                },
                'email' : {
                    required : "Email must not be empty",
                    validate_email : "Please enter the correct format"
                },
                'address1' : {
                    required : "Please enter the address"
                },
                'country_id' : {
                    required : "Please select the country"
                },
                'state_id' : {
                    required : "Please select the state"
                },
                'city_id' : {
                    required : "Please select the city"
                },
                'zipcode' : {
                    required : "Zipcode must not be empty",
                    maxlength : "Zipcode must not be greater than 6"
                },
               'job_type_id' : {
                    required : "Please select the job type"
                }
            },
            errorPlacement: function(error, element) {
                if (element.hasClass("logo"))
                   error.insertAfter($(element).closest('label'));
               
                else
                   error.insertAfter(element.parents('.form-group'));
           }
        });
    }
    validatefirst();

    function validatesecond() {
            $('#candidate-apply-second-step').validate({
            rules: {
                'hours_available': {
                    required: true,
                },
                'working_shifts[1][][day_id]': {
                    require_from_group: [1, ".shift-group"]
                },
                'working_shifts[2][][day_id]': {
                    require_from_group: [1, ".shift-group"]
                },
                'working_shifts[3][][day_id]': {
                    require_from_group: [1, ".shift-group"]
                }
            },
            messages: {
               'hours_available': {
                    required: "Please enter the hours available",
                },
                'working_shifts[1][][day_id]': {
                    required: "Select the days for the shift"
                }
            },
            errorPlacement: function(error, element) {
                if (element.hasClass("logo"))
                   error.insertAfter($(element).closest('label'));
               
                else
                   error.insertAfter(element.parents('.form-group'));
           }
        });
    }
    validatethird();
    function validatethird() {
        $('#candidate-apply-third-step').validate({
            rules: {
                'writing_test': {
                    required: true
                }
            },
            messages: {
               'writing_test': {
                    required: "Please write down what you have seen in the picture"
                }
            },
            errorPlacement: function(error, element) {
                if (element.hasClass("logo"))
                   error.insertAfter($(element).closest('label'));
               
                else
                   error.insertAfter(element.parents('.form-group'));
           }
        })

        function toDate(dateStr) {
           var parts = dateStr.split("/");
           return new Date(parts[2], parts[1] - 1, parts[0]);
        }
        $.validator.addMethod("greaterThan", function (value, element, params) {
               
               return this.optional(element) || toDate(value) >= toDate($(params).val());
           },
           'Must be greater than pickup date.'
        );

        $('#candidate-history').validate({
            rules: {
                'company_name': {
                    required: true
                },
                'from_work' : {
                    required: true
                },
                'to_work' : {
                    required: true,
                    greaterThan : "#from-work"
                },
                'where_employ' : {
                    required: true
                },
                'designation' : {
                    required: true
                },
                'client_name': {
                    required: true
                },
                'rate_paid': {
                    required: true
                },
                'hours_per_shift': {
                    required: true
                }                
            },
            messages: {
                'company_name': {
                    required: "Please enter company name"
                },
                'from_work': {
                    required: "Please enter the work starting time"
                },
                'to_work': {
                    required: "Please enter the work end time"
                },
                'where_employ' : {
                    required: "Please enter the state"
                },
                'designation' : {
                    required: "Please enter Your job title/Rank"
                },
                'client_name' : {
                    required: "Please enter a client name"
                },
                'rate_paid' : {
                    required: "Please enter a rate paid"
                },
                'hours_per_shift' : {
                    required: "Please enter a hours per shift"
                }
            },
            errorPlacement: function(error, element) {
                if (element.hasClass("logo"))
                   error.insertAfter($(element).closest('label'));
               
                else
                   error.insertAfter(element.parents('.form-group'));
           }
        });
    };

    //disabled the validated hidden field@ stop them to send value in form submit.
    function addDisabledField() {
        $('#candidate-history').find('input, select, button').each(
            function () {
                if ($(this).parents('.hide').length) {
                    $(this).attr('disabled', true);
                } else {
                    $(this).attr('disabled', false).removeClass('disabled');
                }
            }
        )
    }

    //reset validation form on opening modal
    $(document).on('click', '#historyModalIcon', function() {
        var validater = $('#candidate-history').validate();
        validater.resetForm();

    })

    $("#add-note").validate({
        rules: {
            'note_by': {
                required: true
            },
            'candidate_note': {
                required: true
            }
        },
        messages: {
            'note_by': {
                required: "Enter the name"
            },
            'candidate_note': {
                required: "Enter the Note"
            }
        },
        errorPlacement: function(error, element) {
               error.insertAfter(element.parents('.form-group'));
       }
    });

      function calculateScore() {
        var total = 0;

       $( ".score" ).each(function() {
          total = total + parseFloat($( this ).val());
        });
       $('.total').html(total);
    }
    calculateScore();
    history();
})();
