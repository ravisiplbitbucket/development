(function () {
	$("#training").validate({
        rules: {
            'name': {
                required: true
            },
            'description': {
                required: true
            },
            'hours' :{
            	required : true
            }
        },
        messages: {
            'name': {
                required: "Training name must not be empty"
            },
            'description': {
                required: "Training description must not be empty"
            },
            'hours' :{
            	required : "Training hours must not be empty"
            }
        },
        errorPlacement: function(error, element) {
               error.insertAfter(element.parents('.form-group'));
       }
    });

    $("#certificate").validate({
        rules: {
            'name': {
                required: true
            },
            'image' :{
            	extension : 'jpg|png|jpeg|gif'
            }
        },
        messages: {
            'name': {
                required: "Certificate name must not be empty"
            },
            'image' :{
            	extension : "Select the file with valid extension"
            }
        },
        errorPlacement: function(error, element) {
               error.insertAfter(element.parents('.form-group'));
       }
    });

    $("#equipment").validate({
        rules: {
            'item': {
                required: true
            },
            'description': {
                required: true
            }
        },
        messages: {
            'item': {
                required: "Equipment item must not be empty"
            },
            'description': {
                required: "Equipment description must not be empty"
            }
        },
        errorPlacement: function(error, element) {
               error.insertAfter(element.parents('.form-group'));
       }
    });

    $("#question").validate({
        rules: {
            'name': {
                required: true
            }
        },
        messages: {
            'name': {
                required: "Interview Question must not be empty"
            }
        },
        errorPlacement: function(error, element) {
               error.insertAfter(element.parents('.form-group'));
       }
    });

    $('.edit-training').on('click',function(){
        var url = $(this).attr('data-url');
        $.ajax({
                type:"GET",
                url:url,        
                success: function(data){
                    console.log(data);
                    $('#edit-training').find('.modal-body').html(data);
                    $('#edit-training').modal('show');
                    $('#edittraining').validate({
                        rules: {
                            'name' : {
                                required : true
                            },
                            'description' : {
                                required : true 
                            },
                            'hours' : {
                                required : true
                            }                            
                        },
                        messages: {
                            'name' : {
                                required : "Training Name must not be empty"
                            },
                            'description' : {
                                required : "Description must not empty" 
                            },
                            'hours' : {
                                required : "Hours must not be empty"
                            }    
                        }
                    });
                },
               
            });
        });

        $('.edit-certificate').on('click',function(){
        var url = $(this).attr('data-url');
        $.ajax({
                type:"GET",
                url:url,        
                success: function(data){
                    console.log(data);
                    $('#edit-certificate').find('.modal-body').html(data);
                    $('#edit-certificate').modal('show');
                    $('#edit-certificate1').validate({
                        rules: {
                            'name' : {
                                required : true
                            },
                            'description' : {
                                required : true
                            } ,
                            'image' : {
                                required : true,
                                extension : 'png|jpg|jpeg|gif'
                            }                    
                        },
                        messages: {
                            'name' : {
                                required : "Certificate name must not be empty"
                            },
                            'description' : {
                                required : "Certificate description must not be empty"
                            },
                            'image' : {
                                required : "Image must not be empty",
                                extension : "Please upload the correct format of image"
                            }  
                        }
                    });
                },
               
            });
        });

        $('.edit-equipment').on('click',function(){
        var url = $(this).attr('data-url');
        $.ajax({
                type:"GET",
                url:url,        
                success: function(data){
                    console.log(data);
                    $('#edit-equipment').find('.modal-body').html(data);
                    $('#edit-equipment').modal('show');
                    $('#edit-equipment1').validate({
                        rules: {
                            'item' : {
                                required : true
                            },
                            'description' : {
                                required : true
                            }                 
                        },
                        messages: {
                           'item' : {
                                required : "Item Name must not be empty"
                            },
                            'description' : {
                                required : "Item Description must not be empty"
                            }      
                        }
                    });
                },
               
            });
        });
        $('.edit-question').on('click',function(){
        var url = $(this).attr('data-url');
        $.ajax({
                type:"GET",
                url:url,        
                success: function(data){
                    console.log(data);
                    $('#edit-question').find('.modal-body').html(data);
                    $('#edit-question').modal('show');
                    $('#question-edit1').validate({
                        rules: {
                          'name' : {
                            required : true
                          }                          
                        },
                        messages: {
                            'name' : {
                                required : "Question must not be empty"
                            } 
                        }
                    });
                },
               
            });
        });
        $('.edit-division').on('click',function(){
        var url = $(this).attr('data-url');
        $.ajax({
                type:"GET",
                url:url,        
                success: function(data){
                    console.log(data);
                    $('#edit-division').find('.modal-body').html(data);
                    $('#edit-division').modal('show');
                    $('#division-edit-form').validate({
                        rules: {
                          'name' : {
                            required : true
                          }                          
                        },
                        messages: {
                            'name' : {
                                required : "Division must not be empty"
                            } 
                        }
                    });
                },
               
            });
        });
        $('.edit-admin').on('click',function(){
        var url = $(this).attr('data-url');
        $.ajax({
                type:"GET",
                url:url,        
                success: function(data){
                    console.log(data);
                    $('#editadmin').find('.modal-body').html(data);
                    $('#editadmin').modal('show');
                    $('#edit-admin').validate({
                        rules: {
                           'username' : {
                            required : true
                           },
                           'email' : {
                            required : true,
                            email : true
                           },
                           'user_profile[first_name]' : {
                            required : true
                           },
                           'user_profile[last_name]' : {
                            required : true
                           }                
                        },
                        messages: {
                           'username' : {
                            required : "Username must not be empty"
                           },
                           'email' : {
                            required : "Email must not be empty",
                            email : "Please enter the correct format"
                           },
                           'user_profile[first_name]' : {
                            required : "First Name must not be empty"
                           },
                           'user_profile[last_name]' : {
                            required : "Last name must not be empty"
                           }       
                        }
                    });
                },
               
            });
        });

        $("#admin").validate({
         rules: {
                   'username' : {
                    required : true
                   },
                   'email' : {
                    required : true,
                    email : true
                   },
                   'user_profile[first_name]' : {
                    required : true
                   },
                   'user_profile[last_name]' : {
                    required : true
                   },
                   'password' : {
                    required : true,
                    maxlength:8
                   },
                   'user_profile[division_id]' : {
                    required : true
                   }           
                },
                messages: {
                   'username' : {
                    required : "Username must not be empty"
                   },
                   'email' : {
                    required : "Email must not be empty",
                    email : "Please enter the correct format"
                   },
                   'user_profile[first_name]' : {
                    required : "First Name must not be empty"
                   },
                   'user_profile[last_name]' : {
                    required : "Last name must not be empty"
                   },
                   password : {
                    required: "password must not be empty",
                    maxlength : "Password must not be greater than 8 characters"
                   },
                   'user_profile[division_id]' : {
                        required : "Select the division"
                   }
            },
        errorPlacement: function(error, element) {
               error.insertAfter(element.parents('.form-group'));
       }
    });

     $('input[type = "checkbox"]').click(function(){
        if($(this). prop("checked") == true){
            url = base_url+'users/add-selected-questions/'+$(this).val()+'.json';
            $.ajax({
                url: url,
                }).done(function(html) {
                   $('.content-wrapper').before('<div class="message success text-center alert alert-success main-success-box">Interview Question Has been Selected</div>');
                   $('.success').delay(5000).fadeOut();
                });
            }
        else if($(this). prop("checked") == false){
            url = base_url+'users/delete-selected-questions/'+$(this).val()+'.json';
            $.ajax({
                url: url,
                }).done(function(html) {
                   $('.content-wrapper').before('<div class="message success text-center alert alert-success main-success-box">Interview Question Has been de-selected</div>'); 
                   $('.success').delay(5000).fadeOut();
                });
            }
   });
})();