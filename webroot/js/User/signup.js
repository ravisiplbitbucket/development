(function () {
    jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-z\s]+$/i.test(value);
    }, "Enter Letters only please");

    $.validator.addMethod("cus_url", function(value, element) { 
    if(value.substr(0,7) != 'http://'){
        value = 'http://' + value;
    }
    if(value.substr(value.length-1, 1) != '/'){
        value = value + '/';
    }
    return this.optional(element) || /^(http|https|ftp):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i.test(value); 
    }, "Not valid url.");

    jQuery.validator.addMethod("alphanumeric", function(value, element) {
        return this.optional(element) || /^[a-zA-Z0-9\s]+$/.test(value);
    });

    jQuery.validator.addMethod("validate_email",function(value, element)
     {       
        if(/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test( value ))
            {           
                return true;        
            } else {         
                 return false;        
        } },"Please enter a valid Email.");
    $('#signup').validate({
        rules: {
            'email': {
                required: true,
                email: true,
                validate_email :true
            },
            'password': {
                required: true,
                minlength: 8
            },
            'confirm_password': {
                required: true,
                equalTo: '#password'
            },
            'username' :{
                required : true,
            },
            'company[first_name]' : {
                required : true,
                lettersonly : true
            },
            'company[last_name]' : {
                required : true,
                lettersonly : true
            },
            'company[company]' : {
                required : true
            },
           ' company[address1]' : {
                required : true
            },
            'company[country]' : {
                required : true
            },
            'company[state]' : {
                required : true
            },
            'company[city]' : {
                required : true
            },
           'company[zipcode]' : {
                required : true,
                maxlength : 6
            },
            'company[telephone]' : {
                required : true,
                //digits : true
            },
            'company[know_about_us]' :{
                required : true
            },
            'company[home_url]' : {
                required : true
            },
            'company[website_name]' : {
                required : true,
                alphanumeric : true
            },
            'company[website_logo]'  :{
                required : true,
                extension : 'png|jpeg|jpg|png'
            },
            'agreement' : {
                required : true
            }
        },
        messages: {
            'email': {
                required: "Enter the Email",
                email: "Enter the correct format of the Email"
            },
            'password': {
                required: "Enter the password",
                minlength: "Password must be of atleast 6 characters"
            },
            'confirm_password': {
                required: "Enter the confirm password",
                equalTo: 'Password and confirm password do not match'
            },
            'username' :{
                required : 'Username must not be empty',
            },
            'company[first_name]' : {
                required : "First Name must not be empty"
            },
            'company[last_name]' : {
                required : "Last Name must not be empty"
            },
            'company[company]' : {
                required : "Company must not be empty"
            },
            'company[address1]' : {
                required : "Address must not be empty"
            },
            'company[country]' : {
                required : "Country must not be empty"
            },
            'company[state]' : {
                required : "State must not be empty"
            },
            'company[city]' : {
                required : "City must not be empty"
            },
            'company[zipcode]' : {
                required : "Zipcode must not be empty",
                maxlength : "Zipcode must not be greater than 8 characters"
            },
            'company[telephone]' : {
                required : "Telephone must not be empty",
                digits : "Telephone must be in digits only"
            },
            'company[know_about_us]' :{
                required : "Please tell us from where you heard about us"
            },
            'company[home_url]' : {
                required : "Website Url must not be empty"
            },
            'company[website_name]' : {
                required : "Website name must be required",
                alphanumeric : "Enter only alphanumeric characters"
            },
            'company[website_logo]'  :{
                required : "Please upload the logo",
                extension : "Logo must have jpg,png,gif extension"
            },
            'agreement' : {
                required : "You must agree with terms and conditions"
            }
        },
        errorPlacement: function(error, element) {
            if (element.hasClass("logo"))
               error.insertAfter($(element).closest('label'));                            
           
            else
               error.insertAfter(element.parents('.form-group'));
       }
    });
    

    $(".next-step").click(function (e) { 
        
        if ($('#signup').valid() == true){
            var $active = $('.wizard .nav-tabs li.active');
            $active.next().removeClass('disabled');
            nextTab($active); 
        }           
           
    });

    $(".prev-step").click(function (e) {    
        var $active = $('.wizard .nav-tabs li.active');
        prevTab($active);        
    });
    
    function nextTab(elem) {
        $(elem).next().find('a[data-toggle="tab"]').click();
    }
    function prevTab(elem) {
        $(elem).prev().find('a[data-toggle="tab"]').click();
    }
    $('select[name="company[country_id]"]').on('change', function() {
       url = base_url+'users/get-states/'+$(this).val();
       $.ajax({
            url: url,
        }).done(function(html) {
            $(".state-container").html(html);
            console.log(pcode);
            $('.country-phone-code').val(pcode);
            $('.country-phone-code1').html('+'+pcode);
        });
    });

    $(document).on('change', 'select[name="company[state_id]"]', function() {
       url = base_url+'users/get-cities/'+$(this).val();
       $.ajax({
            url: url,
        }).done(function(html) {
            $(".city-container").html(html);
        });
    });
     
    $("#login").validate({
        rules: {
            'username': {
                required: true
            },
            'password': {
                required: true
            }
        },
        messages: {
            'username': {
                required: "Enter the username"
            },
            'password': {
                required: "Enter the password"
            }
        },
        errorPlacement: function(error, element) {
               error.insertAfter(element.parents('.input-group'));
       }
    });
   
    $("#forgot-password").validate({
        rules: {
            'email': {
                required: true,
                validate_email : true
            }
        },
        messages: {
            email: {
                required: "Enter the Email",
                validate_email : "Enter the correct format of email"
            }
        },
        errorPlacement: function(error, element) {
               error.insertAfter(element.parents('.input-group'));
       }
    });
    
    $("#recover-password").validate({
        rules: {
            'password': {
                required: true
            },
            'new_password': {
                required: true,
                equalTo : '#password'
            }
        },
        messages: {
            'password': {
                required: "Enter the new password"
            },
            'new_password': {
                required: "Enter the confirm password"
            }
        },
        errorPlacement: function(error, element) {
               error.insertAfter(element.parents('.input-group'));
       }
    });
    $('#signup').submit(function(event){        
       var file = $('input[name="company[website_logo]"]').val();
       if(file == ""){
           $('input[name="company[website_logo]"]').closest('.form-group').append("<label class='error logo-error'>Company logo is required</lable")
           event.preventDefault();
       }else{
           var ext = file.split('.').pop().toLowerCase();
           if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
               $('input[name="company[website_logo]"]').closest('.form-group').append("<label class='error logo-error'>Invalid file extension.Please select the image of valid extension(jpg,jpeg,png,gif)</lable")
               event.preventDefault();
           }else {
               $('.logo-error').remove();
           }
       }
   });
    $("#register-phone").mask("999 999 9999");

    $("#setup").validate({
        rules: {
            'company[company]': {
                required: true
            },
            'company[address1]': {
                required: true
            },
            'company[country_id]' :{
                required : true
            },
            'company[state_id]' : {
                required : true
            },
            'company[city_id]' : {
                required : true
            },
            'company[zipcode]' :{
                required : true,
                maxlength : 6
            },
            'company[website_url]' : {
                required : true
            },
            'company[home_url]' : {
                required : true,
                cus_url : true
            },
            'company[overview]' : {
                required : true
            },
            'company[website_logo]' : {
                extension : 'jpeg|png|jpg|gif'
            }
        },
        messages: {
            'company[company]': {
                required: "Company name must not be empty"
            },
            'company[address1]': {
                required: "Address must not be empty"
            },
            'company[country_id]' :{
                required : "Please select the country"
            },
            'company[state_id]' : {
                required : "Please select the state"
            },
            'company[city_id]' : {
                required : "Please select the city"
            },
            'company[zipcode]' :{
                required : "Postal code must not be empty",
                maxlength : "Postal code must not be greater than 6 characters"
            },
            'company[website_url]' : {
                required : "Website url must not be empty"
            },
            'company[home_url]' : {
                required : "Home url must not be empty",
                cus_url : "Please enter the valid url"
            },
            'company[overview]' : {
                required : "Company overview must not be empty"
            },
            'company[website_logo]' : {
                extension : 'Please select the correct format'
            }
        },
        errorPlacement: function(error, element) {
               error.insertAfter(element.parents('.form-group'));
       }
    });
    
    $('#company-overview').on('keyup',function(){
        if($(this).length > 0) {
            $('#company-website-url-error').addClass('hide');
        }else{
            $('#company-website-url-error').removeClass('hide');
        }
    })
})();


