(function () {
    function disableEnable(elementclass, state) {
        $(elementclass).find('input, select, button').each(
            function () {                    
                $(this).attr('disabled', state).removeClass('disabled');
            }
        );
    }

    jQuery.validator.addMethod("validate_email",function(value, element)
     {       
        if(/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test( value ))
            {           
                return true;        
            } else {         
                 return false;        
        } },"Please enter a valid Email.");

    function nextTab(elem) {
        $(elem).next().find('a[data-toggle="tab"]').click();
    }
    function prevTab(elem) {
        $(elem).prev().find('a[data-toggle="tab"]').click();
    }
    education();
    reference();
    $('#candidate-apply-first-step').submit(function(event){
        if($('#candidate-apply-first-step').valid() ){
            event.preventDefault();
            showLoader();
            $.ajax({
                url: $('#candidate-apply-first-step').attr('action')+'.json',
                type: 'POST',
                data: $('#candidate-apply-first-step').serialize() ,
                dataType: 'json'
            }).done(function(data) {
                var $active = $('.wizard .nav-tabs li.active');
                $active.next().removeClass('disabled');
                nextTab($active);
                hideLoader();
                $(".prev-step").click(function (e) {            
                    var $active = $('.wizard .nav-tabs li.active');
                    prevTab($active);
                
                });

            });
        }
    })

    $(document).find('#candidate-apply-second-step').submit(function(event){
        if($('#candidate-apply-second-step').valid() ){
            event.preventDefault()
            showLoader();
            $.ajax({
                url: $('#candidate-apply-second-step').attr('action')+'.json',
                type: 'POST',
                data: $('#candidate-apply-second-step').serialize() ,
            }).done(function(data) {
                var $active = $('.wizard .nav-tabs li.active');
                $active.next().removeClass('disabled');
                nextTab($active);
                $(".prev-step").click(function (e) {            
                    var $active = $('.wizard .nav-tabs li.active');
                    prevTab($active);                    
                });
                hideLoader();
            });
        }
    });

    function history(){
         $(document).find('#edit-candidate-history').submit(function(event){
            event.preventDefault();
            if ($(this).valid()) {
                showLoader();
                $.ajax({
                    url: $('#edit-candidate-history').attr('action'),
                    type: 'POST',
                    data: $('#edit-candidate-history').serialize() ,
                    // dataType:'json'
                }).done(function(data) {
                    hideLoader();
                    $('#history-list').append(data);
                    $('#employment-history').modal('hide');
                    $('#edit-history-modal').modal('hide');
                    getHistoryDetail();
                    // commonScript(); 
                });
            }
        });
    }

    function education() {        
        $(document).find('#candidate-education-form').submit(function(event){
            event.preventDefault();
            if ($(this).valid()) {
                showLoader();
                $.ajax({
                    url: $('#candidate-education-form').attr('action'),
                    type: 'POST',
                    data: $('#candidate-education-form').serialize() ,
                    // dataType:'json'
                }).done(function(data) {
                    hideLoader();
                    $('#education-list').append(data);
                    $('#candidate-education').modal('hide');

                });
            }
        });
    }
    function reference() {        
        $(document).find('#candidate-reference-form').submit(function(event){
            event.preventDefault();
            if ($(this).valid()) {
                showLoader();
                $.ajax({
                    url: $('#candidate-reference-form').attr('action'),
                    type: 'POST',
                    data: $('#candidate-reference-form').serialize() ,
                    // dataType:'json'
                }).done(function(data) {
                    hideLoader();
                    $('#reference-list').append(data);
                    $('#candidate-reference').modal('hide');
                    // commonScript(); 
                });
            }
        });
    }

    $(document).on('submit', '#candidate-history',function(event){
        event.preventDefault();
        if ($(this).valid()) {
            showLoader();
            $.ajax({
                url: $('#candidate-history').attr('action'),
                type: 'POST',
                data: $('#candidate-history').serialize() ,
                dataType:'json'
            }).done(function(data) {
                hideLoader();
                $('#edit-history-modal').modal('hide');
                alert(data.response.msg); 
            });
        }
     });
    $(document).on('click','.delete-education-link', function(){
        var url= $(this).attr('data-url');
        var ele = this;
        showLoader();
        $.ajax({
            url: url,
            type: 'GET',
            dataType:'json'
        }).done(function(data) {
            hideLoader();
            if(data.response.status == 200){
                $(ele).closest('.col-sm-12').remove();
            }
        });
    });

    $(document).on('click','.get-history', function(){
        var url= $(this).attr('data-url');
        var ele = this;
        showLoader();
        $.ajax({
            url: url,
            type: 'GET',
            // dataType:'json'
        }).done(function(data) {
            hideLoader();
            $('#edit-history-modal').find('.modal-content').html(data);
            $('#edit-history-modal').modal('show');
            history();
            historyDisplayFields()

            $('.multiselect-ui').multiselect({
                 includeSelectAllOption: true
            });
        
            $('.report-submit').multiselect({
                includeSelectAllOption: true
            });

            $('.languages-speak').multiselect({
                includeSelectAllOption: true
            });

        });
    });

  
    function getHistoryDetail() {
        $('.get-history-detail').on('click', function(){
            showLoader();
            var url = $(this).attr('data-url');
            $.ajax({
                url: url,
                type: 'get',
                // dataType:'json'
            }).done(function(data) {
                $('#history-detail-modal').find('.modal-body').html(data);
                $('#history-detail-modal').modal('show');
                hideLoader();
            });
        });
    }


    function historyDisplayFields () {
         if($('.guard:checked').val()=="1"){
            $('.security-guard').removeClass('hide');
            disableEnable('.security-guard', false);
        }
        else{
             $('.security-guard').addClass('hide');
            disableEnable('.security-guard', true);
        }

        if($('.patrols:checked').val()=="1"){
            $('.foot-patrols').removeClass('hide');
            disableEnable('.foot-patrols', false);
        }
        else{
             $('.foot-patrols').addClass('hide');
             disableEnable('.foot-patrols', true);
        }

        if($('.reports:checked').val()=="1"){
            $('.submit-reports').removeClass('hide');
            disableEnable('.submit-reports', false);
        }
        else{
             $('.submit-reports').addClass('hide');
             disableEnable('.submit-reports', true);
        }

        if($('.vehicle:checked').val()=="1"){
            $('.vechicle-required').removeClass('hide');
            disableEnable('.vechicle-required', false);
        }
        else{
             $('.vechicle-required').addClass('hide');
             disableEnable('.vechicle-required', true);
        }

        if($('.emergency-services:checked').val()=="1"){
            $('.emergency-services-section').removeClass('hide');
            disableEnable('.emergency-services-section', false);
        }
        else{
             $('.emergency-services-section').addClass('hide');
             disableEnable('.emergency-services-section', true);
        }
  
        if($('.injured:checked').val()=="1"){
            $('.injured-section').removeClass('hide');
            disableEnable('.injured-section', false);
        }
        else{
             $('.injured-section').addClass('hide');
             disableEnable('.injured-section', true);
        }
        if($('.accident-involved:checked').val()=="1"){
            $('.accident-involved-section').removeClass('hide');
            disableEnable('.accident-involved-section', false);
        }
        else{
             $('.accident-involved-section').addClass('hide');
             disableEnable('.accident-involved-section', true);
        }
        if($('.equipment:checked').val()=="1"){
            $('.equipment-section').removeClass('hide');
            disableEnable('.equipment-section', false);
        }
        else{
            $('.equipment-section').addClass('hide');
            disableEnable('.equipment-section', true);
        }

        $('#date_started').datetimepicker({
               format: 'DD/MM/YYYY'
        });
        $('#date_ended').datetimepicker({
               format: 'DD/MM/YYYY'
        });
        
        
    }

    $("#candidate-apply-first-step").validate({
        rules: {
            'social_security': {
                required: true,
            },
            'language_ids' : {
                required : true
            }
        },
        messages: {
            'social_security': {
                required: "Social security number must not be empty",
            },
            'language_ids' : {
                required : "Select one of the language"
            }
        },
        errorPlacement: function(error, element) {
               error.insertAfter(element.parents('.form-group'));
        }
    });
    $("#candidate-education-form").validate({
        rules: {
            'education_id': {
                required: true,
            },
            'from_year' : {
                required : true
            },
            'to_year' : {
                required : true
            },
            'grade' : {
                required : true
            },
            'degree' : {
                required : true
            }
        },
        messages: {
            'education_id': {
                required: "Select the education type"
            },
            'from_year' : {
                required : "Select the from year of your education"
            },
            'to_year' : {
                required :" Select the to year of your education"
            },
            'grade' : {
                required : "Emter the grade"
            },
            'degree' : {
                required : "Enter the degree"
            }
        },
        errorPlacement: function(error, element) {
               error.insertAfter(element.parents('.form-group'));
        }
    });
    // $("#candidate-apply-second-step").validate({
    //     rules: {
    //         'candidate_military[military_from_year]': {
    //             required: true,
    //         },
    //         'candidate_military[military_to_year]' : {
    //             required : true
    //         },
    //         'candidate_military[branch]' : {
    //             required : true
    //         },
    //         'candidate_military[meeting_date]' : {
    //             required : true
    //         },
    //         'candidate_military[skills]' : {
    //             required : true
    //         }
    //     },
    //     messages: {
    //         'candidate_military[military_from_year]': {
    //             required: "Select the from year",
    //         },
    //         'candidate_military[military_to_year]' : {
    //             required : "Select the to year"
    //         },
    //         'candidate_military[branch]' : {
    //             required : "Enter the branch"
    //         },
    //         'candidate_military[meeting_date]' : {
    //             required : "Enter the meeting Date"
    //         },
    //         'candidate_military[skills]' : {
    //             required : "Enter the skills"
    //         }
    //     },
    //     errorPlacement: function(error, element) {
    //            error.insertAfter(element.parents('.form-group'));
    //     }
    // });
    $("#candidate-reference-form").validate({
        rules: {
           'name': {
                required: true,
            },
            'address' : {
                required : true
            },
            'occupation' : {
                required : true
            }
        },
        messages: {
            'name': {
                required: "Enter the name",
            },
            'address' : {
                required : "Enter the address"
            },
            'occupation' : {
                required : "Enter the Occupation"
            }
        },
        errorPlacement: function(error, element) {
               error.insertAfter(element.parents('.form-group'));
        }
    });

    $('#client-number').mask("(+99)999 999 9999");

    $("#edit-candidate-history").validate({
        rules: {
           'company_email1': {
                required: true,
                validate_email : true
            },
            'company_email2' : {
                required : true,
                validate_email : true
            },
            'city' : {
                required : true
            },
            'zipcode' : {
                required : true,
                maxlength : 6
            },
            'client_number' :{
                required : true
            },
            'required_submit_report' : {
                required : true
            },
            'is_comfot_sotware' : {
                required : true
            },
            'used_equipment' :{
                required : true
            },
            'contact_emergency' :{
                required : true
            },
            'ever_injured' :{
                required : true
            },
            'operate_vehicle' : {
                required : true
            }
        },
        messages: {
            'company_email1': {
                required: "Company Email must not be empty"
            },
            'company_email2' : {
                required : "Company email 2 must not be empty"
            },
            'city' : {
                required : "City must not be empty"
            },
            'zipcode' : {
                required : "Zipcode must not be empty",
                maxlength : "Zipcode must not be greater than 6 digits"
            },
            'client_number' :{
                required : "Client number must not be empty"
            },
            'required_submit_report' : {
                required : "Select one of the radio button"
            },
            'is_comfot_sotware' : {
                required :"Select one of the radio button"
            },
            'used_equipment' :{
                required : "Select one of the radio button"
            },
            'contact_emergency' :{
                required : "Select one of the radio button"
            },
            'ever_injured' :{
                required : "Select one of the radio button"
            },
            'operate_vehicle' : {
                required : "Select one of the radio button"
            }
        },
        errorPlacement: function(error, element) {
               error.insertAfter(element.parents('.form-group'));
        }
    });

    function addDisabledField() {
        $('input, select, button').each(
            function () {
                if ($(this).parents('.hide').length) {
                    $(this).attr('disabled', true);
                } else {
                    $(this).attr('disabled', false).removeClass('disabled');
                }
            }
        )
    }
    addDisabledField();

    
})();