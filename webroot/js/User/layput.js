    function disableEnable(elementclass, state) {
        $(elementclass).find('input, select, button').each(
            function () {                    
                $(this).attr('disabled', state).removeClass('disabled');
            }
        );
    }
$(document).ready(function () {
                //loadimg
            
                $('#upload').loadImg({
                    "text"  : "+",
                    "fileExt" : ["png","jpg"],
                   
                });         
            
                //Wizard
                $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
                
                    var $target = $(e.target);
                    
                    if ($target.parent().hasClass('disabled')) {
                        return false;
                    }
                });
                //Add Inactive Class To All Accordion Headers
                $('.accordion-header').toggleClass('inactive-header');
                
                //Set The Accordion Content Width
                var contentwidth = $('.accordion-header').width();
                $('.accordion-content').css({});
                
                //Open The First Accordion Section When Page Loads
                $('.accordion-header').first().toggleClass('active-header').toggleClass('inactive-header');
                $('.accordion-content').first().slideDown().toggleClass('open-content');
                
                // The Accordion Effect
                $('.accordion-header').click(function () {
                    if($(this).is('.inactive-header')) {
                        $('.active-header').toggleClass('active-header').toggleClass('inactive-header').next().slideToggle().toggleClass('open-content');
                        $(this).toggleClass('active-header').toggleClass('inactive-header');
                        $(this).next().slideToggle().toggleClass('open-content');
                    }
                    
                    else {
                        $(this).toggleClass('active-header').toggleClass('inactive-header');
                        $(this).next().slideToggle().toggleClass('open-content');
                    }
                });
            });

            // Employee Applicaion

        $(document).on('click','.worked-for-company', function(){
        if($(this).val()=="1"){
            $('.workedfor-company').removeClass('hide');
            $('.workedfor-company').find('input, select, button').each(
                function () {                    
                    $(this).attr('disabled', false).removeClass('disabled');
                }
            );
        }
        else{
            $('.workedfor-company').addClass('hide');
            $('.workedfor-company').find('input, select, button').each(
                function () {                    
                    $(this).attr('disabled', true).removeClass('disabled');
                }
            );
        }
    })
    
    $(document).on('click', '.convicted-of-crime',function(){
        if($(this).val()=="1"){
            $('.crime').removeClass('hide');
        }
        else{
             $('.crime').addClass('hide');
        }
    })
    
     $(document).on('click','.licence', function(){
        if($(this).val()=="1"){
            $('.valid-licence').removeClass('hide');
            disableEnable('.valid-licence', false);
        }
        else{
             $('.valid-licence').addClass('hide');
            disableEnable('.valid-licence', true);

        }
    })
    
    $(document).on('click','.automobile', function(){
        if($(this).val()=="1"){
            $('.dependable-automobile').removeClass('hide');
            disableEnable('.dependable-automobile', false);
        }
        else{
            $('.dependable-automobile').addClass('hide');
            disableEnable('.dependable-automobile', true);

        }
    })
    
    $(document).on('click', '.guard',function(){
        if($(this).val()=="1"){
            $('.security-guard').removeClass('hide');
            disableEnable('.security-guard', false);
        }
        else{
            $('.security-guard').addClass('hide');
            disableEnable('.security-guard', true);
        }
    })
    
    $(document).on('click', '.patrols',function(){
        if($(this).val()=="1"){
            $('.foot-patrols').removeClass('hide');
            disableEnable('.foot-patrols', false);
        }
        else{
             $('.foot-patrols').addClass('hide');
             disableEnable('.foot-patrols', false);
        }
    })
    
     $(document).on('click','.reports', function(){
        if($(this).val()=="1"){
            $('.submit-reports').removeClass('hide');
            disableEnable('.reports', false);
        }
        else{
             $('.submit-reports').addClass('hide');
            disableEnable('.reports', true);
        }
    })
    
       
    $(document).on('click','.vehicle', function(){
        if($(this).val()=="1"){
            $('.vechicle-required').removeClass('hide');
            disableEnable('.vechicle-required', false);
        }
        else{
             $('.vechicle-required').addClass('hide');
             disableEnable('.vechicle-required', true);
        }
    })
    
    $(document).on('click', '.equipment', function(){
        if($(this).val()=="1"){
            $('.equipment-section').removeClass('hide');
            disableEnable('.equipment-section', false);
        }
        else{
            $('.equipment-section').addClass('hide');
            disableEnable('.equipment-section', true);
        }
    })
    
    $(document).on('click', '.emergency-services', function(){
        if($(this).val()=="1"){
            $('.emergency-services-section').removeClass('hide');
            disableEnable('.emergency-services-section', false);
        }
        else{
             $('.emergency-services-section').addClass('hide');
             disableEnable('.emergency-services-section', true);
        }
    })
    
    $(document).on('click','.injured', function(){
        if($(this).val()=="1"){
            $('.injured-section').removeClass('hide');
            disableEnable('.injured-section', false);
        }
        else{
            $('.injured-section').addClass('hide');
            disableEnable('.injured-section', true);
        }
    })
    $(document).on('click','.accident-involved', function(){
        if($(this).val()=="1"){
            $('.accident-involved-section').removeClass('hide');
            disableEnable('.accident-involved-section', false);
        }
        else{
            $('.accident-involved-section').addClass('hide');            
            disableEnable('.accident-involved-section', true);
        }
    })
    
    $(document).on('click','.certified-licensed', function(){
        if($(this).val()=="1"){
            $('.certified-licensed-section').removeClass('hide');
            $('.certified-licensed-section').find('input, select, button').each(
                function () {                    
                    $(this).attr('disabled', false).removeClass('disabled');
                }
            );
        }
        else{
             $('.certified-licensed-section').addClass('hide');
            $('.certified-licensed-section').find('input, select, button').each(
                function () {                    
                    $(this).attr('disabled', true).removeClass('disabled');
                }
            );
        }
    })
    
    $(document).on('click','.certified-licensed-armed', function(){
        if($(this).val()=="1"){
            $('.certified-licensed-armed-section').removeClass('hide');
            disableEnable('.accident-involved-section', false);
        }
        else{
            $('.certified-licensed-armed-section').addClass('hide');
            disableEnable('.accident-involved-section', true);
        }
    })
    
    $(document).on('click','.cpr-aed-certified', function(){
        if($(this).val()=="1"){
            $('.cpr-aed-certified-section').removeClass('hide');
            disableEnable('.cpr-aed-certified-section', false);
        }
        else{
            $('.cpr-aed-certified-section').addClass('hide');
            disableEnable('.cpr-aed-certified-section', true);
        }
    })
    
    $(document).on('click', '.twic-certified',function(){
        if($(this).val()=="1"){
            $('.twic-certified-section').removeClass('hide');
            disableEnable('.twic-certified-section', false);
        }
        else{
            $('.twic-certified-section').addClass('hide');
            disableEnable('.twic-certified-section', true);
        }
    })
            
            
            $(function() {
                $('.multiselect-ui').multiselect({
                     includeSelectAllOption: true
                });
            
                $('.report-submit').multiselect({
                 includeSelectAllOption: true
                });

                $('.languages-speak').multiselect({
                 includeSelectAllOption: true
                });

            });
            function hideLoader() {
                $(document).find('.loader').hide();
            }
            function showLoader() {
                $(document).find('.loader').show();
            }
      
        $(function () {
            $('#datetimepicker11').datetimepicker({
                  viewMode: 'years',
                   format: 'MM/YYYY'
            });
             $('#datetimepicker21').datetimepicker({
                  viewMode: 'years',
                   format: 'MM/YYYY'
            });

            $('#datetimepicker12').datetimepicker({
                  viewMode: 'years',
                   format: 'MM/YYYY'
            });
            $('#datetimepicker22').datetimepicker({
                  viewMode: 'years',
                   format: 'MM/YYYY'
            });
             $('#datetimepicker13').datetimepicker({
                  viewMode: 'years',
                   format: 'MM/YYYY'
            });
             $('#datetimepicker23').datetimepicker({
                  viewMode: 'years',
                   format: 'MM/YYYY'
            });
             $('#datetimepicker14').datetimepicker({
                  viewMode: 'years',
                   format: 'MM/YYYY'
            });
             $('#datetimepicker24').datetimepicker({
                  viewMode: 'years',
                   format: 'MM/YYYY'
            });
               $('#datetimepicker15').datetimepicker({
                  viewMode: 'years',
                   format: 'DD/MM/YYYY'
            });

              $('#datetimepicker25').datetimepicker({
                   format: 'MM/YYYY'
            });
               $('#datetimepicker16').datetimepicker({
                   format: 'MM/YYYY'
            });
               $('#datetimepicker17').datetimepicker({
                   format: 'MM/YYYY'
            });
            $('#datetimepicker18').datetimepicker({
                viewMode: 'years',
                format: 'DD/MM/YYYY'
            });
            $('#armed-expiry-date').datetimepicker({
                viewMode: 'years',
                format: 'DD/MM/YYYY'
            });
            $('#cpr-expiry-date').datetimepicker({
                viewMode: 'years',
                format: 'DD/MM/YYYY'
            });
            $('#twic-exiry-date').datetimepicker({
                viewMode: 'years',
                format: 'DD/MM/YYYY'
            });
            $('#datetimepicker1').datetimepicker({
                  viewMode: 'years',
                   format: 'MM/YYYY'
            });
            $('#datetimepicker2').datetimepicker({
                  viewMode: 'years',
                   format: 'MM/YYYY'
            });
            $('#date_started').datetimepicker({
                   format: 'DD/MM/YYYY'
            });
            $('#date_ended').datetimepicker({
                   format: 'DD/MM/YYYY'
            });
        });