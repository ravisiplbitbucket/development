(function () {
    jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-z\s]+$/i.test(value);
    }, "Enter Letters only please");
    
    $('.edit-profile').on('click',function(){
        var url = $(this).attr('data-url');
        $.ajax({
                type:"GET",
                url:url,        
                success: function(data){
                    console.log(data);
                    $('#profile').find('.modal-body').html(data);
                    $('#profile').modal('show');
                    $("#register-phone").mask("999 999 9999");
                    $("#company-mobile-number").mask("999 999 9999");
                    $("#organization-profile").validate({
                        rules: {
                            'company[first_name]': {
                                required: true,
                                lettersonly : true
                            },
                            'company[last_name]': {
                                required: true,
                                lettersonly : true
                            },
                            'company[telephone]' : {
                                required : true
                            },
                            'email' : {
                                required : true,
                                email : true
                            }
                        },
                        messages: {
                            'company[first_name]': {
                                required: "First Name must not be empty",
                                lettersonly : "Enter Letters Only"
                            },
                            'company[last_name]': {
                                required: "Last Name must not be empty",
                                lettersonly : "Enter Letters only"
                            },
                            'company[telephone]' : {
                                required : "Phone Number must not be empty"
                            },
                            'email' : {
                                required : "Email must not be empty",
                                email : "Email format must be correct"
                            }
                        },
                        errorPlacement: function(error, element) {
                               error.insertAfter(element.parents('.form-group'));
                       }
                    });
                    $('#organization-profile').submit(function(event){        
                       var file = $('input[name="company[website_logo]"]').val();
                       if(file == ""){
                           $('input[name="company[website_logo]"]').closest('.form-group').append("<label class='error logo-error'>Company logo is required</lable")
                           event.preventDefault();
                       }else{
                           var ext = file.split('.').pop().toLowerCase();
                           if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
                               $('input[name="company[website_logo]"]').closest('.form-group').append("<label class='error logo-error'>Invalid file extension.Please select the image of valid extension(jpg,jpeg,png,gif)</lable")
                               event.preventDefault();
                           }else {
                               $('.logo-error').remove();
                           }
                       }
                   });
                }
               
            });
    });
})();