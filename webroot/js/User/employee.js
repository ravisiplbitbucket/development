(function() {
	$('#date').datetimepicker({
        minDate: new Date()
    });
    $('#date-commendation').datetimepicker({
        format:'YYYY/MM/DD'
    });
    $('#date-discip').datetimepicker({
        format:'YYYY/MM/DD'
    });
    $('#date-training').datetimepicker({
        format:'YYYY/MM/DD'
    });
    $('#date-expiration').datetimepicker({
        format:'YYYY/MM/DD'
    });
	//Reset form and multiselect
    $('#addTraining').on('click', function() {
    	$('#addEmployeetraining').trigger('reset');
    	$('#availableTraining').multiselect('refresh');
    })

    $('#addEmployeetraining').on('submit', function(event) {
    	event.preventDefault();
    	console.log($(this).serialize());
        if ($(this).valid()) {
            showLoader();
            $.ajax({
                url: $(this).attr('action'),
                type: 'POST',
                data: $(this).serialize() ,
                dataType:'json'
            }).done(function(data) {
                hideLoader();                
            });
        }
     });

    $("#add-emp-training").validate({
        rules: {
            'training_id': {
                required: true,
            },
            'instructor' : {
                required : true
            },
            'training_date' : {
                required : true
            }
        },
        messages: {
            'training_id': {
                required: "Select the training",
            },
            'instructor' : {
                required : "Enter the instructor name"
            },
            'training_date' : {
                required: "Enter the date of training"
            }
        }
    });

    $("#add-emp-certificate").validate({
        rules: {
            'certification_id': {
                required: true,
            },
            'expiration_date' : {
                required : true
            }
        },
        messages: {
            'certification_id': {
                required: "Select the Certifications",
            },
            'expiration_date' : {
                required: "Enter the date of expiration"
            }
        }
    });

    $("#add-emp-equipment").validate({
        rules: {
            'equipment_id': {
                required: true,
            },
            'quantity' : {
                required : true
            }
        },
        messages: {
            'certification_id': {
                required: "Select the Uniform and Equipment",
            },
            'quantity' : {
                required: "Enter the Quantity"
            }
        }
    });

    $("#add-commendation").validate({
        rules: {
            'title': {
                required: true,
            },
            'issuer' : {
                required : true
            },
            'commend_date' : {
                required : true
            }
        },
        messages: {
            'title': {
                required: "Title must not be empty",
            },
            'issuer' : {
                required : "Enter the issuer"
            },
            'commend_date' : {
                required : "Select the date of commendation"
            }
        }
    });

    $("#add-disciplinary").validate({
        rules: {
            'title': {
                required: true,
            },
            'type_discip' : {
                required : true
            },
            'discip_date' : {
                required : true
            },
            'result' : {
                required : true
            }
        },
        messages: {
            'title': {
                required: "Title must not be empty",
            },
            'type_discip' : {
                required : "Type must not be empty"
            },
            'discip_date' : {
                required : "Select the date of disciplinary"
            },
            'result' : {
                required : "Result must not be empty"
            }
        }
    });

     $(document).on('change', 'select[name="equipment_id"]', function() {
       url = base_url+'employees/get-description/'+$(this).val()+'.json';
       $.ajax({
            url: url,
        }).done(function(html) {
            $(".description").html('<label>Description</label><div>'+html.description.description+'</div>');
        });
    });

    
})();

