(function () {
    function disableEnable(elementclass, state) {
        $(elementclass).find('input, select, button').each(
            function () {                    
                $(this).attr('disabled', state).removeClass('disabled');
            }
        );
    }

    jQuery.validator.addMethod("validate_email",function(value, element)
    {       
        if(/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test( value ))
        {           
            return true;        
        } else {         
             return false;        
        } 
    },"Please enter a valid Email.");

    function hideLoader() {
        $(document).find('.loader').hide();
    }
    function showLoader() {
        $(document).find('.loader').show();
    }

    function nextTab(elem) {
        $(elem).next().find('a[data-toggle="tab"]').click();
    }
    function prevTab(elem) {
        $(elem).prev().find('a[data-toggle="tab"]').click();
    }

    $('#datetimepicker1').datetimepicker({
        viewMode: 'years',
        format: 'YYYY'
    });

    $('#when_worked').datetimepicker({
        viewMode: 'years',
        format: 'YYYY'
    });

    $('#from-work').datetimepicker({
        viewMode: 'years',
        format: 'DD/MM/YYYY'
    });
    $('#to-work').datetimepicker({
        viewMode: 'years',
        format: 'DD/MM/YYYY'
    });

    
    $('select[name="country_id"]').on('change', function() {
    url = base_url+'jobs/get-states/'+$(this).val();
    $.ajax({
        url: url,
    }).done(function(html) {
            $(".state-container").html(html);
        });
    });

    $(document).on('change', 'select[name="state_id"]', function() {
        url = base_url+'jobs/get-cities/'+$(this).val();
        $.ajax({
            url: url,
        }).done(function(html) {
            $(".city-container").html(html);
        });
    });

    $('select[name="country_id"]').on('change', function() {
    url = base_url+'jobs/get-insurances/'+$(this).val();
    $.ajax({
        url: url,
    }).done(function(html) {
            $(".insurance-container").html(html);
        });
    });

    $('#candidate-apply-first-step').submit(function(event){
        if($('#candidate-apply-first-step').valid() ){
            event.preventDefault();
            showLoader();
            $.ajax({
                url: $('#candidate-apply-first-step').attr('action')+'.json',
                type: 'POST',
                data: $('#candidate-apply-first-step').serialize() ,
                dataType: 'json'
            }).done(function(data) {
                // $('#step2').html(data);
                var $active = $('.wizard .nav-tabs li.active');
                $active.next().removeClass('disabled');
                nextTab($active);
                setStepTwo();
                hideLoader();
                validatesecond();
                $(".prev-step").click(function (e) {            
                    var $active = $('.wizard .nav-tabs li.active');
                    prevTab($active);
                
                });

            });
        }
    })

    function setStepTwo() {
        $(document).find('#candidate-apply-second-step').submit(function(event){
            if($('#candidate-apply-second-step').valid() ){
                event.preventDefault()
                showLoader();
                $.ajax({
                    url: $('#candidate-apply-second-step').attr('action')+'.json',
                    type: 'POST',
                    data: $('#candidate-apply-second-step').serialize() ,
                }).done(function(data) {
                    // $('#step3').html(data);
                    var $active = $('.wizard .nav-tabs li.active');
                    $active.next().removeClass('disabled');
                    nextTab($active);
                    validatethird();
                    $(".prev-step").click(function (e) {            
                        var $active = $('.wizard .nav-tabs li.active');
                        prevTab($active);
                    
                    });
                    history();
                    getHistoryDetail()
                    commonScript();
                    hideLoader();
                });
            }
        })
    }

    $('#candidate-apply-fourth-step').submit(function(event){
        if($('#candidate-apply-fourth-step').valid() ){
            event.preventDefault();
            showLoader();
            $.ajax({
                url: $('#candidate-apply-fourth-step').attr('action')+'.json',
                type: 'POST',
                data: $('#candidate-apply-fourth-step').serialize() ,
                dataType: 'json'
            }).done(function(data) {
                // $('#step2').html(data);
                var $active = $('.wizard .nav-tabs li.active');
                $active.next().removeClass('disabled');
                nextTab($active);
                setStepTwo();
                hideLoader();
                validatesecond();
                $(".prev-step").click(function (e) {            
                    var $active = $('.wizard .nav-tabs li.active');
                    prevTab($active);
                
                });

            });
        }
    })

   
    

    function getHistoryDetail() {
        $('.get-history-detail').on('click', function(){
            showLoader();
            var url = $(this).attr('data-url');
            $.ajax({
                url: url,
                type: 'get',
                // dataType:'json'
            }).done(function(data) {
                $('#history-detail-modal').find('.modal-body').html(data);
                $('#history-detail-modal').modal('show');
                hideLoader();
            });
        });
    }

    //Wizard
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
    
    var $target = $(e.target);
    
    if ($target.parent().hasClass('disabled')) {
        return false;
    }
    });
            
    $(".prev-step").click(function (e) {            
        var $active = $('.wizard .nav-tabs li.active');
        prevTab($active);
    
    });

    function commonScript() {
        addDisabledField();
        $('.guard').on('click', function(){
            if($(this).val()=="1"){
                $('.security-guard').removeClass('hide');
            }
            else{
                 $('.security-guard').addClass('hide');
            }
            addDisabledField();
        })
        
        $('.patrols').on('click', function(){
            if($(this).val()=="1"){
                $('.foot-patrols').removeClass('hide');
            }
            else{
                 $('.foot-patrols').addClass('hide');
            }
            addDisabledField();
        })
        
         $('.reports').on('click', function(){
            if($(this).val()=="1"){
                $('.submit-reports').removeClass('hide');
            }
            else{
                 $('.submit-reports').addClass('hide');
            }
            addDisabledField();
        })
        
           
        $('.vehicle').on('click', function(){
            if($(this).val()=="1"){
                $('.vechicle-required').removeClass('hide');
            }
            else{
                 $('.vechicle-required').addClass('hide');
            }
            addDisabledField();
        })
        
        
        $(function() {
            $('.multiselect-ui').multiselect({
                 includeSelectAllOption: true
            });
        
            $('.report-submit').multiselect({
             includeSelectAllOption: true
            });
        });
    }
    commonScript()

    function validatefirst(){
        $('#candidate-apply-first-step').validate({
            rules: {
                'first_name': {
                    required: true
                },
                'last_name': {
                    required: true
                },
                'home_phone' :{
                    required : true,
                    maxlength : 10
                },
                'mobile' : {
                    maxlength : 10
                },
                'email' : {
                    required : true,
                    validate_email : true
                },
                'address1' : {
                    required : true
                },
                'country_id' : {
                    required : true
                },
                'state_id' : {
                    required : true
                },
                'city_id' : {
                    required : true
                },
                'zipcode' : {
                    required : true,
                    maxlength : 6
                },
               'job_type_id' : {
                    required : true
                }
            },
            messages: {
                'first_name': {
                    required: "First name must not be empty"
                },
                'last_name': {
                    required: "Last name must not be empty"
                },
                'home_phone' :{
                    required : "Home phone number must not be empty",
                    maxlength : "Home phone number must not be greater than 10 digits"
                },
                'mobile' : {
                    maxlength : "Mobile Phone number must not be greater than 10 digits"
                },
                'email' : {
                    required : "Email must not be empty",
                    validate_email : "Please enter the correct format"
                },
                'address1' : {
                    required : "Please enter the address"
                },
                'country_id' : {
                    required : "Please select the country"
                },
                'state_id' : {
                    required : "Please select the state"
                },
                'city_id' : {
                    required : "Please select the city"
                },
                'zipcode' : {
                    required : "Zipcode must not be empty",
                    maxlength : "Zipcode must not be greater than 6"
                },
               'job_type_id' : {
                    required : "Please select the job type"
                }
            },
            errorPlacement: function(error, element) {
                if (element.hasClass("logo"))
                   error.insertAfter($(element).closest('label'));
               
                else
                   error.insertAfter(element.parents('.form-group'));
           }
        });
    }
    validatefirst();

    function validatesecond() {
            $('#candidate-apply-second-step').validate({
            rules: {
                'hours_available': {
                    required: true,
                },
                 'working_shifts[1][][day_id]': {
                    require_from_group: [1, ".shift-group"]
                },
                'working_shifts[2][][day_id]': {
                    require_from_group: [1, ".shift-group"]
                },
                'working_shifts[3][][day_id]': {
                    require_from_group: [1, ".shift-group"]
                }
            },
            messages: {
               'hours_available': {
                    required: "Please enter the hours available",
                },
                'working_shifts[1][][day_id]': {
                    require_from_group: "Select the days for the shift"
                }
            },
            errorPlacement: function(error, element) {
                if (element.hasClass("logo"))
                   error.insertAfter($(element).closest('label'));
               
                else
                   error.insertAfter(element.parents('.form-group'));
           }
        });
    }
    validatethird();
    function validatethird() {
        $('#candidate-apply-third-step').validate({
            rules: {
                'writing_test': {
                    required: true
                }
            },
            messages: {
               'writing_test': {
                    required: "Please write down what you have seen in the picture"
                }
            },
            errorPlacement: function(error, element) {
                if (element.hasClass("logo"))
                   error.insertAfter($(element).closest('label'));
               
                else
                   error.insertAfter(element.parents('.form-group'));
           }
        })

        $('#candidate-history').validate({
            rules: {
                'company_name': {
                    required: true
                },
                'from_work' : {
                    required: true
                },
                'to_work' : {
                    required: true
                },
                'where_employ' : {
                    required: true
                },
                'designation' : {
                    required: true
                },
                'client_name': {
                    required: true
                },
                'rate_paid': {
                    required: true
                },
                'hours_per_shift': {
                    required: true
                }                
            },
            messages: {
                'company_name': {
                    required: "Please enter company name"
                },
                'from_work': {
                    required: "Please enter the work starting time"
                },
                'to_work': {
                    required: "Please enter the work end time"
                },
                'where_employ' : {
                    required: "Please enter the state"
                },
                'designation' : {
                    required: "Please enter Your job title/Rank"
                },
                'client_name' : {
                    required: "Please enter a client name"
                },
                'rate_paid' : {
                    required: "Please enter a rate paid"
                },
                'hours_per_shift' : {
                    required: "Please enter a hours per shift"
                }
            },
            errorPlacement: function(error, element) {
                if (element.hasClass("logo"))
                   error.insertAfter($(element).closest('label'));
               
                else
                   error.insertAfter(element.parents('.form-group'));
           }
        });
    };

    function history(){
         $(document).find('#candidate-history, #edit-candidate-history').submit(function(event){
            event.preventDefault();
            var action = $(this).attr('action');
            var data = $(this).serialize();
            if ($(this).valid()) {
                showLoader();
                $.ajax({
                    url: action,
                    type: 'POST',
                    data: data,
                    // dataType:'json'
                }).done(function(data) {
                    hideLoader();
                    $('#history-list').append(data);
                    $('#employment-history').modal('hide');
                    $('#edit-history-modal').modal('hide');
                    getHistoryDetail();
                    commonScript(); 
                });
            }
        });
    }
     
    //disabled the validated hidden field@ stop them to send value in form submit.
    function addDisabledField() {
        $('#candidate-history').find('input, select, button').each(
            function () {
                if ($(this).parents('.hide').length) {
                    $(this).attr('disabled', true);
                } else {
                    $(this).attr('disabled', false).removeClass('disabled');
                }
            }
        )
    }

    //reset validation form on opening modal
    $(document).on('click', '#historyModalIcon', function() {
        var validater = $('#candidate-history').validate();
        validater.resetForm();

    })

    $(document).on('click','.worked-for-company', function(){
        if($(this).val()=="1"){
            $('.workedfor-company').removeClass('hide');
            $('.workedfor-company').find('input, select, button').each(
                function () {                    
                    $(this).attr('disabled', false).removeClass('disabled');
                }
            );
        }
        else{
            $('.workedfor-company').addClass('hide');
            $('.workedfor-company').find('input, select, button').each(
                function () {                    
                    $(this).attr('disabled', true).removeClass('disabled');
                }
            );
        }
    })
    
    $(document).on('click', '.convicted-of-crime',function(){
        if($(this).val()=="1"){
            $('.crime').removeClass('hide');
        }
        else{
             $('.crime').addClass('hide');
        }
    })
    
     $(document).on('click','.licence', function(){
        if($(this).val()=="1"){
            $('.valid-licence').removeClass('hide');
            disableEnable('.valid-licence', false);
        }
        else{
             $('.valid-licence').addClass('hide');
            disableEnable('.valid-licence', true);

        }
    })
    
    $(document).on('click','.automobile', function(){
        if($(this).val()=="1"){
            $('.dependable-automobile').removeClass('hide');
            disableEnable('.dependable-automobile', false);
        }
        else{
            $('.dependable-automobile').addClass('hide');
            disableEnable('.dependable-automobile', true);

        }
    })
    
    $(document).on('click', '.guard',function(){
        if($(this).val()=="1"){
            $('.security-guard').removeClass('hide');
            disableEnable('.security-guard', false);
        }
        else{
            $('.security-guard').addClass('hide');
            disableEnable('.security-guard', true);
        }
    })
    
    $(document).on('click', '.patrols',function(){
        if($(this).val()=="1"){
            $('.foot-patrols').removeClass('hide');
            disableEnable('.foot-patrols', false);
        }
        else{
             $('.foot-patrols').addClass('hide');
             disableEnable('.foot-patrols', false);
        }
    })
    
     $(document).on('click','.reports', function(){
        if($(this).val()=="1"){
            $('.submit-reports').removeClass('hide');
            disableEnable('.reports', false);
        }
        else{
             $('.submit-reports').addClass('hide');
            disableEnable('.reports', true);
        }
    })
    
       
    $(document).on('click','.vehicle', function(){
        if($(this).val()=="1"){
            $('.vechicle-required').removeClass('hide');
            disableEnable('.vechicle-required', false);
        }
        else{
             $('.vechicle-required').addClass('hide');
             disableEnable('.vechicle-required', true);
        }
    })
    
    $(document).on('click', '.equipment', function(){
        if($(this).val()=="1"){
            $('.equipment-section').removeClass('hide');
            disableEnable('.equipment-section', false);
        }
        else{
            $('.equipment-section').addClass('hide');
            disableEnable('.equipment-section', true);
        }
    })
    
    $(document).on('click', '.emergency-services', function(){
        if($(this).val()=="1"){
            $('.emergency-services-section').removeClass('hide');
            disableEnable('.emergency-services-section', false);
        }
        else{
             $('.emergency-services-section').addClass('hide');
             disableEnable('.emergency-services-section', true);
        }
    })
    
    $(document).on('click','.injured', function(){
        if($(this).val()=="1"){
            $('.injured-section').removeClass('hide');
            disableEnable('.injured-section', false);
        }
        else{
            $('.injured-section').addClass('hide');
            disableEnable('.injured-section', true);
        }
    })
    $(document).on('click','.accident-involved', function(){
        if($(this).val()=="1"){
            $('.accident-involved-section').removeClass('hide');
            disableEnable('.accident-involved-section', false);
        }
        else{
            $('.accident-involved-section').addClass('hide');            
            disableEnable('.accident-involved-section', true);
        }
    })
    
    $(document).on('click','.certified-licensed', function(){
        if($(this).val()=="1"){
            $('.certified-licensed-section').removeClass('hide');
            $('.certified-licensed-section').find('input, select, button').each(
                function () {                    
                    $(this).attr('disabled', false).removeClass('disabled');
                }
            );
        }
        else{
             $('.certified-licensed-section').addClass('hide');
            $('.certified-licensed-section').find('input, select, button').each(
                function () {                    
                    $(this).attr('disabled', true).removeClass('disabled');
                }
            );
        }
    })
    
    $(document).on('click','.certified-licensed-armed', function(){
        if($(this).val()=="1"){
            $('.certified-licensed-armed-section').removeClass('hide');
            disableEnable('.accident-involved-section', false);
        }
        else{
            $('.certified-licensed-armed-section').addClass('hide');
            disableEnable('.accident-involved-section', true);
        }
    })
    
    $(document).on('click','.cpr-aed-certified', function(){
        if($(this).val()=="1"){
            $('.cpr-aed-certified-section').removeClass('hide');
            disableEnable('.cpr-aed-certified-section', false);
        }
        else{
            $('.cpr-aed-certified-section').addClass('hide');
            disableEnable('.cpr-aed-certified-section', true);
        }
    })
    
    $(document).on('click', '.twic-certified',function(){
        if($(this).val()=="1"){
            $('.twic-certified-section').removeClass('hide');
            disableEnable('.twic-certified-section', false);
        }
        else{
            $('.twic-certified-section').addClass('hide');
            disableEnable('.twic-certified-section', true);
        }
    })
    $(function() {
        $('.multiselect-ui').multiselect({
             includeSelectAllOption: true
        });
    
        $('.report-submit').multiselect({
         includeSelectAllOption: true
        });

        $('.languages-speak').multiselect({
         includeSelectAllOption: true
        });

    });
    
    $('#datetimepicker30').datetimepicker({
          viewMode: 'years',
           format: 'MM/YYYY'
    });
    $('#datetimepicker11').datetimepicker({
          viewMode: 'years',
           format: 'MM/YYYY'
    });
     $('#datetimepicker21').datetimepicker({
          viewMode: 'years',
           format: 'MM/YYYY'
    });

    $('#datetimepicker12').datetimepicker({
          viewMode: 'years',
           format: 'MM/YYYY'
    });
    $('#datetimepicker22').datetimepicker({
          viewMode: 'years',
           format: 'MM/YYYY'
    });
     $('#datetimepicker13').datetimepicker({
          viewMode: 'years',
           format: 'MM/YYYY'
    });
     $('#datetimepicker23').datetimepicker({
          viewMode: 'years',
           format: 'MM/YYYY'
    });
     $('#datetimepicker14').datetimepicker({
          viewMode: 'years',
           format: 'MM/YYYY'
    });
     $('#datetimepicker24').datetimepicker({
          viewMode: 'years',
           format: 'MM/YYYY'
    });
       $('#datetimepicker15').datetimepicker({
          viewMode: 'years',
           format: 'DD/MM/YYYY'
    });

      $('#datetimepicker25').datetimepicker({
           format: 'MM/YYYY'
    });
       $('#datetimepicker16').datetimepicker({
           format: 'MM/YYYY'
    });
       $('#datetimepicker17').datetimepicker({
           format: 'MM/YYYY'
    });
    $('#datetimepicker18').datetimepicker({
        viewMode: 'years',
        format: 'DD/MM/YYYY'
    });
    $('#armed-expiry-date').datetimepicker({
        viewMode: 'years',
        format: 'DD/MM/YYYY'
    });
    $('#cpr-expiry-date').datetimepicker({
        viewMode: 'years',
        format: 'DD/MM/YYYY'
    });
    $('#twic-exiry-date').datetimepicker({
        viewMode: 'years',
        format: 'DD/MM/YYYY'
    });
    $('#datetimepicker1').datetimepicker({
          viewMode: 'years',
           format: 'MM/YYYY'
    });
    $('#datetimepicker2').datetimepicker({
          viewMode: 'years',
           format: 'MM/YYYY'
    });
    $('#date_started').datetimepicker({
           format: 'DD/MM/YYYY'
    });
    $('#date_ended').datetimepicker({
           format: 'DD/MM/YYYY'
    });
    
    education();
    reference();
    function education() {        
        $(document).find('#candidate-education-form').submit(function(event){
            event.preventDefault();
            if ($(this).valid()) {
                showLoader();
                $.ajax({
                    url: $('#candidate-education-form').attr('action'),
                    type: 'POST',
                    data: $('#candidate-education-form').serialize() ,
                    // dataType:'json'
                }).done(function(data) {
                    hideLoader();
                    $('#education-list').append(data);
                    $('#candidate-education').modal('hide');

                });
            }
        });
    }
    function reference() {        
        $(document).find('#candidate-reference-form').submit(function(event){
            event.preventDefault();
            if ($(this).valid()) {
                showLoader();
                $.ajax({
                    url: $('#candidate-reference-form').attr('action'),
                    type: 'POST',
                    data: $('#candidate-reference-form').serialize() ,
                    // dataType:'json'
                }).done(function(data) {
                    hideLoader();
                    $('#reference-list').append(data);
                    $('#candidate-reference').modal('hide');
                    // commonScript(); 
                });
            }
        });
    }
    $(document).on('click','.delete-education-link', function(){
        var url= $(this).attr('data-url');
        var ele = this;
        showLoader();
        $.ajax({
            url: url,
            type: 'GET',
            dataType:'json'
        }).done(function(data) {
            hideLoader();
            if(data.response.status == 200){
                $(ele).closest('.col-sm-12').remove();
            }
        });
    });
    function hideLoader() {
        $(document).find('.loader').hide();
    }
    function showLoader() {
        $(document).find('.loader').show();
    }
    function nextTab(elem) {
        $(elem).next().find('a[data-toggle="tab"]').click();
    }
    function prevTab(elem) {
        $(elem).prev().find('a[data-toggle="tab"]').click();
    }
    $('#candidate-apply-third-step').submit(function(event){
        if($('#candidate-apply-third-step').valid() ){
            event.preventDefault();
            showLoader();
            $.ajax({
                url: $('#candidate-apply-third-step').attr('action')+'.json',
                type: 'POST',
                data: $('#candidate-apply-third-step').serialize() ,
                dataType: 'json'
            }).done(function(data) {
                var $active = $('.wizard .nav-tabs li.active');
                $active.next().removeClass('disabled');
                nextTab($active);
                hideLoader();
                $(".prev-step").click(function (e) {            
                    var $active = $('.wizard .nav-tabs li.active');
                    prevTab($active);
                
                });

            });
        }
    })
    $('#candidate-apply-fourth-step').submit(function(event){
        if($('#candidate-apply-fourth-step').valid() ){
            event.preventDefault();
            showLoader();
            $.ajax({
                url: $('#candidate-fourth-third-step').attr('action')+'.json',
                type: 'POST',
                data: $('#candidate-fourth-third-step').serialize() ,
                dataType: 'json'
            }).done(function(data) {
                var $active = $('.wizard .nav-tabs li.active');
                $active.next().removeClass('disabled');
                nextTab($active);
                hideLoader();
                $(".prev-step").click(function (e) {            
                    var $active = $('.wizard .nav-tabs li.active');
                    prevTab($active);
                
                });

            });
        }
    })
    $('#candidate-apply-fifth-step').submit(function(event){
        if($('#candidate-apply-fifth-step').valid() ){
            event.preventDefault();
            showLoader();
            $.ajax({
                url: $('#candidate-apply-fifth-step').attr('action')+'.json',
                type: 'POST',
                data: $('#candidate-apply-fifth-step').serialize() ,
                dataType: 'json'
            }).done(function(data) {
                var $active = $('.wizard .nav-tabs li.active');
                $active.next().removeClass('disabled');
                nextTab($active);
                hideLoader();
                $(".prev-step").click(function (e) {            
                    var $active = $('.wizard .nav-tabs li.active');
                    prevTab($active);
                
                });

            });
        }
    })
    
    $(document).on('click','.get-history', function(){
        var url= $(this).attr('data-url');
        var ele = this;
        showLoader();
        $.ajax({
            url: url,
            type: 'GET',
            // dataType:'json'
        }).done(function(data) {
            hideLoader();
            $('#edit-history-modal').find('.modal-content').html(data);
            $('#edit-history-modal').modal('show');
            history();

            historyDisplayFields()

            $('.multiselect-ui').multiselect({
                 includeSelectAllOption: true
            });
        
            $('.report-submit').multiselect({
                includeSelectAllOption: true
            });

            $('.languages-speak').multiselect({
                includeSelectAllOption: true
            });

        });
    });
    function historyDisplayFields () {
        if($('.guard:checked').val()=="1"){
            $('.security-guard').removeClass('hide');
            disableEnable('.security-guard', false);
        }
        else{
             $('.security-guard').addClass('hide');
            disableEnable('.security-guard', true);
        }

        if($('.patrols:checked').val()=="1"){
            $('.foot-patrols').removeClass('hide');
            disableEnable('.foot-patrols', false);
        }
        else{
             $('.foot-patrols').addClass('hide');
             disableEnable('.foot-patrols', true);
        }

        if($('.reports:checked').val()=="1"){
            $('.submit-reports').removeClass('hide');
            disableEnable('.submit-reports', false);
        }
        else{
             $('.submit-reports').addClass('hide');
             disableEnable('.submit-reports', true);
        }

        if($('.vehicle:checked').val()=="1"){
            $('.vechicle-required').removeClass('hide');
            disableEnable('.vechicle-required', false);
        }
        else{
             $('.vechicle-required').addClass('hide');
             disableEnable('.vechicle-required', true);
        }

        if($('.emergency-services:checked').val()=="1"){
            $('.emergency-services-section').removeClass('hide');
            disableEnable('.emergency-services-section', false);
        }
        else{
             $('.emergency-services-section').addClass('hide');
             disableEnable('.emergency-services-section', true);
        }
  
        if($('.injured:checked').val()=="1"){
            $('.injured-section').removeClass('hide');
            disableEnable('.injured-section', false);
        }
        else{
             $('.injured-section').addClass('hide');
             disableEnable('.injured-section', true);
        }
        if($('.accident-involved:checked').val()=="1"){
            $('.accident-involved-section').removeClass('hide');
            disableEnable('.accident-involved-section', false);
        }
        else{
             $('.accident-involved-section').addClass('hide');
             disableEnable('.accident-involved-section', true);
        }
        if($('.equipment:checked').val()=="1"){
            $('.equipment-section').removeClass('hide');
            disableEnable('.equipment-section', false);
        }
        else{
            $('.equipment-section').addClass('hide');
            disableEnable('.equipment-section', true);
        }

        $('#date_started').datetimepicker({
               format: 'DD/MM/YYYY'
        });
        $('#date_ended').datetimepicker({
               format: 'DD/MM/YYYY'
        });
        
    }
    function addDisabledField() {
        $('input, select, button').each(
            function () {
                if ($(this).parents('.hide').length) {
                    $(this).attr('disabled', true);
                } else {
                    $(this).attr('disabled', false).removeClass('disabled');
                }
            }
        )
    }
    addDisabledField();

    $('.get-education').on('click',function(){
        var url = $(this).attr('data-url');
        $.ajax({
                type:"GET",
                url:url,        
                success: function(data){
                    console.log(data);
                    $('#edit-education-modal').find('.modal-content').html(data);
                    $('#edit-education-modal').modal('show');
                    $('#candidate-education-edit-form').submit(function(event){
                            event.preventDefault();
                            showLoader();
                            $.ajax({
                                url: $('#candidate-education-edit-form').attr('action')+'.json',
                                type: 'POST',
                                data: $('#candidate-education-edit-form').serialize() ,
                                dataType: 'json'
                            }).done(function(data) {
                                $('#edit-education-modal').modal('hide');
                            });
                    })
                }
               
            });
    });
    $('.get-reference').on('click',function(){
        var url = $(this).attr('data-url');
        $.ajax({
                type:"GET",
                url:url,        
                success: function(data){
                    console.log(data);
                    $('#edit-reference-modal').find('.modal-content').html(data);
                    $('#edit-reference-modal').modal('show');
                    $('#candidate-reference-edit-form').submit(function(event){
                            event.preventDefault();
                            showLoader();
                            $.ajax({
                                url: $('#candidate-reference-edit-form').attr('action')+'.json',
                                type: 'POST',
                                data: $('#candidate-reference-edit-form').serialize() ,
                                dataType: 'json'
                            }).done(function(data) {
                                $('#edit-reference-modal').modal('hide');
                            });
                    })
                }
               
            });
    });
    
})();