(function () {
$('select[name="country_id"]').on('change', function() {
    $(document).find('select[name="state_id"]').html('<option value="">Select State</option>');
    $(document).find('select[name="city_id"]').html('<option value="">Select City</option>');
       url = base_url+'jobs/get-states/'+$(this).val();
       $.ajax({
            url: url,
        }).done(function(html) {
            $(".state-container").html(html);
        });
    });

$(document).on('change', 'select[name="state_id"]', function() {
    $(document).find('select[name="city_id"]').html('<option value="">Select City</option>');

   url = base_url+'jobs/get-cities/'+$(this).val();
   $.ajax({
        url: url,
    }).done(function(html) {
        $(".city-container").html(html);
    });
});



    $('#edit-job').validate({
        rules: {
            'job_title': {
                required: true
            },
            'country_id': {
                required: true
            },
            'state_id': {
                required: true
            },
            'city_id' :{
                required : true,
            },
           'zipcode' : {
                required : true,
                maxlength : 6
            },
            'job_type_id' : {
                required : true
            },
            'education_id' :{
                required : true
            },
            'responsibility' : {
                required : true
            },
            'qualification' : {
                required : true
            },
            'experience'  :{
                required : true
            },
            'certification' : {
                required : true
            },
            'language_skills' : {
                required : true
            },
            'other_qualification' : {
                required : true
            },
            'physical_demands' :{
                required : true
            },
            'salary_from' : {
                required : true,
                digits : true
            },
            'salary_to' : {
                required : true,
                digits : true
            }
            
            
        },
        messages: {
            'job_title': {
                required: "Job title must not be empty"
            },
            'country_id': {
                required: "Please select the country"
            },
            'state_id': {
                required: "Please select the state"
            },
            'city_id' :{
                required : "Please select the city",
            },
           'zipcode' : {
                required : "Zipcode must not be empty",
                maxlength : "Zipcode must not be greater than six"
            },
            'job_type_id' : {
                required : "select the Job Type"
            },
            'education_id' :{
                required : "select the education"
            },
            'responsibility' : {
                required : "Reposibilities must not be empty"
            },
            'qualification' : {
                required : "Qualification must not be empty"
            },
            'experience'  :{
                required : "Experience must not be empty"
            },
            'certification' : {
                required : "Certification must not be empty"
            },
            'language_skills' : {
                required : "Language Skills must not be empty"
            },
            'other_qualification' : {
                required : "other Qualifications must not be empty"
            },
            'physical_demands' :{
                required : "Physical demands must not be empty"
            },
            'salary_from' : {
                required : "Enter the min range of salary",
                digits : "Enter digits only"
            },
            'salary_to' : {
                required : "Enter the max range of salary",
                digits : "Enter digits only"
            }
        },
        errorPlacement: function(error, element) {
            if (element.hasClass("logo"))
               error.insertAfter($(element).closest('label'));                            
           
            else
               error.insertAfter(element.parents('.form-group'));
       }
    });
    $('#job-post').validate({
        rules: {
            'job_title': {
                required: true
            },
            'country_id': {
                required: true
            },
            'state_id': {
                required: true
            },
            'city_id' :{
                required : true,
            },
           'zipcode' : {
                required : true,
                maxlength : 6
            },
            'job_type_id' : {
                required : true
            },
            'education_id' :{
                required : true
            },
            'responsibility' : {
                required : true
            },
            'qualification' : {
                required : true
            },
            'experience'  :{
                required : true
            },
            'certification' : {
                required : true
            },
            'language_skills' : {
                required : true
            },
            'other_qualification' : {
                required : true
            },
            'physical_demands' :{
                required : true
            },
            'salary_from' : {
                required : true,
                digits : true,
                maxlength : 2
            },
            'salary_to' : {
                required : true,
                digits : true,
                maxlength : 3
            }
        },
        messages: {
            'job_title': {
                required: "Job title must not be empty"
            },
            'country_id': {
                required: "Please select the country"
            },
            'state_id': {
                required: "Please select the state"
            },
            'city_id' :{
                required : "Please select the city",
            },
           'zipcode' : {
                required : "Zipcode must not be empty",
                maxlength : "Zipcode must not be greater than six"
            },
            'job_type_id' : {
                required : "select the Job Type"
            },
            'education_id' :{
                required : "select the education"
            },
            'responsibility' : {
                required : "Reposibilities must not be empty"
            },
            'qualification' : {
                required : "Qualification must not be empty"
            },
            'experience'  :{
                required : "Experience must not be empty"
            },
            'certification' : {
                required : "Certification must not be empty"
            },
            'language_skills' : {
                required : "Language Skills must not be empty"
            },
            'other_qualification' : {
                required : "Other Qualifications must not be empty"
            },
            'physical_demands' :{
                required : "Physical demands must not be empty"
            },
            'salary_from' : {
                required : "Enter the min range of salary",
                digits : "Enter digits only",
                maxlength : "Maxlength must only 2 digits"
            },
            'salary_to' : {
                required : "Enter the max range of salary",
                digits : "Enter digits only",
                maxlength : "Maxlength must only 3 digits"
            }
        },
        errorPlacement: function(error, element) {
            if (element.hasClass("logo"))
               error.insertAfter($(element).closest('label'));                            
           
            else
               error.insertAfter(element.closest('.form-group'));
       }
    });
})();