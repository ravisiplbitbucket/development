(function () {
$('select[name="country_id"]').on('change', function() {
    $(document).find('select[name="state_id"]').html('<option value="">Select State</option>');
    $(document).find('select[name="city_id"]').html('<option value="">Select City</option>');
       url = base_url+'jobs/search-states/'+$(this).val();
       $.ajax({
            url: url,
        }).done(function(html) {
            $(".state-container").html(html);
        });
    });

$(document).on('change', 'select[name="state_id"]', function() {
    $(document).find('select[name="city_id"]').html('<option value="">Select City</option>');

   url = base_url+'jobs/search-cities/'+$(this).val();
   $.ajax({
        url: url,
    }).done(function(html) {
        $(".city-container").html(html);
    });
});
})();