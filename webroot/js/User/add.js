(function () {
    function disableEnable(elementclass, state) {
        $(elementclass).find('input, select, button').each(
            function () {                    
                $(this).attr('disabled', state).removeClass('disabled');
            }
        );
    }
    function hideLoader() {
        $(document).find('.loader').hide();
    }
    function showLoader() {
        $(document).find('.loader').show();
    }
    $(document).on('click','.worked-for-company', function(){
        if($(this).val()=="1"){
            $('.workedfor-company').removeClass('hide');
            disableEnable('.workedfor-company',false);
        }
        else{
             $('.workedfor-company').addClass('hide');
             disableEnable('.workedfor-company',true);
        }
    })
    
    $(document).on('click', '.convicted-of-crime',function(){
        if($(this).val()=="1"){
            $('.crime').removeClass('hide');
        }
        else{
             $('.crime').addClass('hide');
        }
    })
    
     $(document).on('click','.licence', function(){
        if($(this).val()=="1"){
            $('.valid-licence').removeClass('hide');
            disableEnable('.valid-licence',false);
        }
        else{
             $('.valid-licence').addClass('hide');
             disableEnable('.valid-licence',true);
        }
    })
    
      $(document).on('click','.automobile', function(){
        if($(this).val()=="1"){
            $('.dependable-automobile').removeClass('hide');
            disableEnable('.dependable-automobile',false);
        }
        else{
             $('.dependable-automobile').addClass('hide');
             disableEnable('.dependable-automobile',true);
        }
    })
    
    $(document).on('click', '.guard',function(){
        if($(this).val()=="1"){
            $('.security-guard').removeClass('hide');
        }
        else{
             $('.security-guard').addClass('hide');
        }
    })
    
    $(document).on('click', '.patrols',function(){
        if($(this).val()=="1"){
            $('.foot-patrols').removeClass('hide');
        }
        else{
             $('.foot-patrols').addClass('hide');
        }
    })
    
     $(document).on('click','.reports', function(){
        if($(this).val()=="1"){
            $('.submit-reports').removeClass('hide');
        }
        else{
             $('.submit-reports').addClass('hide');
        }
    })
    
       
    $(document).on('click','.vehicle', function(){
        if($(this).val()=="1"){
            $('.vechicle-required').removeClass('hide');
        }
        else{
             $('.vechicle-required').addClass('hide');
        }
    })
    
    $(document).on('click', '.equipment', function(){
        if($(this).val()=="1"){
            $('.equipment-section').removeClass('hide');
        }
        else{
             $('.equipment-section').addClass('hide');
        }
    })
    
    $(document).on('click', '.emergency-services', function(){
        if($(this).val()=="1"){
            $('.emergency-services-section').removeClass('hide');
        }
        else{
             $('.emergency-services-section').addClass('hide');
        }
    })
    
    $(document).on('click','.injured', function(){
        if($(this).val()=="1"){
            $('.injured-section').removeClass('hide');
        }
        else{
             $('.injured-section').addClass('hide');
        }
    })
    $(document).on('click','.accident-involved', function(){
        if($(this).val()=="1"){
            $('.accident-involved-section').removeClass('hide');
        }
        else{
             $('.accident-involved-section').addClass('hide');
        }
    })
    
    $(document).on('click','.certified-licensed', function(){
        if($(this).val()=="yes"){
            $('.certified-licensed-section').removeClass('hide');
            disableEnable('.certified-licensed-section',false);
        }
        else{
             $('.certified-licensed-section').addClass('hide');
             disableEnable('.certified-licensed-section',true);
        }
    })
    
    $(document).on('click','.certified-licensed-armed', function(){
        if($(this).val()=="yes"){
            $('.certified-licensed-armed-section').removeClass('hide');
            disableEnable('.certified-licensed-armed-section',false);
        }
        else{
             $('.certified-licensed-armed-section').addClass('hide');
             disableEnable('.certified-licensed-armed-section',true);
        }
    })
    
    $(document).on('click','.cpr-aed-certified', function(){
        if($(this).val()=="1"){
            $('.cpr-aed-certified-section').removeClass('hide');
            disableEnable('.cpr-aed-certified-section',false);
        }
        else{
            $('.cpr-aed-certified-section').addClass('hide');
            disableEnable('.cpr-aed-certified-section',true);
        }
    })
    
    $(document).on('click', '.twic-certified',function(){
        if($(this).val()=="1"){
            $('.twic-certified-section').removeClass('hide');
            disableEnable('.twic-certified-section',false);
        }
        else{
            $('.twic-certified-section').addClass('hide');
            disableEnable('.twic-certified-section',true);
        }
    })
    $(function() {
        $('.multiselect-ui').multiselect({
             includeSelectAllOption: true
        });
    
        $('.report-submit').multiselect({
         includeSelectAllOption: true
        });

        $('.languages-speak').multiselect({
         includeSelectAllOption: true
        });

    });
    
    $('#datetimepicker30').datetimepicker({
          viewMode: 'years',
           format: 'MM/YYYY'
    });
    $('#datetimepicker11').datetimepicker({
          viewMode: 'years',
           format: 'MM/YYYY'
    });
     $('#datetimepicker21').datetimepicker({
          viewMode: 'years',
           format: 'MM/YYYY'
    });

    $('#datetimepicker12').datetimepicker({
          viewMode: 'years',
           format: 'MM/YYYY'
    });
    $('#datetimepicker22').datetimepicker({
          viewMode: 'years',
           format: 'MM/YYYY'
    });
     $('#datetimepicker13').datetimepicker({
          viewMode: 'years',
           format: 'MM/YYYY'
    });
     $('#datetimepicker23').datetimepicker({
          viewMode: 'years',
           format: 'MM/YYYY'
    });
     $('#datetimepicker14').datetimepicker({
          viewMode: 'years',
           format: 'MM/YYYY'
    });
     $('#datetimepicker24').datetimepicker({
          viewMode: 'years',
           format: 'MM/YYYY'
    });
       $('#datetimepicker15').datetimepicker({
          viewMode: 'years',
           format: 'DD/MM/YYYY'
    });

      $('#datetimepicker25').datetimepicker({
           format: 'MM/YYYY'
    });
       $('#datetimepicker16').datetimepicker({
           format: 'MM/YYYY'
    });
       $('#datetimepicker17').datetimepicker({
           format: 'MM/YYYY'
    });
    $('#datetimepicker18').datetimepicker({
        viewMode: 'years',
        format: 'DD/MM/YYYY'
    });
    $('#armed-expiry-date').datetimepicker({
        viewMode: 'years',
        format: 'DD/MM/YYYY'
    });
    $('#cpr-expiry-date').datetimepicker({
        viewMode: 'years',
        format: 'DD/MM/YYYY'
    });
    $('#twic-exiry-date').datetimepicker({
        viewMode: 'years',
        format: 'DD/MM/YYYY'
    });
    $('#datetimepicker1').datetimepicker({
          viewMode: 'years',
           format: 'MM/YYYY'
    });
    $('#datetimepicker2').datetimepicker({
          viewMode: 'years',
           format: 'MM/YYYY'
    });
    $('#date_started').datetimepicker({
           format: 'DD/MM/YYYY'
    });
    $('#date_ended').datetimepicker({
           format: 'DD/MM/YYYY'
    });
    
    education();
    reference();
    function education() {        
        $(document).find('#candidate-education-form').submit(function(event){
            event.preventDefault();
            if ($(this).valid()) {
                showLoader();
                $.ajax({
                    url: $('#candidate-education-form').attr('action'),
                    type: 'POST',
                    data: $('#candidate-education-form').serialize() ,
                    // dataType:'json'
                }).done(function(data) {
                    hideLoader();
                    $('#education-list').append(data);
                    $('#candidate-education').modal('hide');

                });
            }
        });
    }
    function reference() {        
        $(document).find('#candidate-reference-form').submit(function(event){
            event.preventDefault();
            if ($(this).valid()) {
                showLoader();
                $.ajax({
                    url: $('#candidate-reference-form').attr('action'),
                    type: 'POST',
                    data: $('#candidate-reference-form').serialize() ,
                    // dataType:'json'
                }).done(function(data) {
                    hideLoader();
                    $('#reference-list').append(data);
                    $('#candidate-reference').modal('hide');
                    // commonScript(); 
                });
            }
        });
    }
    $(document).on('click','.delete-education-link', function(){
        var url= $(this).attr('data-url');
        var ele = this;
        showLoader();
        $.ajax({
            url: url,
            type: 'GET',
            dataType:'json'
        }).done(function(data) {
            hideLoader();
            if(data.response.status == 200){
                $(ele).closest('.col-sm-12').remove();
            }
        });
    });
    function hideLoader() {
        $(document).find('.loader').hide();
    }
    function showLoader() {
        $(document).find('.loader').show();
    }
    function nextTab(elem) {
        $(elem).next().find('a[data-toggle="tab"]').click();
    }
    function prevTab(elem) {
        $(elem).prev().find('a[data-toggle="tab"]').click();
    }
    $('#candidate-apply-third-step').submit(function(event){
        if($('#candidate-apply-third-step').valid() ){
            event.preventDefault();
            showLoader();
            $.ajax({
                url: $('#candidate-apply-third-step').attr('action')+'.json',
                type: 'POST',
                data: $('#candidate-apply-third-step').serialize() ,
                dataType: 'json'
            }).done(function(data) {
                var $active = $('.wizard .nav-tabs li.active');
                $active.next().removeClass('disabled');
                nextTab($active);
                hideLoader();
                $(".prev-step").click(function (e) {            
                    var $active = $('.wizard .nav-tabs li.active');
                    prevTab($active);
                
                });

            });
        }
    })

    function addDisabledField() {
        $('input, select, button').each(
            function () {
                if ($(this).parents('.hide').length) {
                    $(this).attr('disabled', true);
                } else {
                    $(this).attr('disabled', false).removeClass('disabled');
                }
            }
        )
    }
    addDisabledField();
})();