$(function() {
	$('.delete-list').on('click', function(){
		var element = $(this);
		var id = $(this).attr('data-id');
		$('#heading').html('Delete Template');
		$('#innerText').html('Are you sure you want to delete this template?');
		$('#confirmDelete').modal('show');
		$('#confirm').unbind('click').click(function() {
			deleteList(id, element);
		});
	});

	$('#clearForm').on('click', function (event) {
		event.preventDefault();
		$('#emailForm').trigger('reset');
	})
});


//Function to Delete the List
function deleteList(id, element){
	$('#confirmDelete').modal('hide');
	$.ajax({
		method: 'GET',
		url: window.url+ 'api/Admins/deleteEmail.json?id='+id,
		success: function(data) {
			$('#messageModal').modal('show');
			if (data.response.status) {
				$('#modalHead').html('Template Removed!');
				$('#modalText').html(data.response.message);
				element.parent().parent().remove();
				//By default tr length is 1 for th body
				if ($('.table tr').length == 1) {
					location.reload();
				}
			 } else {
			 	$('#modalHead').html('Template Removed!');
				$('#modalText').html(data.response.message);
			}	
		},
		complete: function(){
         	$("#loader").hide(); //hide loading here
        },
		error: function(error) {
			$("#loader").hide();
			$('#messageModal').modal('show');
			$('#modalHead').html('Template Removed!');
			$('#modalText').html(error);
		}
	});
}