<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Mailer\Email;
use Cake\Core\Configure;

class ExpirationShell extends Shell
{
    public function main()
    {
        $this->loadModel('Companies');
        $companies = $this->Companies->find()
                       ->where(['Companies.is_active' => 1])
                       ->contain(['Users']);
        foreach ($companies as $key => $value) {
            $this->expiration29($value->id, $value['user']->email);
            $this->expiration44($value->id, $value['user']->email);
            $this->expiration59($value->id, $value['user']->email);
            $this->expiration89($value->id, $value['user']->email);
            $this->expiration120($value->id, $value['user']->email);
        }
    }
    public function updateCertification($id, $day) {
        $this->loadModel('EmployeeCertifications');
        $employeeCertificate = $this->EmployeeCertifications->get($id);
        $employeeCertificate->notification = $day;
        $this->EmployeeCertifications->save($employeeCertificate);

    }

    public function expiration29($company_id, $company_email) {
      $this->loadModel('EmployeeCertifications');
      $expirationDates29 = $this->EmployeeCertifications->find()
                       ->matching(
                           'Candidates', function ($q) use ($company_id) {
                               return $q->where(['Candidates.company_id' => $company_id]);
                           }
                       )
                       ->where(['(DATEDIFF(EmployeeCertifications.expiration_date,NOW())) BETWEEN 0 AND 29'])
                       ->contain(['Candidates','Certifications']);
       $tmp = '';
       foreach($expirationDates29 as $key => $value) {
           if(!in_array($value->notification,[0,1])) {
               $tmp = $tmp.'<div class="container">
                    <div class="row">
                        <div class="col">Name:'.
                      $value['candidate']->first_name.' '.$value['candidate']->last_name.'
                        </div>
                        <div class="col">Certification:'.
                      $value['certification']->name.'
                        </div>
                        <div class="col">Expiration:'.
                      date('Y-m-d',strtotime($value->expiration_date)).'
                        </div>
                    </div>
                </div>';
                
                 
           }
           $this->updateCertification($value['id'], 1);
       }
        $this->loadModel('EmailTemplates');
        if(!empty($tmp)) {
        $temp = $this->EmailTemplates->find()
                    ->where(['EmailTemplates.id' => 5])
                    -> first();
                      
                    $temp['mail_body'] = str_replace(
                            array('#DAYS','#EXPIRATION'),
                            array(
                                '59 days left Panel',
                                $tmp
                            ), 
                        $temp['mail_body']
                    );
        $this->_sendEmailMessage($company_email, $temp['mail_body'], $temp['subject']);
        }
   }

    public function expiration44($company_id, $company_email) {
        $this->loadModel('EmployeeCertifications');
       $expirationDates44 = $this->EmployeeCertifications->find()
                       ->matching(
                           'Candidates', function ($q) use ($company_id) {
                               return $q->where(['Candidates.company_id' => $company_id]);
                           }
                       )
                       ->where(['(DATEDIFF(EmployeeCertifications.expiration_date,NOW())) BETWEEN 30 AND 44'])
                       ->contain(['Candidates','Certifications']);
        $tmp = '';
        foreach($expirationDates44 as $key => $value) {
            if(!in_array($value->notification,[0,2])) {
                $tmp = $tmp.'<div class="container">
                    <div class="row">
                        <div class="col">Name:'.
                      $value['candidate']->first_name.' '.$value['candidate']->last_name.'
                        </div>
                        <div class="col">Certification:'.
                      $value['certification']->name.'
                        </div>
                        <div class="col">Expiration:'.
                      date('Y-m-d',strtotime($value->expiration_date)).'
                        </div>
                    </div>
                </div>';
            }
            $this->updateCertification($value['id'], 2);
        }
        $this->loadModel('EmailTemplates');
        if(!empty($tmp)) {
        $temp = $this->EmailTemplates->find()->where(['EmailTemplates.id' => 5])
                    -> first();
                      
                    $temp['mail_body'] = str_replace(
                            array('#DAYS','#EXPIRATION'),
                            array(
                                '44 days left Panel',
                                $tmp
                            ), 
                        $temp['mail_body']
                    );
        $this->_sendEmailMessage($company_email, $temp['mail_body'], $temp['subject']); 
        }
    }

    public function expiration59($company_id, $company_email) {
        $this->loadModel('EmployeeCertifications');
       $expirationDates59 = $this->EmployeeCertifications->find()
                       ->matching(
                           'Candidates', function ($q) use ($company_id) {
                               return $q->where(['Candidates.company_id' => $company_id]);
                           }
                       )
                       ->where(['(DATEDIFF(EmployeeCertifications.expiration_date,NOW())) BETWEEN 45 AND 59'])
                       ->contain(['Candidates','Certifications']);
        $tmp = '';
        foreach($expirationDates59 as $key => $value) {
            if(!in_array($value->notification,[0,3])) {
                 $tmp = $tmp.'<div class="container">
                    <div class="row">
                        <div class="col">Name:'.
                      $value['candidate']->first_name.' '.$value['candidate']->last_name.'
                        </div>
                        <div class="col">Certification:'.
                      $value['certification']->name.'
                        </div>
                        <div class="col">Expiration:'.
                      date('Y-m-d',strtotime($value->expiration_date)).'
                        </div>
                    </div>
                </div>';
            }
            $this->updateCertification($value['id'], 3);
        }
        $this->loadModel('EmailTemplates');
        if(!empty($tmp)) {
        $temp = $this->EmailTemplates->find()->where(['EmailTemplates.id' => 5])
                    -> first();
                      
                    $temp['mail_body'] = str_replace(
                            array('#DAYS','#EXPIRATION'),
                            array(
                                '59 days left Panel',
                                $tmp
                            ), 
                        $temp['mail_body']
                    );
        
        $this->_sendEmailMessage($company_email, $temp['mail_body'], $temp['subject']); 
        }
    }

    public function expiration89($company_id, $company_email) {
        $this->loadModel('EmployeeCertifications');
        $expirationDates89 = $this->EmployeeCertifications->find()
                       ->matching(
                           'Candidates', function ($q) use ($company_id) {
                               return $q->where(['Candidates.company_id' => $company_id]);
                           }
                       )
                       ->where(['(DATEDIFF(EmployeeCertifications.expiration_date,NOW())) BETWEEN 60 AND 89'])
                       ->contain(['Candidates','Certifications']);
        $tmp = '';
        foreach($expirationDates89 as $key => $value) {
           if(!in_array($value->notification,[0,4])) {
                $tmp = $tmp.'<div class="container">
                    <div class="row">
                        <div class="col">Name:'.
                      $value['candidate']->first_name.' '.$value['candidate']->last_name.'
                        </div>
                        <div class="col">Certification:'.
                      $value['certification']->name.'
                        </div>
                        <div class="col">Expiration:'.
                      date('Y-m-d',strtotime($value->expiration_date)).'
                        </div>
                    </div>
                </div>';
            }
            $this->updateCertification($value['id'], 4);
        }
        $this->loadModel('EmailTemplates');
        if(!empty($tmp)) {
        $temp = $this->EmailTemplates->find()->where(['EmailTemplates.id' => 5])
                    -> first();
                      
                    $temp['mail_body'] = str_replace(
                            array('#DAYS','#EXPIRATION'),
                            array(
                                '89 days left Panel',
                                $tmp
                            ), 
                        $temp['mail_body']
                    );
        $this->_sendEmailMessage($company_email, $temp['mail_body'], $temp['subject']); 
        }
    }

    public function expiration120($company_id, $company_email) {
        $this->loadModel('EmployeeCertifications');
        $expirationDates120 = $this->EmployeeCertifications->find()
                       ->matching(
                           'Candidates', function ($q) use ($company_id) {
                               return $q->where(['Candidates.company_id' => $company_id]);
                           }
                       )
                       ->where(['(DATEDIFF(EmployeeCertifications.expiration_date,NOW())) BETWEEN 90 AND 120'])
                       ->contain(['Candidates','Certifications']);
        $tmp = '';
        foreach($expirationDates120 as $key => $value) {
           if(!in_array($value->notification,[0,5])) {
                $tmp = $tmp.'<div class="container">
                    <div class="row">
                        <div class="col">Name:'.
                      $value['candidate']->first_name.' '.$value['candidate']->last_name.'
                        </div>
                        <div class="col">Certification:'.
                      $value['certification']->name.'
                        </div>
                        <div class="col">Expiration:'.
                      date('Y-m-d',strtotime($value->expiration_date)).'
                        </div>
                    </div>
                </div>';
            }
            $this->updateCertification($value['id'], 5);
        }
        $this->loadModel('EmailTemplates');
        if(!empty($tmp)) {
        $temp = $this->EmailTemplates->find()->where(['EmailTemplates.id' => 5])
                    -> first();
                      
                    $temp['mail_body'] = str_replace(
                            array('#DAYS','#EXPIRATION'),
                            array(
                                '120 days left Panel',
                                $tmp
                            ), 
                        $temp['mail_body']
                    );
        $this->_sendEmailMessage($company_email, $temp['mail_body'], $temp['subject']); 
        }
    }
    
     /**
     * Method _sendEmailMessage to send email from website
     *
     * @param $to string contain the receiver's email address
     * @param $email_body string the body of the email to send
     * @param $subject string subject of the email
     * @param $attachments array containg the array of attachments if any
     * @return bool
     */
    protected function _sendEmailMessage($to = null, $email_body = null, $subject = null, $attachments = [])
    {
        $email = new Email('default');
        $email->from(['hr@ayllus.com'])
            ->to($to)
            ->emailFormat('html')
            ->subject($subject);
        if (!empty($attachments)) {
            $email->attachments($attachments);
        }
        if ($email->send($email_body)) {
            return true;
        }
        return false;
    }
}
?>