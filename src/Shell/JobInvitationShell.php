<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Mailer\Email;
use Cake\Core\Configure;
use Cake\Routing\Router;

/**
 * JobInvitation shell command.
 */
class JobInvitationShell extends Shell
{

    /**
     * Manage the available sub-commands along with their arguments and help
     *
     * @see http://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     *
     * @return \Cake\Console\ConsoleOptionParser
     */
    public function getOptionParser()
    {
        $parser = parent::getOptionParser();

        return $parser;
    }

    /**
     * main() method.
     *
     * @return bool|int|null Success or error code.
     */
    public function main()
    { $this->loadModel('Jobs');
        $jobs = $this->Jobs->find()
                    ->where(['Jobs.is_delete' => 0, 'is_invitation_sent' => 0])
                    ->contain(['Countries','States', 'Cities', 'JobTypes']);
        foreach ($jobs as $key => $value) {
            $this->__jobCandidates($value);
            $value->is_invitation_sent =1;
            $this->Jobs->save($value);
        }
    }

    protected function __jobCandidates($job) {
        $this->loadModel('Candidates');
        $this->Candidates->addBehavior('Geo.Geocoder', ['address' => 'zipcode']);
        $options = ['lat' => $job->lat, 'lng' => $job->lng, 'distance' => $job->miles];
        $query = $this->Candidates->find('distance', $options)
                        ->where(['Candidates.company_id' => $job->company_id])
                        ->contain(['Jobs']);
        $query->order(['distance' => 'ASC']);
        if(!$query->isEmpty()) {
            foreach ($query as $k => $v) {
                if(!empty($v)) {
                    $this->__sendInvitation($v, $job);
                }
            }
        }
    }

    protected function __sendInvitation($candidate, $job) {
        $this->loadModel('EmailTemplates');
        $temp = $this->EmailTemplates->find()
                ->where(['id' => 6])
                ->first();
        $link ='https://securityofficerhr.net/Jobs/apply/'.base64_encode($job->id).'/'.base64_encode($job->company_id);

        $temp['mail_body'] = str_replace(
            array(
                '#CANDIDATE',
                '#JOB',
                '#COUNTRY',
                '#STATE',
                '#CITY',
                '#TYPE',
                '#EDUCATION',
                '#EXPERIENCE',
                '#CERTIFICATION',
                '#SKILL',
                '#LINK'
            ),
            array(              
                $candidate['first_name'].' '. $candidate['middle_name']. ' '. $candidate['last_name'],
                $job->job_title, 
                $job->country->name,   
                $job->state->name,   
                $job->city->name,   
                $job->job_type->job_name,   
                $job->qualification,   
                $job->experience,   
                $job->certification,   
                $job->language_skills,  
                $link
            ), 
            $temp['mail_body']
        );
        return $this->_sendEmailMessage(
            $candidate['email'], 
            $temp['mail_body'],
            $temp['subject']
        );
    }

    protected function _sendEmailMessage($to = null, $email_body = null, $subject = null, $attachments = [])
    {
        $email = new Email('default');
        if(!empty($to)) {
            $email->from(['noreply@securityofficerhr.com'])
                ->to($to)
                ->emailFormat('html')
                ->subject($subject);
            if (!empty($attachments)) {
                $email->attachments($attachments);
            }
            if ($email->send($email_body)) {
                return true;
            }
        }
        return false;
    }
}
