<?php
namespace App\View\Cell;

use Cake\View\Cell;

/**
 * Employee cell
 */
class EmployeeCell extends Cell
{

    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    /**
     * Default display method.
     *
     * @return void
     */
    public function display($id)
    {
        $this->loadModel('Candidates');
        $points = $this->Candidates->calculate($id);
        $this->set(compact('points'));

    }
}
