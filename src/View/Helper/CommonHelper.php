<?php 
namespace App\View\Helper;

use Cake\View\Helper;

class CommonHelper extends Helper
{
	public function valueExist($value) {
		
		return (!empty($value)) ? $value : "-" ;
	}

	public function valueExistInArray($array, $index = null , $field , $key = 0) {
		if($index) {
			return (!empty($array[$key])) ? $this->valueExist($array[$key][$index]->$field) : "-" ;
		} else {
			return (!empty($array[$key])) ? $this->valueExist($array[$key]->$field) : "-" ;
		}
		
	}

	public function getCompanyImagePath($folderName, $fileName)
	{
		return 'logo'.DS.'companies'.DS.'website_logo'.DS.$folderName.DS.$fileName;
	}

	public function workingShifts($day,$shift,$datas) {
		foreach ($datas as $key => $data) {
			if($data->shift_id == $shift && $data->day_id == $day){
				return true;
			}
		}
		return false;
	}

}