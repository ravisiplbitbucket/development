<?php use Cake\Core\Configure; ?>
<div class="content-wrapper clearfix ">
<?php echo $this->Flash->render('positive');?>
    <div class="top-header clearfix">
        <div class="col-sm-6 col-xs-6 main-title">Setup</div>
    </div>
    <?php if(!in_array($this->request->session()->read('Auth.User.user_role_id'),[3,4,5])) { ?>
    <?= $this->Form->create($user,array('url' => array(
                    'controller' => 'Users',
                    'action' => 'index'
                ),
                'id' => 'setup',
                'type' => 'file'
                )
            ); 
    ?>
    <div class="col-sm-6 setup-page">
        <div class="col-sm-12">
            <div class="form-group">
                <label>Company <span class ="star">*</span></label>
                <?php
                    echo $this->Form->input('company.company',
                        array(
                            'class' => 'form-control',
                            'div' => false,
                            'label' => false,
                            'required' => true
                        )
                    );
                ?>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group">
                <label>Address 1 <span class ="star">*</span></label>
                <?php
                    echo $this->Form->input('company.address1',
                        array(
                            'class' => 'form-control',
                            'div' => false,
                            'label' => false,
                            'required' => true
                        )
                    );
                ?>
            </div>
        </div>
          <div class="col-sm-12">
            <div class="form-group">
                <label>Address 2</label>
                <?php
                    echo $this->Form->input('company.address2',
                        array(
                            'class' => 'form-control',
                            'div' => false,
                            'label' => false
                        )
                    );
                ?>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Country <span class ="star">*</span></label>
                        <?php
                            echo $this->Form->input('company.country_id',
                                array(
                                    'class' => 'form-control',
                                    'div' => false,
                                    'label' => false,
                                    'required' => true,
                                    'options' => $country
                                )
                            );
                        ?>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group state-container">
                        <label>State/Province <span class ="star">*</span></label>
                        <?php
                            echo $this->Form->input('company.state_id',
                                array(
                                    'class' => 'form-control',
                                    'div' => false,
                                    'label' => false,
                                    'required' => true,
                                    'options' => $states
                                )
                            );
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-6">
                   <div class="form-group city-container">
                        <label>City <span class ="star">*</span></label>
                        <?php
                            echo $this->Form->input('company.city_id',
                                array(
                                    'class' => 'form-control',
                                    'div' => false,
                                    'label' => false,
                                    'required' => true,
                                    'options' => $cities
                                )
                            );
                        ?>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Postal Code <span class ="star">*</span></label>
                        <?php
                            echo $this->Form->input('company.zipcode',
                                array(
                                    'class' => 'form-control',
                                    'div' => false,
                                    'label' => false,
                                    'required' => true
                                )
                            );
                        ?>
                    </div>
                </div>
            </div>
        </div>
         <div class="col-sm-12">
            <div class="form-group">
                <label> Custom Website URL <span class ="star">*</span></label>
                <?php
                    echo $this->Form->input('company.website_url',
                        array(
                            'class' => 'form-control',
                            'div' => false,
                            'label' => false,
                            'required' => true,
                            'value' => Configure::read('SiteUrl').'Jobs/jobListing/'.preg_replace('/[^a-zA-Z0-9\']/', '-', $user['company']->company).'-'.$user['company']->id
                        )
                    );
                ?>
            </div>
            <?php if(empty($user['company']->overview)) { ?>
            <label id="company-website-url-error" class="error" for="company-website-url">Please Save the Company Overview before attempting to use the Custom Website URL</label>
            <?php } ?>
        </div>
        <div class="col-sm-12">
            <div class="form-group">
                <label> Home Page URL <span class ="star">*</span></label>
                <?php
                    echo $this->Form->input('company.home_url',
                        array(
                            'class' => 'form-control',
                            'div' => false,
                            'label' => false,
                            'required' => true
                        )
                    );
                ?>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group">
                <label>Company Overview <span class ="star">*</span></label>
                <?php
                    echo $this->Form->input('company.overview',
                        array(
                            'class' => 'form-control',
                            'div' => false,
                            'label' => false,
                            'required' => true,
                            'type' => 'textarea'
                        )
                    );
                ?>
            </div>
        </div>
        <div class="col-sm-12 form-group">
            <div class="form-group">
                <label>Upload your company logo <span class ="star">*</span></label>
            </div>
        </div>
        <div class="col-sm-12 form-group">
            <div class="row">
             <?php $image = '/logo/companies/website_logo/'.$this->request->session()->read('Auth.User.company.logo_path').'/square_'.$this->request->session()->read('Auth.User.company.website_logo'); ?>
            <?php echo $this->Html->link(
                $this->Html->image($image),
                '#',
                array(
                    'class' => 'navbar-brand',
                    'escape' => false
                    )
            );?>
            </div>
            <div style="margin-top:50px;">
            <label id="upload">
            <?php echo $this->Form->control('company.website_logo',array('type' => 'file','label' => 'form-control logo','label' => false));?>
            </label>
            </div>
        </div>
        <div class="">
        <div class="col-sm-12">
            <?php
                echo $this->Form->button(__('Submit'),[
                    'class' => 'btn  btn-blue',
                    'type' => 'submit'
                ]);
            ?>
        </div>
        <?php } ?>
         <div class="col-sm-12">
            <?php echo $this->Html->link(
                '<span>'.$this->Html->image('lecture.svg',array('alt' =>'training','width'=> 100)).'Trainings</span>',
                '/Users/trainingListing',
                array('class' => 'ancher',
                    'escape' => false
                    )
            );?>
            <?php echo $this->Html->link(
                '<span>'.$this->Html->image('certificate.svg',array('alt' =>'certificate','width'=> 100)).'Certifications</span>',
                '/Users/certificateListing',
                array('class' => 'ancher',
                    'escape' => false
                    )
            );?>
            <?php 
                    echo $this->Html->link(
                        '<span>'.$this->Html->image('hat.svg',array('alt' =>'equipment','width'=> 100)).'Uniform & Equipments</span>',
                        '/Users/equipmentListing',
                        array('class' => 'ancher',
                            'escape' => false
                            )
                    );
            ?>
            <?php echo $this->Html->link(
                '<span>'.$this->Html->image('user.svg',array('alt' =>'admin','width'=> 100)).'Additional Admins</span>',
                '/Users/adminListing',
                array('class' => 'ancher',
                    'escape' => false
                    )
            );?>
            <?php echo $this->Html->link(
                '<span>'.$this->Html->image('interview.svg',array('alt' =>'question','width'=> 100)).'Interview Questions</span>',
                '/Users/questionListing',
                array('class' => 'ancher',
                    'escape' => false
                    )
            );?>
            <?php if($this->request->session()->read('Auth.User.user_role_id') == 2) { ?>
            <?php echo $this->Html->link(
                '<span>'.$this->Html->image('networking.svg',array('alt' =>'question','width'=> 100)).'Divisons</span>',
                '/Users/divisionListing',
                array('class' => 'ancher',
                    'escape' => false
                    )
            );?>
            <?php } ?>
         </div>
    </div>
</div>
<?php echo $this->Html->script('User/signup', ['block' => 'scriptBottom']); ?>