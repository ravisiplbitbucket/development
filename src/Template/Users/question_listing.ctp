<div class="content-wrapper clearfix ">
<?php echo $this->Flash->render('positive');?>
<div class="top-header clearfix">
    <div class="col-sm-6 col-xs-6 main-title">
        <?php echo $this->Html->link('<i class="fa fa-arrow-circle-left" aria-hidden="true"></i>',
            '/Users/index',
            array(
                'escape' => false
                )
        );?>Interview Questions 
    </div>
    <div class="col-sm-6 col-xs-6 text-right main-title"><a href="#" data-toggle="modal" data-target="#addQuestion" class="btn btn-blue small "><i class="fa fa-plus" aria-hidden="true"></i> Add New</a></div>
</div>
<div class="col-sm-12 setup-page">
    <div class="col-sm-12">
        <table class="table table-bordered thik">
            <thead>
                <tr>
                    <th>Questions</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php if(!empty($questions->toArray())) {
                    foreach($questions as $question) {
                        $question_checkbox = $question->_matchingData['CompanyDeleteQuestions']->interview_question_id;
                ?>
                <tr>
                    <td><?php echo $question->name;?></td>
                    <td class="text-center no-border-right" width="150">
                    <input type="checkbox" <?php echo !empty($question_checkbox) ? "checked" : ""?> id = "<?php echo 'text'.$question->id ?>" value="<?php echo $question->id ?>">
                    <?php
                        if($question->id > 99) {
                    ?> 
                    <?= $this->Html->Link("<span class='fa fa-pencil-square-o'></span>",
                               'javascript:void(0)',
                               [
                                   'escape'   => false,
                                   'class'    => 'edit-question',
                                   'data-url' =>   $this->Url->build(
                                               [  
                                                   "controller" => "Users",
                                                   "action" => "questionEdit",
                                                   base64_encode($question->id)
                                               ],true
                                           ),
                                    'title'  => 'edit'                           
                               ]
                           )
                        ?>
                    <?php } ?>
                    </td>
                </tr>
                <?php } } else {
                    echo "No record found";
                    }?>
            </tbody>
        </table>
    </div>
</div>
<div class="modal fade" id="edit-question" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit</h4>
      </div>
      <div class="modal-body clearfix">
      </div>
    </div>
  </div>
</div>
<?php echo $this->element('Setup/question');?>
<?php echo $this->Html->script('User/addon', ['block' => 'scriptBottom']); ?>