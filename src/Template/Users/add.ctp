<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="outer-bg">
    <div class="container signup-page">
    <?php echo $this->Flash->render('positive');?>
        <div class="col-sm-8 col-sm-offset-2">
            <div class="row">
                <h1 class="">Sign Up </h1>
            </div>
        </div>
        <div class="wizard col-sm-8 col-sm-offset-2">
            <div class="wizard-inner">
                <div class="connecting-line"></div>
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Step 1">
                        <span class="round-tab">
                        Step 1
                        </span>
                        </a>
                    </li>
                    <li role="presentation" class="disabled">
                        <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="Step 2">
                        <span class="round-tab">
                        Step 2
                        </span>
                        </a>
                    </li>
                </ul>
            </div>
            <?= $this->Form->create($user,array('url' => array(
                    'controller' => 'Users',
                    'action' => 'add'),
                    'type' => 'file',
                    'id' => 'signup')
                ); 
            ?>
                <div class="tab-content">
                    <div class="tab-pane active" role="tabpanel" id="step1">
                        <div class="step1">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>First Name <span class ="star">*</span></label>
                                    <?php echo $this->Form->control('company.first_name',array('class' => 'form-control','placeholder' => 'First Name','label' => false));?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Last Name <span class ="star">*</span></label>
                                    <?php echo $this->Form->control('company.last_name',array('class' => 'form-control','placeholder' => 'Last Name','label' => false));?>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Company <span class ="star">*</span></label>
                                    <?php echo $this->Form->control('company.company',array('class' => 'form-control','placeholder' => 'Company name','label' => false));?>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Address1 <span class ="star">*</span></label>
                                    <?php echo $this->Form->control('company.address1',array('class' => 'form-control','label' => false));?>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Address2</label>
                                    <?php echo $this->Form->control('company.address2',array('class' => 'form-control','label' => false));?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Country <span class ="star">*</span></label>
                                     <?php echo $this->Form->control('company.country_id',array('class' => 'form-control target','label' => false,'options' => $country,'id' => 'dropDownId', 'empty' => 'Select Country'));?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group state-container">
                                    <label>State/Province <span class ="star">*</span></label>
                                    <?php echo $this->Form->control('company.state_id',array('class' => 'form-control target','label' => false,'options' => [],'id' => '', 'empty' => 'Select State'));
                                    ?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group city-container">
                                    <label>City <span class ="star">*</span></label>
                                    <?php echo $this->Form->control('company.city_id',array('class' => 'form-control target','label' => false,'options' => [],'id' => '','empty' => 'Select City'));
                                    ?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Postal Code <span class ="star">*</span></label>
                                    <?php echo $this->Form->control('company.zipcode',array('class' => 'form-control','placeholder' => 'Postal Code','label' => false));?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Phone No <span class ="star">*</span></label>
                                    <div class="input-group">
                                        <span class="input-group-addon country-phone-code1" id="basic-addon1">757</span>
                                        <?php echo $this->Form->control('company.telephone',array('class' => 'form-control','placeholder' => 'phone no','aria-describedby'=>"basic-addon1",'label' => false,'id' => 'register-phone'));?>
                                        <?php echo $this->Form->hidden('company.country_phone_code',['class' => 'country-phone-code']);
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Email ID <span class ="star">*</span></label>
                                   <?php echo $this->Form->control('email',array('class' => 'form-control','placeholder' => 'Email ID','label' => false));?>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>How did you know about us? <span class ="star">*</span></label>
                                    <?php echo $this->Form->control('company.way_know_id',array('options' => $know,'class'=>'form-control','label' => false));?>
                                </div>
                            </div>
                            <!-- <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Division<span class ="star">*</span></label>
                                    <?php echo $this->Form->control('divisions.0.name',array('class'=>'form-control','label' => false));?>
                                </div>
                            </div> -->
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>User Name <span class ="star">*</span></label>
                                    <?php echo $this->Form->control('username',array('class' => 'form-control','label' => false));?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Password <span class ="star">*</span></label>
                                    <?php echo $this->Form->control('password',array('class' => 'form-control','label' => false));?>
                                    <span id="result"></span>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Confirm Password <span class ="star">*</span></label>
                                    <?php echo $this->Form->control('confirm_password',array('type' => 'password','class' => 'form-control','label' => false));?>
                                </div>
                            </div>
                            <div class="col-sm-12 form-group">
                                <div class="checkbox">
                                    <label>
                                    <input type="checkbox" name = "agreement"> By checking this box you agree to the Terms of Service Agreement
                                    </label>
                                </div>
                            </div>
                        </div>
                        <ul class="list-inline col-sm-12">
                            <li><button type="button" class="btn btn-blue next-step">Save and continue</button></li>
                        </ul>
                    </div>
                    <div class="tab-pane" role="tabpanel" id="step2">
                        <div class="step2">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>What is your company website? <span class ="star">*</span></label>
                                    <?php echo $this->Form->control('company.home_url',array('class' => 'form-control','label' => false));?>
                                </div>
                            </div>
                            <!-- <div class="col-sm-12">
                                <div class="form-group">
                                    <label> Choose a Custom website name. <span class ="star">*</span></label>
                                   <?php echo $this->Form->control('company.website_name',array('class' => 'form-control','label' => false));?>
                                </div>
                            </div> -->
                            <div class="col-sm-12 form-group">
                                <div class="form-group">
                                    <label>Upload your company logo <span class ="star">*</span></label>
                                </div>
                            </div>
                            <div class="col-sm-12 form-group">
                                <label id="upload">
                                <?php echo $this->Form->control('company.website_logo',array('type' => 'file','label' => 'form-control logo','label' => false));?>
                                </label>
                            </div>
                        </div>
                        <ul class="list-inline col-sm-12">
                            <li><button type="button" class="btn btn-cancel prev-step">Previous</button></li>
                            <li><button type="submit" class="btn btn-blue">Submit</button></li>
                        </ul>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php echo $this->Html->script('User/signup', ['block' => 'scriptBottom']); ?>
<style type="text/css">
    label.error{color: red;}
</style>