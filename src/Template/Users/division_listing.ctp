<div class="content-wrapper clearfix ">
<?php echo $this->Flash->render('positive');?>
<div class="top-header clearfix">
    <div class="col-sm-6 col-xs-6 main-title">
        <?php echo $this->Html->link('<i class="fa fa-arrow-circle-left" aria-hidden="true"></i>',
            '/Users/index',
            array(
                'escape' => false
                )
        );?>Divisions
    </div>
    <div class="col-sm-6 col-xs-6 text-right main-title"><a href="#" data-toggle="modal" data-target="#adddivison" class="btn btn-blue small "><i class="fa fa-plus" aria-hidden="true"></i> Add New</a></div>
</div>
<div class="col-sm-12 setup-page">
    <div class="col-sm-12">
        <table class="table table-bordered thik">
            <thead>
                <tr>
                    <th>Division</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php if(!empty($divisions->toArray())) {
                    foreach($divisions as $division) {
                ?>
                <tr>
                    <td><?php echo $division->name;?></td>
                    <td class="text-center no-border-right" width="150">
                    <?= $this->Html->Link("<i class='fa fa-trash-o aria-hidden='true'></i>",
                           '/Users/divisionDelete/'.base64_encode($division->id),
                           [
                           'escape' => false,
                           'confirm' => 'Are you sure you want to delete?'
                           ]
                       )
                ?> 
                <?= $this->Html->Link("<span class='fa fa-pencil-square-o'></span>",
                           'javascript:void(0)',
                           [
                               'escape'   => false,
                               'class'    => 'edit-division',
                               'data-url' =>   $this->Url->build(
                                           [  
                                               "controller" => "Users",
                                               "action" => "divisionEdit",
                                               base64_encode($division->id)
                                           ],true
                                       ),
                                'title'  => 'edit'                           
                           ]
                       )
                    ?>
                    </td>
                </tr>
                <?php } } else {
                    echo "No record found";
                    }?>
            </tbody>
        </table>
    </div>
</div>
<div class="modal fade" id="edit-division" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit</h4>
      </div>
      <div class="modal-body clearfix">
      </div>
    </div>
  </div>
</div>
<?php echo $this->element('Setup/division');?>
<?php echo $this->Html->script('User/addon', ['block' => 'scriptBottom']); ?>