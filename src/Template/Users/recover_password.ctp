<div class="outer-bg">
    <div class="container login-page">
    <?php echo $this->Flash->render('positive');?>
        <div class="signup-wrapper col-sm-6 col-sm-offset-3 clearfix">
                <?php
                    echo $this->Form->create(null,
                            array(
                                    'type' => 'post',
                                    'id' => 'recover-password'
                                    
                                )
                        );
                ?>
                <div class="forget-title text-center"> Reset Password </div>
                <div class="forget-text text-center">Enter your password below to change the 
                        password on your account.</div>

                <div class="col-sm-12">
                    <div class="form-group">
                        <label>New Password <span class ="star">*</span></label>
                       <?php
                        echo $this->Form->input('password',
                                array(
                                        'class' => 'form-control',
                                        'placeholder' => __('Enter your new password'),
                                        'div' => false,
                                        'label' => false,
                                        'required' => true,
                                        'type' => 'password'
                                    )
                            );
                        ?>
                    </div>
                    <div class="form-group">
                        <label>Re-enter Password <span class ="star">*</span></label>
                        <?php
                        echo $this->Form->input('new_password',
                                array(
                                        'class' => 'form-control',
                                        'placeholder' => __('Enter Confirm password'),
                                        'div' => false,
                                        'label' => false,
                                        'required' => true,
                                        'type' => 'password'
                                    )
                            );
                        ?>
                    </div>
                </div>
            <div class="col-sm-12 text-center form-group clearfix">
                
                <?php
                        echo $this->Form->button(__('Reset Password'),[
                                'class' => 'btn btn-blue btn-block',
                                'type' => 'submit'
                            ]);
                    ?>
                </div>
            </form>
        </div>
    </div>
</div>
<?php echo $this->Html->script('User/signup', ['block' => 'scriptBottom']); ?>