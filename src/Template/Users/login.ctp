<div class="outer-bg">
    <div class="container login-page">
    <?php echo $this->Flash->render('positive');?>
        <div class="signup-wrapper col-sm-6 col-sm-offset-3 clearfix">
            <?php
                echo $this->Form->create(null,
                        array(
                                'type' => 'post',
                                'id' => 'login'
                                
                            )
                    );
            ?>
                <h1 class="text-center">Login</h1>
                <div class="col-sm-12">
                    <div class="form-group">
                        <label>Username <span class ="star">*</span></label>
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1"><i class="fa fa-user" aria-hidden="true"></i></span>
                            <?php
                                echo $this->Form->input('username',
                                        array(
                                                'class' => 'form-control',
                                                'placeholder' => __('Username'),
                                                'div' => false,
                                                'label' => false,
                                                'required' => true
                                            )
                                    );
                            ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <label>Password <span class ="star">*</span></label>
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1"><i class="fa fa-lock" aria-hidden="true"></i></span>
                            <?php
                                echo $this->Form->input('password',
                                        array(
                                                'class' => 'form-control',
                                                'placeholder' => __('Password'),
                                                'div' => false,
                                                'label' => false,
                                                'required' => true

                                            )
                                    );
                            ?>
                        </div>
                    </div>
                </div>
                <div class="form-group clearfix">
                    <div class="forget-pass col-lg-6 col-xs-6">
                        <?php echo $this->Html->link('Forgot Password?',
                            '/Users/forgotPassword'
                        );?>
                    </div>
                    <div class="col-lg-6 col-xs-6 text-right">
                        <?php
                        echo $this->Form->button(__('Log In'),[
                                'class' => 'btn  btn-blue',
                                'type' => 'submit'
                            ]);
                        ?>
                    </div>
                </div>
                <div class="form-group border-bottom col-center text-center">
                    <span class="bottom-text text-center">
                    Don't have an account?  
                    <?php echo $this->Html->link('Sign up',
                        '/Users/add',
                        array(
                            'class' => 'signup-login'
                            )
                        );
                    ?>
                    </span>
                </div>
            </form>
            <!-- <div class="col-sm-12 text-center ">
                <button class="btn btn-blue btn-block">Login</button>
                </div> -->
        </div>
    </div>
</div>
<?php echo $this->Html->script('User/signup', ['block' => 'scriptBottom']); ?>