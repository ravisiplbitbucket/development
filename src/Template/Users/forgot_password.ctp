<div class="outer-bg">
    <div class="container login-page">
       <?php echo $this->Flash->render('positive');?>
        <div class="signup-wrapper col-sm-6 col-sm-offset-3 clearfix">
            <?php
                echo $this->Form->create(null,
                        array(
                                'type' => 'post',
                                'url' => array(
                                        'controller' => 'Users',
                                        'action' => 'forgotPassword'
                                    ),
                                'id' => 'forgot-password'
                                
                            )
                    );
                ?>
                <div class="forget-title text-center"> Forgot your password? </div>
                <div class="forget-text text-center">Don't worry! Just fill in your email and we'll help you<br> reset your password.</div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <label>Email <span class ="star">*</span></label>
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1"><i class="fa fa-envelope" aria-hidden="true"></i></span>
                            <?php
                                echo $this->Form->input('email',
                                        array(
                                                'class' => 'form-control',
                                                'placeholder' => __('Enter your registered Email Address'),
                                                'div' => false,
                                                'label' => false,
                                                'required' => true,
                                                'type' => 'email'
                                            )
                                    );
                            ?>
                        </div>
                    </div>
                </div>
                 
            <div class="col-sm-12 text-center form-group clearfix">
                <?php
                    echo $this->Form->button(__('Send Email'),[
                            'class' => 'btn btn-blue btn-block',
                            'type' => 'submit'
                        ]);
                ?>
                </div>
                <div class=" col-sm-12 border-bottom border-forget clearfix form-group  form-group text-center col-center">
                    <span class="bottom-text text-center">
                    <?php echo $this->Html->link(__('Back to Sign in'),
                        '/Users/login'
                    );?>
                    </span>
                </div>
            </form>
        </div>
    </div>
</div>
<?php echo $this->Html->script('User/signup', ['block' => 'scriptBottom']); ?>