<div class="content-wrapper clearfix ">
<?php echo $this->Flash->render('positive');?>
<div class="top-header clearfix">
   <div class="col-sm-6 col-xs-6 main-title"> 
      <?php echo $this->Html->link('<i class="fa fa-arrow-circle-left" aria-hidden="true"></i>',
            '/Users/index',
            array(
                'escape' => false
                )
        );?>
        Uniform & Equipments
   </div>
   <div class="col-sm-6 col-xs-6 text-right main-title">
        <a href="#" data-toggle="modal" data-target="#addEquipment" class="btn btn-blue small "><i class="fa fa-plus" aria-hidden="true"></i> Add New</a>
   </div>
</div>
<div class="col-sm-12 setup-page">
   <div class="col-sm-12">
      <table class="table table-bordered thik">
         <thead>
            <tr>
               <th>Uniform & Equipments</th>
               <th></th>
            </tr>
         </thead>
         <tbody>
            <?php if(!empty($equipments->toArray())) {
                foreach($equipments as $equipment) {
                ?>
            <tr>
               <td><?php echo $equipment->item;?></td>
               <td class="text-center no-border-right">
               <?= $this->Html->Link("<i class='fa fa-trash-o aria-hidden='true'></i>",
                           '/Users/equipmentDelete/'.base64_encode($equipment->id),
                           [
                           'escape' => false,
                           'confirm' => 'Are you sure you want to delete?'
                           ]
                       )
                ?> 
                <?= $this->Html->Link("<span class='fa fa-pencil-square-o'></span>",
                           'javascript:void(0)',
                           [
                               'escape'   => false,
                               'class'    => 'edit-equipment',
                               'data-url' =>   $this->Url->build(
                                           [  
                                               "controller" => "Users",
                                               "action" => "equipmentEdit",
                                               base64_encode($equipment->id)
                                           ],true
                                       ),
                                'title'  => 'edit'                           
                           ]
                       )
                    ?>
               </td>
            </tr>
            <?php } } else {
                echo "No Record Found";
                } ?>
         </tbody>
      </table>
   </div>
</div>
<div class="modal fade" id="edit-equipment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit</h4>
      </div>
      <div class="modal-body clearfix">
      </div>
    </div>
  </div>
</div>
<?php echo $this->element('Setup/equipment');?>
<?php echo $this->Html->script('User/addon', ['block' => 'scriptBottom']); ?>
