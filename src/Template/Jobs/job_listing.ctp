<?php echo $this->Flash->render('positive');?>
<div class="container">
    <div class="homaer-page-application">
        <div class="top-home-section">
            <h1>Welcome to the<span class="company-name"> <?php echo $id[0]->company;?> </span> Career Page</h1>
            <p><?php echo (!empty($id[0]->overview)) ? $id[0]->overview : ""?></p>
        </div>
        <div class="filter-option clearfix">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="col-sm-1"><label>Filter BY:</label></div>
                        <?= $this->Form->create(null,array('url' => array(
                            'controller' => 'Jobs',
                            'action' => 'jobListing',$this->request->params['pass'][0]),
                            'type' => 'get')
                        ); 
                        ?>
                            <div class="col-sm-2">
                                <?php 
                                    echo $this->Form->control('country_id',array('class' => 'form-control target','label' => false,'options' => $country,'empty' => 'Country',
                                        'default' => (!empty($this->request->query['country_id']))
                                        ? $this->request->query['country_id']
                                        :''
                                        )
                                    );
                                ?>
                            </div>
                            <div class="col-sm-2 state-container">
                                <?php echo $this->Form->control('state_id',array('class' => 'form-control target','label' => false,'options' => $states,'empty' => 'State','default' => (!empty($this->request->query['stateid']))
                                        ? $this->request->query['state_id']
                                        :''));?>
                            </div>
                            <div class="col-sm-2 city-container">
                                <?php echo $this->Form->control('city_id',array('class' => 'form-control target','label' => false,'options' => $cities,'empty' => 'City',
                                    'default' => (!empty($this->request->query['city_id']))
                                        ? $this->request->query['city_id']
                                        :''
                                    ));
                                ?>
                            </div>
                            <div class="col-sm-2">
                                <?php
                                    echo $this->Form->button(__('Submit'),[
                                        'class' => 'btn  btn-blue',
                                        'type' => 'submit'
                                    ]);
                                ?>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="table-home table-responsive">
            <table class="table table-bordered table-striped">
                <caption>
                    <h3>Open Positions</h3>
                </caption>
                <thead>
                    <tr>
                        <th>Job #</th>
                        <th>Job Title</th>
                        <th>Location<br>City, State</th>
                        <th>Job Type</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if(!empty($jobs->toArray())) {
                        foreach($jobs as $job) {
                    ?>
                    <tr>
                        <td>
                            <?php echo $this->Html->link($job->job_number,
                            '/Jobs/apply/'.base64_encode($job->id).'/'.base64_encode($job->company_id)
                            );?>    
                        </td>
                        <td><?php echo $job->job_title;?></td>
                        <td><?php echo $job['city']['name'].','.$job['state']['name'].','.$job['country']['sortname']?>
                        </td>
                        <td><?php echo $job['job_type']->job_name?></td>
                    </tr>
                    <?php } } else { ?>
                        <tr>
                            <td>No record found</td>
                        </tr>
                    <?php  } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php echo $this->Html->script('User/search', ['block' => 'scriptBottom']); ?>
<div class="modal fade" id="thankyouModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        Thank you for applying for a position with our company. If your background and experience are a good fit we will contact you to setup an interview. Thank you for your interest in our company!
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<?php
    $this->Html->scriptStart(['block' => 'scriptBottom']);
?>
    <?php        
        if(!empty($this->request->query['popup'])) {
    ?>
        $('#thankyouModal').modal('show');
    <?php
        }
    ?> 
<?php
    $this->Html->scriptEnd();
?>