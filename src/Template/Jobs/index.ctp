<div class="content-wrapper clearfix ">
<?php echo $this->Flash->render('positive');?>
    <div class="top-header clearfix">
        <div class="col-sm-6 col-xs-6 main-title">Jobs</div>
    </div>
    <div class="col-sm-12 setup-page ">
        <div class=" col-sm-12 table-responsive">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Job #</th>
                        <th>Job Title</th>
                        <th>Location<br>City, State</th>
                        <th>Date Posted</th>
                        <th># of applicants</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if(!empty($jobs->toArray())) {
                        foreach($jobs as $job) {
                        ?>
                    <tr>
                        <td>
                        <?php echo $this->Html->link($job->job_number,
                            '/Jobs/view/'.base64_encode($job->id)
                        );?>
                        </td>
                        <td><?php echo $job->job_title;?></td>
                        <td><?php echo $job['city']['name'].','.$job['state']['name'].','.$job['country']['sortname']?></td>
                        <td><?php echo date('M d,Y',strtotime($job->created))?></td>
                        <td><?php echo count($job['candidates']);?></td>
                        <td><span class="label label-success"><?php echo ($job->active == 1) ? "open" : "closed"?></span></td>
                        <td class="actions">
                            <?php echo $this->Html->link(
                                '<i class="fa fa-pencil-square" aria-hidden="true"></i>',
                                '/Jobs/edit/'.base64_encode($job->id),
                                array(
                                    'escape' => false,
                                    'title' => 'edit'
                                    )
                            );?>
                            <?php echo $this->Html->link(
                                '<i class="fa fa-eye-slash" aria-hidden="true"></i>',
                                '/Jobs/view/'.base64_encode($job->id),
                                array(
                                    'escape' => false,
                                    'title' => 'view'
                                    )
                            );?>
                            <?php 
                                if($job->active == 1) {
                                    echo $this->Html->link(
                                        '<span class="label label-success">Active</span>',
                                        '/Jobs/deactivate/'.base64_encode($job->id),
                                        array(
                                            'escape' => false,
                                            'title' => 'active'
                                            )
                                    );  
                                }else{
                                    echo $this->Html->link(
                                        '<span class="label label-warning">Paused</span>',
                                        '/Jobs/activate/'.base64_encode($job->id),
                                        array(
                                            'escape' => false,
                                            'title' => 'Paused'
                                            )
                                    );  
                                }
                                ?>
                        </td>
                    </tr>
                    <?php } } else { ?>
                        <tr>
                            <td>No record found</td>
                        </tr>
                  <?php  } ?>
                </tbody>
            </table>
            <?php echo $this->element('pagination'); ?>
        </div>
    </div>
</div>