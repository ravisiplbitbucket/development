<div class="container job-description-home">
<h1>Job Description</h1>
<div class="homaer-page-application clearfix">
    <div class="col-sm-12 setup-page jd-page">
        <div class="row">
            <div class="col-sm-12">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Job#</label>
                        <div><?php echo $job[0]->job_number;?></div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Job Title</label>
                        <div><?php echo $job[0]->job_title;?></div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Country</label>
                        <div><?php echo $job[0]['country']->name;?></div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Job State</label>
                        <div><?php echo $job[0]['state']->name;?></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Job City</label>
                        <div><?php echo $job[0]['city']->name;?></div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Job Site Zip Code</label>
                        <div><?php echo $job[0]->zipcode;?></div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Job Type</label>
                        <div><?php echo $job[0]['job_type']->job_name;?></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group">
                <label>Company Overview </label>
                <div>
                   <?php echo $job[0]['company']->overview;?>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group">
                <label>Responsibilities  </label>
                <div><?php echo $job[0]->responsibility?></div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group">
                <label>Qualifications</label>
                <div><?php echo $job[0]->qualification?></div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group">
                <label>Education and/or Experience</label>
                <div><?php echo $job[0]->experience?></div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group">
                <label>Certificates/Licenses/Registrations</label>
                <div><?php echo $job[0]->certification?></div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group">
                <label>Language Skills</label>
                <div><?php echo $job[0]->language_skills?></div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group">
                <label>Other Qualifications</label>
                <div><?php echo $job[0]->other_qualification?></div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group">
                <label>Physical Demands</label>
                <div><?php echo $job[0]->physical_demands?></div>
            </div>
        </div>
        <div class="col-sm-12 form-group">
            <div class="row">
                <div class="form-group">
                    <label class="col-sm-12">Salary &amp; Benefits </label>
                    <div class="col-sm-6">
                        $<?php echo $job[0]->salary_from?> - $<?php echo $job[0]->salary_to ?> per Hour
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 bottom-btn">
            <?php echo $this->Html->link('Apply',
                '/Jobs/candidateApply/'.base64_encode($job[0]->id).'/'.base64_encode($job[0]->company_id),
                array(
                    'class' => 'btn btn-blue'
                    )
            );?>
            <?php 
            $path = preg_replace('/[^a-zA-Z0-9\']/', '-',$url[0]->company);
            echo $this->Html->link('Cancel',
                '/Jobs/jobListing/'.$path.'-'.$url[0]->id,
                array(
                    'class' => 'btn btn-cancel'
                    )
            );?>
        </div>
    </div>
</div>

