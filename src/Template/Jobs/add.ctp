<div class="content-wrapper clearfix ">
<?php echo $this->Flash->render('positive');?>
    <div class="col-sm-6 col-sm-offset-3">
        <div class=" top-title-dash text-center">
            <h2>Post A Job</h2>
        </div>
        <?= $this->Form->create($job,array('url' => array(
                    'controller' => 'Jobs',
                    'action' => 'add'),
                    'id' => 'job-post')
                ); 
        ?>
        <div class="col-sm-12">
            <div class="form-group">
                <label>Job#</label>
                <?php
                    echo $this->Form->input('job_number',
                        array(
                            'class' => 'form-control',
                            'placeholder' => __('646'),
                            'div' => false,
                            'label' => false,
                            'required' => true,
                            'value' => time(),
                            'readonly'
                        )
                    );
                ?>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group">
                <label>Job Title <span class ="star">*</span></label>
                <?php
                    echo $this->Form->input('job_title',
                        array(
                            'class' => 'form-control',
                            'div' => false,
                            'label' => false,
                            'required' => true
                        )
                    );
                ?>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Country <span class ="star">*</span></label>
                        <?php echo $this->Form->control('country_id',array('class' => 'form-control target','label' => false,'options' => $country,'id' => 'dropDownId', 'empty' => 'Select Country'));?>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group state-container">
                        <label>Job State <span class ="star">*</span></label>
                        <?php echo $this->Form->control('state_id',array('class' => 'form-control target','label' => false,'options' => [],'id' => '', 'empty' => 'Select State'));
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group city-container ">
                        <label>Job City <span class ="star">*</span></label>
                       <?php echo $this->Form->control('city_id',array('class' => 'form-control target','label' => false,'options' => [],'id' => '','empty' => 'Select City'));
                                    ?>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Job Site Zip Code <span class ="star">*</span></label>
                        <?php
                            echo $this->Form->input('zipcode',
                                array(
                                    'class' => 'form-control',
                                    'div' => false,
                                    'label' => false,
                                    'required' => true
                                )
                            );
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Job Type <span class ="star">*</span></label>
                        <?php echo $this->Form->control('job_type_id',array('class' => 'form-control target','label' => false,'options' => $jobTypes));?>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Required Education <span class ="star">*</span></label>
                        <?php echo $this->Form->control('education_id',array('class' => 'form-control target','label' => false,'options' => $educations));?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group">
                <label>Company Overview</label>
                <?php
                    echo $this->Form->input('company_overview',
                        array(
                            'class' => 'form-control',
                            'div' => false,
                            'label' => false,
                            'placeholder' => 'This is autopopulated from Setup',
                            'type' => 'textarea',
                            'value' => $this->request->session()->read('Auth.User.company.overview'),
                            'readonly'
                        )
                    );
                ?>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group">
                <label>Responsibilities <span class ="star">*</span></label>
                <?php
                    echo $this->Form->input('responsibility',
                        array(
                            'class' => 'form-control',
                            'div' => false,
                            'label' => false,
                            'required' => true,
                            'type' => 'textarea',
                            'row' => 3
                        )
                    );
                ?>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group">
                <label>Qualifications <span class ="star">*</span></label>
                <?php
                    echo $this->Form->input('qualification',
                        array(
                            'class' => 'form-control',
                            'div' => false,
                            'label' => false,
                            'required' => true,
                            'type' => 'textarea',
                            'row' => 3
                        )
                    );
                ?>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group">
                <label>Education and/or Experience <span class ="star">*</span></label>
                <?php
                    echo $this->Form->input('experience',
                        array(
                            'class' => 'form-control',
                            'div' => false,
                            'label' => false,
                            'required' => true,
                            'type' => 'textarea',
                            'row' => 3
                        )
                    );
                ?>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group">
                <label>Certificates/Licenses/Registrations <span class ="star">*</span></label>
                <?php
                    echo $this->Form->input('certification',
                        array(
                            'class' => 'form-control',
                            'div' => false,
                            'label' => false,
                            'required' => true,
                            'type' => 'textarea',
                            'row' => 3
                        )
                    );
                ?>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group">
                <label>Language Skills <span class ="star">*</span></label>
                <?php
                    echo $this->Form->input('language_skills',
                        array(
                            'class' => 'form-control',
                            'div' => false,
                            'label' => false,
                            'required' => true,
                            'type' => 'textarea',
                            'row' => 3
                        )
                    );
                ?>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group">
                <label>Other Qualifications <span class ="star">*</span></label>
                <?php
                    echo $this->Form->input('other_qualification',
                        array(
                            'class' => 'form-control',
                            'div' => false,
                            'label' => false,
                            'required' => true,
                            'type' => 'textarea',
                            'row' => 3
                        )
                    );
                ?>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group">
                <label>Physical Demands <span class ="star">*</span></label>
                <?php
                    echo $this->Form->input('physical_demands',
                        array(
                            'class' => 'form-control',
                            'div' => false,
                            'label' => false,
                            'required' => true,
                            'type' => 'textarea',
                            'row' => 3
                        )
                    );
                ?>
            </div>
        </div>
        <div class="col-sm-12 form-group">
            <div class="row">
              
                    <label class="col-sm-12">Salary & Benefits <span class ="star">*</span></label>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="input-group">
                              <div class="input-group-addon">$</div>
                             <?php
                                echo $this->Form->input('salary_from',
                                    array(
                                        'class' => 'form-control',
                                        'div' => false,
                                        'label' => false,
                                        'required' => true,
                                        'placeholder' => 'from'
                                    )
                                );
                            ?>
                            </div>
                        </div> 
                    </div> 
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">$</div>
                                <?php
                                    echo $this->Form->input('salary_to',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                            'required' => true,
                                            'placeholder' => 'to'
                                        )
                                    );
                                ?>
                            </div>
                    </div> 
                </div>
            </div>
        </div>
        <div class="col-sm-12 form-group">
            <div class="checkbox">
                <label class="form-group">
                    <input name="candidate" type="checkbox"> Send to all candidates within 
                    <?php
                        echo $this->Form->input('miles',
                            array(
                                'templates' => [
                                       'inputContainer' => '{{content}}',
                                       'inputContainerError' => '{{content}}{{error}}'
                                   ],
                                'label' => false,
                                'empty' => 'select miles',
                                'options' => ['5' => '5','15'=>'15','25' => '25','50'=> '50','100' => '100']
                            )
                        );
                    ?>
                    miles of job site
                </label>
            </div>
        </div>
        <div class="col-sm-12">
            <?php
                echo $this->Form->button(__('Submit'),[
                    'class' => 'btn  btn-blue',
                    'type' => 'submit'
                ]);
            ?>
        </div>
    </div>
</div>
<?php 
    echo $this->Html->script([
            'User/job'
        ], [
            'block' => 'scriptBottom'
        ]); 
?>