<?php
/**
* CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
* Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
*
* Licensed under The MIT License
* For full copyright and license information, please see the LICENSE.txt
* Redistributions of files must retain the above copyright notice.
*
* @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
* @link          http://cakephp.org CakePHP(tm) Project
* @since         0.10.0
* @license       http://www.opensource.org/licenses/mit-license.php MIT License
*/
$cakeDescription = __('Security Officer');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?= $this->Html->charset() ?>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>
            <?= $cakeDescription ?>:
            <?= $this->fetch('title') ?>
        </title>
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700" rel="stylesheet">
        <?= $this->Html->meta('icon') ?>
        <!-- Latest compiled and minified CSS -->
        <?= $this->Html->css('bootstrap.min') ?>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <?= $this->Html->css('backend/stylesheet'); ?>
        <?= $this->Html->css('backend/style') ?>
        <!-- <?= $this->Html->css('vue-modal') ?>
        <?= $this->Html->css('star-rating.min') ?> -->
        <?= $this->Html->css('font-awesome.min') ?>
        <?= $this->Html->css('bootstrap-switch.min') ?>

        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>
    </head>
    <body>
        <div class="wrapper" id="vue-container">
            <?= $this->Flash->render();?>
            <?php
                echo $this->element('Admin/header');
                echo $this->element('Admin/sidebar');
            ?>

            <div class="container-wrapper clearfix">
                <?php echo $this->fetch('content'); ?>
            </div>
            <footer>
            </footer>
            <?php echo $this->fetch('modals'); ?>
        </div>
        
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <?= $this->Html->script('jQuery-2.1.4.min') ?>
        <?= $this->Html->script('bootstrap-switch.min') ?>
        <?= $this->Html->script('jquery.validate') ?>

        <!-- Latest compiled and minified JavaScript -->
        <?= $this->Html->script('bootstrap.min') ?>
        <script type="text/javascript">
            window.url = '<?php echo  $this->Url->build('/',true); ?>';
        </script>
        <?= $this->fetch('script') ?>
        <?= $this->fetch('scriptBottom') ?>
        <?= $this->element('Modal/info')?>
        <?= $this->element('Modal/delete')?>
    </body>
</html>