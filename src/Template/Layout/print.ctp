<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Employee Application</title>
        <!-- Bootstrap -->
        <?php
            echo $this->Html->css([
                'bootstrap.min',
                'style',
                'font-awesome.min',
                'employee-application',
                'print'
            ]);
            echo $this->fetch('meta');
            echo $this->fetch('css');
        ?>
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i,800,800i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,600,600i" rel="stylesheet">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="js/html5shiv.min.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <?php echo $this->fetch('content'); ?>
        <?php
            echo $this->Html->script([
                'jQuery-2.1.4.min',
                'bootstrap.min'
            ]);
            echo $this->fetch('scriptBottom');
            echo $this->fetch('scriptInline');
        ?>
    </body>
</html>