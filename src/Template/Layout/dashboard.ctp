<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Dashboard</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.5 -->
        <?php
            echo $this->Html->css([
                'bootstrap.min',
                'style',
                'font-awesome.min',
                'AdminLTE',
                'skin-blue',
                'sticky-footer-navbar',
                'bootstrap-datetimepicker.min',
                'multiselect'
            ]);
            echo $this->fetch('meta');
            echo $this->fetch('css');
        ?>
        <!-- Font Awesome -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i,800,800i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,600,600i" rel="stylesheet">
        <!-- iCheck -->
        <!-- Morris chart -->
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script type="text/javascript">
            var base_url = "<?= $this->Url->build('/', true); ?>";
            // console.log(base_url);
        </script>
    </head>
    <body class="hold-transition skin-blue sidebar-mini ">
        <div class="wrapper">
            <?php echo $this->element('Dashboard/header');?>
            <?php echo $this->element('Dashboard/sidebar');?>
            <?php echo $this->fetch('content'); ?>
            <?php //echo $this->element('Dashboard/footer');?>
        </div>
         <?php
            echo $this->Html->script([
                'jQuery-2.1.4.min',
                'jquery.validate',
                'additional-methods',
                'custom',
                'bootstrap.min',
                'app',
                'jquery.maskedinput',
                'moment.min',
                'bootstrap-datetimepicker',
                'bootstrap-multiselect',
                'loadimg',
                'underscore-min',
                'loadimg'

            ]);
            echo $this->fetch('scriptBottom');
            echo $this->fetch('scriptInline');
        ?>
        <script type="text/javascript">
            $(window).ready(function(){
                var equalto =$("body").innerHeight();
                $(".main-sidebar").innerHeight(equalto);
            });
        </script>
    </body>
</html>