<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Home Page</title>
        <!-- Bootstrap -->
        <?php
            echo $this->Html->css([
                'bootstrap.min',
                'apllication-home',
                'sticky-footer-navbar',
                'font-awesome.min',
                'loadimg.css',
                'passwordscheck.css',
                'bootstrap-datetimepicker.min',
                'multiselect'
            ]);
            echo $this->fetch('meta');
            echo $this->fetch('css');
        ?>
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i,800,800i" rel="stylesheet">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="js/html5shiv.min.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->
        <script type="text/javascript">
            var base_url = "<?= $this->Url->build('/', true); ?>";
            // console.log(base_url);
        </script>
    </head>  
    <body>
        <?php echo $this->element('Home/header');?>
        <?php echo $this->fetch('content'); ?>
        <?php echo $this->element('Home/footer');?>
        <?php
            echo $this->Html->script([
                'jQuery-2.1.4.min',
                'bootstrap.min.js',
                'jquery.validate',
                'additional-methods',
                'loadimg',
                'jquery.maskedinput',
                'passwordscheck.js',
                'bootstrap-multiselect',
                'moment.min',
                'bootstrap-datetimepicker',
                'underscore-min',
                'User/layput'
            ]);
            echo $this->fetch('scriptBottom');
            echo $this->fetch('scriptInline');
        ?>
        <style type="text/css">
            .loader1 {
                border: 16px solid #f3f3f3; /* Light grey */
                border-top: 16px solid #3498db; /* Blue */
                border-radius: 50%;
                width: 120px;
                height: 120px;
                animation: spin 1s linear infinite;
                position:fixed;
                top: 35%;left: 50%;
                transform: translate(-50%, -50%);
                color:#454544;
            }
            @keyframes spin {
                0% { transform: rotate(0deg); }
                100% { transform: rotate(360deg); }
            }
        </style>
    </body>

</html>