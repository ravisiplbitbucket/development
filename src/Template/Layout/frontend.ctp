<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Login</title>
        <!-- Bootstrap -->
        <?php
            echo $this->Html->css([
                'bootstrap.min.css',
                'login.css',
                'sticky-footer-navbar.css',
                'loadimg.css',
                'passwordscheck.css',
                'bootstrap-datetimepicker.min',
                'font-awesome.min',
                'multiselect'

            ]);
            echo $this->fetch('meta');
            echo $this->fetch('css');
        ?>
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i,800,800i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,600,600i" rel="stylesheet">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="js/html5shiv.min.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->
        <script type="text/javascript">
            var base_url = "<?= $this->Url->build('/', true); ?>";
            // console.log(base_url);
        </script>
    </head>
    <body>
        <div class="modal-backdrop fade in loader" style="display:none;">
            <div class="loader1"></div>
        </div>
        <?php echo $this->element('Frontend/header');?>
        <?php echo $this->fetch('content'); ?>
        <?php echo $this->element('Frontend/footer');?>
        <?php
            echo $this->Html->script([
                'jQuery-2.1.4.min',
                'jquery.validate',
                'additional-methods',
                'loadimg',
                'bootstrap.min',
                'jquery.maskedinput',
                'passwordscheck.js',
                'bootstrap-multiselect',
                'moment.min',
                'bootstrap-datetimepicker',
                'underscore-min'
            ]);
            echo $this->fetch('scriptBottom');
            echo $this->fetch('scriptInline');
        ?>
        <script>
            $(document).ready(function () {
                //loadimg
            
                $('#upload').loadImg({
                    "text"  : "+",
                    "fileExt" : ["png","jpg"],
                   
                });         
            
                //Wizard
                $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
                
                    var $target = $(e.target);
                    
                    if ($target.parent().hasClass('disabled')) {
                        return false;
                    }
                });
                //Add Inactive Class To All Accordion Headers
                $('.accordion-header').toggleClass('inactive-header');
                
                //Set The Accordion Content Width
                var contentwidth = $('.accordion-header').width();
                $('.accordion-content').css({});
                
                //Open The First Accordion Section When Page Loads
                $('.accordion-header').first().toggleClass('active-header').toggleClass('inactive-header');
                $('.accordion-content').first().slideDown().toggleClass('open-content');
                
                // The Accordion Effect
                $('.accordion-header').click(function () {
                    if($(this).is('.inactive-header')) {
                        $('.active-header').toggleClass('active-header').toggleClass('inactive-header').next().slideToggle().toggleClass('open-content');
                        $(this).toggleClass('active-header').toggleClass('inactive-header');
                        $(this).next().slideToggle().toggleClass('open-content');
                    }
                    
                    else {
                        $(this).toggleClass('active-header').toggleClass('inactive-header');
                        $(this).next().slideToggle().toggleClass('open-content');
                    }
                });
            });

            // Employee Applicaion

            $(document).on('click','.worked-for-company', function(){
                if($(this).val()=="1"){
                    $('.workedfor-company').removeClass('hide');
                }
                else{
                     $('.workedfor-company').addClass('hide');
                }
            })
            
            $(document).on('click', '.convicted-of-crime',function(){
                if($(this).val()=="1"){
                    $('.crime').removeClass('hide');
                }
                else{
                     $('.crime').addClass('hide');
                }
            })
            
             $(document).on('click','.licence', function(){
                if($(this).val()=="1"){
                    $('.valid-licence').removeClass('hide');
                }
                else{
                     $('.valid-licence').addClass('hide');
                }
            })
            
              $(document).on('click','.automobile', function(){
                if($(this).val()=="1"){
                    $('.dependable-automobile').removeClass('hide');
                }
                else{
                     $('.dependable-automobile').addClass('hide');
                }
            })
            
            $(document).on('click', '.guard',function(){
                if($(this).val()=="1"){
                    $('.security-guard').removeClass('hide');
                }
                else{
                     $('.security-guard').addClass('hide');
                }
            })
            
            $(document).on('click', '.patrols',function(){
                if($(this).val()=="1"){
                    $('.foot-patrols').removeClass('hide');
                }
                else{
                     $('.foot-patrols').addClass('hide');
                }
            })
            
             $(document).on('click','.reports', function(){
                if($(this).val()=="1"){
                    $('.submit-reports').removeClass('hide');
                }
                else{
                     $('.submit-reports').addClass('hide');
                }
            })
            
               
            $(document).on('click','.vehicle', function(){
                if($(this).val()=="1"){
                    $('.vechicle-required').removeClass('hide');
                }
                else{
                     $('.vechicle-required').addClass('hide');
                }
            })
            
            $(document).on('click', '.equipment', function(){
                if($(this).val()=="1"){
                    $('.equipment-section').removeClass('hide');
                }
                else{
                     $('.equipment-section').addClass('hide');
                }
            })
            
            $(document).on('click', '.emergency-services', function(){
                if($(this).val()=="1"){
                    $('.emergency-services-section').removeClass('hide');
                }
                else{
                     $('.emergency-services-section').addClass('hide');
                }
            })
            
            $(document).on('click','.injured', function(){
                if($(this).val()=="1"){
                    $('.injured-section').removeClass('hide');
                }
                else{
                     $('.injured-section').addClass('hide');
                }
            })
            $(document).on('click','.accident-involved', function(){
                if($(this).val()=="1"){
                    $('.accident-involved-section').removeClass('hide');
                }
                else{
                     $('.accident-involved-section').addClass('hide');
                }
            })
            
            $(document).on('click','.certified-licensed', function(){
                if($(this).val()=="yes"){
                    $('.certified-licensed-section').removeClass('hide');
                }
                else{
                     $('.certified-licensed-section').addClass('hide');
                }
            })
            
            $(document).on('click','.certified-licensed-armed', function(){
                if($(this).val()=="yes"){
                    $('.certified-licensed-armed-section').removeClass('hide');
                }
                else{
                     $('.certified-licensed-armed-section').addClass('hide');
                }
            })
            
            $(document).on('click','.cpr-aed-certified', function(){
                if($(this).val()=="1"){
                    $('.cpr-aed-certified-section').removeClass('hide');
                }
                else{
                    $('.cpr-aed-certified-section').addClass('hide');
                }
            })
            
            $(document).on('click', '.twic-certified',function(){
                if($(this).val()=="1"){
                    $('.twic-certified-section').removeClass('hide');
                }
                else{
                    $('.twic-certified-section').addClass('hide');
                }
            })
            
            
            $(function() {
                $('.multiselect-ui').multiselect({
                     includeSelectAllOption: true
                });
            
                $('.report-submit').multiselect({
                 includeSelectAllOption: true
                });

                $('.languages-speak').multiselect({
                 includeSelectAllOption: true
                });

            });
            function hideLoader() {
                $(document).find('.loader').hide();
            }
            function showLoader() {
                $(document).find('.loader').show();
            }
        </script>
        <script type="text/javascript">
        $(function () {
            $('#datetimepicker11').datetimepicker({
                  viewMode: 'years',
                   format: 'MM/YYYY'
            });
             $('#datetimepicker21').datetimepicker({
                  viewMode: 'years',
                   format: 'MM/YYYY'
            });

            $('#datetimepicker12').datetimepicker({
                  viewMode: 'years',
                   format: 'MM/YYYY'
            });
            $('#datetimepicker22').datetimepicker({
                  viewMode: 'years',
                   format: 'MM/YYYY'
            });
             $('#datetimepicker13').datetimepicker({
                  viewMode: 'years',
                   format: 'MM/YYYY'
            });
             $('#datetimepicker23').datetimepicker({
                  viewMode: 'years',
                   format: 'MM/YYYY'
            });
             $('#datetimepicker14').datetimepicker({
                  viewMode: 'years',
                   format: 'MM/YYYY'
            });
             $('#datetimepicker24').datetimepicker({
                  viewMode: 'years',
                   format: 'MM/YYYY'
            });
               $('#datetimepicker15').datetimepicker({
                  viewMode: 'years',
                   format: 'MM/YYYY'
            });

              $('#datetimepicker25').datetimepicker({
                   format: 'MM/YYYY'
            });
               $('#datetimepicker16').datetimepicker({
                   format: 'MM/YYYY'
            });
               $('#datetimepicker17').datetimepicker({
                   format: 'MM/YYYY'
            });
            $('#datetimepicker18').datetimepicker({
                viewMode: 'years',
                format: 'DD/MM/YYYY'
            });
            $('#armed-expiry-date').datetimepicker({
                viewMode: 'years',
                format: 'DD/MM/YYYY'
            });
            $('#cpr-expiry-date').datetimepicker({
                viewMode: 'years',
                format: 'DD/MM/YYYY'
            });
            $('#twic-exiry-date').datetimepicker({
                viewMode: 'years',
                format: 'DD/MM/YYYY'
            });
            $('#datetimepicker1').datetimepicker({
                  viewMode: 'years',
                   format: 'MM/YYYY'
            });
            $('#datetimepicker2').datetimepicker({
                  viewMode: 'years',
                   format: 'MM/YYYY'
            });
            $('#date_started').datetimepicker({
                   format: 'DD/MM/YYYY'
            });
            $('#date_ended').datetimepicker({
                   format: 'DD/MM/YYYY'
            });
        });
    </script>
        <style type="text/css">
            .loader1 {
                border: 16px solid #f3f3f3; /* Light grey */
                border-top: 16px solid #3498db; /* Blue */
                border-radius: 50%;
                width: 120px;
                height: 120px;
                animation: spin 1s linear infinite;
                position:fixed;
                top: 35%;left: 50%;
                transform: translate(-50%, -50%);
                color:#454544;
            }
            @keyframes spin {
                0% { transform: rotate(0deg); }
                100% { transform: rotate(360deg); }
            }
        </style>
    </body>
</html>