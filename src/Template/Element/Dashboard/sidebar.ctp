<aside class="main-sidebar">
    <div class="sidebar">
        <ul class="sidebar-menu">
            <?php if(!in_array($this->request->session()->read('Auth.User.user_role_id'),[4,5])) { ?>
            <li class="treeview active">
                <?php echo $this->Html->link(
                    '<i class="fa fa-user-plus" aria-hidden="true"></i><span>Candidates</span>',
                    '/Candidates/index',
                    array(
                        'escape' => false
                        )
                );?>
            </li>
            <?php } ?>
            <li class="treeview">
                <a href="#">
                <i class="fa fa-users" aria-hidden="true"></i>
                <span>Employess</span>
                <i class="fa fa-angle-right pull-right"></i>
                </a>
                <ul class="treeview-menu ">
                    <li>                        
                        <?php
                            echo $this->Html->link(__('Roster'), [
                                'controller' => 'employees',
                                'action' => 'index'
                            ]);
                        ?>
                    </li>
                    <?php if(!in_array($this->request->session()->read('Auth.User.user_role_id'),[4,5])) { ?>
                    <li>
                        <?php
                            echo $this->Html->link(__('Expiration'), [
                                'controller' => 'employees',
                                'action' => 'expiration'
                            ]);
                        ?>
                    </li>
                    <?php } ?>
                </ul>
            </li>
            <?php if(!in_array($this->request->session()->read('Auth.User.user_role_id'),[4,5])) { ?>
            <li class="treeview">
                <a href="#">
                <i class="fa fa-plus" aria-hidden="true"></i>
                <span>Post Job</span>
                <i class="fa fa-angle-right pull-right"></i>
                </a>
                <ul class="treeview-menu ">
                    <li>
                        <?php echo $this->Html->link('Jobs',
                            '/Jobs/index'
                        );?>
                    </li>
                    <li>
                        <?php echo $this->Html->link('Add New Job',
                            '/Jobs/add'
                        );?>
                    </li>
                </ul>
            </li>
            <?php } ?>
        </ul>
    </div>
</aside>