<div class="col-xs-12 clearfix">
    <?php
        echo $this->Form->create($user,
                array(
                        'type' => 'file',
                        'id' => 'organization-profile'
                        
                    )
            );
    ?>
        <div class="col-sm-12">
            <div class="form-group">
                <label>Username</label>
                <?php
                    echo $this->Form->input('username',
                        array(
                            'class' => 'form-control',
                            'div' => false,
                            'label' => false,
                            'required' => true,
                            'readonly'
                        )
                    );
                ?>
            </div>
        </div>  
        <div class="col-sm-12">
            <div class="form-group">
                <label>First Name</label>
                <?php
                    echo $this->Form->input('company.first_name',
                        array(
                            'class' => 'form-control',
                            'div' => false,
                            'label' => false,
                            'required' => true
                        )
                    );
                ?>
            </div>
        </div> 
        <div class="col-sm-12">
            <div class="form-group">
                <label>Last Name</label>
                <?php
                    echo $this->Form->input('company.last_name',
                        array(
                            'class' => 'form-control',
                            'div' => false,
                            'label' => false,
                            'required' => true
                        )
                    );
                ?>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group">
                <label>Phone No</label>
                <div class="input-group">
                    <span class="input-group-addon country-phone-code1" id="basic-addon1"><?php echo $user['company']->country_phone_code;?></span>
                    <?php echo $this->Form->control('company.telephone',array('class' => 'form-control','placeholder' => 'phone no','aria-describedby'=>"basic-addon1",'label' => false,'id' => 'register-phone'));?>
                </div>
            </div>
        </div> 
        <div class="col-sm-12">
            <div class="form-group">
                <label>Mobile Number</label>
                <div class="input-group">
                <span class="input-group-addon country-phone-code1" id="basic-addon1"><?php echo $user['company']->country_phone_code;?></span>
                <?php
                    echo $this->Form->input('company.mobile_number',
                        array(
                            'class' => 'form-control',
                            'div' => false,
                            'label' => false,
                            'required' => true
                        )
                    );
                ?>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group">
                <label>Email</label>
                <?php
                    echo $this->Form->input('email',
                        array(
                            'class' => 'form-control',
                            'div' => false,
                            'label' => false,
                            'required' => true
                        )
                    );
                ?>
            </div>
        </div> 
        <button type="submit" class="btn btn-blue next-step">Submit</button>    
    </form>
</div>
                   