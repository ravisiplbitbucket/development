<header class="main-header">
    <!-- Logo -->
    <?php echo $this->html->link(
        '<span class="logo-mini">'.$this->Html->image('mini-logo.png').'</span>
        <span class="logo-lg">'.$this->Html->image('logo.png').'</span>',
        '/Users/index',
        array(
            'escape' =>false,
            'class' => 'logo'
            )
    );?>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top nav-custom" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="glyphicon glyphicon-menu-hamburger"></span>
        </a>
        <!-- <div class="col-xs-4  search-top hidden-xs">
            Post job
            </div> -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="">
                        <?php echo $this->request->session()->read('Auth.User.username');?>
                        </span>
                        <!--  <img src="img/profile-logo.png" class="user-image" alt="User Image"> -->
                        <i class="fa fa-angle-down" aria-hidden="true"></i>
                    </a>
                    <ul aria-labelledby="drop3" class="dropdown-menu">
                        <li>
                            <?= $this->Html->Link("My Profile",
                                   'javascript:void(0)',
                                   [
                                       'escape'   => false,
                                       'class'    => 'edit-profile',
                                       'data-url' =>   $this->Url->build(
                                                   [  
                                                       "controller" => "Users",
                                                       "action" => "profile",
                                                       base64_encode($this->request->session()->read('Auth.User.id'))
                                                   ],true
                                               )
                                                            
                                   ]
                               )
                            ?> 
                        </li>
                        <?php if(!in_array($this->request->session()->read('Auth.User.user_role_id'),[4,5])){ ?>
                        <li>
                            <?php echo $this->Html->link('Setup',
                                '/Users/index'
                            );?>
                        </li>
                        <?php } ?>
                        <li>
                            <?php echo $this->Html->link('Logout',
                                '/Users/logout'
                            );
                            ?>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
<div class="modal Educational-info fade" id="profile" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><?php echo __('Edit Profile'); ?></h4>
            </div>
            <div class="modal-body clearfix">
            </div>
        </div>
    </div>
</div>
<?php echo $this->Html->script('User/profile', ['block' => 'scriptBottom']); ?>