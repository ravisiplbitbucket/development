<div class="modal Educational-info fade" id="addEmployeeEquipment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><?php echo __('Add Uniform And Equipment'); ?></h4>
            </div>
            <div class="modal-body clearfix">
                <div class="col-xs-12 clearfix">
                    <?= $this->Form->create(null,array('url' => array(
                            'controller' => 'Employees',
                            'action' => 'addEquipment'),
                            'type' => 'post',
                            'id' => 'add-emp-equipment')
                        ); 
                    ?>
                    <?php  $id = $this->request->getParam('pass')[0];?>
                        <?php echo $this->Form->hidden('candidate_id',array('value' =>base64_decode($id)));?>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Item<span class ="star">*</span></label>
                                <?php
                                    echo $this->Form->input('equipment_id',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                            'required' => true,
                                            'options' => $equipmentList
                                        )
                                    );
                                ?>
                            </div>
                        </div> 
                        <div class="col-sm-12">
                            <div class="form-group description">
                            
                            </div>
                        </div>   
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Quantity<span class ="star">*</span></label>
                                <?php
                                    echo $this->Form->input('quantity',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                            'required' => true
                                        )
                                    );
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Condition<span class ="star">*</span></label>
                                <?php
                                    echo $this->Form->input('equipment_condition',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                            'required' => true
                                        )
                                    );
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-12 form-group">
                        <button type="submit" class="btn btn-blue next-step">Save</button>  
                        </div>  
                    <?php echo $this->Form->end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>       