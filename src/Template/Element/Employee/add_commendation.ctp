<div class="modal Educational-info fade" id="addcommendation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><?php echo __('Add Commendations'); ?></h4>
            </div>
            <div class="modal-body clearfix">
                <div class="col-xs-12 clearfix">
                     <?= $this->Form->create(null,array('url' => array(
                            'controller' => 'Employees',
                            'action' => 'addCommendation'),
                            'type' => 'post',
                            'id' => 'add-commendation')
                        ); 
                    ?>
                        <?php  $id = $this->request->getParam('pass')[0];?>
                        <?php echo $this->Form->hidden('employee_id',array('value' =>base64_decode($id)));?>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Title <span class ="star">*</span></label>
                                <?php
                                    echo $this->Form->input('title',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                            'required' => true,
                                            'type' => 'text'
                                        )
                                    );
                                ?>
                            </div>
                        </div>  
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Issuer <span class ="star">*</span></label>
                                <?php
                                    echo $this->Form->input('issuer',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                            'required' => true,
                                            'type' => 'text',
                                            'value' => $this->request->session()->read('Auth.User.company.first_name').' '.$this->request->session()->read('Auth.User.company.last_name'),
                                            'readonly'
                                        )
                                    );
                                ?>
                            </div>
                        </div> 
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Date of Commendation<span class ="star">*</span></label>
                                <?php
                                    echo $this->Form->input('commend_date',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                            'required' => true,
                                            'id' => 'date-commendation'
                                        )
                                    );
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Details<span class ="star">*</span></label>
                                <?php
                                    echo $this->Form->input('detail',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                            'required' => true,
                                            'type' => 'textarea'
                                        )
                                    );
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-12 form-group">
                        <button type="submit" class="btn btn-blue next-step">Save</button>  
                        </div>  
                    <?php echo $this->Form->end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>       