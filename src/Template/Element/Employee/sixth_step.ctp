<?= $this->Form->create($candidatecomplete,
        array(
            'type' => 'post',
            'url' => array(
                'controller' => 'Employees',
                'action' => 'Apply'
            ),
            'id' => 'candidate-apply-sixth-step'
        )
    ); 
?>
<div class="step6">
    <div class="certification-section">
        <div class="col-sm-12 subheading">
            <h3>Certification </h3>
        </div>
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Are  you certified/licensed/commissioned to work unarmed </label>
                        <div>
                            <label class="radio-inline">
                            <input class="certified-licensed" type="radio" value="yes" name="unarmed">Yes
                            </label>
                            <label class="radio-inline">
                            <input class="certified-licensed" type="radio" checked value ="no" name="unarmed">No
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row hide certified-licensed-section">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>What is your License number if applicable </label>
                        <?php
                            echo $this->Form->input('unarmed_licence_number',
                                array(
                                    'class' => 'form-control',
                                    'div' => false,
                                    'label' => false,
                                )
                            );
                        ?>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Expiration   date    if  applicable </label>
                        <?php
                            echo $this->Form->input('unarmed_expiry_date',
                                array(
                                    'type' => 'text',
                                    'class' => 'form-control',
                                    'div' => false,
                                    'label' => false,
                                    'id'    => "datetimepicker15"
                                )
                            );
                        ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Are  you certified/licensed/commissioned to work armed </label>
                        <div>
                            <label class="radio-inline">
                            <input class="certified-licensed-armed" type="radio" value="yes" name="armed">Yes
                            </label>
                            <label class="radio-inline">
                            <input class="certified-licensed-armed" type="radio" checked value ="no" name="armed">No
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row hide certified-licensed-armed-section">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>What is your License number if applicable </label>
                        <?php
                            echo $this->Form->input('armed_licence_number',
                                array(
                                    'class' => 'form-control',
                                    'div' => false,
                                    'label' => false,
                                )
                            );
                        ?>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Expiration date if applicable</label>
                        <?php
                            echo $this->Form->input('armed_expiry_date',
                                array(
                                    'type' => 'text',
                                    'class' => 'form-control',
                                    'div' => false,
                                    'label' => false,
                                    'id'    => "armed-expiry-date"
                                )
                            );
                        ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Are  you CPR/AED Certified</label>
                        <div>
                            <label class="radio-inline">
                            <input class="cpr-aed-certified" type="radio" value="1" name="is_cpr_certified">Yes
                            </label>
                            <label class="radio-inline">
                            <input class="cpr-aed-certified" type="radio" checked value ="0" name="is_cpr_certified">No
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row hide cpr-aed-certified-section">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Expiration date</label>
                        <?php
                            echo $this->Form->input('cpr_expiry_date',
                                array(
                                    'type' => 'text',
                                    'class' => 'form-control',
                                    'div' => false,
                                    'label' => false,
                                    'id'=>'cpr-expiry-date'
                                )
                            );
                        ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Are  you TWIC certified </label>
                        <div>
                            <label class="radio-inline">
                            <input class="twic-certified" type="radio" value="1" name="is_twic_certified">Yes
                            </label>
                            <label class="radio-inline">
                            <input class="twic-certified" type="radio" checked value ="0" name="is_twic_certified">No
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row hide twic-certified-section">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Expiration date</label>
                        <?php
                            echo $this->Form->input('twic_expiry_date',
                                array(
                                    'type' => 'text',
                                    'class' => 'form-control',
                                    'div' => false,
                                    'label' => false,
                                    'id'=>'twic-exiry-date'
                                )
                            );
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<ul class="list-inline col-sm-12">
    <li><button type="button" class="btn btn-cancel prev-step">Previous</button></li>
    <li><button type="submit" class="btn btn-blue next-step">Submit</button></li>
</ul>
<?= $this->Form->end()?>
<!-- Modal -->