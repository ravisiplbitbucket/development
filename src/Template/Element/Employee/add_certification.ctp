<div class="modal Educational-info fade" id="addEmployeeCertification" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><?php echo __('Add Certification'); ?></h4>
            </div>
            <div class="modal-body clearfix">
                <div class="col-xs-12 clearfix">
                    <?= $this->Form->create(null,array('url' => array(
                            'controller' => 'Employees',
                            'action' => 'addCertification'),
                            'type' => 'file',
                            'id' => 'add-emp-certificate')
                        ); 
                    ?>
                    <?php  $id = $this->request->getParam('pass')[0];?>
                        <?php echo $this->Form->hidden('candidate_id',array('value' =>base64_decode($id)));?>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Available Certification <span class ="star">*</span></label>
                                <?php
                                    echo $this->Form->input('certification_id',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                            'required' => true,
                                            'options' => $certificateList
                                        )
                                    );
                                ?>
                            </div>
                        </div>  
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Expiration Date<span class ="star">*</span></label>
                                <?php
                                    echo $this->Form->input('expiration_date',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                            'required' => true,
                                            'id' => 'date-expiration'
                                        )
                                    );
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Image</label>
                                <?php
                                    echo $this->Form->input('image',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                            'type' => 'file'
                                        )
                                    );
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-12 form-group">
                        <button type="submit" class="btn btn-blue next-step">Save</button>  
                        </div>  
                    <?php echo $this->Form->end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>       