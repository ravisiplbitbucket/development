<div class="modal Educational-info fade" id="adddiscip" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><?php echo __('Add Disciplinary'); ?></h4>
            </div>
            <div class="modal-body clearfix">
                <div class="col-xs-12 clearfix">
                    <?= $this->Form->create(null,array('url' => array(
                            'controller' => 'Employees',
                            'action' => 'addDisciplinary'),
                            'type' => 'file',
                            'id' => 'add-disciplinary')
                        ); 
                    ?>
                    <?php  $id = $this->request->getParam('pass')[0];?>
                        <?php echo $this->Form->hidden('employee_id',array('value' =>base64_decode($id)));?>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Title <span class ="star">*</span></label>
                                <?php
                                    echo $this->Form->input('title',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                            'required' => true,
                                            'type' => 'text'
                                        )
                                    );
                                ?>
                            </div>
                        </div>  
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Type <span class ="star">*</span></label>
                                <?php
                                    echo $this->Form->input('type_discip',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                            'required' => true,
                                            'type' => 'text'
                                        )
                                    );
                                ?>
                            </div>
                        </div> 
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Details<span class ="star">*</span></label>
                                <?php
                                    echo $this->Form->input('result',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                            'required' => true,
                                            'type' => 'textarea'
                                        )
                                    );
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Date of Disciplinary<span class ="star">*</span></label>
                                <?php
                                    echo $this->Form->input('discip_date',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                            'required' => true,
                                            'id' => 'date-discip'
                                        )
                                    );
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Image<span class ="star">*</span></label>
                                <?php
                                    echo $this->Form->input('image',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                            'required' => true,
                                            'type' => 'file'
                                        )
                                    );
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-12 form-group">
                        <button type="submit" class="btn btn-blue next-step">Save</button>  
                        </div>  
                    <?php echo $this->Form->end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>       