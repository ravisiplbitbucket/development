<?= $this->Form->create($candidate,
        array(
            'type' => 'post',
            'url' => array(
                'controller' => 'Employees',
                'action' => 'set-session'
            ),
            'id' => 'candidate-apply-third-step'
        )
    ); 
echo $this->Form->hidden('step',['value' => 4]);
?>
<div class="step3">
    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-12 subheading">
                <h3>Employment History</h3>
                <!-- <h3>Employment History  <a data-toggle="modal" data-target="#employment-history" href="#"><i class="fa fa-plus" id='historyModalIcon' aria-hidden="true"></i></a></h3> -->
                <?php echo $this->Html->link(
                    'Add Employer',
                    '#',
                    array(
                        'class' => 'btn btn-primary',
                        'data-target' => '#employment-history',
                        'data-toggle'=> "modal",
                        'id' => 'historyModalIcon'
                        )
                );?>
            </div>
        </div>
    </div>
    <div id='history-list'>        
        <?php
            if(!empty($candidate['candidate_histories'])) {
                foreach ($candidate['candidate_histories'] as $key => $value) {
        ?>
                <div class="col-sm-12 form-group">                
                    <?= $this->Html->link(
                            __('<h4>'.$value['company_name'].'</h4>'),
                            'javascript:void(0)',
                            [
                                'escape' => false,
                                'class'     => 'get-history-detail',
                                'data-url'  => $this->Url->build(['controller' => 'jobs', 'action' => 'get-history', base64_encode($value['id'])])
                            ]
                        ) 
                    ?>
                </div>
        <?php           
                }
            }
        ?>
    </div>
    <div class="certification-section">
        <div class="col-sm-12 subheading">
            <h3>Certification </h3>
        </div>
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Are  you certified/licensed/commissioned to work unarmed </label>
                        <div>
                            <label class="radio-inline">
                            <input class="certified-licensed" type="radio" value="yes" name="unarmed">Yes
                            </label>
                            <label class="radio-inline">
                            <input class="certified-licensed" type="radio" checked value ="no" name="unarmed">No
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row hide certified-licensed-section">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>What is your License number if applicable </label>
                        <?php
                            echo $this->Form->input('candidate_completes.0.unarmed_licence_number',
                                array(
                                    'class' => 'form-control',
                                    'div' => false,
                                    'label' => false,
                                )
                            );
                        ?>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Expiration   date    if  applicable </label>
                        <?php
                            echo $this->Form->input('candidate_completes.0.unarmed_expiry_date',
                                array(
                                    'type' => 'text',
                                    'class' => 'form-control',
                                    'div' => false,
                                    'label' => false,
                                    'id'    => "datetimepicker15"
                                )
                            );
                        ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Are  you certified/licensed/commissioned to work armed </label>
                        <div>
                            <label class="radio-inline">
                            <input class="certified-licensed-armed" type="radio" value="yes" name="armed">Yes
                            </label>
                            <label class="radio-inline">
                            <input class="certified-licensed-armed" type="radio" checked value ="no" name="armed">No
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row hide certified-licensed-armed-section">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>What is your License number if applicable </label>
                        <?php
                            echo $this->Form->input('candidate_completes.0.armed_licence_number',
                                array(
                                    'class' => 'form-control',
                                    'div' => false,
                                    'label' => false,
                                )
                            );
                        ?>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Expiration date if applicable</label>
                        <?php
                            echo $this->Form->input('candidate_completes.0.armed_expiry_date',
                                array(
                                    'type' => 'text',
                                    'class' => 'form-control',
                                    'div' => false,
                                    'label' => false,
                                    'id'    => "armed-expiry-date"
                                )
                            );
                        ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Are  you CPR/AED Certified</label>
                        <div>
                            <label class="radio-inline">
                            <input class="cpr-aed-certified" type="radio" value="1" name="candidate_completes[0][is_cpr_certified]">Yes
                            </label>
                            <label class="radio-inline">
                            <input class="cpr-aed-certified" type="radio" checked value ="0" name="candidate_completes[0][is_cpr_certified]">No
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row hide cpr-aed-certified-section">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Expiration date</label>
                        <?php
                            echo $this->Form->input('candidate_completes.0.cpr_expiry_date',
                                array(
                                    'type' => 'text',
                                    'class' => 'form-control',
                                    'div' => false,
                                    'label' => false,
                                    'id'=>'cpr-expiry-date'
                                )
                            );
                        ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Are  you TWIC certified </label>
                        <div>
                            <label class="radio-inline">
                            <input class="twic-certified" type="radio" value="1" name="candidate_completes[0][is_twic_certified]">Yes
                            </label>
                            <label class="radio-inline">
                            <input class="twic-certified" type="radio" checked value ="0" name="candidate_completes[0][is_twic_certified]">No
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row hide twic-certified-section">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Expiration date</label>
                        <?php
                            echo $this->Form->input('candidate_completes.0.twic_expiry_date',
                                array(
                                    'type' => 'text',
                                    'class' => 'form-control',
                                    'div' => false,
                                    'label' => false,
                                    'id'=>'twic-exiry-date'
                                )
                            );
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<ul class="list-inline col-sm-12">
    <li><button type="button" class="btn btn-cancel prev-step">Previous</button></li>
    <li><button type="submit" class="btn btn-blue next-step">Next</button></li>
</ul>
<?= $this->Form->end()?>
<?php echo $this->element('Employee/add_history');?>
<div class="modal fade" id="history-detail-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>                                   </button>
                <h4 class="modal-title" id="myModalLabel">History Detail</h4>
            </div>
            <div class="modal-body clearfix">
                
                
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn  btn-cancel" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-blue">Save</button>
            </div>
        </div>
    </div>
</div>
