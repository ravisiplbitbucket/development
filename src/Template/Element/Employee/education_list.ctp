_l<div class="col-sm-12 form-group">     
    <h4>
        <?= $educations->education_name?>
    </h4>           
    <?= $this->Html->link(
            __('<i class="fa fa-trash"></i>'),
            'javascript:void(0)',
            [
                'escape' => false,
                'class'     => 'delete-education-link',
                'data-url'  => $this->Url->build(['controller' => 'Employees', 'action' => 'delete-education', base64_encode($educationIdInSession)])
            ]
        ) 
    ?>
</div>