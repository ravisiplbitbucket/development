<div class="modal Educational-info fade" id="addEquipment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><?php echo __('Add Uniform and Equipment'); ?></h4>
            </div>
            <div class="modal-body clearfix">
                <div class="col-xs-12 clearfix">
                    <?= $this->Form->create(null,array('url' => array(
                            'controller' => 'Users',
                            'action' => 'equipment'
                            ),
                            'id' => 'equipment'
                            )
                        ); 
                    ?>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Item <span class ="star">*</span></label>
                                <?php
                                    echo $this->Form->input('item',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                            'required' => true
                                        )
                                    );
                                ?>
                            </div>
                        </div>  
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Description <span class ="star">*</span></label>
                                <?php
                                    echo $this->Form->input('description',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                            'required' => true,
                                            'type' => 'textarea'
                                        )
                                    );
                                ?>
                            </div>
                        </div> 
                        <div class="col-sm-12 form-group">
                        <button type="submit" class="btn btn-blue next-step">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>       