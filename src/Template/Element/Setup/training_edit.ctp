<?= $this->Form->create($training,array(
        'id' => 'edittraining',
        'type' => 'post'
        )
    ); 
?>
    <div class="col-sm-12">
        <div class="form-group">
            <label>Name <span class ="star">*</span></label>
            <?php
                echo $this->Form->input('name',
                    array(
                        'class' => 'form-control',
                        'div' => false,
                        'label' => false,
                        'required' => true
                    )
                );
            ?>
        </div>
    </div>  
    <div class="col-sm-12">
        <div class="form-group">
            <label>Description <span class ="star">*</span></label>
            <?php
                echo $this->Form->input('description',
                    array(
                        'class' => 'form-control',
                        'div' => false,
                        'label' => false,
                        'required' => true,
                        'type' => 'textarea'
                    )
                );
            ?>
        </div>
    </div> 
    <div class="col-sm-12">
        <div class="form-group">
            <label>Hours <span class ="star">*</span></label>
            <?php
                echo $this->Form->input('hours',
                    array(
                        'class' => 'form-control',
                        'div' => false,
                        'label' => false,
                        'required' => true
                    )
                );
            ?>
        </div>
    </div>
    <div class="col-sm-12 form-group">
    <button type="submit" class="btn btn-blue next-step">Click It</button>  
    </div>  
</form>