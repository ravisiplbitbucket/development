<div class="col-xs-12 clearfix">
    <?= $this->Form->create($user,array(
            'id' => 'edit-admin'
            )
        ); 
    ?>
        <div class="col-sm-12">
            <div class="form-group">
                <label>Username <span class ="star">*</span></label>
                <?php
                    echo $this->Form->input('username',
                        array(
                            'class' => 'form-control',
                            'div' => false,
                            'label' => false,
                            'required' => true
                        )
                    );
                ?>
            </div>
        </div>  
        <div class="col-sm-12">
            <div class="form-group">
                <label>Email <span class ="star">*</span></label>
                <?php
                    echo $this->Form->input('email',
                        array(
                            'class' => 'form-control',
                            'div' => false,
                            'label' => false,
                            'required' => true
                        )
                    );
                ?>
            </div>
        </div> 
        <div class="col-sm-6">
                <div class="form-group">
                    <label>First Name <span class ="star">*</span></label>
                    <?php echo $this->Form->control('user_profile.first_name',array('class' => 'form-control','placeholder' => 'First Name','label' => false));?>
                </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label>Last Name <span class ="star">*</span></label>
                <?php echo $this->Form->control('user_profile.last_name',array('class' => 'form-control','placeholder' => 'Last Name','label' => false));?>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group">
                <label>Select the type of admin <span class ="star">*</span></label>
                <?php 
                    if($this->request->session()->read('Auth.User.user_role_id') == 2) {
                        $roles = ['3' => 'Full Admin','4' => 'Uniform Admin','5' => 'Training Admin'];
                    } elseif ($this->request->session()->read('Auth.User.user_role_id') == 3) {
                       $roles = ['4' => 'Uniform Admin','5' => 'Training Admin'];
                    }
                ?>
                <?php echo $this->Form->control('user_role_id',array(
                        'class' => 'form-control',
                        'label' => false,
                        'options' => $roles
                    )
                );?>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group">
                <label>Division <span class ="star">*</span></label>
                <?php
                echo $this->Form->input('user_profile.division_id',
                    array(
                    'class' => 'form-control',
                    'div' => false,
                    'label' => false,
                    'required' => true,
                    'options' => $division
                    )
                );
                ?>
            </div>
        </div>
        <div class="col-sm-12 form-group">
        <button type="submit" class="btn btn-blue next-step">Submit</button>  
        </div>  
    </form>
</div>
