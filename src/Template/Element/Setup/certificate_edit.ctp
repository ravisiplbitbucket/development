<?= $this->Form->create($certificate,array(
        'id' => 'edit-certificate1',
        'type' => 'file'
        )
    ); 
    ?>
    <div class="col-sm-12">
        <div class="form-group">
            <label>Name <span class ="star">*</span></label>
            <?php
                echo $this->Form->input('name',
                    array(
                        'class' => 'form-control',
                        'div' => false,
                        'label' => false,
                        'required' => true
                    )
                );
            ?>
        </div>
    </div>  
    <div class="col-sm-12">
        <div class="form-group">
            <label>Description</label>
            <?php
                echo $this->Form->input('description',
                    array(
                        'class' => 'form-control',
                        'div' => false,
                        'label' => false,
                        'required' => true,
                        'type' => 'textarea'
                    )
                );
            ?>
        </div>
    </div> 
    <div class="col-sm-12 form-group">
    <button type="submit" class="btn btn-blue next-step">Submit</button>    
    </div>
 </form>