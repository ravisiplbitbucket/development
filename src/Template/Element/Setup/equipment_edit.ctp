<?= $this->Form->create($equipment,array(
        'id' => 'edit-equipment1'
        )
    ); 
?>
    <div class="col-sm-12">
        <div class="form-group">
            <label>Item <span class ="star">*</span></label>
            <?php
                echo $this->Form->input('item',
                    array(
                        'class' => 'form-control',
                        'div' => false,
                        'label' => false,
                        'required' => true
                    )
                );
            ?>
        </div>
    </div>  
    <div class="col-sm-12">
        <div class="form-group">
            <label>Description <span class ="star">*</span></label>
            <?php
                echo $this->Form->input('description',
                    array(
                        'class' => 'form-control',
                        'div' => false,
                        'label' => false,
                        'required' => true,
                        'type' => 'textarea'
                    )
                );
            ?>
        </div>
    </div> 
    <div class="col-sm-12 form-group">
    <button type="submit" class="btn btn-blue next-step">Submit</button>
    </div>
</form>