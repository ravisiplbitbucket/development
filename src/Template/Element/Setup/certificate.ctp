<div class="modal Educational-info fade" id="addCertificate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1"><?php echo __('Add Certification'); ?></h4>
            </div>
            <div class="modal-body clearfix">
                <div class="col-xs-12 clearfix">
                    <?= $this->Form->create(null,array('url' => array(
                            'controller' => 'Users',
                            'action' => 'certificate'
                            ),
                            'id' => 'certificate',
                            'type' => 'file'
                            )
                        ); 
                    ?>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Name <span class ="star">*</span></label>
                                <?php
                                    echo $this->Form->input('name',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                            'required' => true
                                        )
                                    );
                                ?>
                            </div>
                        </div>  
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Description</label>
                                <?php
                                    echo $this->Form->input('description',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                            'required' => true,
                                            'type' => 'textarea'
                                        )
                                    );
                                ?>
                            </div>
                        </div> 
                        <div class="col-sm-12 form-group">
                        <button type="submit" class="btn btn-blue next-step">Submit</button>    
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>       