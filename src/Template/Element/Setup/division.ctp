<div class="modal Educational-info fade" id="adddivison" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><?php echo __('Divisions'); ?></h4>
            </div>
            <div class="modal-body clearfix">
                <div class="col-xs-12 clearfix">
                    <?= $this->Form->create(null,array('url' => array(
                            'controller' => 'Users',
                            'action' => 'division'
                            ),
                            'id' => 'division '
                            )
                        ); 
                    ?>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Division <span class ="star">*</span></label>
                                <?php
                                    echo $this->Form->input('name',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                            'required' => true
                                        )
                                    );
                                ?>
                            </div>
                        </div> 
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Address1 <span class ="star">*</span></label>
                                <?php
                                    echo $this->Form->input('address1',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                            'required' => true
                                        )
                                    );
                                ?>
                            </div>
                        </div>  
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Address2</label>
                                <?php
                                    echo $this->Form->input('address2',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false
                                        )
                                    );
                                ?>
                            </div>
                        </div>  
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>City <span class ="star">*</span></label>
                                <?php
                                    echo $this->Form->input('city',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                            'required' => true
                                        )
                                    );
                                ?>
                            </div>
                        </div> 
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>State <span class ="star">*</span></label>
                                <?php
                                    echo $this->Form->input('state',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                            'required' => true
                                        )
                                    );
                                ?>
                            </div>
                        </div> 
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Zipcode <span class ="star">*</span></label>
                                <?php
                                    echo $this->Form->input('zip',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                            'required' => true,
                                            'type' => 'text'
                                        )
                                    );
                                ?>
                            </div>
                        </div>   
                        <div class="col-sm-12 form-group">
                            <button type="submit" class="btn btn-blue next-step">Submit</button>  
                        </div>  
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>       