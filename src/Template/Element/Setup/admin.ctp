<div class="modal Educational-info fade" id="addAdmin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><?php echo __('Add Additional Admin'); ?></h4>
            </div>
            <div class="modal-body clearfix">
                <div class="col-xs-12 clearfix">
                    <?= $this->Form->create(null,array('url' => array(
                            'controller' => 'Users',
                            'action' => 'addAdmin'
                            ),
                            'id' => 'admin'
                            )
                        ); 
                    ?>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Username <span class ="star">*</span></label>
                                <?php
                                    echo $this->Form->input('username',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                            'required' => true
                                        )
                                    );
                                ?>
                            </div>
                        </div>  
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Email <span class ="star">*</span></label>
                                <?php
                                    echo $this->Form->input('email',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                            'required' => true
                                        )
                                    );
                                ?>
                            </div>
                        </div> 
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Password <span class ="star">*</span></label>
                                <?php
                                    echo $this->Form->input('password',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                            'required' => true
                                        )
                                    );
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-6">
                                <div class="form-group">
                                    <label>First Name <span class ="star">*</span></label>
                                    <?php echo $this->Form->control('user_profile.first_name',array('class' => 'form-control','placeholder' => 'First Name','label' => false));?>
                                </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Last Name <span class ="star">*</span></label>
                                <?php echo $this->Form->control('user_profile.last_name',array('class' => 'form-control','placeholder' => 'Last Name','label' => false));?>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Select the type of admin <span class ="star">*</span></label>
                                <select name="user_role_id" class = "form-control">
                                    <?php if($this->request->session()->read('Auth.User.user_role_id') == 2) { ?><option value ="3">Full Admin</option><?php } ?>
                                    <option value = "4">Uniform Admin</option>
                                    <option value="5">Training Admin</option>
                                    <option value="6">Supervisor</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Division <span class ="star">*</span></label>
                                <?php
                                    echo $this->Form->input('user_profile.division_id',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                            'required' => true,
                                            'options' => $division
                                        )
                                    );
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-12 form-group">
                        <button type="submit" class="btn btn-blue next-step">Submit</button>  
                        </div>  
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>       