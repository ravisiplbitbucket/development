<?= $this->Form->create($question,array(
        'id' => 'question-edit1 '
        )
    ); 
?>
    <div class="col-sm-12">
        <div class="form-group">
            <label>Question <span class ="star">*</span></label>
            <?php
                echo $this->Form->input('name',
                    array(
                        'class' => 'form-control',
                        'div' => false,
                        'label' => false,
                        'required' => true
                    )
                );
            ?>
        </div>
    </div>  
    <div class="col-sm-12 form-group">
        <button type="submit" class="btn btn-blue next-step">Submit</button>  
    </div>  
</form>