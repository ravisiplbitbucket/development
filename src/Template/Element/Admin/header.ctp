<header class="main-header">
    <?php 
        echo $this->Html->link(
            $this->Html->image('logo.png',array('class' => 'img-responsive')), [
                'controller' => 'Admins',
                'action' => 'dashboard'
            ], [
                'class' => 'navbar-brand logo',
                'escape' => false
        ]);
    ?>
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#"><?php echo __('Administrator Panel'); ?></a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">                       
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">    Welcome <?= $this->request->session()->read('Auth.User.first_name'); ?>
                            <!-- <img src="https://adminlte.io/themes/AdminLTE/dist/img/user2-160x160.jpg" class="user-image" alt="User Image"> -->
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <?= 
                                    $this->Html->link('<i class="fa fa-sign-out fa-fw"></i>' . __('Logout'),[
                                        'controller' => 'Admins',
                                        'action' => 'logout',
                                        'prefix' => 'admin'
                                    ],[
                                        'escape' => false
                                    ]);
                                ?>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
</header>