<aside class="sidebar">
    <section>
        <ul class="list-group">
            <li>
                <?php
                    $active = $page === 'dashboard' ? 'active' : '';
                    echo $this->Html->link('<i class="fa fa-tachometer fa-fw"></i>' . __('Dashboard'),[
                        'controller' => 'Admins',
                        'action' => 'dashboard'
                    ],[
                        'class' => "list-group-item {$active}",
                        'escape' => false
                    ]);
                ?>
            </li>
            <li>
                <?php 
                    $active = $page === 'manage-companies' ? 'active' : ''; 
                    echo $this->Html->link('<i class="fa fa-users fa-fw"></i>' . __('Manage Companies'), [
                            'controller' => 'Admins',
                            'action' => 'manageCompanies'
                        ], [
                            'class' => "list-group-item {$active}",
                            'escape' => false
                        ]);
                ?>
            </li>
            <li>
                <?php 
                    $active = $page === 'manage-jobs' ? 'active' : '';
                    $image = $this->Html->image('job');
                    echo $this->Html->link('<i class="fa fa-address-card-o fa-fw"></i>' . __('Manage Jobs'),[
                        'controller' => 'Admins',
                        'action' => 'manageJobs'
                    ],[
                        'class' => "list-group-item {$active}",
                        'escape' => false
                    ]);
                ?>
            </li>
            <li>
                <?php 
                    $active = $page === 'job-applications' ? 'active' : '';
                   //$image = $this->Html->image('job');
                    echo $this->Html->link('<i class="fa fa-address-card-o fa-fw"></i>' . __('Job Applications'),[
                        'controller' => 'Admins',
                        'action' => 'jobApplications'
                    ],[
                        'class' => "list-group-item {$active}",
                        'escape' => false
                    ]);
                ?>
            </li>
            <li>
                <?php 
                    $active = $page === '' ? 'active' : '';
                    echo $this->Html->link('<i class="fa fa-clone fa-fw"></i>' . __('Manage CMS Pages'),
                        [
                            'controller' => 'cms-pages',
                            'action'    => 'index'
                        ],
                        [
                            'class' => "list-group-item {$active}",
                            'escape' => false
                        ]);
                ?>
            </li>
            <li>
                <?php 
                    $active = $page === 'Email Templates' ? 'active' : '';
                    echo $this->Html->link('<i class="fa fa-paper-plane-o fa-fw"></i>' . __('Manage Email Templates'), [
                            'controller' => 'EmailTemplates',
                            'action' => 'emailTemplateList'
                        ], [
                            'class' => "list-group-item {$active}",
                            'escape' => false
                        ]);
                ?>
            </li>
        </ul>
    </section>
</aside>