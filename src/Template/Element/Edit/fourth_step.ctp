<div class="tab-pane active clearfix" role="tabpanel" id="step1">
    <?= $this->Form->create($candidate,array('type' => 'post','url' => array(
                'controller' => 'Edits',
                'action' => 'set-session'
                ),
                'id' => 'candidate-apply-fourth-step')
            ); 
    echo $this->Form->hidden('step',['value' => 5]);
    echo $this->Form->hidden('candidate_completes.id',['value' => $candidate['candidate_completes'][0]->id]);
    ?>
    <div class="step1">
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-12 subheading">
                    <h3>Personal Information</h3>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Social Security#</label>
                        <?php
                            echo $this->Form->input('candidate_completes.social_security',
                                array(
                                    'class' => 'form-control',
                                    'div' => false,
                                    'label' => false,
                                    'value' => $candidate['candidate_completes'][0]->social_security
                                )
                            );
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label>List names of any relatives employed by our company</label>
                        <?php
                            echo $this->Form->input('candidate_completes.relative_employed',
                                array(
                                    'class' => 'form-control',
                                    'div' => false,
                                    'label' => false,
                                    'rows' => 4,
                                    'type' => 'textarea',
                                    'value' => $candidate['candidate_completes'][0]->relative_employed
                                )
                            );
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label>What Languages do you speak</label>
                <?php
                    echo $this->Form->input('candidate_completes.language_ids',
                        array(
                            'class' => 'form-control languages-speak',
                            'div' => false,
                            'label' => false,
                            'options' => $language,
                            'multiple' => 'multiple',
                            'value' => explode(',', $candidate['candidate_completes'][0]->language_ids),
                            'id' => 'dates-field2'
                        )
                    );
                ?>
            </div>
        </div>
    </div>
    <ul class="list-inline col-sm-12">
        <li><button type="button" class="btn btn-cancel prev-step">Previous</button></li>
        <li><button type="submit" class="btn btn-blue next-step">Next</button></li>
    </ul>
    <?= $this->Form->end()?>
</div>