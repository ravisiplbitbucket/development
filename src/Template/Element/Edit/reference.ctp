<?= $this->Form->create($reference,
                array(
                    'type' => 'post',
                    'url' => array(
                    'controller' => 'Edits',
                    'action' => 'reference',
                    base64_encode($reference->id)
                ),
                    'id' => 'candidate-reference-edit-form')
                ); 
            ?>
            <div class="modal-body">
                <div class="form-group">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Address</th>
                                    <th>Phone</th>
                                    <th>Occupation</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <?php
                                            echo $this->Form->input('name',
                                                array(
                                                    'class' => 'form-control',
                                                    'div' => false,
                                                    'label' => false
                                                )
                                            );
                                        ?>
                                    </td>
                                    <td>
                                            <?php
                                                echo $this->Form->input('address',
                                                    array(
                                                        'class' => 'form-control',
                                                        'div' => false,
                                                        'label' => false
                                                    )
                                                );
                                            ?>
                                        </td>
                                    <td>
                                            <?php
                                                echo $this->Form->input('phone',
                                                    array(
                                                        'class' => 'form-control',
                                                        'div' => false,
                                                        'label' => false
                                                    )
                                                );
                                            ?>
                                        </td>
                                    <td>
                                        <?php
                                            echo $this->Form->input('occupation',
                                                array(
                                                    'class' => 'form-control',
                                                    'div' => false,
                                                    'label' => false
                                                )
                                            );
                                        ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>                      
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
            <?= $this->Form->end() ?>