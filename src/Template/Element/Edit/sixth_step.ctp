<?= $this->Form->create($candidate,
        array(
            'type' => 'post',
            'url' => array(
                'controller' => 'Edits',
                'action' => 'edit',
                base64_encode($candidate->id)
            ),
            'id' => 'candidate-apply-sixth-step'
        )
    ); 
echo $this->Form->hidden('candidate_completes.0.id',['value' => $candidate['candidate_completes'][0]->id]);
?>
<div class="step3">
    <div class="certification-section">
        <div class="col-sm-12 subheading">
            <h3>Certification </h3>
        </div>
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Are  you certified/licensed/commissioned to work unarmed </label>
                        <div>
                            <label class="radio-inline">
                            <input class="certified-licensed" type="radio" value="1" name="unarmed" <?php echo ($candidate->certified_unarmed == 1) ? "checked" : ""?> >Yes
                            </label>
                            <label class="radio-inline">
                            <input class="certified-licensed" type="radio" value ="0" name="unarmed" <?php echo ($candidate->certified_unarmed == 0) ? "checked" : ""?>>No
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <?php 
                // pr($candidate);
            ?>
            <div class="row certified-licensed-section <?= ($candidate->certified_unarmed)?'':'hide' ?>">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>What is your License number if applicable </label>
                        <?php
                            echo $this->Form->input('candidate_completes.0.unarmed_licence_number',
                                array(
                                    'class' => 'form-control',
                                    'div' => false,
                                    'label' => false,
                                    'value' => $candidate['candidate_completes'][0]->unarmed_licence_number
                                )
                            );
                        ?>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Expiration   date    if  applicable </label>
                        <?php
                            echo $this->Form->input('candidate_completes.0.unarmed_expiry_date',
                                array(
                                    'type' => 'text',
                                    'class' => 'form-control',
                                    'div' => false,
                                    'label' => false,
                                    'id'    => "datetimepicker15",
                                    'value' => date('d/m/Y',strtotime($candidate['candidate_completes'][0]->unarmed_expiry_date))
                                )
                            );
                        ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Are  you certified/licensed/commissioned to work armed </label>
                        <div>
                            <label class="radio-inline">
                            <input class="certified-licensed-armed" type="radio" value="1" name="armed" <?php echo ($candidate->certified_armed == 1) ? "checked" : ""?>>Yes
                            </label>
                            <label class="radio-inline">
                            <input class="certified-licensed-armed" type="radio" value ="0" name="armed" <?php echo ($candidate->certified_armed == 0) ? "checked" : ""?> >No
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row certified-licensed-armed-section <?= ($candidate->certified_armed)?'':'hide' ?>">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>What is your License number if applicable </label>
                        <?php
                            echo $this->Form->input('candidate_completes.0.armed_licence_number',
                                array(
                                    'class' => 'form-control',
                                    'div' => false,
                                    'label' => false,
                                    'value' => $candidate['candidate_completes'][0]->armed_licence_number
                                )
                            );
                        ?>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Expiration date if applicable</label>
                        <?php
                            echo $this->Form->input('candidate_completes.0.armed_expiry_date',
                                array(
                                    'type' => 'text',
                                    'class' => 'form-control',
                                    'div' => false,
                                    'label' => false,
                                    'id'    => "armed-expiry-date",
                                    'value' => date('d/m/Y',strtotime($candidate['candidate_completes'][0]->armed_expiry_date))
                                )
                            );
                        ?>
                    </div>
                </div>
            </div>
            <?php
                if(!empty($candidate['candidate_completes'][0])) {
                    $is_cpr_certified = $candidate['candidate_completes'][0]->is_cpr_certified;
                }                
            ?>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Are  you CPR/AED Certified</label>
                        <div>
                            <label class="radio-inline">
                            <input class="cpr-aed-certified" type="radio" value="1" name="candidate_completes[0][is_cpr_certified]" <?= ($is_cpr_certified)?'checked':'' ?> >Yes
                            </label>
                            <label class="radio-inline">
                            <input class="cpr-aed-certified" type="radio" value ="0" name="candidate_completes[0][is_cpr_certified]" <?= ($is_cpr_certified)?'':'checked' ?>>No
                            </label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row cpr-aed-certified-section <?= ($is_cpr_certified)?'':'hide' ?> ">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Expiration date</label>
                        <?php
                            echo $this->Form->input('candidate_completes.0.cpr_expiry_date',
                                array(
                                    'type' => 'text',
                                    'class' => 'form-control',
                                    'div' => false,
                                    'label' => false,
                                    'id'=>'cpr-expiry-date',
                                    'value' => date('d/m/Y',strtotime($candidate['candidate_completes'][0]->cpr_expiry_date))
                                )
                            );
                        ?>
                    </div>
                </div>
            </div>
            <?php
                if(!empty($candidate['candidate_completes'][0])) {
                    $is_twic_certified = $candidate['candidate_completes'][0]->is_twic_certified;
                }                
            ?>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Are  you TWIC certified </label>
                        <div>
                            <label class="radio-inline">
                            <input class="twic-certified" type="radio" value="1" name="candidate_completes[0][is_twic_certified]" <?= ($is_twic_certified)?'checked':'' ?> >Yes
                            </label>
                            <label class="radio-inline">
                            <input class="twic-certified" type="radio" value ="0" name="candidate_completes[0][is_twic_certified]" <?= ($is_twic_certified)?'':'checked' ?> >No
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row twic-certified-section <?= ($is_twic_certified)?'':'hide' ?> ">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Expiration date</label>
                        <?php
                            echo $this->Form->input('candidate_completes.0.twic_expiry_date',
                                array(
                                    'type' => 'text',
                                    'class' => 'form-control',
                                    'div' => false,
                                    'label' => false,
                                    'id'=>'twic-exiry-date',
                                    'value' => date('d/m/Y',strtotime($candidate['candidate_completes'][0]->twic_expiry_date))
                                )
                            );
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<ul class="list-inline col-sm-12">
    <li><button type="button" class="btn btn-cancel prev-step">Previous</button></li>
    <li><button type="submit" class="btn btn-blue next-step">Submit</button></li>
</ul>
<?= $this->Form->end()?>
<!-- Modal -->
