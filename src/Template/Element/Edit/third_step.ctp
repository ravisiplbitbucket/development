<?= $this->Form->create($candidate,
        array(
            'type' => 'post',
            'url' => array(
                'controller' => 'Edits',
                'action' => 'set-session'
            ),
            'id' => 'candidate-apply-third-step'
        )
    ); 
echo $this->Form->hidden('step',['value' => 4]);
echo $this->Form->hidden('id',['value' => $candidate->id]);
?>
<div class="step3">
    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-12 subheading">
                <h3>Employment History  <a data-toggle="modal" data-target="#employment-history" href="#"><i class="fa fa-plus" id='historyModalIcon' aria-hidden="true"></i></a></h3>
            </div>
        </div>
    </div>
    <div id='history-list'>        
        <?php
        if(!empty($histories)) {
            foreach ($histories as $key => $value) {
    ?>
        <div class="col-sm-12 form-group">
            <h4>
                <?= 
                    $this->Html->link($value,'javascript:void(0)',[
                        'class' => 'get-history',
                        'data-url' => $this->Url->build([
                            'controller' => 'Edits', 
                            'action' =>'get-history',
                            base64_encode($key)
                        ])
                    ]) 
                ?>
            </h4>
        </div>
    <?php
            }
        }
    ?>
    </div>
    <div class="writing-test-section">
        <div class="col-sm-12 subheading">
            <h3>Writing Test </h3>
        </div>
        <div class="col-sm-12">
            <div class="row form-group">
                <div class="col-sm-6">
                    <?php echo $this->Html->image('writing.jpg',array('class' => 'img-responsive'));?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Please describe in  detail what you see in the picture above  </label>
                        <?php
                            echo $this->Form->input('writing_test',
                                array(
                                    'class' => 'form-control',
                                    'div' => false,
                                    'label' => false,
                                    
                                    'type' => 'textarea',
                                    'rows' => 4
                                )
                            );
                        ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label>Would you like to receive Email Alerts for other security  positions in your area?</label>
                        <div>
                            <label class="radio-inline">
                            <input class="" type="radio" value="1" name="email_alerts">Yes
                            </label>
                            <label class="radio-inline">
                            <input class="" type="radio" checked value ="0" name="email_alerts">No
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<ul class="list-inline col-sm-12">
    <li><button type="button" class="btn btn-cancel prev-step">Previous</button></li>
    <li><button type="submit" class="btn btn-blue next-step">Next</button></li>
</ul>
<?= $this->Form->end()?>
<?php echo $this->element('Edit/add_history');?>
<div class="modal fade" id="edit-history-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            
        </div>
    </div>
</div>
