<?= $this->Form->create($education,
                array(
                    'type' => 'post',
                    'url' => array(
                        'controller' => 'Edits',
                        'action' => 'education',
                        base64_encode($education->id)
                    ),
                    'id' => 'candidate-education-edit-form'
                    )
                ); 
            ?>
            <div class="modal-body">
                <div class="form-group">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Name & Address of School</th>
                                    <th>From mo/yr</th>
                                    <th>To mo/yr</th>
                                    <th>Highest Grade Completed</th>
                                    <th>Degree/Major</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">
                                        <?= 
                                            $this->Form->input('education_id',[
                                                'options' => $educations,
                                                'class' => 'form-control',
                                                'div' => false,
                                                'label' => false,
                                            ]); 
                                        ?>
                                    </th>
                                    <td>
                                    <?php
                                        echo $this->Form->input('from_year',
                                            array(
                                                'class' => 'form-control',
                                                'div' => false,
                                                'label' => false,
                                                'id' => 'datetimepicker1'
                                            )
                                        );
                                    ?>
                                    </td>
                                    <td>
                                    <?php
                                        echo $this->Form->input('to_year',
                                            array(
                                                'class' => 'form-control',
                                                'div' => false,
                                                'label' => false,
                                                'id' => 'datetimepicker2'
                                            )
                                        );
                                    ?>
                                    </td>
                                    <td>
                                    <?php
                                        echo $this->Form->input('grade',
                                            array(
                                                'class' => 'form-control',
                                                'div' => false,
                                                'label' => false
                                            )
                                        );
                                    ?>
                                    </td>
                                    <td>
                                    <?php
                                        echo $this->Form->input('degree',
                                            array(
                                                'class' => 'form-control',
                                                'div' => false,
                                                'label' => false
                                            )
                                        );
                                    ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>                        
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
            <?= $this->Form->end() ?>