<?= $this->Form->create($candidate,
    array(
        'type' => 'post',
        'url' => array(
        'controller' => 'Edits',
        'action' => 'set-session'
    ),
        'id' => 'candidate-apply-fifth-step')
    );
echo $this->Form->hidden('step',['value' => 6]);
echo $this->Form->hidden('candidate_military.id',['value' => $candidate['candidate_completes'][0]['candidate_military']->id]);
?>
<div class="step2">
    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-12 subheading">
                <h3>
                    Education
                    <a data-toggle="modal" data-target="#candidate-education" href="#"><i class="fa fa-plus" id="historyModalIcon" aria-hidden="true"></i></a>
                </h3>
            </div>
            <div id='education-list'>
                <?php
                    if(!empty($candidate['candidate_completes'][0]['candidate_educations'])) {
                            foreach ($candidate['candidate_completes'][0]['candidate_educations'] as $key => $value) {
                    ?>
                        <div class="col-sm-12 form-group">
                            <h4>
                                <?= 
                                    $this->Html->link($value['education']->education_name,'javascript:void(0)',[
                                        'class' => 'get-education',
                                        'data-url' => $this->Url->build([
                                            'controller' => 'employees', 
                                            'action' =>'education',
                                            base64_encode($value->id)
                                        ])
                                    ]) 
                                ?>
                            </h4>
                        </div>
                    <?php
                            }
                        }
                    ?>
            </div>
        </div>
    </div>    
    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-12 subheading">
                <h3>RECORD  OF  US  MILITARY    AND RESERVE STATUS</h3>
            </div>
        </div>
    </div>
    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label>From</label>
                    <?php
                        echo $this->Form->input('candidate_military.military_from_year',
                            array(
                                'class' => 'form-control',
                                'div' => false,
                                'label' => false,
                                'id' => 'datetimepicker16',
                                'value' => $candidate['candidate_completes'][0]['candidate_military']->military_from_year
                            )
                        );
                    ?>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label>To</label>
                    <?php
                        echo $this->Form->input('candidate_military.military_to_year',
                            array(
                                'class' => 'form-control',
                                'div' => false,
                                'label' => false,
                                'id' => 'datetimepicker17',
                                'value' => $candidate['candidate_completes'][0]['candidate_military']->military_to_year
                            )
                        );
                    ?>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label>Branch</label>
                    <?php
                        echo $this->Form->input('candidate_military.branch',
                            array(
                                'class' => 'form-control',
                                'div' => false,
                                'label' => false,
                                'value' => $candidate['candidate_completes'][0]['candidate_military']->branch
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Reserve Unit</label>
                    <?php
                        echo $this->Form->input('candidate_military.reserver_unit',
                            array(
                                'class' => 'form-control',
                                'div' => false,
                                'label' => false,
                                'value' => $candidate['candidate_completes'][0]['candidate_military']->reserver_unit
                            )
                        );
                    ?>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Meeting Date</label>
                    <?php
                        echo $this->Form->input('candidate_military.meeting_date',
                            array(
                                'type' => 'text',
                                'class' => 'form-control',
                                'div' => false,
                                'label' => false,
                                'id' => 'datetimepicker18',
                                'value' => date('d/m/Y',strtotime($candidate['candidate_completes'][0]['candidate_military']->meeting_date))
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label>Summarize    Skills, training    or  qualifications</label>
                    <?php
                        echo $this->Form->input('candidate_military.skills',
                            array(
                                'class' => 'form-control',
                                'div' => false,
                                'label' => false,
                                'rows' => 5,
                                'type' => 'textarea',
                                'value' => $candidate['candidate_completes'][0]['candidate_military']->skills
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-12 subheading">
                <h3>PROFESSIONAL/PERSONAL   REFERENCES
                <a data-toggle="modal" data-target="#candidate-reference" href="#"><i class="fa fa-plus" id="historyModalIcon" aria-hidden="true"></i></a>
                </h3>
            </div>
            <div id='reference-list'>
                <?php
                    if(!empty($candidate['candidate_completes'][0]['candidate_references'])) {
                            foreach ($candidate['candidate_completes'][0]['candidate_references'] as $key => $value) {
                    ?>
                        <div class="col-sm-12 form-group">
                            <h4>
                                <?= 
                                    $this->Html->link($value->name,'javascript:void(0)',[
                                        'class' => 'get-reference',
                                        'data-url' => $this->Url->build([
                                            'controller' => 'employees', 
                                            'action' =>'reference',
                                            base64_encode($value->id)
                                        ])
                                    ]) 
                                ?>
                            </h4>
                        </div>
                    <?php
                            }
                        }
                    ?>
            </div>
        </div>
    </div>
    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-12">
                
            </div>
        </div>
    </div>
</div>
<ul class="list-inline col-sm-12">
    <li><button type="button" class="btn btn-cancel prev-step">Previous</button></li>
    <li><button type="submit" class="btn btn-blue next-step">Next</button></li>
</ul>
<?= $this->Form->end()?>
<!-- Modal -->
<div class="modal fade" id="candidate-education" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add Education</h4>
            </div>
            <?= $this->Form->create(null,
                array(
                    'type' => 'post',
                    'url' => array(
                    'controller' => 'Employees',
                    'action' => 'set-education'
                ),
                    'id' => 'candidate-education-form')
                ); 
            ?>
            <div class="modal-body">
                <div class="form-group">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Name & Address of School</th>
                                    <th>From mo/yr</th>
                                    <th>To mo/yr</th>
                                    <th>Highest Grade Completed</th>
                                    <th>Degree/Major</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    if($educations->toArray()) {
                                ?>
                                <tr>
                                    <th scope="row">
                                        <?= 
                                            $this->Form->input('education_id',[
                                                'options' => $educations->toArray(),
                                                'class' => 'form-control',
                                                'div' => false,
                                                'label' => false,
                                            ]); 
                                        ?>
                                    </th>
                                    <td>
                                    <?php
                                        echo $this->Form->input('from_year',
                                            array(
                                                'class' => 'form-control',
                                                'div' => false,
                                                'label' => false,
                                                'id' => 'datetimepicker1'
                                            )
                                        );
                                    ?>
                                    </td>
                                    <td>
                                    <?php
                                        echo $this->Form->input('to_year',
                                            array(
                                                'class' => 'form-control',
                                                'div' => false,
                                                'label' => false,
                                                'id' => 'datetimepicker2'
                                            )
                                        );
                                    ?>
                                    </td>
                                    <td>
                                    <?php
                                        echo $this->Form->input('grade',
                                            array(
                                                'class' => 'form-control',
                                                'div' => false,
                                                'label' => false
                                            )
                                        );
                                    ?>
                                    </td>
                                    <td>
                                    <?php
                                        echo $this->Form->input('degree',
                                            array(
                                                'class' => 'form-control',
                                                'div' => false,
                                                'label' => false
                                            )
                                        );
                                    ?>
                                    </td>
                                </tr>
                                <?php 
                                    } 
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>                        
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="candidate-reference" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add Reference</h4>
            </div>
            <?= $this->Form->create(null,
                array(
                    'type' => 'post',
                    'url' => array(
                    'controller' => 'Employees',
                    'action' => 'set-reference'
                ),
                    'id' => 'candidate-reference-form')
                ); 
            ?>
            <div class="modal-body">
                <div class="form-group">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Address</th>
                                    <th>Phone</th>
                                    <th>Occupation</th>
                                    <th>Number Of Years Known</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <?php
                                            echo $this->Form->input('name',
                                                array(
                                                    'class' => 'form-control',
                                                    'div' => false,
                                                    'label' => false
                                                )
                                            );
                                        ?>
                                    </td>
                                    <td>
                                            <?php
                                                echo $this->Form->input('address',
                                                    array(
                                                        'class' => 'form-control',
                                                        'div' => false,
                                                        'label' => false
                                                    )
                                                );
                                            ?>
                                        </td>
                                    <td>
                                            <?php
                                                echo $this->Form->input('phone',
                                                    array(
                                                        'class' => 'form-control',
                                                        'div' => false,
                                                        'label' => false
                                                    )
                                                );
                                            ?>
                                        </td>
                                    <td>
                                        <?php
                                            echo $this->Form->input('occupation',
                                                array(
                                                    'class' => 'form-control',
                                                    'div' => false,
                                                    'label' => false
                                                )
                                            );
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                            echo $this->Form->input('number_of_years_known',
                                                array(
                                                    'class' => 'form-control',
                                                    'div' => false,
                                                    'label' => false,
                                                    'type' => 'number'
                                                )
                                            );
                                        ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>                      
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
<div class="modal fade" id="edit-education-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            
        </div>
    </div>
</div>
<div class="modal fade" id="edit-reference-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            
        </div>
    </div>
</div>