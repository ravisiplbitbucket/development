<div class="modal fade" id="employment-history" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Employment History</h4>
            </div>
            <?= $this->Form->create($candidate,
                array(
                    'type' => 'post',
                    'url' => array(
                        'controller' => 'Edits',
                        'action' => 'save-candidate-history',
                    ),
                    'id' => 'candidate-history')
                ); 
            ?>
            <?= 
                $this->Form->hidden('candidate_id',['value' => $candidate->id])
            ?>
            <div class="modal-body clearfix">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Company</label>
                                <?php
                                    echo $this->Form->input('company_name',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                        )
                                    );
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>From</label>
                                <?php
                                    echo $this->Form->input('from_work',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                            'id' => 'from-work'
                                        )
                                    );
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>To</label>
                                <?php
                                    echo $this->Form->input('to_work',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                            'id' => 'to-work'
                                        )
                                    );
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>State</label>
                                <?php
                                    echo $this->Form->input('where_employ',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                        )
                                    );
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Company  Mailing Address 1</label>
                                <?php
                                    echo $this->Form->input('company_email1',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                        )
                                    );
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Company  Mailing Address 2</label>
                                <?php
                                    echo $this->Form->input('company_email2',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                        )
                                    );
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Address</label>
                                <?php
                                    echo $this->Form->input('address',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                        )
                                    );
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>City</label>
                                <?php
                                    echo $this->Form->input('city',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                        )
                                    );
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Zip</label>
                                <?php
                                    echo $this->Form->input('zipcode',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                        )
                                    );
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Your job title/Rank</label>
                                <?php
                                    echo $this->Form->input('designation',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                        )
                                    );
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Is this a security guard company</label>
                                <div>
                                    <label class="radio-inline">
                                    <input class="guard" type="radio" value="1" name="security_company">Yes
                                    </label>
                                    <label class="radio-inline">
                                    <input class="guard" type="radio" checked value ="0" name="security_company">No
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 security-guard hide">
                    <div class="row">
                        <div class="col-sm-12">
                            <h4>
                                Please  complete    this    section to  allow   us  to  gauge   your    general work    experience
                            </h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Did you report directly to the client contact</label>
                                <div>
                                    <label class="radio-inline">
                                    <input class="client-contact" type="radio" value="1" name="report_client">Yes
                                    </label>
                                    <label class="radio-inline">
                                    <input class="client-contact" type="radio" checked value ="0" name="report_client">No
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Client Contact Name</label>
                                <?php
                                    echo $this->Form->input('client_name',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                        )
                                    );
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Client Contact Phone Number</label>
                                <?php
                                    echo $this->Form->input('client_number',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                            'id' => 'client-number'
                                        )
                                    );
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>May we contact this person as a reference?</label>
                                <div>
                                    <label class="radio-inline">
                                    <input class="reference" type="radio" value="1" name="may_we_contact">Yes
                                    </label>
                                    <label class="radio-inline">
                                    <input class="reference" type="radio"  checked value ="0" name="may_we_contact">No
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Date you started at the site</label>
                                <?php
                                    echo $this->Form->input('date_started',
                                        array(
                                            'type' => 'text',
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                            'id' => 'date_started'
                                        )
                                    );
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Date you ended at the site</label>
                                <?php
                                    echo $this->Form->input('date_ended',
                                        array(
                                            'type' => 'text',
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                            'id' => 'date_ended'
                                        )
                                    );
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Kind of site</label>
                                <select name ="kind_site" class="form-control">
                                    <option value="Residential">Residential</option>
                                    <option value="Industrial">Industrial</option>
                                    <option value="Office Building">Office Building</option>
                                    <option value="Entertainment">Entertainment</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Pay rate while at the site</label>
                                <?php
                                    echo $this->Form->input('rate_paid',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                        )
                                    );
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="form-group">
                                <label>Were you comfortable using   this    
                                software?   </label>
                                <div>
                                    <label class="radio-inline">
                                    <!-- <input class="reports" type="radio" value="1" name="required_submit_report">Yes -->
                                        <?= 
                                            $this->Form->radio('is_comfot_sotware', [['value'=>'1','text'=>'Yes']],['label' => false,'class' => 'reports','hiddenField'=>false]) 
                                        ?>Yes
                                    </label>
                                    <label class="radio-inline">
                                    <!-- <input class="reports" type="radio" checked value ="0" name="required_submit_report">No -->
                                        <?= 
                                            $this->Form->radio('is_comfot_sotware', [['value'=>'0','text'=>'No']],['label' => false,'class' => 'reports','hiddenField'=>false]) 
                                        ?>No
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Did you foot patrols at this site?</label>
                                <div>
                                    <label class="radio-inline">
                                    <input class="patrols" type="radio" value="1" name="foot_patrol">Yes
                                    </label>
                                    <label class="radio-inline">
                                    <input class="patrols" type="radio" checked value ="0" name="foot_patrol">No
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row foot-patrols hide">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>How many hours per shift did you spend waliking</label>
                                <?php
                                    echo $this->Form->input('hours_per_shift',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                        )
                                    );
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>where you able to consistently perform the patrols</label>
                                <div>
                                    <label class="radio-inline">
                                    <input class="" type="radio" value="1" name="perform_patrols">Yes
                                    </label>
                                    <label class="radio-inline">
                                    <input class="" type="radio" checked value ="0" name="perform_patrols">No
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>How were rounds verfified</label>
                                <select id="dates-field2" name="rounds_verified[]" class="multiselect-ui form-control" multiple="multiple">
                                    <option value="Wand">Wand</option>
                                    <option value="App">App</option>
                                    <option value="Clock">Clock</option>
                                    <option value="Not Verified">Not Verified</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="form-group">
                                <label>Were you required to submit reports?</label>
                                <div>
                                    <label class="radio-inline">
                                    <input class="reports" type="radio" value="1" name="required_submit_report">Yes
                                    </label>
                                    <label class="radio-inline">
                                    <input class="reports" type="radio" checked value ="0" name="required_submit_report">No
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row submit-reports hide">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>How were reports submitted</label>
                                <select id="dates-field2" name="way_report_submitted[]" class="report-submit form-control" multiple="multiple">
                                    <option value="Handwritten">Handwritten</option>
                                    <option value="via Computer">via Computer</option>
                                    <option value="via App">via App</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row ">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Which shift did you work at the site?</label>
                                <select id="dates-field2" name="shift_work[]" class="report-submit form-control" multiple="multiple">
                                    <option value="1st">1st</option>
                                    <option value="2nd">2nd</option>
                                    <option value="3rd">3rd</option>
                                    <option value="Weekdays">Weekdays</option>
                                    <option value="Weekends">Weekends</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row ">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Was this site armed?</label>
                                <div>
                                    <label class="radio-inline">
                                    <input class="" type="radio" value="1" name="is_site_armed">Yes
                                    </label>
                                    <label class="radio-inline">
                                    <input class="" type="radio" checked value ="0" name="is_site_armed">No
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row ">
                        <div class="col-sm-8">
                            <div class="form-group">
                                <label>Please check all equipment that you were issued for this site</label>
                                <select id="dates-field2" name="equipment_issued[]" class="report-submit form-control" multiple="multiple">
                                    <option value="Handcuffs">Handcuffs</option>
                                    <option value="batons">batons</option>
                                    <option value="pepper spray">pepper spray</option>
                                    <option value="handgun">handgun</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row ">
                        <div class="col-sm-8">
                            <div class="form-group">
                                <label>Did you ever have to use this equipment</label>
                                <div>
                                    <label class="radio-inline">
                                    <input class="equipment" type="radio" value="1" name="used_equipment">Yes
                                    </label>
                                    <label class="radio-inline">
                                    <input class="equipment" type="radio" checked value ="0" name="used_equipment">No
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row hide equipment-section">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Please   describe    the situation and   your    response</label>
                                <?php
                                    echo $this->Form->input('equipment_situation',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                            'type' => 'textarea',
                                            'rows' => 5
                                        )
                                    );
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="form-group">
                                <label>Did you ever have to contact emergency services</label>
                                <div>
                                    <label class="radio-inline">
                                    <input class="emergency-services" type="radio" value="yes" name="contact_emergency">Yes
                                    </label>
                                    <label class="radio-inline">
                                    <input class="emergency-services" checked type="radio" checked value ="no" name="contact_emergency">No
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row hide emergency-services-section">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Please   describe    the situation and   your    response</label>
                                <?php
                                    echo $this->Form->input('emergency_situation',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                            'type' => 'textarea',
                                            'rows' => 5
                                        )
                                    );
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="form-group">
                                <label>Were you ever injured at this location</label>
                                <div>
                                    <label class="radio-inline">
                                    <input class="" type="radio" value="1" name="ever_injured">Yes
                                    </label>
                                    <label class="radio-inline">
                                    <input class="" type="radio" checked value ="0" name="ever_injured">No
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row hide  injured-section">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Please   describe    the situation </label>
                                <?php
                                    echo $this->Form->input('injured_situation',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                            'type' => 'textarea',
                                            'rows' => 5
                                        )
                                    );
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="form-group">
                                <label>Were you required to operate any type of vehicle on site</label>
                                <div>
                                    <label class="radio-inline">
                                        <?= 
                                            $this->Form->radio('operate_vehicle', [['value'=>'1','text'=>'Yes']],['label' => false,'class' => 'vehicle','hiddenField'=>false]) 
                                        ?>Yes
                                    </label>
                                    <label class="radio-inline">
                                    <!-- <input class="vehicle" type="radio" checked value ="0" name="operate_vehicle">No -->
                                    <?= 
                                        $this->Form->radio('operate_vehicle', [['value'=>'0','text'=>'No']],['label' => false,'class' => 'vehicle','hiddenField'=>false]) 
                                    ?>No
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row hide vechicle-required">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Please check which ones</label>
                                <?php
                                    echo $this->Form->select('vechicle_type', [
                                            "Automobile" => "Automobile",
                                            'bicycle'    => 'bicycle',
                                            'Segway'   => 'Segway',
                                            'other'    => 'other'
                                        ], 
                                        [
                                            'multiple'=>"multiple",
                                            'class' => 'report-submit form-control', 
                                            'label' => false, 
                                            'id' => 'dates-field2'
                                        ]
                                    );
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Were you ever    involved    in  an  accident    in  the vehicle:    </label>
                                <div>
                                    <label class="radio-inline">
                                    <!-- <input class="reports" type="radio" value="1" name="required_submit_report">Yes -->
                                        <?= 
                                            $this->Form->radio('accident_vehicle', [['value'=>'1','text'=>'Yes']],['label' => false,'class' => 'accident-involved','hiddenField'=>false]) 
                                        ?>Yes
                                    </label>
                                    <label class="radio-inline">
                                    <!-- <input class="reports" type="radio" checked value ="0" name="required_submit_report">No -->
                                        <?= 
                                            $this->Form->radio('accident_vehicle', [['value'=>'0','text'=>'No']],['label' => false,'class' => 'accident-involved','hiddenField'=>false]) 
                                        ?>No
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row hide  accident-involved-section">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Please   describe    the situation </label>
                                <?php
                                    echo $this->Form->input('vecicle_accident_situation',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                            'type' => 'textarea',
                                            'rows' => 5
                                        )
                                    );
                                ?>
                            </div>
                        </div>
                    </div>
            
        </div>
        <div class="modal-footer">
                <button type="button" class="btn  btn-cancel" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-blue">Save</button>
        </div>
        <?= $this->Form->end()?>

    </div>
</div>
</div>
</div>