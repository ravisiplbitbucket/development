<?= $this->Form->create($candidate,
    array(
        'type' => 'post',
        'url' => array(
        'controller' => 'Edits',
        'action' => 'set-session'
    ),
        'id' => 'candidate-apply-second-step')
    ); 
        echo $this->Form->hidden('step',['value' => 3]);
        echo $this->Form->hidden('id',['value' => $candidate->id]);

?>
    <div class="step2">
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-12 subheading">
                    <h3>Work Availability</h3>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Date available to begin working</label>
                        <?php
                            echo $this->Form->input('date_begin_work',
                                array(
                                    'class' => 'form-control',
                                    'div' => false,
                                    'label' => false,
                                    
                                    'placeholder' => '12/06/2017'
                                )
                            );
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Total hours availbale per week</label>
                        <?php
                            echo $this->Form->input('hours_available',
                                array(
                                    'class' => 'form-control',
                                    'div' => false,
                                    'label' => false,
                                    'min' => 1
                                    
                                )
                            );
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label>Please check the days and shifts that you are available to work below:</label>
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Sun</th>
                                        <th>Mon</th>
                                        <th>Tue</th>
                                        <th>Wed</th>
                                        <th>Thu</th>
                                        <th>Fri</th>
                                        <th>Sat</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th scope="row">1<sup>ST</sup>shift</th>
                                        <td><input class="shift-group" type="checkbox" name="working_shifts[1][][day_id]" value ='1' <?php echo ($this->Common->workingShifts(1,1,$candidate->working_shifts)) ? "checked": ""?>></td>
                                        <td><input class="shift-group" type="checkbox" name="working_shifts[1][][day_id]" value ='2' <?php echo ($this->Common->workingShifts(2,1,$candidate->working_shifts)) ? "checked": ""?>></td>
                                        <td><input class="shift-group" type="checkbox" name="working_shifts[1][][day_id]" value ='3' <?php echo ($this->Common->workingShifts(3,1,$candidate->working_shifts)) ? "checked": ""?>></td>
                                        <td><input class="shift-group" type="checkbox" name="working_shifts[1][][day_id]" value ='4' <?php echo ($this->Common->workingShifts(4,1,$candidate->working_shifts)) ? "checked": ""?>></td>
                                        <td><input class="shift-group" type="checkbox" name="working_shifts[1][][day_id]" value ='5' <?php echo ($this->Common->workingShifts(5,1,$candidate->working_shifts)) ? "checked": ""?>></td>
                                        <td><input class="shift-group" type="checkbox" name="working_shifts[1][][day_id]" value ='6' <?php echo ($this->Common->workingShifts(6,1,$candidate->working_shifts)) ? "checked": ""?>></td>
                                        <td><input class="shift-group" type="checkbox" name="working_shifts[1][][day_id]" value ='7' <?php echo ($this->Common->workingShifts(7,1,$candidate->working_shifts)) ? "checked": ""?>></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">2<sup>nd</sup>shift</th>
                                        <td><input class="shift-group" type="checkbox" name="working_shifts[2][][day_id]" value ='1' <?php echo ($this->Common->workingShifts(1,2,$candidate->working_shifts)) ? "checked": ""?>></td>
                                        <td><input class="shift-group" type="checkbox" name="working_shifts[2][][day_id]" value ='2' <?php echo ($this->Common->workingShifts(2,2,$candidate->working_shifts)) ? "checked": ""?>></td>
                                        <td><input class="shift-group" type="checkbox" name="working_shifts[2][][day_id]" value ='3' <?php echo ($this->Common->workingShifts(3,2,$candidate->working_shifts)) ? "checked": ""?>></td>
                                        <td><input class="shift-group" type="checkbox" name="working_shifts[2][][day_id]" value ='4' <?php echo ($this->Common->workingShifts(4,2,$candidate->working_shifts)) ? "checked": ""?>></td>
                                        <td><input class="shift-group" type="checkbox" name="working_shifts[2][][day_id]" value ='5' <?php echo ($this->Common->workingShifts(5,2,$candidate->working_shifts)) ? "checked": ""?>></td>
                                        <td><input class="shift-group" type="checkbox" name="working_shifts[2][][day_id]" value ='6' <?php echo ($this->Common->workingShifts(6,2,$candidate->working_shifts)) ? "checked": ""?>></td>
                                        <td><input class="shift-group" type="checkbox" name="working_shifts[2][][day_id]" value ='7' <?php echo ($this->Common->workingShifts(7,2,$candidate->working_shifts)) ? "checked": ""?>></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">3<sup>rd</sup>shift</th>
                                        <td><input class="shift-group" type="checkbox" name="working_shifts[3][][day_id]" value ='1' <?php echo ($this->Common->workingShifts(1,3,$candidate->working_shifts)) ? "checked": ""?>></td>
                                        <td><input class="shift-group" type="checkbox" name="working_shifts[3][][day_id]" value ='2' <?php echo ($this->Common->workingShifts(2,3,$candidate->working_shifts)) ? "checked": ""?>></td>
                                        <td><input class="shift-group" type="checkbox" name="working_shifts[3][][day_id]" value ='3' <?php echo ($this->Common->workingShifts(3,3,$candidate->working_shifts)) ? "checked": ""?>></td>
                                        <td><input class="shift-group" type="checkbox" name="working_shifts[3][][day_id]" value ='4' <?php echo ($this->Common->workingShifts(4,3,$candidate->working_shifts)) ? "checked": ""?>></td>
                                        <td><input class="shift-group" type="checkbox" name="working_shifts[3][][day_id]" value ='5' <?php echo ($this->Common->workingShifts(5,3,$candidate->working_shifts)) ? "checked": ""?>></td>
                                        <td><input class="shift-group" type="checkbox" name="working_shifts[3][][day_id]" value ='6' <?php echo ($this->Common->workingShifts(6,3,$candidate->working_shifts)) ? "checked": ""?>></td>
                                        <td><input class="shift-group" type="checkbox" name="working_shifts[3][][day_id]" value ='7' <?php echo ($this->Common->workingShifts(7,3,$candidate->working_shifts)) ? "checked": ""?>></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <ul class="list-inline col-sm-12">
        <li><button type="button" class="btn btn-cancel prev-step">Previous</button></li>
        <li><button type="submit" class="btn btn-blue next-step">Next</button></li>
    </ul>
    <?= $this->Form->end()?>
