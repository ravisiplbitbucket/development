<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="myModalLabel">Edit History</h4>
</div>
<?= $this->Form->create($history,
    array(
        'type' => 'post',
        'url' => array(
            'controller' => 'Edits',
            'action' => 'edit-history',
            base64_encode($history->id)
        ),
        'id' => 'candidate-history')
    ); 
?>
<div class="modal-body clearfix">
    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label>Company  Mailing Address 1</label>
                    <?php
                        echo $this->Form->input('company_email1',
                            array(
                                'class' => 'form-control',
                                'div' => false,
                                'label' => false,
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label>Company  Mailing Address 2</label>
                    <?php
                        echo $this->Form->input('company_email2',
                            array(
                                'class' => 'form-control',
                                'div' => false,
                                'label' => false,
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label>Address</label>
                    <?php
                        echo $this->Form->input('address',
                            array(
                                'class' => 'form-control',
                                'div' => false,
                                'label' => false,
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>City</label>
                    <?php
                        echo $this->Form->input('city',
                            array(
                                'class' => 'form-control',
                                'div' => false,
                                'label' => false,
                            )
                        );
                    ?>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Zip</label>
                    <?php
                        echo $this->Form->input('zipcode',
                            array(
                                'class' => 'form-control',
                                'div' => false,
                                'label' => false,
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Is this a security guard company</label>
                    <div>
                        <label class="radio-inline">
                        <?= 
                            $this->Form->radio('security_company', [
                                ['value'=>'1','text'=>'Yes']],
                                [
                                    'label' => false,'class' => 'guard','hiddenField'=>false,
                                    ($history->security_company)?'checked':''
                                ]
                            ) 
                        ?>Yes

                        </label>
                        <label class="radio-inline">
                            <?= 
                                $this->Form->radio('security_company', [
                                    ['value'=>'0','text'=>'No','class' => 'guard']],
                                    [
                                        'label' => false,'class' => 'guard','hiddenField'=>false,
                                        ($history->security_company)?'':'checked'
                                    ]
                                ) 
                            ?>No
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-12 security-guard <?= ($history->security_company)?'':'hide' ?>">
        <div class="row">
            <div class="col-sm-12">
                <h4>
                    Please  complete    this    section to  allow   us  to  gauge   your    general work    experience
                </h4>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label>Company that you worked at</label>
                    <?php
                        echo $this->Form->input('company_name',
                            array(
                                'class' => 'form-control',
                                'div' => false,
                                'label' => false,
                                'id' => 'client-number',
                                'value' => $history->company_name
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label>Client Contact Phone Number</label>
                    <?php
                        echo $this->Form->input('client_number',
                            array(
                                'class' => 'form-control',
                                'div' => false,
                                'label' => false,
                                'id' => 'client-number',
                                'value' => $history->client_number
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Date you started at the site</label>
                    <?php
                        echo $this->Form->input('date_started',
                            array(
                                'type' => 'text',
                                'class' => 'form-control',
                                'div' => false,
                                'label' => false,
                                'id' => 'date_started',
                                'value' => ($history->date_started)?date('d/m/Y',strtotime($history->date_started)):''
                            )
                        );
                    ?>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Date you ended at the site</label>
                    <?php
                        echo $this->Form->input('date_ended',
                            array(
                                'type' => 'text',
                                'class' => 'form-control',
                                'div' => false,
                                'label' => false,
                                'id' => 'date_ended',
                                'value' => ($history->date_ended)?date('d/m/Y',strtotime($history->date_ended)):''
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-8">
                <div class="form-group">
                    <label>Were you required to submit reports?</label>
                    <div>
                        <label class="radio-inline">
                        <!-- <input class="reports" type="radio" value="1" name="required_submit_report">Yes -->
                            <?= 
                                $this->Form->radio('required_submit_report', [
                                    ['value'=>'1','text'=>'Yes']],[
                                    'label' => false,'class' => 'reports','hiddenField'=>false,
                                    ($history->required_submit_report)?'checked':''
                                ]) 
                            ?>Yes
                        </label>
                        <label class="radio-inline">
                        <!-- <input class="reports" type="radio" checked value ="0" name="required_submit_report">No -->
                            <?= 
                                $this->Form->radio('required_submit_report', [['value'=>'0','text'=>'No']],['label' => false,'class' => 'reports','hiddenField'=>false,
                                    ($history->required_submit_report)?'':'checked'
                                ]) 
                            ?>No
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row submit-reports <?= ($history->required_submit_report)?'':'hide' ?>">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>How were reports submitted</label>
                    <?php
                        echo $this->Form->select('way_report_submitted', [
                                "Handwritten" => "Handwritten",
                                'via Computer'    => 'via Computer',
                                'via App'   => 'via App'
                            ], 
                            [
                                'value' => explode(',', $history->way_report_submitted),
                                'multiple'=>"multiple",
                                'class' => 'report-submit form-control', 
                                'label' => false, 
                                'id' => 'dates-field2', 
                            ]
                        );
                    ?>
                </div>
            </div>
        </div>
        <!-- If answer  is app  or  software then this option is show: -->
        <div class="row">
            <div class="col-sm-8">
                <div class="form-group">
                    <label>Were you comfortable using   this    
                    software?   </label>
                    <div>
                        <label class="radio-inline">
                        <!-- <input class="reports" type="radio" value="1" name="required_submit_report">Yes -->
                            <?= 
                                $this->Form->radio('is_comfot_sotware', [['value'=>'1','text'=>'Yes']],[
                                        'label' => false,'class' => 'reports','hiddenField'=>false,
                                        ($history->is_comfot_sotware)?'checked':''
                                ]) 
                            ?>Yes
                        </label>
                        <label class="radio-inline">
                        <!-- <input class="reports" type="radio" checked value ="0" name="required_submit_report">No -->
                            <?= 
                                $this->Form->radio('is_comfot_sotware', [['value'=>'0','text'=>'No']],['label' => false,'class' => 'reports','hiddenField'=>false,
                                    ($history->is_comfot_sotware)?'':'checked'

                                ]) 
                            ?>No
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row ">
            <div class="col-sm-8">
                <div class="form-group">
                    <label>Did you ever have to use this equipment</label>
                    <div>
                        <label class="radio-inline">
                        <!-- <input class="equipment" type="radio" value="1" name="used_equipment">Yes -->
                            <?= 
                                $this->Form->radio('used_equipment', [['value'=>'1','text'=>'Yes']],['label' => false,'class' => 'equipment','hiddenField'=>false,
                                    ($history->used_equipment)?'checked':'']) 
                            ?>Yes
                        </label>
                        <label class="radio-inline">
                            <!-- <input class="equipment" type="radio" checked value ="0" name="used_equipment">No -->
                            <?= 
                                $this->Form->radio('used_equipment', [['value'=>'0','text'=>'No']],['label' => false,'class' => 'equipment','hiddenField'=>false,
                                    ($history->used_equipment)?'':'checked']) 
                            ?>No
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row equipment-section <?=($history->used_equipment)?'':'hide' ?>">
            <div class="col-sm-12">
                <div class="form-group">
                    <label>Please   describe    the situation and   your    response</label>
                    <?php
                        echo $this->Form->input('equipment_situation',
                            array(
                                'class' => 'form-control',
                                'div' => false,
                                'label' => false,
                                'type' => 'textarea',
                                'rows' => 5,
                                'value' => $history->client_number
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label>Did you ever have to contact emergency services</label>
                    <div>
                        <label class="radio-inline">
                        <!-- <input class="emergency-services" type="radio" value="yes" name="contact_emergency">Yes -->
                            <?= 
                                $this->Form->radio('contact_emergency', [['value'=>'1','text'=>'Yes']],['label' => false,'class' => 'emergency-services','hiddenField'=>false,
                                    ($history->contact_emergency)?'checked':'']) 
                            ?>Yes
                        </label>
                        <label class="radio-inline">
                            <!-- <input class="emergency-services" checked type="radio" checked value ="no" name="contact_emergency">No -->
                            <?= 
                                $this->Form->radio('contact_emergency', [['value'=>'0','text'=>'No']],['label' => false,'class' => 'emergency-services','hiddenField'=>false,
                                    ($history->contact_emergency)?'':'checked']) 
                            ?>No
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row emergency-services-section  <?=($history->used_equipment)?'':'hide' ?>">
            <div class="col-sm-12">
                <div class="form-group">
                    <label>Please   describe    the situation and   your    response</label>
                    <?php
                        echo $this->Form->input('emergency_situation',
                            array(
                                'class' => 'form-control',
                                'div' => false,
                                'label' => false,
                                'type' => 'textarea',
                                'rows' => 5,
                                'value' => $history->emergency_situation
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-8">
                <div class="form-group">
                    <label>Were you ever injured at this location</label>
                   <div>
                        <label class="radio-inline">
                        <!-- <input class="" type="radio" value="1" name="ever_injured">Yes -->
                            <?= 
                                $this->Form->radio('ever_injured', [['value'=>'1','text'=>'Yes']],['label' => false,'class' => 'injured','hiddenField'=>false,
                                    ($history->ever_injured)?'checked':'']) 
                            ?>Yes
                        </label>
                        <label class="radio-inline">
                        <!-- <input class="" type="radio" checked value ="0" name="ever_injured">No -->
                            <?= 
                                $this->Form->radio('ever_injured', [['value'=>'0','text'=>'No']],['label' => false,'class' => 'injured','hiddenField'=>false,
                                    ($history->ever_injured)?'':'checked']) 
                            ?>No
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row injured-section  <?=($history->ever_injured)?'':'hide' ?>">
            <div class="col-sm-12">
                <div class="form-group">
                    <label>Please   describe    the situation </label>
                    <?php
                        echo $this->Form->input('injured_situation',
                            array(
                                'class' => 'form-control',
                                'div' => false,
                                'label' => false,
                                'type' => 'textarea',
                                'rows' => 5,
                                'value' => $history->injured_situation
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-8">
                <div class="form-group">
                    <label>Were you required to operate any type of vehicle on site</label>
                    <div>
                        <label class="radio-inline">
                            <?= 
                                $this->Form->radio('operate_vehicle', [['value'=>'1','text'=>'Yes']],['label' => false,'class' => 'vehicle','hiddenField'=>false,
                                    ($history->operate_vehicle)?'checked':'']) 
                            ?>Yes
                        </label>
                        <label class="radio-inline">
                        <!-- <input class="vehicle" type="radio" checked value ="0" name="operate_vehicle">No -->
                        <?= 
                            $this->Form->radio('operate_vehicle', [['value'=>'0','text'=>'No']],['label' => false,'class' => 'vehicle','hiddenField'=>false,
                                    ($history->operate_vehicle)?'':'checked']) 
                        ?>No
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row vechicle-required <?=($history->operate_vehicle)?'':'hide' ?>">
            <div class="col-sm-12">
                <div class="form-group">
                    <label>Please check which ones</label>
                    <?php
                        echo $this->Form->select('vechicle_type', [
                                "Automobile" => "Automobile",
                                'bicycle'    => 'bicycle',
                                'Segway'   => 'Segway',
                                'other'    => 'other'
                            ], 
                            [
                                'value' => explode(',', $history->vechicle_type),
                                'multiple'=>"multiple",
                                'class' => 'report-submit form-control', 
                                'label' => false, 
                                'id' => 'dates-field2'
                            ]
                        );
                    ?>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                    <label>Were you ever    involved    in  an  accident    in  the vehicle:    </label>
                    <div>
                        <label class="radio-inline">
                            <?= 
                                $this->Form->radio('accident_vehicle', [['value'=>'1','text'=>'Yes']],['label' => false,'class' => 'accident-involved','hiddenField'=>false,
                                    ($history->accident_vehicle)?'checked':'']) 
                            ?>Yes
                        </label>
                        <label class="radio-inline">
                            <?= 
                                $this->Form->radio('accident_vehicle', [['value'=>'0','text'=>'No']],['label' => false,'class' => 'accident-involved','hiddenField'=>false,
                                    ($history->accident_vehicle)?'':'checked']) 
                            ?>No
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row accident-involved-section <?=($history->accident_vehicle)?'':'hide' ?>">
            <div class="col-sm-12">
                <div class="form-group">
                    <label>Please   describe    the situation </label>
                    <?php
                        echo $this->Form->input('vecicle_accident_situation',
                            array(
                                'class' => 'form-control',
                                'div' => false,
                                'label' => false,
                                'type' => 'textarea',
                                'rows' => 5,
                                'value' => $history->vecicle_accident_situation
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn  btn-cancel" data-dismiss="modal">Close</button>
    <button type="submit" class="btn btn-blue">Save</button>
</div>
<?= $this->Form->end()?>