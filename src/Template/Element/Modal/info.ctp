<div class="modal fade" id="messageModal" tabindex="-1" style="z-index: 1051; padding-top: 66px;" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
    <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 id="modalHead" class="modal-title text-center">Message!</h4>
            </div>
            <div class="modal-body">
                <p id= "modalText" class="text-center">Some text in the modal.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>