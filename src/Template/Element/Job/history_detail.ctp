<?php
	if(!empty($historyDetail)) {
?>
	<dl class="dl-horizontal">
	  	<dt>Company</dt>
	  	<dd><?= $historyDetail->company_name ?></dd>
	  	<dt>From</dt>
	  	<dd><?= $historyDetail->from_work ?></dd>
	  	<dt>To</dt>
	  	<dd><?= $historyDetail->to_work ?></dd>
	  	<dt>State</dt>
	  	<dd><?= $historyDetail->where_employ ?></dd>
	  	<dt>Job Title/Rank</dt>
	  	<dd><?= $historyDetail->desingnation ?></dd>
	  	<?php
	  		if($historyDetail->security_company) {
		?>		
		  	<dt>Report directly to the client contact</dt>
		  	<dd><?= ($historyDetail->report_client)?'Yes':'No' ?></dd>
		  	<dt>Client contact name</dt>
		  	<dd><?= $historyDetail->client_name ?></dd>
		  	<dt>May we contact this person as a reference?</dt>
		  	<dd><?= ($historyDetail->may_we_contact)?'Yes':'No' ?></dd>
		  	<dt>Kind of site</dt>
		  	<dd><?= $historyDetail->kind_site ?></dd>
		  	<dt>Pay rate while at the site</dt>
		  	<dd><?= $historyDetail->rait_paid ?></dd>
		  	<dt>Did you foot patrols at this site?</dt>
		  	<dd><?= ($historyDetail->foot_patrol)?'Yes':'No' ?></dd>
		  	<?php
		  		if($historyDetail->foot_patrol){
		  	?>
		  		<dt>How many hours per shift did you spend waliking</dt>
		  		<dd><?= $historyDetail->hours_per_shift ?></dd>
		  	<?php	}
		  	?>
		  	<dt>Were you required to submit reports?</dt>
		  	<dd><?= ($historyDetail->required_submit_report)?'Yes':'No' ?></dd>
		  	<?php
		  		if($historyDetail->required_submit_report){
		  	?>
		  		<dt>How were reports submitted</dt>
		  		<dd><?= $historyDetail->way_report_submitted ?></dd>
		  	<?php	}
		  	?>
		  	<dt>Which shift did you work at the site?</dt>
		  	<dd><?= $historyDetail->shift_work ?></dd>
		  	<dt>Was this site armed?</dt>
		  	<dd><?= ($historyDetail->is_site_armed)?'Yes':'No' ?></dd>
		  	<dt>Equipment that you were issued for this site</dt>
		  	<dd><?= $historyDetail->equipment_issued ?></dd>

		  	<dt>Did you ever have to use this equipment?</dt>
		  	<dd><?= ($historyDetail->used_equipment)?'Yes':'No' ?></dd>

		  	<dt>Did you ever have to contact emergency services?</dt>
		  	<dd><?= ($historyDetail->contact_emergency)?'Yes':'No' ?></dd>

		  	<dt>Were you ever injured at this location?</dt>
		  	<dd><?= ($historyDetail->ever_injured)?'Yes':'No' ?></dd>

		  	<dt>Were you required to operate any type of vehicle on site?</dt>
		  	<dd><?= ($historyDetail->operate_vehicle)?'Yes':'No' ?></dd>
		  	<?php
		  		if($historyDetail->operate_vehicle	){
		  	?>
		  		<dt>Please check which ones</dt>
		  		<dd><?= $historyDetail->vechicle_type ?></dd>
		  	<?php	}
		  	?>
		<?php
	  		}
	  	?>
	</dl>

<?php
	}else{
?>
<h3>Detail not found</h3>
<?php
	}
?>