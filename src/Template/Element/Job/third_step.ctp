<?= $this->Form->create($candidate,
        array(
            'type' => 'post',
            'url' => array(
                'controller' => 'Jobs',
                'action' => 'candidateApply',
                $jobid,$companyId,$candidateId
            ),
            'id' => 'candidate-apply-third-step'
        )
    ); 
?>
<div class="step3">
    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-12 subheading">
            <h3>Employment History</h3>
                <!-- <h3>Employment History<a data-toggle="modal" data-target="" href="#"><i class="fa fa-plus" id='historyModalIcon' aria-hidden="true"></i></a></h3> -->
                <?php echo $this->Html->link(
                    'Add Employer',
                    '#',
                    array(
                        'class' => 'btn btn-primary',
                        'data-target' => '#employment-history',
                        'data-toggle'=> "modal"
                        )
                );?>
            </div>
        </div>
    </div>
    <div id='history-list'>        
        <?php
            if(!empty($candidate['candidate_histories'])) {
                foreach ($candidate['candidate_histories'] as $key => $value) {
        ?>
                <div class="col-sm-12 form-group">                
                    <?= $this->Html->link(
                            __('<h4>'.$value['company_name'].'</h4>'),
                            'javascript:void(0)',
                            [
                                'escape' => false,
                                'class'     => 'get-history-detail',
                                'data-url'  => $this->Url->build(['controller' => 'jobs', 'action' => 'get-history', base64_encode($value['id'])])
                            ]
                        ) 
                    ?>
                </div>
        <?php           
                }
            }
        ?>
    </div>
    <div class="certification-section">
        <div class="col-sm-12 subheading">
            <h3>Certification</h3>
        </div>
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Are  you certified/licensed/commissioned to work unarmed </label>
                        <div>
                            <label class="radio-inline">
                            <input class="" type="radio" value="1" name="certified_unarmed">Yes
                            </label>
                            <label class="radio-inline">
                            <input class="" type="radio" checked value ="0" name="certified_unarmed">No
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Are  you certified/licensed/commissioned to work armed </label>
                        <div>
                            <label class="radio-inline">
                            <input class="" type="radio" value="1" name="certified_armed">Yes
                            </label>
                            <label class="radio-inline">
                            <input class="" type="radio" checked value ="0" name="certified_armed">No
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="writing-test-section">
        <div class="col-sm-12 subheading">
            <h3>Writing Test </h3>
        </div>
        <div class="col-sm-12">
            <div class="row form-group">
                <div class="col-sm-6">
                    <?php echo $this->Html->image('writing.jpg',array('class' => 'img-responsive'));?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Please describe in  detail what you see in the picture above <span class ="star">*</span></label>
                        <?php
                            echo $this->Form->input('writing_test',
                                array(
                                    'class' => 'form-control',
                                    'div' => false,
                                    'label' => false,
                                    
                                    'type' => 'textarea',
                                    'rows' => 4
                                )
                            );
                        ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label>Would you like to receive Email Alerts for other security  positions in your area?</label>
                        <div>
                            <label class="radio-inline">
                            <input class="" type="radio" value="1" name="email_alerts">Yes
                            </label>
                            <label class="radio-inline">
                            <input class="" type="radio" checked value ="0" name="email_alerts">No
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<ul class="list-inline col-sm-12">
    <li><button type="button" class="btn btn-cancel prev-step">Previous</button></li>
    <li><button type="submit" class="btn btn-blue next-step">Submit</button></li>
</ul>
<?= $this->Form->end()?>
<?php echo $this->element('Job/employment_history');?>
<div class="modal fade" id="history-detail-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="myModalLabel">History Detail</h4>
            </div>
            <div class="modal-body clearfix">
                
                
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn  btn-cancel" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-blue">Save</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="edit-history-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content clearfix">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit History</h4>
            </div>
            <div class="modal-body clearfix">
                
                
                
            </div>
        </div>
    </div>
</div>