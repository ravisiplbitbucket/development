<?= 
$this->Form->create($history,
    array(
        'type' => 'post',
        'url' => array(
            'controller' => 'Jobs',
            'action' => 'edit-history',
            base64_encode($hist_id)
        ),
        'id' => 'edit-candidate-history')
    ); 
?>
<div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Company <span class ="star">*</span></label>
                                <?php
                                    echo $this->Form->input('company_name',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                        )
                                    );
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>From <span class ="star">*</span></label>
                                <?php
                                    echo $this->Form->input('from_work',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                            'id' => 'from-work'
                                        )
                                    );
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>To <span class ="star">*</span></label>
                                <?php
                                    echo $this->Form->input('to_work',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                            'id' => 'to-work'
                                        )
                                    );
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>State <span class ="star">*</span></label>
                                <?php
                                    echo $this->Form->input('where_employ',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                        )
                                    );
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Your job title/Rank <span class ="star">*</span></label>
                                <?php
                                    echo $this->Form->input('designation',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                        )
                                    );
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Is this a security guard company</label>
                                <div>
                                    <label class="radio-inline">
                                    <input class="guard" type="radio" <?php echo ($history->security_company == 1) ? "checked": ""?> value="1" name="security_company">Yes
                                    </label>
                                    <label class="radio-inline">
                                    <input class="guard" type="radio" <?php echo ($history->security_company == 0) ? "checked": ""?> value ="0" name="security_company">No
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 security-guard  <?php echo ($history->security_company == 0) ? "hide": ""?>">
                    <div class="row">
                        <div class="col-sm-12">
                            <h4>
                                Please  complete    this    section to  allow   us  to  gauge   your    general work    experience
                            </h4>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Client company/location that you provided security at?</label>
                                <?php
                                    echo $this->Form->input('company_name',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                        )
                                    );
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Client Company address</label>
                                <?php
                                    echo $this->Form->input('client_address',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                        )
                                    );
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Client Company Phone</label>
                                <?php
                                    echo $this->Form->input('client_number',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                        )
                                    );
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Did you report directly to the client contact</label>
                                <div>
                                    <label class="radio-inline">
                                    <input class="client-contact" type="radio" <?php echo ($history->report_client == 1) ? "checked" : ""?> value="1" name="report_client">Yes
                                    </label>
                                    <label class="radio-inline">
                                    <input class="client-contact" type="radio" <?php echo ($history->report_client == 0) ? "checked" : ""?>  value ="0" name="report_client">No
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Client Contact Name <span class ="star">*</span></label>
                                <?php
                                    echo $this->Form->input('client_name',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                        )
                                    );
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>May we contact this person as a reference?</label>
                                <div>
                                    <label class="radio-inline">
                                    <input class="reference" type="radio" <?php echo ($history->may_we_contact == 1) ? "checked" : ""?>  value="1" name="may_we_contact">Yes
                                    </label>
                                    <label class="radio-inline">
                                    <input class="reference" type="radio"  <?php echo ($history->may_we_contact == 0) ? "checked" : ""?>  value ="0" name="may_we_contact">No
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Kind of site <span class ="star">*</span></label>
                                <?php
                                    echo $this->Form->select('kind_site', [
                                            "Residential" => "Residential",
                                            'Industrial'    => 'Industrial',
                                            'Office Building'   => 'Office Building',
                                            'Entertainment'    => 'Entertainment'
                                        ], 
                                        [
                                            'value' => explode(',', $history->kind_site),
                                            'multiple'=>"multiple",
                                            'class' => 'multiselect-ui form-control', 
                                            'label' => false, 
                                            'id' => 'dates-field2'
                                        ]
                                    );
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Pay rate while at the site <span class ="star">*</span></label>
                                <?php
                                    echo $this->Form->input('rate_paid',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                        )
                                    );
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Did you foot patrols at this site?</label>
                                <div>
                                    <label class="radio-inline">
                                    <input class="patrols" type="radio" <?php echo ($history->foot_patrol == 1) ? "checked" : ""?> value="1" name="foot_patrol">Yes
                                    </label>
                                    <label class="radio-inline">
                                    <input class="patrols" type="radio" <?php echo ($history->foot_patrol == 0) ? "checked" : ""?>  value ="0" name="foot_patrol">No
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row foot-patrols <?php echo ($history->foot_patrol == 1) ? "" : "hide"?>">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>How many hours per shift did you spend waliking <span class ="star">*</span></label>
                                <?php
                                    echo $this->Form->input('hours_per_shift',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                        )
                                    );
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>where you able to consistently perform the patrols</label>
                                <div>
                                    <label class="radio-inline">
                                    <input class="" type="radio" value="1" <?php echo ($history->perform_patrols == 1) ? "checked" : ""?> name="perform_patrols">Yes
                                    </label>
                                    <label class="radio-inline">
                                    <input class="" type="radio" <?php echo ($history->perform_patrols == 0) ? "checked" : ""?> value ="0" name="perform_patrols">No
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>How were rounds verfified</label>
                                <?php
                                    echo $this->Form->select('rounds_verified', [
                                            "Wand" => "Wand",
                                            'App'    => 'App',
                                            'Clock'   => 'Clock',
                                            'Not Verified'    => 'Not Verified'
                                        ], 
                                        [
                                            'value' => explode(',', $history->rounds_verified),
                                            'multiple'=>"multiple",
                                            'class' => 'multiselect-ui form-control', 
                                            'label' => false, 
                                            'id' => 'dates-field2'
                                        ]
                                    );
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="form-group">
                                <label>Were you required to submit reports?</label>
                                <div>
                                    <label class="radio-inline">
                                    <input class="reports" type="radio" value="1" <?php echo ($history->required_submit_report == 1) ? "checked" : ""?> name="required_submit_report">Yes
                                    </label>
                                    <label class="radio-inline">
                                    <input class="reports" type="radio" <?php echo ($history->required_submit_report == 0) ? "checked" : ""?> value ="0" name="required_submit_report">No
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row submit-reports <?php echo ($history->required_submit_report == 0) ? "" : "hide"?>">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>How were reports submitted</label>
                                <?php
                                    echo $this->Form->select('way_report_submitted', [
                                            "Handwritten" => "Handwritten",
                                            'via Computer'    => 'via Computer',
                                            'via App'   => 'via App'
                                        ], 
                                        [
                                            'value' => explode(',', $history->way_report_submitted),
                                            'multiple'=>"multiple",
                                            'class' => 'report-submit form-control', 
                                            'label' => false, 
                                            'id' => 'dates-field2', 
                                        ]
                                    );
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="row ">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Which shift did you work at the site?</label>
                                <?php
                                    echo $this->Form->select('shift_work', [
                                            "1st" => "1st",
                                            '2nd'    => '2nd',
                                            '3rd'   => '3rd',
                                            'Weekdays'    => 'Weekdays',
                                            'Weekends' => 'Weekends'
                                        ], 
                                        [
                                            'value' => explode(',', $history->shift_work),
                                            'multiple'=>"multiple",
                                            'class' => 'report-submit form-control', 
                                            'label' => false, 
                                            'id' => 'dates-field2'
                                        ]
                                    );
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="row ">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Was this site armed?</label>
                                <div>
                                    <label class="radio-inline">
                                    <input class="" type="radio" value="1" <?php echo ($history->is_site_armed == 1) ? "checked" : ""?> name="is_site_armed">Yes
                                    </label>
                                    <label class="radio-inline">
                                    <input class="" type="radio"  <?php echo ($history->is_site_armed == 0) ? "checked" : ""?> value ="0" name="is_site_armed">No
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row ">
                        <div class="col-sm-8">
                            <div class="form-group">
                                <label>Please check all equipment that you were issued for this site</label>
                                <?php
                                    echo $this->Form->select('equipment_issued', [
                                            "Handcuffs" => "Handcuffs",
                                            'batons'    => 'batons',
                                            'pepper spray'   => 'pepper spray',
                                            'handgun'    => 'handgun'
                                        ], 
                                        [
                                            'value' => explode(',', $history->equipment_issued),
                                            'multiple'=>"multiple",
                                            'class' => 'report-submit form-control', 
                                            'label' => false, 
                                            'id' => 'dates-field2'
                                        ]
                                    );
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="row ">
                        <div class="col-sm-8">
                            <div class="form-group">
                                <label>Did you ever have to use this equipment</label>
                                <div>
                                    <label class="radio-inline">
                                    <input class="equipment" type="radio" value="1"  <?php echo ($history->used_equipment == 1) ? "checked" : ""?> name="used_equipment">Yes
                                    </label>
                                    <label class="radio-inline">
                                    <input class="equipment" type="radio" <?php echo ($history->used_equipment == 0) ? "checked" : ""?> value ="0" name="used_equipment">No
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="form-group">
                                <label>Did you ever have to contact emergency services</label>
                                <div>
                                    <label class="radio-inline">
                                    <input class="emergency-services" type="radio" <?php echo ($history->contact_emergency == 1) ? "checked" : ""?> value="1" name="contact_emergency">Yes
                                    </label>
                                    <label class="radio-inline">
                                    <input class="emergency-services"  <?php echo ($history->contact_emergency == 0) ? "checked" : ""?> type="radio" checked value ="0" name="contact_emergency">No
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="form-group">
                                <label>Were you ever injured at this location</label>
                                <div>
                                    <label class="radio-inline">
                                    <input class="" type="radio" value="1"  <?php echo ($history->ever_injured == 1) ? "checked" : ""?> name="ever_injured">Yes
                                    </label>
                                    <label class="radio-inline">
                                    <input class="" type="radio" <?php echo ($history->ever_injured == 0) ? "checked" : ""?> value ="0" name="ever_injured">No
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="form-group">
                                <label>Were you required to operate any type of vehicle on site</label>
                                <div>
                                    <label class="radio-inline">
                                    <input class="vehicle" type="radio" <?php echo ($history->operate_vehicle == 1) ? "checked" : ""?> value="1" name="operate_vehicle">Yes
                                    </label>
                                    <label class="radio-inline">
                                    <input class="vehicle" type="radio" <?php echo ($history->operate_vehicle == 0) ? "checked" : ""?> value ="0" name="operate_vehicle">No
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row <?php echo ($history->operate_vehicle == 1) ? "" : "hide"?> vechicle-required">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Please check which ones</label>
                                <?php
                                    echo $this->Form->select('vechicle_type', [
                                            "Automobile" => "Automobile",
                                            'bicycle'    => 'bicycle',
                                            'Segway'   => 'Segway',
                                            'other'    => 'other'
                                        ], 
                                        [
                                            'value' => explode(',', $history->vechicle_type),
                                            'multiple'=>"multiple",
                                            'class' => 'report-submit form-control', 
                                            'label' => false, 
                                            'id' => 'dates-field2'
                                        ]
                                    );
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn  btn-cancel" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-blue">Save</button>
            </div>
            <?= $this->Form->end()?>
<?= $this->Form->end()?>