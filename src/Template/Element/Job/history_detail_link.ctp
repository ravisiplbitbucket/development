<div class="col-sm-12 form-group">                
    <?= $this->Html->link(
            __('<h4>'.$history['company_name'].'</h4>'),
            'javascript:void(0)',
            [
                'escape' => false,
                'class'     => 'get-history-detail',
                'data-url'  => $this->Url->build(['controller' => 'jobs', 'action' => 'get-history', base64_encode($historyIdInSession)])
            ]
        ) 
    ?>
    <?php echo $this->Html->link(
        '<i class="fa fa-pencil-square" aria-hidden="true"></i>',
        'javascript:void(0)',
        array(
            'data-url' => $this->Url->build(['controller' => 'jobs', 'action' => 'edit-history', base64_encode($historyIdInSession)]),
            'escape' => false,
            'title' => 'edit',
            'class' => 'edit-history'
            )
    );?>
</div>