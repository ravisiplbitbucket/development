<div class="tab-pane active clearfix" role="tabpanel" id="step1">
<?= $this->Form->create($candidate,
    array(
        'type' => 'post',
        'url' => array(
            'controller' => 'Jobs',
            'action' => 'set-session',
            $jobid,$companyId,$candidateId
        ),
        'id' => 'candidate-apply-first-step')
    ); 
    echo $this->Form->hidden('step',['value' => 2]);
?>
    <div class="step1">
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-12 subheading">
                    <h3>Personal Information</h3>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>First Name <span class ="star">*</span></label>
                        <?php
                            echo $this->Form->input('first_name',
                                array(
                                    'class' => 'form-control',
                                    'div' => false,
                                    'label' => false,
                                    
                                    'placeholder' => 'First Name'
                                )
                            );
                        ?>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Middle Initial</label>
                        <?php
                            echo $this->Form->input('middle_name',
                                array(
                                    'class' => 'form-control',
                                    'div' => false,
                                    'label' => false,
                                    'placeholder' => 'Middle Initial'
                                )
                            );
                        ?>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Last Name <span class ="star">*</span></label>
                        <?php
                            echo $this->Form->input('last_name',
                                array(
                                    'class' => 'form-control',
                                    'div' => false,
                                    'label' => false,
                                    
                                    'placeholder' => 'Last Name'
                                )
                            );
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Home Phone <span class ="star">*</span></label>
                        <?php
                            echo $this->Form->input('home_phone',
                                array(
                                    'class' => 'form-control',
                                    'div' => false,
                                    'label' => false,
                                    'maxlength' => 10,
                                    'placeholder' => 'Home Phone'
                                )
                            );
                        ?>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Mobile Phone</label>
                        <?php
                            echo $this->Form->input('mobile',
                                array(
                                    'class' => 'form-control',
                                    'div' => false,
                                    'label' => false,
                                    'maxlength' => 10,
                                    'placeholder' => 'Mobile Phone'
                                )
                            );
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group">
                <label>Email Address <span class ="star">*</span></label>
                <?php
                    echo $this->Form->input('email',
                        array(
                            'class' => 'form-control',
                            'div' => false,
                            'label' => false,
                            
                            'placeholder' => 'Email Address'
                        )
                    );
                ?>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group">
                <label>Address1 <span class ="star">*</span></label>
                <?php
                    echo $this->Form->input('address1',
                        array(
                            'class' => 'form-control',
                            'div' => false,
                            'label' => false,
                                                                           
                        )
                    );
                ?>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group">
                <label>Address2</label>
                <?php
                    echo $this->Form->input('address2',
                        array(
                            'class' => 'form-control',
                            'div' => false,
                            'label' => false,
                            
                        )
                    );
                ?>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label>Country <span class ="star">*</span></label>
                <?php
                    echo $this->Form->input('country_id',
                        array(
                            'class' => 'form-control',
                            'div' => false,
                            'label' => false,
                            'empty' => 'Select',
                            'options' => $country
                        )
                    );
                ?>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group state-container">
                <label>State <span class ="star">*</span></label>
                <?php
                    echo $this->Form->input('state_id',
                        array(
                            'class' => 'form-control',
                            'div' => false,
                            'label' => false,
                            
                            'options' => []
                        )
                    );
                ?>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group city-container">
                <label>City <span class ="star">*</span></label>
                <?php
                    echo $this->Form->input('city_id',
                        array(
                            'class' => 'form-control',
                            'div' => false,
                            'label' => false,
                            
                            'options' => []
                        )
                    );
                ?>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label>Zip Code <span class ="star">*</span></label>
                <?php
                    echo $this->Form->input('zipcode',
                        array(
                            'class' => 'form-control',
                            'div' => false,
                            'label' => false,
                            
                            'placeholder' => 'Zip Code'
                        )
                    );
                ?>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Years of Work Experience <span class ="star">*</span></label>
                        <?php
                            echo $this->Form->input('experience',
                                array(
                                    'class' => 'form-control',
                                    'div' => false,
                                    'label' => false
                                )
                            );
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>You are looking for <span class ="star">*</span></label>
                        <?php
                            echo $this->Form->input('job_type_id',
                                array(
                                    'class' => 'form-control',
                                    'div' => false,
                                    'label' => false,
                                    
                                    'options' => $jobtypes
                                )
                            );
                        ?>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Are you over the age of 18? <span class ="star">*</span></label>
                        <div>
                            <label class="radio-inline">
                            <input type="radio" value="1" name="age_under_eighteen">Yes
                            </label>
                            <label class="radio-inline">
                            <input type="radio" value="0" checked name="age_under_eighteen">No
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Have you ever worked for this company</label>
                        <div>
                            <label class="radio-inline">
                            <input class="worked-for-company" type="radio" value="1" name="worked_before">Yes
                            </label>
                            <label class="radio-inline">
                            <input class="worked-for-company" checked type="radio" value ="0" name="worked_before">No
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 hide workedfor-company">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>When</label>
                        <?php
                            echo $this->Form->input('when_worked',
                                array(
                                    'class' => 'form-control',
                                    'div' => false,
                                    'label' => false,
                                    'id' => 'when_worked'
                                )
                            );
                        ?>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Where</label>
                        <?php
                            echo $this->Form->input('where_worked',
                                array(
                                    'class' => 'form-control',
                                    'div' => false,
                                    'label' => false,
                                    
                                )
                            );
                        ?>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Position Held</label>
                        <?php
                            echo $this->Form->input('position',
                                array(
                                    'class' => 'form-control',
                                    'div' => false,
                                    'label' => false,
                                    
                                )
                            );
                        ?>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Reason for leaving</label>
                        <?php
                            echo $this->Form->input('reason_leaving',
                                array(
                                    'class' => 'form-control',
                                    'div' => false,
                                    'label' => false,
                                    
                                )
                            );
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Have you ever been convicted fo a crime?</label>
                        <div>
                            <label class="radio-inline">
                            <input class="convicted-of-crime" type="radio" value="1" name="convicted_crime">Yes
                            </label>
                            <label class="radio-inline">
                            <input class="convicted-of-crime" checked type="radio" value ="0" name="convicted_crime">No
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 hide crime">
            <div class="form-group">
                <label>Please explain the circumstances below without identifying the names of any other persons involed in the incident:</label>
                <?php
                    echo $this->Form->input('crime_reason',
                        array(
                            'class' => 'form-control',
                            'div' => false,
                            'label' => false,
                            
                            'type' => 'textarea',
                            'rows' => 5
                        )
                    );
                ?>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Valid Drivers Licence</label>
                        <div>
                            <label class="radio-inline">
                            <input class="licence" type="radio" value="1" name="driving_licence">Yes
                            </label>
                            <label class="radio-inline">
                            <input class="licence" type="radio" checked value ="0" name="driving_licence">No
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 hide valid-licence">
            <div class="form-group">
                <label>Driver’s License Number</label>
                <?php
                    echo $this->Form->input('licence_number',
                        array(
                            'class' => 'form-control',
                            'div' => false,
                            'label' => false,
                            
                        )
                    );
                ?>
            </div>
        </div>
        <div class="col-sm-12 hide valid-licence">
            <div class="form-group insurance-container">
                <label>State of Issuance</label>
                <?php
                    echo $this->Form->input('state_insurance_id',
                        array(
                            'class' => 'form-control',
                            'div' => false,
                            'label' => false,
                            'options' => []
                            
                        )
                    );
                ?>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Do you own a dependable automobile:</label>
                        <div>
                            <label class="radio-inline">
                            <input class="automobile" type="radio" value="1" name="dependable_automobile">Yes
                            </label>
                            <label class="radio-inline">
                            <input class="automobile" type="radio" checked value ="0" name="dependable_automobile">No
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 hide dependable-automobile">
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Make</label>
                        <?php
                            echo $this->Form->input('automobile_make',
                                array(
                                    'class' => 'form-control',
                                    'div' => false,
                                    'label' => false,
                                    
                                )
                            );
                        ?>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Model</label>
                        <?php
                            echo $this->Form->input('automobile_model',
                                array(
                                    'class' => 'form-control',
                                    'div' => false,
                                    'label' => false,
                                    
                                )
                            );
                        ?>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Year</label>
                        <?php
                            echo $this->Form->input('automobile_year',
                                array(
                                    'class' => 'form-control',
                                    'div' => false,
                                    'label' => false,
                                    
                                    'id' => 'datetimepicker1'
                                )
                            );
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <!--   <div class="col-sm-12 form-group">
            <div class="checkbox">
                <label>
                <input type="checkbox"> By checking this box you agree to the terms of the Terms of Service Agreement
                </label>
            </div>
            </div> -->
    </div>
    <ul class="list-inline col-sm-12">
        <li><button type="submit" class="btn btn-blue next-step">Next</button></li>
    </ul> 
    <?= $this->Form->end()?>
</div>