<div class="modal fade" id="employment-history" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Employment History</h4>
            </div>
            <?= $this->Form->create($candidate,
                array(
                    'type' => 'post',
                    'url' => array(
                        'controller' => 'Jobs',
                        'action' => 'save-candidate-history',
                    ),
                    'id' => 'candidate-history')
                ); 
            ?>
            <?= 
                $this->Form->hidden('candidate_id',['value' => $candidate->id])
            ?>
            <div class="modal-body clearfix">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Company <span class ="star">*</span></label>
                                <?php
                                    echo $this->Form->input('company_name',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                        )
                                    );
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>From <span class ="star">*</span></label>
                                <?php
                                    echo $this->Form->input('from_work',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                            'id' => 'from-work'
                                        )
                                    );
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>To <span class ="star">*</span></label>
                                <?php
                                    echo $this->Form->input('to_work',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                            'id' => 'to-work'
                                        )
                                    );
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>State <span class ="star">*</span></label>
                                <?php
                                    echo $this->Form->input('where_employ',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                        )
                                    );
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Your job title/Rank <span class ="star">*</span></label>
                                <?php
                                    echo $this->Form->input('designation',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                        )
                                    );
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Is this a security guard company</label>
                                <div>
                                    <label class="radio-inline">
                                    <input class="guard" type="radio" value="1" name="security_company">Yes
                                    </label>
                                    <label class="radio-inline">
                                    <input class="guard" type="radio" checked value ="0" name="security_company">No
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 security-guard hide">
                    <div class="row">
                        <div class="col-sm-12">
                            <h4>
                                Please  complete    this    section to  allow   us  to  gauge   your    general work    experience
                            </h4>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Client company/location that you provided security at?</label>
                                <?php
                                    echo $this->Form->input('client_company',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                        )
                                    );
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Client Company address</label>
                                <?php
                                    echo $this->Form->input('client_address',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                        )
                                    );
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Client Company Phone</label>
                                <?php
                                    echo $this->Form->input('client_number',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                        )
                                    );
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Did you report directly to the client contact</label>
                                <div>
                                    <label class="radio-inline">
                                    <input class="client-contact" type="radio" value="1" name="report_client">Yes
                                    </label>
                                    <label class="radio-inline">
                                    <input class="client-contact" type="radio" checked value ="0" name="report_client">No
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Client Contact Name <span class ="star">*</span></label>
                                <?php
                                    echo $this->Form->input('client_name',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                        )
                                    );
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>May we contact this person as a reference?</label>
                                <div>
                                    <label class="radio-inline">
                                    <input class="reference" type="radio" value="1" name="may_we_contact">Yes
                                    </label>
                                    <label class="radio-inline">
                                    <input class="reference" type="radio"  checked value ="0" name="may_we_contact">No
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Kind of site <span class ="star">*</span></label>
                                <select name ="kind_site" class="form-control">
                                    <option value="Residential">Residential</option>
                                    <option value="Industrial">Industrial</option>
                                    <option value="Office Building">Office Building</option>
                                    <option value="Entertainment">Entertainment</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Pay rate while at the site <span class ="star">*</span></label>
                                <?php
                                    echo $this->Form->input('rate_paid',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                        )
                                    );
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Did you foot patrols at this site?</label>
                                <div>
                                    <label class="radio-inline">
                                    <input class="patrols" type="radio" value="1" name="foot_patrol">Yes
                                    </label>
                                    <label class="radio-inline">
                                    <input class="patrols" type="radio" checked value ="0" name="foot_patrol">No
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row foot-patrols hide">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>How many hours per shift did you spend waliking <span class ="star">*</span></label>
                                <?php
                                    echo $this->Form->input('hours_per_shift',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                        )
                                    );
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>where you able to consistently perform the patrols</label>
                                <div>
                                    <label class="radio-inline">
                                    <input class="" type="radio" value="1" name="perform_patrols">Yes
                                    </label>
                                    <label class="radio-inline">
                                    <input class="" type="radio" checked value ="0" name="perform_patrols">No
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>How were rounds verfified</label>
                                <select id="dates-field2" name="rounds_verified[]" class="multiselect-ui form-control" multiple="multiple">
                                    <option value="Wand">Wand</option>
                                    <option value="App">App</option>
                                    <option value="Clock">Clock</option>
                                    <option value="Not Verified">Not Verified</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="form-group">
                                <label>Were you required to submit reports?</label>
                                <div>
                                    <label class="radio-inline">
                                    <input class="reports" type="radio" value="1" name="required_submit_report">Yes
                                    </label>
                                    <label class="radio-inline">
                                    <input class="reports" type="radio" checked value ="0" name="required_submit_report">No
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row submit-reports hide">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>How were reports submitted</label>
                                <select id="dates-field2" name="way_report_submitted[]" class="report-submit form-control" multiple="multiple">
                                    <option value="Handwritten">Handwritten</option>
                                    <option value="via Computer">via Computer</option>
                                    <option value="via App">via App</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row ">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Which shift did you work at the site?</label>
                                <select id="dates-field2" name="shift_work[]" class="report-submit form-control" multiple="multiple">
                                    <option value="1st">1st</option>
                                    <option value="2nd">2nd</option>
                                    <option value="3rd">3rd</option>
                                    <option value="Weekdays">Weekdays</option>
                                    <option value="Weekends">Weekends</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row ">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Was this site armed?</label>
                                <div>
                                    <label class="radio-inline">
                                    <input class="" type="radio" value="1" name="is_site_armed">Yes
                                    </label>
                                    <label class="radio-inline">
                                    <input class="" type="radio" checked value ="0" name="is_site_armed">No
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row ">
                        <div class="col-sm-8">
                            <div class="form-group">
                                <label>Please check all equipment that you were issued for this site</label>
                                <select id="dates-field2" name="equipment_issued[]" class="report-submit form-control" multiple="multiple">
                                    <option value="Handcuffs">Handcuffs</option>
                                    <option value="batons">batons</option>
                                    <option value="pepper spray">pepper spray</option>
                                    <option value="handgun">handgun</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row ">
                        <div class="col-sm-8">
                            <div class="form-group">
                                <label>Did you ever have to use this equipment</label>
                                <div>
                                    <label class="radio-inline">
                                    <input class="equipment" type="radio" value="1" name="used_equipment">Yes
                                    </label>
                                    <label class="radio-inline">
                                    <input class="equipment" type="radio" checked value ="0" name="used_equipment">No
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="form-group">
                                <label>Did you ever have to contact emergency services</label>
                                <div>
                                    <label class="radio-inline">
                                    <input class="emergency-services" type="radio" value="1" name="contact_emergency">Yes
                                    </label>
                                    <label class="radio-inline">
                                    <input class="emergency-services" checked type="radio" checked value ="0" name="contact_emergency">No
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="form-group">
                                <label>Were you ever injured at this location</label>
                                <div>
                                    <label class="radio-inline">
                                    <input class="" type="radio" value="1" name="ever_injured">Yes
                                    </label>
                                    <label class="radio-inline">
                                    <input class="" type="radio" checked value ="0" name="ever_injured">No
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="form-group">
                                <label>Were you required to operate any type of vehicle on site</label>
                                <div>
                                    <label class="radio-inline">
                                    <input class="vehicle" type="radio" value="1" name="operate_vehicle">Yes
                                    </label>
                                    <label class="radio-inline">
                                    <input class="vehicle" type="radio" checked value ="0" name="operate_vehicle">No
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row hide vechicle-required">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Please check which ones</label>
                                <select id="dates-field2" name="vechicle_type[]" class="report-submit form-control" multiple="multiple">
                                    <option value="Automobile">Automobile</option>
                                    <option value="bicycle">bicycle</option>
                                    <option value="Segway">Segway</option>
                                    <option value="other">other</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn  btn-cancel" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-blue">Save</button>
            </div>
            <?= $this->Form->end()?>

        </div>
    </div>
</div>