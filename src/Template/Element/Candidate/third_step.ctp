<?= $this->Form->create($candidatecomplete,
        array(
            'type' => 'post',
            'url' => array(
                'controller' => 'candidates',
                'action' => 'complete-application',
               $candId,$companyId
            ),
            'id' => 'candidate-apply-third-step'
        )
    ); 
?>
<div class="step3">
    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-12 subheading">
                <h3>Employment History</h3>
            </div>
        </div>
    </div>
    <?php
        if(!$histories->isEmpty()) {
            foreach ($histories as $key => $value) {
                # code...
    ?>
        <div class="col-sm-12 form-group">
            <h4>
                <?= 
                    $this->Html->link($value,'javascript:void(0)',[
                        'class' => 'get-history',
                        'data-url' => $this->Url->build([
                            'controller' => 'candidates', 
                            'action' =>'get-history',
                            base64_encode($key)
                        ])
                    ]) 
                ?>
            </h4>
        </div>
    <?php
            }
        }
    ?>
    <div class="certification-section">
        <div class="col-sm-12 subheading">
            <h3>Certification </h3>
        </div>
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Are  you certified/licensed/commissioned to work unarmed </label>
                        <div>
                            <label class="radio-inline">
                            <input class="certified-licensed" type="radio" value="1" <?php echo ($candidates->certified_unarmed == 1) ? "checked" : ""?> name="unarmed">Yes
                            </label>
                            <label class="radio-inline">
                            <input class="certified-licensed" type="radio" <?php echo ($candidates->certified_unarmed == 0) ? "checked" : ""?> value ="0" name="unarmed">No
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row  certified-licensed-section <?= ($candidates->certified_unarmed)?'':'hide' ?>">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>What is your License number if applicable </label>
                        <?php
                            echo $this->Form->input('unarmed_licence_number',
                                array(
                                    'class' => 'form-control',
                                    'div' => false,
                                    'label' => false,
                                )
                            );
                        ?>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Expiration   date    if  applicable </label>
                        <?php
                            echo $this->Form->input('unarmed_expiry_date',
                                array(
                                    'type' => 'text',
                                    'class' => 'form-control',
                                    'div' => false,
                                    'label' => false,
                                    'id'    => "datetimepicker15"
                                )
                            );
                        ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Are  you certified/licensed/commissioned to work armed </label>
                        <div>
                            <label class="radio-inline">
                            <input class="certified-licensed-armed" type="radio" value="1" <?php echo ($candidates->certified_armed == 1) ? "checked" : ""?> name="armed">Yes
                            </label>
                            <label class="radio-inline">
                            <input class="certified-licensed-armed" type="radio" <?php echo ($candidates->certified_armed == 0) ? "checked" : ""?> value ="0" name="armed">No
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row certified-licensed-armed-section <?= ($candidates->certified_armed)?'':'hide' ?>">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>What is your License number if applicable </label>
                        <?php
                            echo $this->Form->input('armed_licence_number',
                                array(
                                    'class' => 'form-control',
                                    'div' => false,
                                    'label' => false,
                                )
                            );
                        ?>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Expiration date if applicable</label>
                        <?php
                            echo $this->Form->input('armed_expiry_date',
                                array(
                                    'type' => 'text',
                                    'class' => 'form-control',
                                    'div' => false,
                                    'label' => false,
                                    'id'    => "armed-expiry-date"
                                )
                            );
                        ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Are  you CPR/AED Certified</label>
                        <div>
                            <label class="radio-inline">
                            <input class="cpr-aed-certified" type="radio" value="1" name="is_cpr_certified">Yes
                            </label>
                            <label class="radio-inline">
                            <input class="cpr-aed-certified" type="radio" checked value ="0" name="is_cpr_certified">No
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row hide cpr-aed-certified-section">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Expiration date</label>
                        <?php
                            echo $this->Form->input('cpr_expiry_date',
                                array(
                                    'type' => 'text',
                                    'class' => 'form-control',
                                    'div' => false,
                                    'label' => false,
                                    'id'=>'cpr-expiry-date'
                                )
                            );
                        ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Are  you TWIC certified </label>
                        <div>
                            <label class="radio-inline">
                            <input class="twic-certified" type="radio" value="1" name="is_twic_certified">Yes
                            </label>
                            <label class="radio-inline">
                            <input class="twic-certified" type="radio" checked value ="0" name="is_twic_certified">No
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row hide twic-certified-section">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Expiration date</label>
                        <?php
                            echo $this->Form->input('twic_expiry_date',
                                array(
                                    'type' => 'text',
                                    'class' => 'form-control',
                                    'div' => false,
                                    'label' => false,
                                    'id'=>'twic-exiry-date'
                                )
                            );
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="writing-test-section clearfix col-sm-12">
        <div class="row">
            <div class="col-sm-12 subheading">
                <h3>Writing Test </h3>
                <div><?php echo $candidates->writing_test ?></div>
            </div>
            <!-- <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-12 subheading">
                        <h4>Add Language    for Notice  Concerning  Consumer    Reports and investigative  <br> 
                            Consumer    reports. 
                        </h4>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label>Race/Ethnic  Group</label>
                            <div>
                                <span class="checkbox-inline"><input type="checkbox" value="">Option 1</span>
                                <span class="checkbox-inline"><input type="checkbox" value="">Option 2</span>
                                <span class="checkbox-inline"><input type="checkbox" value="">Option 3</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label>Gender</label>
                            <div>
                                <span class="checkbox-inline"><input type="checkbox" value="">Option 1</span>
                                <span class="checkbox-inline"><input type="checkbox" value="">Option 2</span>
                                <span class="checkbox-inline"><input type="checkbox" value="">Option 3</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
        </div>
    </div>
</div>
<ul class="list-inline col-sm-12">
    <li><button type="button" class="btn btn-cancel prev-step">Previous</button></li>
    <li><button type="submit" class="btn btn-blue next-step">Submit</button></li>
</ul>
<?= $this->Form->end()?>
<!-- Modal -->
<div class="modal fade" id="edit-history-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            
        </div>
    </div>
</div>