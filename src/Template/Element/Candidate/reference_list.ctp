<div class="col-sm-12 form-group">     
    <h4>
        <?= $reference->name?>
    </h4>           
    <?= $this->Html->link(
            __('<i class="fa fa-trash"></i>'),
            'javascript:void(0)',
            [
                'escape' => false,
                'class'     => 'delete-education-link',
                'data-url'  => $this->Url->build(['controller' => 'candidates', 'action' => 'delete-reference', base64_encode($referenceIdInSession)])
            ]
        ) 
    ?>
</div>