<div class="modal fade" id="employment-history" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Employment History</h4>
            </div>
            <?= $this->Form->create($history,
                array(
                    'type' => 'post',
                    'url' => array(
                        'controller' => 'Jobs',
                        'action' => 'save-candidate-history',
                    ),
                    'id' => 'candidate-history')
                ); 
            ?>
            <div class="modal-body clearfix">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Company  Mailing Address 1</label>
                                <input type="text" class="form-control">
                                <?php
                                    echo $this->Form->input('rate_paid',
                                        array(
                                            'class' => 'form-control',
                                            'div' => false,
                                            'label' => false,
                                        )
                                    );
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Company  Mailing Address 2</label>
                                <input type="text" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>City</label>
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Zip</label>
                                <input type="text" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Is this a security guard company</label>
                                <div>
                                    <label class="radio-inline">
                                    <input class="guard" type="radio" value="yes" name="optradio">Yes
                                    </label>
                                    <label class="radio-inline">
                                    <input class="guard" type="radio" checked value ="no" name="optradio">No
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 security-guard hide">
                    <div class="row">
                        <div class="col-sm-12">
                            <h4>
                                Please  complete    this    section to  allow   us  to  gauge   your    general work    experience
                            </h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Client Contact Phone Number</label>
                                <input type="text" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Date you started at the site</label>
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Date you ended at the site</label>
                                <input type="text" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="form-group">
                                <label>Were you required to submit reports?</label>
                                <div>
                                    <label class="radio-inline">
                                    <input class="reports" type="radio" value="yes" name="optradio">Yes
                                    </label>
                                    <label class="radio-inline">
                                    <input class="reports" type="radio" checked value ="no" name="optradio">No
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row submit-reports hide">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>How were reports submitted</label>
                                <select id="dates-field2" class="report-submit form-control" multiple="multiple">
                                    <option value="cheese">Handwritten</option>
                                    <option value="tomatoes">via Computer</option>
                                    <option value="mozarella">via App</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <!-- If answer  is app  or  software then this option is show: -->
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="form-group">
                                <label>Were you comfortable using   this    
                                software?   </label>
                                <div>
                                    <label class="radio-inline">
                                    <input class="reports" type="radio" value="yes" name="optradio">Yes
                                    </label>
                                    <label class="radio-inline">
                                    <input class="reports" type="radio" checked value ="no" name="optradio">No
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row ">
                        <div class="col-sm-8">
                            <div class="form-group">
                                <label>Did you ever have to use this equipment</label>
                                <div>
                                    <label class="radio-inline">
                                    <input class="equipment" type="radio" value="yes" name="optradio">Yes
                                    </label>
                                    <label class="radio-inline">
                                    <input class="equipment" type="radio" checked value ="no" name="optradio">No
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row hide equipment-section">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Please   describe    the situation and   your    response</label>
                                <textarea class="form-control" rows="5"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Did you ever have to contact emergency services</label>
                                <div>
                                    <label class="radio-inline">
                                    <input class="emergency-services" type="radio" value="yes" name="optradio">Yes
                                    </label>
                                    <label class="radio-inline">
                                    <input class="emergency-services" type="radio" checked value ="no" name="optradio">No
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row hide emergency-services-section">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Please   describe    the situation and   your    response</label>
                                <textarea class="form-control" rows="5"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="form-group">
                                <label>Were you ever injured at this location</label>
                                <div>
                                    <label class="radio-inline">
                                    <input class="injured" type="radio" value="yes" name="optradio">Yes
                                    </label>
                                    <label class="radio-inline">
                                    <input class="injured" type="radio" checked value ="no" name="optradio">No
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row hide  injured-section">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Please   describe    the situation </label>
                                <textarea class="form-control" rows="5"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="form-group">
                                <label>Were you required to operate any type of vehicle on site</label>
                                <div>
                                    <label class="radio-inline">
                                    <input class="vehicle" type="radio" value="yes" name="optradio">Yes
                                    </label>
                                    <label class="radio-inline">
                                    <input class="vehicle" type="radio" checked value ="no" name="optradio">No
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row hide vechicle-required">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Please check which ones</label>
                                <select id="dates-field2" class="report-submit form-control" multiple="multiple">
                                    <option value="cheese">Automobile</option>
                                    <option value="tomatoes">bicycle</option>
                                    <option value="mozarella">Segway</option>
                                    <option value="mozarella">other</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Were you ever    involved    in  an  accident    in  the vehicle:    </label>
                                <div>
                                    <label class="radio-inline">
                                    <input class="accident-involved" type="radio" value="yes" name="optradio">Yes
                                    </label>
                                    <label class="radio-inline">
                                    <input class="accident-involved" type="radio" checked value ="no" name="optradio">No
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row hide  accident-involved-section">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Please   describe    the situation </label>
                                <textarea class="form-control" rows="5"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn  btn-cancel" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-blue">Save</button>
            </div>
            <?= $this->Form->end()?>
        </div>
    </div>
</div>