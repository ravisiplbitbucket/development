<div class="container employee-application">
    <div class="col-sm-12 form-group">
        <div class="col-sm-6">
            <label>Job Reference Number</label>
            <div><?php echo $candidate['job']->job_number ?></div>
        </div>
        <div class="col-sm-6 text-right">
            <label>Candidate ID</label>
            <div>CAN000<?php echo $candidate->id?></div>
        </div>
    </div>
    <div class="col-sm-12">
        <h3>Personal Information</h3>
    </div>
    <div class="col-sm-12 form-group">
        <div class="col-sm-4">
            <label>First Name</label>
            <div><?php echo $candidate->first_name ?></div>
        </div>
        <div class="col-sm-4">
            <label>Middle Initial</label>
            <div><?php echo $candidate->middle_name ?></div>
        </div>
        <div class="col-sm-4">
            <label>Last Name</label>
            <div><?php echo $candidate->last_name ?></div>
        </div>
    </div>
    <div class="col-sm-12 form-group">
        <div class="col-sm-4">
            <label>Home Phone</label>
            <div><?php echo $candidate->home_phone ?></div>
        </div>
        <div class="col-sm-4">
            <label>Mobile Phone</label>
            <div><?php echo $this->Common->valueExist($candidate->mobile); ?></div>
        </div>
        <div class="col-sm-4">
            <label>Email Address</label>
            <div><?php echo $candidate->email ?></div>
        </div>
    </div>
    <div class="col-sm-12 form-group">
        <div class="col-sm-6">
            <label>Address 1</label>
            <div><?php echo $this->Common->valueExist($candidate->address1);?></div>
        </div>
        <div class="col-sm-6">
            <label>Address 1</label>
            <div><?php echo $this->Common->valueExist($candidate->address2); ?></div>
        </div>
    </div>
    <div class="col-sm-12 form-group">
        <div class="col-sm-3">
            <label>Country</label>
            <div><?php echo  $this->Common->valueExist($candidate['country']->name); ?></div>
        </div>
        <div class="col-sm-3">
            <label>State</label>
            <div><?php echo $this->Common->valueExist($candidate['state']->name);?></div>
        </div>
        <div class="col-sm-3">
            <label>City</label>
            <div><?php echo $this->Common->valueExist($candidate['city']->name);?></div>
        </div>
        <div class="col-sm-3">
            <label>Zip Code</label>
            <div><?php echo $this->Common->valueExist($candidate->zipcode); ?></div>
        </div>
    </div>
    <div class="col-sm-12 form-group">
        <div class="col-sm-4">
            <label>Experience</label>
            <div><?php echo $this->Common->valueExist($candidate->experience); ?></div>
        </div>
        <div class="col-sm-4">
            <label>You are looking for</label>
            <div><?php echo $this->Common->valueExist($candidate['job_type']->job_name); ?></div>
        </div>
        <div class="col-sm-4">
            <label>Are you Under the Age of 18</label>
            <div><?php echo ($candidate->age_under_eighteen == 1) ? "Yes" : "No" ?></div>
        </div>
    </div>
    <div class="col-sm-12 form-group">
        <div class="col-sm-12 form-group">
            <label>Have you ever worked for this company</label>
            <div><?php echo ($candidate->worked_before == 1) ? "Yes" : "No" ?></div>
        </div>
        <?php if($candidate->worked_before == 1 ) { ?>
        <div class="col-sm-6 form-group">
            <label>When</label>
            <div><?php echo $this->Common->valueExist($candidate->when_worked);?></div>
        </div>
        <div class="col-sm-6 form-group">
            <label>Where</label>
            <div><?php echo $this->Common->valueExist($candidate->where_worked); ?></div>
        </div>
        <div class="col-sm-6 form-group">
            <label>Postion Held</label>
            <div><?php echo $this->Common->valueExist($candidate->position); ?></div>
        </div>
        <div class="col-sm-6 form-group">
            <label>Reason for Leaving</label>
            <div><?php echo $this->Common->valueExist($candidate->reason_leaving); ?></div>
        </div>
        <?php } ?>
    </div>
    <div class="col-sm-12 form-group">
        <div class="col-sm-12 form-group">
            <label>Have you ever been convicted fo a crime?</label>
            <div><?php echo ($candidate->convicted_crime == 1) ? "Yes" : "No" ?></div>
        </div>
        <?php if($candidate->convicted_crime == 1) { ?>
        <div class="col-sm-12">
            <label>Please explain the circumstances below without identifying the names of any other persons involed in the incident:</label>
            <div><?php echo $this->Common->valueExist($candidate->crime_reason); ?></div>
        </div>
        <?php } ?>
    </div>
    <div class="col-sm-12 form-group">
        <div class="col-sm-12 form-group">
            <label>Valid Drivers Licence</label>
            <div><?php echo ($candidate->driving_licence == 1) ? "Yes" : "No" ?></div>
        </div>
        <?php if($candidate->driving_licence == 1) { ?>
        <div class="col-sm-12">
            <label>Driver’s License Number</label>
            <div><?php echo $this->Common->valueExist($candidate->licence_number); ?></div>
        </div>
        <?php } ?>
    </div>
    <div class="col-sm-12 form-group">
        <div class="col-sm-12">
            <label>Do you own a dependable automobile:</label>
            <div><?php echo ($candidate->dependable_automobile == 1) ? "Yes" : "No" ?></div>
        </div>
        <?php if($candidate->dependable_automobile == 1) { ?>
        <div class="col-sm-4">
            <label>Make</label>
            <div><?php echo $this->Common->valueExist($candidate->automobile_make); ?></div>
        </div>
        <div class="col-sm-4">
            <label>Modal</label>
            <div><?php echo $this->Common->valueExist($candidate->automobile_model); ?></div>
        </div>
        <div class="col-sm-4">
            <label>Year</label>
            <div><?php echo $this->Common->valueExist($candidate->automobile_year); ?></div>
        </div>
        <?php } ?>
    </div>
    <div class="col-sm-12 form-group">
        <div class="col-sm-6">
            <label>Social Security#</label>
            <div><?php echo $this->Common->valueExistInArray($candidate['candidate_completes'],null,'social_security');?></div>
        </div>
        <div class="col-sm-6">
            <label>Position Applied For</label>
            <div><?php echo $this->Common->valueExist($candidate['job']->job_title); ?></div>
        </div>
    </div>
    <div class="col-sm-12 form-group">
        <div class="col-sm-6">
            <label>List names of any relatives employed by our company</label>
            <div><?php echo $this->Common->valueExistInArray($candidate['candidate_completes'],null,'relative_employed'); ?></div>
        </div>
        <div class="col-sm-6">
            <label>What Languages do you speak</label>
            <div><?php echo (!empty($language)) ? implode(', ',$language) : "No language selected" ?></div>
        </div>
    </div>
    <div class="col-sm-12">
        <h3>Work Availability</h3>
    </div>
    <div class="col-sm-12 form-group">
        <div class="col-sm-6">
            <label>Date available to begin working</label>
            <div><?php echo (!empty($candidate->date_begin_work)) ? date('m-d-Y',strtotime($candidate->date_begin_work)) : "-" ?></div>
        </div>
        <div class="col-sm-6">
            <label>Total hours availbale per week</label>
            <div><?php echo $this->Common->valueExist($candidate->hours_available);?> hrs</div>
        </div>
    </div>
    <div class="form-group col-sm-12">
        <?php
            foreach ($candidate->working_shifts as $key => $value) {
                $shifts[$value->shift_id][] =$value->day_id; 
            }
            $shiftsOne = (array_key_exists ( 1 , $shifts ))?$shifts[1]:'';
            $shiftsTwo = (array_key_exists ( 2 , $shifts ))?$shifts[2]:'';
            $shiftsThree = (array_key_exists ( 3 , $shifts ))?$shifts[3]:'';
        ?>
        <label>
        Please check the days and shifts that you are available to work below:
        </label>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th></th>
                    <th>Sun</th>
                    <th>Mon</th>
                    <th>Tue</th>
                    <th>Wed</th>
                    <th>Thu</th>
                    <th>Fri</th>
                    <th>Sat</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th scope="row">1<sup>ST</sup>shift</th>
                    <td>
                        <span class="<?=(!empty($shiftsOne) && in_array(1,$shiftsOne))?'fa fa-check':'fa fa-times'?>"></span></td>

                    <td><span class="<?=(!empty($shiftsOne) && in_array(2,$shiftsOne))?'fa fa-check':'fa fa-times'?>"></span></td></span></td>
                    <td><span class="<?=(!empty($shiftsOne) && in_array(3,$shiftsOne))?'fa fa-check':'fa fa-times'?>"></span></td></span></td>
                    <td><span class="<?=(!empty($shiftsOne) && in_array(4,$shiftsOne))?'fa fa-check':'fa fa-times'?>"></span></td></span></td>
                    <td><span class="<?=(!empty($shiftsOne) && in_array(5,$shiftsOne))?'fa fa-check':'fa fa-times'?>"></span></td></span></td>
                    <td><span class="<?=(!empty($shiftsOne) && in_array(6,$shiftsOne))?'fa fa-check':'fa fa-times'?>"></span></td></span></td>
                    <td><span class="<?=(!empty($shiftsOne) && in_array(7,$shiftsOne))?'fa fa-check':'fa fa-times'?>"></span></td></span></td>
                </tr>
                <tr>
                    <th scope="row">2<sup>nd</sup>shift</th>
                    <td>
                    <span class="<?=(!empty($shiftsTwo) && in_array(1,$shiftsTwo))?'fa fa-check':'fa fa-times'?>"></span></td></span></td>
                    <td><span class="<?=(!empty($shiftsTwo) && in_array(2,$shiftsTwo))?'fa fa-check':'fa fa-times'?>"></span></td></span></td>
                    <td><span class="<?=(!empty($shiftsTwo) && in_array(3,$shiftsTwo))?'fa fa-check':'fa fa-times'?>"></span></td></span></td>
                    <td><span class="<?=(!empty($shiftsTwo) && in_array(4,$shiftsTwo))?'fa fa-check':'fa fa-times'?>"></span></td></span></td>
                    <td><span class="<?=(!empty($shiftsTwo) && in_array(5,$shiftsTwo))?'fa fa-check':'fa fa-times'?>"></span></td></span></td>
                    <td><span class="<?=(!empty($shiftsTwo) && in_array(6,$shiftsTwo))?'fa fa-check':'fa fa-times'?>"></span></td></span></td>
                    <td><span class="<?=(!empty($shiftsTwo) && in_array(7,$shiftsTwo))?'fa fa-check':'fa fa-times'?>"></span></td></span></td>
                </tr>
                <tr>
                    <th scope="row">3<sup>rd</sup>shift</th>
                    <td><span class="<?=(!empty($shiftsThree) && in_array(1,$shiftsThree))?'fa fa-check':'fa fa-times'?>"></span></td></span></td>
                    <td><span class="<?=(!empty($shiftsThree) && in_array(2,$shiftsThree))?'fa fa-check':'fa fa-times'?>"></span></td></span></td>
                    <td><span class="<?=(!empty($shiftsThree) && in_array(3,$shiftsThree))?'fa fa-check':'fa fa-times'?>"></span></td></span></td>
                    <td><span class="<?=(!empty($shiftsThree) && in_array(4,$shiftsThree))?'fa fa-check':'fa fa-times'?>"></span></td></span></td>
                    <td><span class="<?=(!empty($shiftsThree) && in_array(5,$shiftsThree))?'fa fa-check':'fa fa-times'?>"></span></td></span></td>
                    <td><span class="<?=(!empty($shiftsThree) && in_array(6,$shiftsThree))?'fa fa-check':'fa fa-times'?>"></span></td></span></td>
                    <td><span class="<?=(!empty($shiftsThree) && in_array(7,$shiftsThree))?'fa fa-check':'fa fa-times'?>"></span></td></span></td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="clearfix">
        <div class="col-sm-12">
            <h3>Education</h3>
        </div>
        <div class="col-sm-12">
            <div class="form-group">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Name &amp; Address of School</th>
                            <th>From mo/yr</th>
                            <th>To mo/yr</th>
                            <th>Highest Grade Completed</th>
                            <th>Degree/Major</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(!empty($candidate['candidate_completes'][0]['candidate_educations'])){ 
                            foreach($candidate['candidate_completes'][0]['candidate_educations'] as $education) {
                        ?>
                        <tr>
                            <th scope="row"><?php echo $education['education']->education_name ?></th>
                            <td><?php echo $education->from_year ?></td>
                            <td><?php echo $education->to_year ?></td>
                            <td><?php echo $education->grade ?></td>
                            <td><?php echo $education->degree ?></td>
                        </tr>
                        <?php } } else { ?>
                        <tr>
                            <td>
                                No Record Found
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="clearfix">
        <div class="col-sm-12">
            <h3>RECORD OF US MILITARY AND RESERVE STATUS</h3>
        </div>
        <div class="col-sm-12 form-group">
            <div class="col-sm-4">
                <label>From</label>
                <div><?php echo $this->Common->valueExistInArray($candidate['candidate_completes'], 'candidate_military', 'military_from_year'); ?></div>
            </div>
            <div class="col-sm-4">
                <label>To</label>
                <div><?php echo $this->Common->valueExistInArray($candidate['candidate_completes'],'candidate_military','military_to_year'); ?></div>
            </div>
            <div class="col-sm-4">
                <label>Branch</label>
                <div><?php echo $this->Common->valueExistInArray($candidate['candidate_completes'],'candidate_military','branch');?></div>
            </div>
        </div>
        <div class="col-sm-12 form-group">
            <div class="col-sm-6">
                <label>Reserve Unit</label>
                <div><?php echo $this->Common->valueExistInArray($candidate['candidate_completes'],'candidate_military','reserver_unit'); ?></div>
            </div>
            <div class="col-sm-6">
                <label> Meeting Dates</label>
                <div><?php echo (!empty($candidate['candidate_completes'][0]['candidate_military']->meeting_date)) ? date('m-d-Y',strtotime($candidate['candidate_completes'][0]['candidate_military']->meeting_date)) : "-" ; ?></div>
            </div>
        </div>
        <div class="col-sm-12 form-group">
            <div class="col-sm-12">
                <label>Summarize Skills, training or qualifications</label>
                <div><?php echo $this->Common->valueExistInArray($candidate['candidate_completes'],'candidate_military','skills'); ?></div>
            </div>
        </div>
    </div>
    <div class="clearfix">
        <div class="col-sm-12">
            <h3>PROFESSIONAL/PERSONAL REFERENCES</h3>
        </div>
        <div class="col-sm-12">
            <div class="form-group">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Address</th>
                            <th>Phone</th>
                            <th>Occupation</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(!empty($candidate['candidate_completes'][0]['candidate_references'])) { 
                            foreach($candidate['candidate_completes'][0]['candidate_references'] as $reference) {
                        ?>
                        <tr>
                            <td><?php echo $reference->name ?></td>
                            <td><?php echo $reference->address ?></td>
                            <td><?php echo $reference->phone ?></td>
                            <td><?php echo $reference->job ?></td>
                        </tr>
                        <?php } } else { ?>
                        <tr>
                            <td>
                                No Record Found
                            </td>
                        </tr>
                       <?php  } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <?php if(!empty($candidate['candidate_histories'])) {
            foreach($candidate['candidate_histories'] as $history) { 
    ?>
    <div class="clearfix">
        <div class="col-sm-12">
            <h3>Employment History</h3>
        </div>
        <div class="col-sm-12 form-group">
            <div class="col-sm-4">
                <label>Employer</label>
                <div><?php echo $history->company_name ?></div>
            </div>
            <div class="col-sm-4">
                <label>From</label>
                <div><?php echo $history->from_work ?></div>
            </div>
            <div class="col-sm-4">
                <label>To</label>
                <div><?php $history->to_work ?></div>
            </div>
        </div>
        <div class="col-sm-12 form-group">
            <div class="col-sm-6">
                <label>State</label>
                <div><?php echo $history->where_employ ?></div>
            </div>
            <div class="col-sm-6">
                <label>Your job title/Rank</label>
                <div><?php echo $history->designation ?></div>
            </div>
        </div>
        <!-- div class="col-sm-12 form-group">
            <div class="col-sm-12">
                <label>Is this a security guard company</label>
                <div><?php echo ($history->security_company == 1 && (!empty($history->security_company))) ? "Yes" : "No"?></div>
            </div>
        </div>
        <div class="col-sm-12">
            <h4>Please complete this section to allow us to gauge your general work experience</h4>
        </div>
        <div class="col-sm-12 form-group">
            <div class="col-sm-4">
                <label>Did you report directly to the client contact</label>
                <div><?php echo ($history->report_client == 1 && (!empty($history->report_client))) ? "Yes" : "No" ?></div>
            </div>
            <div class="col-sm-4">
                <label>Client Contact Name</label>
                <div><?php echo (!empty($history->client_name)) ? $history->client_name : "-" ?></div>
            </div>
            <div class="col-sm-4">
                <label>May we contact this person as a reference?</label>
                <div><?php echo ($history->may_we_contact == 1 && (!empty($history->may_we_contact))) ? "Yes" : "No" ?></div>
            </div>
        </div>
        <div class="col-sm-12 form-group">
            <div class="col-sm-4">
                <label>Kind of site</label>
                <div><?php echo $this->Common->valueExist($history->kind_site); ?></div>
            </div>
            <div class="col-sm-4">
                <label>Pay rate while at the site</label>
                <div>$ <?php echo $this->Common->valueExist($history->rate_paid); ?></div>
            </div>
            <div class="col-sm-4">
                <label>Did you foot patrols at this site?</label>
                <div><?php echo ($history->foot_patrol == 1 && (!empty($history->foot_patrol))) ? "Yes" : "No" ?></div>
            </div>
        </div>
        <div class="col-sm-12 form-group">
            <div class="col-sm-4">
                <label>How many hours per shift did you spend waliking</label>
                <div><?php echo $this->Common->valueExist($history->hours_per_shift);?></div>
            </div>
            <div class="col-sm-4">
                <label>where you able to consistently perform the patrols</label>
                <div><?php echo ($history->perform_patrols == 1 && (!empty($history->perform_patrols))) ? "Yes" : "No"?></div>
            </div>
            <div class="col-sm-4">
                <label>How were rounds verfified</label>
                <div><?php echo $this->Common->valueExist($history->rounds_verified); ?></div>
            </div>
        </div>
        <div class="col-sm-12 form-group">
            <div class="col-sm-12">
                <label>Were you required to submit reports?</label>
                <div><?php echo ($history->required_submit_report == 1 && (!empty($history->required_submit_report))) ? "Yes" : "No" ?></div>
            </div>
        </div>
        <div class="col-sm-12 form-group">
            <div class="col-sm-12">
                <label>How were reports submitted</label>
                <div><?php echo $this->Common->valueExist($history->way_report_submitted); ?></div>
            </div>
        </div>
        <div class="col-sm-12 form-group">
            <div class="col-sm-6">
                <label>Which shift did you work at the site?</label>
                <div><?php echo $this->Common->valueExist($history->shift_work);?></div>
            </div>
            <div class="col-sm-6">
                <label>Was this site armed?</label>
                <div><?php echo ($history->is_site_armed == 1 && (!empty($history->is_site_armed))) ? "Yes" : "No" ?></div>
            </div>
        </div>
        <div class="col-sm-12 form-group">
            <div class="col-sm-6">
                <label>Please check all equipment that you were issued for this site</label>
                <div><?php echo $this->Common->valueExist($history->equipment_issued); ?></div>
            </div>
            <div class="col-sm-6">
                <label>Did you ever have to use this equipment</label>
                <div><?php echo ($history->used_equipment == 1 && (!empty($history->used_equipment))) ? "Yes" : "No"?></div>
            </div>
        </div>
        <div class="col-sm-12 form-group">
            <div class="col-sm-6">
                <label>Did you ever have to contact emergency services</label>
                <div><?php echo ($history->contact_emergency == 1 && (!empty($history->contact_emergency))) ? "Yes" : "No"?></div>
            </div>
            <div class="col-sm-6">
                <label>Were you ever injured at this location</label>
                <div><?php echo ($history->ever_injured == 1 && (!empty($history->ever_injured))) ? "Yes" : "No"?></div>
            </div>
        </div>
        <div class="col-sm-12 form-group">
            <div class="col-sm-6">
                <label>Were you required to operate any type of vehicle on site</label>
                <div><?php echo ($history->operate_vehicle == 1 && (!empty($history->operate_vehicle))) ? "Yes" : "No"?></div>
            </div>
            <div class="col-sm-6">
                <label>Please check which ones</label>
                <div><?php echo $this->Common->valueExist($history->vechicle_type);?></div>
            </div>
        </div>
        <div class="col-sm-12 form-group">
            <div class="col-sm-6">
                <label>Company Mailing Address 1</label>
                <div><?php echo $this->Common->valueExist($history->company_email1); ?></div>
            </div>
            <div class="col-sm-6">
                <label>Company Mailing Address 2</label>
                <div><?php echo $this->Common->valueExist($history->company_email2); ?></div>
            </div>
        </div>
        <div class="col-sm-12 form-group">
            <div class="col-sm-6">
                <label>City</label>
                <div><?php echo $this->Common->valueExist($history->city); ?></div>
            </div>
            <div class="col-sm-6">
                <label>Zip</label>
                <div><?php echo $this->Common->valueExist($history->zipcode); ?></div>
            </div>
        </div>
        <div class="col-sm-12 form-group">
            <div class="col-sm-12">
                <label>Client Contact Phone Number</label>
                <div><?php echo $this->Common->valueExist($history->client_number);?></div>
            </div>
        </div>
        <div class="col-sm-12 form-group">
            <div class="col-sm-6">
                <label>Date you started at the site</label>
                <div><?php echo (!empty($history->date_started)) ? date('m-d-Y',strtotime($history->date_started)) : "-" ?></div>
            </div>
            <div class="col-sm-6">
                <label>Date you ended at the site</label>
                <div><?php echo (!empty($history->date_ended)) ? date('m-d-Y',strtotime($history->date_ended)) : "-" ?></div>
            </div>
        </div>
        <div class="col-sm-12 form-group">
            <div class="col-sm-12">
                <label>Were you comfortable using this software?</label>
                <div><?php echo ($history->is_comfot_sotware == 1 && (!empty($history->is_comfot_sotware))) ? "Yes" : "No" ?></div>
            </div>
        </div>
        <div class="col-sm-12 form-group">
            <div class="col-sm-12">
                <label>Did you ever have to use this equipment</label>
                <div><?php echo ($history->used_equipment == 1 && (!empty($history->used_equipment))) ? "Yes" : "No"?></div>
            </div>
        </div>
        <div class="col-sm-12 form-group">
            <div class="col-sm-12">
                <label>Please describe the situation and your response</label>
                <div><?php echo $this->Common->valueExist($history->equipment_situation); ?></div>
            </div>
        </div>
        <div class="col-sm-12 form-group">
            <div class="col-sm-12">
                <label>Did you ever have to contact emergency services</label>
                <div><?php echo ($history->contact_emergency == 1 && (!empty($history->contact_emergency))) ? "Yes" : "No" ?></div>
            </div>
        </div>
        <div class="col-sm-12 form-group">
            <div class="col-sm-12">
                <label>Please describe the situation and your response</label>
                <div><?php echo $this->Common->valueExist($history->emergency_situation); ?></div>
            </div>
        </div>
        <div class="col-sm-12 form-group">
            <div class="col-sm-12">
                <label>Were you ever injured at this location</label>
                <div><?php echo ($history->ever_injured == 1 && !(empty($history->ever_injured))) ? "Yes" : "No" ?></div>
            </div>
        </div>
        <div class="col-sm-12 form-group">
            <div class="col-sm-12">
                <label>Please describe the situation</label>
                <div><?php echo $this->Common->valueExist($history->injured_situation); ?></div>
            </div>
        </div>
        <div class="col-sm-12 form-group">
            <div class="col-sm-12">
                <label>Were you ever involved in an accident in the vehicle:</label>
                <div><?php echo ($history->accident_vehicle == 1 && (!empty($history->accident_vehicle))) ? "Yes" : "No" ?></div>
            </div>
        </div>
        <div class="col-sm-12 form-group">
            <div class="col-sm-12">
                <label>Please describe the situation</label>
                <div><?php echo $this->Common->valueExist($history->vecicle_accident_situation); ?></div>
            </div>
        </div> -->
    </div>
    <?php } } else { ?>
        <div class="clearfix">
            <div class="col-sm-12">
            No employment history
            </div>
        </div>
    <?php } ?>
    <div class="clearfix">
        <div class="col-sm-12">
            <h3>Certification</h3>
        </div>
        <div class="col-sm-12 form-group">
            <div class="col-sm-12">
                <label>Are you certified/licensed/commissioned to work unarmed</label>
                <div><?php echo ($candidate->certified_unarmed == 1) ? "Yes" : "No" ?></div>
            </div>
        </div>
        <div class="col-sm-12 form-group">
            <div class="col-sm-6">
                <label>What is your License number if applicable</label>
                <div><?php echo $this->Common->valueExistInArray($candidate['candidate_completes'],null,'unarmed_licence_number');?></div>
            </div>
            <div class="col-sm-6">
                <label>What is your License expiration date</label>
                <div><?php echo (!empty($candidate['candidate_completes'][0]->unarmed_expiry_date)) ? date('m-d-Y',strtotime($candidate['candidate_completes'][0]->unarmed_expiry_date)) : "-" ?></div>
            </div>
        </div>
        <div class="col-sm-12 form-group">
            <div class="col-sm-12">
                <label>Are you certified/licensed/commissioned to work armed</label>
                <div><?php echo ($candidate->certified_armed == 1) ? "Yes" : "No" ?></div>
            </div>
        </div>
        <div class="col-sm-12 form-group">
            <div class="col-sm-6">
                <label>What is your License number if applicable</label>
                <div><?php echo $this->Common->valueExistInArray($candidate['candidate_completes'],null,'armed_licence_number'); ?></div>
            </div>
            <div class="col-sm-6">
                <label>What is your License expiration date</label>
                <div><?php echo $this->Common->valueExistInArray($candidate['candidate_completes'],null,'armed_expiry_date');?></div>
            </div>
        </div>
        <div class="col-sm-12 form-group">
            <div class="col-sm-6">
                <label>Are you CPR/AED Certified</label>
                <div><?php 
                    echo ((!empty($candidate['candidate_completes'][0]->is_cpr_certified)) && $candidate['candidate_completes'][0]->is_cpr_certified == 1) ? "Yes" : "No" ;
                   ?>
                </div>
            </div>
            <div class="col-sm-6">
                <label>Expiration date</label>
                <div><?php echo (!empty($candidate['candidate_completes'][0]->cpr_expiry_date)) ? date('m-d-Y',strtotime($candidate['candidate_completes'][0]->cpr_expiry_date)) : "-" ?></div>
            </div>
        </div>
        <div class="col-sm-12 form-group">
            <div class="col-sm-6">
                <label>Are you TWIC certified</label>
                <div><?php 
                        echo ((!empty($candidate['candidate_completes'][0]->is_twic_certified)) && $candidate['candidate_completes'][0]->is_twic_certified == 1) ? "Yes" : "No" ;
                    ?>
                </div>
            </div>
            <div class="col-sm-6">
                <label>Expiration date</label>
                <div><?php echo (!empty($candidate['candidate_completes'][0]->twic_expiry_date)) ? date('m-d-Y',strtotime($candidate['candidate_completes'][0]->twic_expiry_date)) : "-" ?></div>
            </div>
        </div>
    </div>
    <div class="col-sm-12">
        <h3>Writing Test</h3>
    </div>
    <div class="col-sm-12 form-group">
        <?php echo $this->Html->image('writing.jpg',array('class' => 'img-responsive'));?>
    </div>
    <div class="col-sm-12 form-group">
        <label>Please describe in detail what you see in the picture above</label>
        <div><?php echo $this->Common->valueExist($candidate->writing_test); ?></div>
    </div>
    <div class="clearfix">
        <div class="col-sm-12">
            <h3>Interview Questions</h3>
        </div>
        <?php 
        
        if(!empty($candidate['candidate_questions'])) {
        foreach($candidate['candidate_questions'] as $question) { 
        //if (!is_null($question->score)) {
        ?>
        <div class="col-sm-12 form-group">
            <div class="col-sm-12">
                <label><?php echo $question['company_delete_question']['interview_question']->name ?></label>
                <div><?php echo (!empty($question->score)) ? $question->score : '-' ?></div>
            </div>
        </div>
        <?php } } else { 
        if(!empty($questions->toArray())) {
            foreach($questions as $question_score) {
        ?>
            <div class="col-sm-12 form-group">
                <div class="col-sm-12">
                    <label><?php echo $question_score['interview_question']->name ?></label>
                </div>
            </div>
        <?php } } }?>
    </div>
</div>
<script type="text/javascript">
      window.onload = function() {
       window.print();
    };
</script>
