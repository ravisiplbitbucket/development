<?php
    echo $this->Html->css('candidate',[
        'block' => true
    ]);
?>

<div class="content-wrapper clearfix ">
<?php echo $this->Flash->render('positive');?>
    <div class="top-header clearfix">
        <div class="col-sm-9 col-xs-9 main-title">
            <a href="javascript:history.go(-1)">
                <i class="fa fa-arrow-circle-left" aria-hidden="true"></i>
            </a>
            <span>Candidate Details</span>
        </div>
       <!--  <div class="col-sm-3 col-xs-3 text-right">
            <a href="javascript:history.go(-1)">
                <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
            </a>
        </div> -->
    </div>
    <div class="col-sm-12 setup-page candidates ">
        <div class="filter-section clearfix">
            <div class="col-sm-12">
                <div class="text-right">
                    <?php echo $this->Html->link('Print Application',
                        array('controller' => 'Candidates','action' => 'printEmployee',base64_encode($candidate->id)),
                        array(
                            'class' => 'btn btn-default',
                            'target' => '_blank'
                            )
                    );?>
                    <?php echo $this->Html->link('Complete Application',
                        array('controller' => 'Candidates','action' => 'emailApplication',base64_encode($candidate->id),base64_encode($candidate->company_id)),
                        array(
                            'class' => 'btn btn-default'
                            )
                    );?>
                </div>
            </div>
            <?= $this->Form->create($candidate,array('url' => array(
                    'controller' => 'Candidates',
                    'action' => 'statusUpdate',base64_encode($candidate->id)),
                    'type' => 'file',
                    'id' => 'status')
                ); 
            ?>
            <div class="col-sm-4 form-group">
                <label>Status</label>
                <?php echo $this->Form->control('status',array(
                    'class' => 'form-control',
                    'label' => false,
                    'options' => array('0' => 'New','1' => 'Job Offered','2' =>'Rejected','3' => 'Completed Application','4' => 'Phone Screened','5' => 'Interviewed')
                    )
                );?>
            </div>
            <div class="col-sm-8 form-group btn-margin text-right">
                <label>&nbsp;</label>
                <a href="#" data-toggle='modal' data-target="#note" class="btn btn-default">Add Note</a>
                <?php echo $this->Html->link('Hire',
                    array('controller' => 'Candidates','action' => 'hire',base64_encode($candidate->id)),
                    array(
                        'class' => 'btn btn-default'
                        )
                );?>
                <?php echo $this->Html->link('Reject',
                    array('controller' => 'Candidates','action' => 'reject',base64_encode($candidate->id)),
                    array(
                        'class' => 'btn btn-default'
                        )
                );?>
            </div>
            <div class="col-sm-12">
                <label>Attach File</label>
            </div>
            <div class="col-sm-12 form-group">
                <div class="row">
                    <div class="col-sm-4 form-group">
                        <div>
                            <?php echo $this->Form->control('image',array(
                                'class' => 'form-control',
                                'label' => false,
                                'type' => 'file'
                                )
                            );?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 form-group">
                <div class="row">
                    <div class="col-sm-4 form-group">
                        <div>
                            <input class="btn btn-primary" value = "Update" type = "submit">
                        </div>
                    </div>
                </div>
            </div>
            </form>
            <div class="col-sm-12">
                <div class="pull-left">
                    <b>Job Number:</b> <?php echo $candidate['job']->job_number;?>
                </div>
                <div class="pull-right">
                    <b>Candidate Score:</b> <?php echo $points;?>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                Personal Information
                                <i class="fa fa-angle-left"></i>
                            </a>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                            <div class="col-xs-8">
                                <div class="row">
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label>First Name</label>
                                                    <div><?php echo $candidate->first_name;?></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label>Middle Initial</label>
                                                    <div><?php echo $candidate->middle_name;?></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label>Last Name</label>
                                                    <div><?php echo $candidate->last_name?></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Home Phone</label>
                                                    <div><?php echo $candidate->home_phone?></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Mobile Phone</label>
                                                    <div><?php echo $candidate->mobile;?></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label>Email Address</label>
                                            <div><?php echo $candidate->email;?></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label>Address1</label>
                                            <div><?php echo $candidate->address1?></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Country</label>
                                            <div><?php echo $candidate['country']->name;?></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>State</label>
                                            <div><?php echo $candidate['state']->name;?></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>City</label>
                                            <div><?php echo $candidate['city']->name;?></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Zip Code</label>
                                            <div><?php echo $candidate->zipcode?></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>You are looking for</label>
                                                    <div><?php echo $candidate['job_type']->job_name?></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Are you Under the Age of 18</label>
                                                    <div><?php echo (($candidate->age_under_eighteen) == 0) ? "No" : "Yes"?></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Have you ever worked for this company</label>
                                                    <div><?php echo (($candidate->worked_before) == 0) ? "No" : "Yes"?></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Have you ever been convicted fo a crime?</label>
                                                    <div><?php echo (($candidate->convicted_crime) == 0) ? "No" : "Yes"?></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Valid Drivers Licence</label>
                                                    <div><?php echo (($candidate->driving_licence) == 0) ? "No" : "Yes"?></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Do you own a dependable automobile:</label>
                                                    <div><?php echo (($candidate->dependable_automobile) == 0) ? "No" : "Yes"?></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                 <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTwo">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                              Work Availability
                              <i class="fa fa-angle-left"></i>
                            </a>
                        </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                        <div class="panel-body">
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Date available to begin working</label>
                                            <div><?php echo date('m-d-Y',strtotime($candidate->date_begin_work))?></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Total hours availbale per week</label>
                                            <div><?php echo $candidate->hours_available?> hours</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-sm-12">
                                    <?php
                                        foreach ($candidate->working_shifts as $key => $value) {
                                            $shifts[$value->shift_id][] =$value->day_id; 
                                        }
                                        $shiftsOne = (array_key_exists ( 1 , $shifts ))?$shifts[1]:'';
                                        $shiftsTwo = (array_key_exists ( 2 , $shifts ))?$shifts[2]:'';
                                        $shiftsThree = (array_key_exists ( 3 , $shifts ))?$shifts[3]:'';
                                    ?>
                                        <div class="form-group">
                                            <label>Please check the days and shifts that you are available to work below:</label>
                                            <div class="table-responsive">
                                                <table class="table table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th></th>
                                                            <th>Sun</th>
                                                            <th>Mon</th>
                                                            <th>Tue</th>
                                                            <th>Wed</th>
                                                            <th>Thu</th>
                                                            <th>Fri</th>
                                                            <th>Sat</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <th scope="row">1<sup>ST</sup>shift</th>
                                                            <td>
                                                                <span class="<?=(!empty($shiftsOne) && in_array(1,$shiftsOne))?'fa fa-check':'fa fa-times'?>"></span></td>

                                                            <td><span class="<?=(!empty($shiftsOne) && in_array(2,$shiftsOne))?'fa fa-check':'fa fa-times'?>"></span></td></span></td>
                                                            <td><span class="<?=(!empty($shiftsOne) && in_array(3,$shiftsOne))?'fa fa-check':'fa fa-times'?>"></span></td></span></td>
                                                            <td><span class="<?=(!empty($shiftsOne) && in_array(4,$shiftsOne))?'fa fa-check':'fa fa-times'?>"></span></td></span></td>
                                                            <td><span class="<?=(!empty($shiftsOne) && in_array(5,$shiftsOne))?'fa fa-check':'fa fa-times'?>"></span></td></span></td>
                                                            <td><span class="<?=(!empty($shiftsOne) && in_array(6,$shiftsOne))?'fa fa-check':'fa fa-times'?>"></span></td></span></td>
                                                            <td><span class="<?=(!empty($shiftsOne) && in_array(7,$shiftsOne))?'fa fa-check':'fa fa-times'?>"></span></td></span></td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row">2<sup>nd</sup>shift</th>
                                                            <td><span class="<?=(!empty($shiftsTwo) && in_array(1,$shiftsTwo))?'fa fa-check':'fa fa-times'?>"></span></td></span></td>
                                                            <td><span class="<?=(!empty($shiftsTwo) && in_array(2,$shiftsTwo))?'fa fa-check':'fa fa-times'?>"></span></td></span></td>
                                                            <td><span class="<?=(!empty($shiftsTwo) && in_array(3,$shiftsTwo))?'fa fa-check':'fa fa-times'?>"></span></td></span></td>
                                                            <td><span class="<?=(!empty($shiftsTwo) && in_array(4,$shiftsTwo))?'fa fa-check':'fa fa-times'?>"></span></td></span></td>
                                                            <td><span class="<?=(!empty($shiftsTwo) && in_array(5,$shiftsTwo))?'fa fa-check':'fa fa-times'?>"></span></td></span></td>
                                                            <td><span class="<?=(!empty($shiftsTwo) && in_array(6,$shiftsTwo))?'fa fa-check':'fa fa-times'?>"></span></td></span></td>
                                                            <td><span class="<?=(!empty($shiftsTwo) && in_array(7,$shiftsTwo))?'fa fa-check':'fa fa-times'?>"></span></td></span></td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row">3<sup>rd</sup>shift</th>
                                                            <td><span class="<?=(!empty($shiftsThree) && in_array(1,$shiftsThree))?'fa fa-check':'fa fa-times'?>"></span></td></span></td>
                                                            <td><span class="<?=(!empty($shiftsThree) && in_array(2,$shiftsThree))?'fa fa-check':'fa fa-times'?>"></span></td></span></td>
                                                            <td><span class="<?=(!empty($shiftsThree) && in_array(3,$shiftsThree))?'fa fa-check':'fa fa-times'?>"></span></td></span></td>
                                                            <td><span class="<?=(!empty($shiftsThree) && in_array(4,$shiftsThree))?'fa fa-check':'fa fa-times'?>"></span></td></span></td>
                                                            <td><span class="<?=(!empty($shiftsThree) && in_array(5,$shiftsThree))?'fa fa-check':'fa fa-times'?>"></span></td></span></td>
                                                            <td><span class="<?=(!empty($shiftsThree) && in_array(6,$shiftsThree))?'fa fa-check':'fa fa-times'?>"></span></td></span></td>
                                                            <td><span class="<?=(!empty($shiftsThree) && in_array(7,$shiftsThree))?'fa fa-check':'fa fa-times'?>"></span></td></span></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingThree">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                              Education
                              <i class="fa fa-angle-left"></i>
                            </a>
                        </h4>

                    </div>
                    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                        <div class="panel-body">
                             <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Name & Address of School</th>
                                            <th>From mo/yr</th>
                                            <th>To mo/yr</th>
                                            <th>Highest Grade Completed</th>
                                            <th>Degree/Major</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                            if(!empty($education)) {
                                        ?>
                                        <tr>
                                            <th scope="row"><?php echo $education['education']->education_name;?></th>
                                            <td><?php echo $education->from_year?></td>
                                            <td><?php echo $education->to_year;?></td>
                                            <td><?php echo $education->grade?></td>
                                            <td><?php echo $education->degree?></td>
                                        </tr>
                                        <?php 
                                            } else {
                                        ?>
                                        <tr>
                                            <td>
                                                No Record Found
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingFour">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                              Military Service
                              <i class="fa fa-angle-left"></i>
                            </a>
                        </h4>

                    </div>
                    <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                        <div class="panel-body">
                            <div class="col-xs-8">
                                <div class="row">
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label>From</label>
                                                    <div><?php echo (!empty($military->military_from_year)) ? $military->military_from_year : "-" ;?></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label>To</label>
                                                    <div><?php echo (!empty($military->military_to_year)) ? $military->military_to_year : "-" ;?></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label>Reserve Unit</label>
                                                    <div><?php echo (!empty($military->reserver_unit)) ? $military->reserver_unit : "-"?></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Meeting Date</label>
                                                    <div><?php echo (!empty($military->meeting_date)) ? date('m-d-Y',strtotime($military->meeting_date)) : "-"?></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Skills</label>
                                                    <div><?php echo (!empty($military->skills)) ? $military->skills : "-";?></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label>Branch</label>
                                            <div><?php echo (!empty($military->branch)) ? $military->branch : "-";?></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>                
                </div>


                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingFive">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                              References
                              <i class="fa fa-angle-left"></i>
                            </a>
                        </h4>

                    </div>
                    <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Address</th>
                                            <th>Phone</th>
                                            <th>Occupation</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if(!empty($reference)) { ?>
                                        <tr>
                                            <td>
                                                <?php echo $reference->name?>   
                                            </td>
                                            <td>
                                                <?php echo $reference->address?>   
                                             </td> 
                                            <td>
                                                <?php echo $reference->phone?>
                                            </td>
                                            <td>
                                            <?php echo $reference->occupation ?>
                                            </td>
                                        </tr>
                                        <?php } else { ?>
                                        <tr>
                                            <td>
                                                No Record Found
                                            </td>
                                        </tr>
                                     <?php  } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingSix">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                              Employment History
                              <i class="fa fa-angle-left"></i>
                            </a>
                        </h4>

                    </div>
                    <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                        <div class="panel-body">
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Date available to begin working</label>
                                            <div><?php echo date('m-d-Y',strtotime($candidate->date_begin_work));?></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Total hours availbale per week</label>
                                            <div><?php echo $candidate->hours_available;?> Hours</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label>Please check the days and shifts that you are available to work below:</label>
                                            <div class="table-responsive">
                                                <table class="table table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th></th>
                                                            <th>From</th>
                                                            <th>To</th>
                                                            <th>State</th>
                                                            <th>You job title/Rank</th>
                                                            <th>Is this a security guard company</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                     <?php if(!empty($candidate['candidate_histories'])){
                                                        foreach ($candidate['candidate_histories'] as $history) {
                                                        ?>
                                                        <tr>
                                                            <th scope="row"><?php echo $history->company_name?></th>
                                                            <td><?php echo date('m-d-Y',strtotime($history->from_work))?></td>
                                                            <td><?php echo date('m-d-Y',strtotime($history->to_work));?></td>
                                                            <td><?php echo $history->where_employ;?></td>
                                                            <td><?php echo $history->designation;?></td>
                                                            <td><?php echo ($history->security_company == 0) ? "No" : "Yes"?></td>
                                                        </tr>
                                                    <?php } } else { ?>
                                                        <tr>
                                                            <td>
                                                                No Employment History
                                                            </td>
                                                        </tr>
                                                    <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingSeven">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                              Certifications
                              <i class="fa fa-angle-left"></i>
                            </a>
                        </h4>

                    </div>
                    <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
                        <div class="panel-body">
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Are  you certified/licensed/commissioned to work unarmed </label>
                                            <div><?php echo ($candidate->certified_unarmed == 0) ? "No" : "Yes"?></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Are you certified/licensed/commissioned to work armed </label>
                                            <div><?php echo ($candidate->certified_armed == 0) ? "No" : "Yes"?></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Are you CPR/AED Certified </label>
                                            <div>
                                            <?php if(!empty($complete))
                                            {   
                                               echo ($complete->is_cpr_certified == 1) ? "yes" : "no";
                                            } else { 
                                                echo "-";
                                            }
                                            ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Are you TWIC certified </label>
                                            <div>
                                            <?php if(!empty($complete))
                                            {   
                                                echo ($complete->is_twic_certified == 1) ? "yes" : "no";
                                            } else { 
                                                echo "-";
                                            }
                                            ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingEight">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                              Writing Test
                              <i class="fa fa-angle-left"></i>
                            </a>
                        </h4>

                    </div>
                    <div id="collapseEight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEight">
                        <div class="panel-body">
                            <div class="col-sm-12">
                                <div class="row form-group">
                                    <div class="col-sm-6">
                                        <?php echo $this->Html->image('writing.jpg',array('class' => 'img-responsive'));?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Please describe in  detail what you see in the picture above  </label>
                                            <div>
                                                <?php echo $candidate->writing_test;?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label>Would you like to receive Email Alerts for other security  positions in your area?</label>
                                            <div><?php echo ($candidate->email_alerts == 0) ? "No" : "Yes"?></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingNine">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                              Notes
                              <i class="fa fa-angle-left"></i>
                            </a>
                        </h4>

                    </div>
                    <div id="collapseNine" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingNine">
                        <div class="panel-body">
                            <div class="col-sm-12">
                                <div class="clearfix form-group">
                                    <a href="" data-toggle='modal' data-target="#note" class="btn btn-default pull-right">
                                        <span class="fa fa-plus"></span>
                                        <span>Note</span>
                                    </a>
                                </div>
                                <div class="row">
                                <?php foreach($notes as $note){ ?>
                                    <div class="col-sm-12">
                                        <div class="form-group well">
                                            <label>Note from <?php echo $note->note_by;?> on <?php echo date('m-d-Y',strtotime($note->created))?></label></br>
                                            <div><?php echo $note->candidate_note;?></div>
                                        </div>
                                    </div>
                                <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTen">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTen" aria-expanded="true" aria-controls="collapseTen">
                              Interview
                              <i class="fa fa-angle-left"></i>
                            </a> 
                        </h4>

                    </div>
                    <div id="collapseTen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTen">
                        <div class="panel-body interview-question">
                            <?php $candQues = (!empty($candidateQuestion)) ? $candidateQuestion : ' ';?>
                            <?= $this->Form->create($candQues,array('url' => array(
                                        'controller' => 'Candidates',
                                        'action' => 'candidateQuestion',$candidate->id),
                                        'id' => 'candidate-question')
                                    ); 
                            ?>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <div class="table-responsive">
                                        <table class="table">                              
                                            <tbody>
                                                <?php foreach($questions as $key => $question){?>
                                                <tr>
                                                    <th>Question</th>
                                                    <th><?php echo $question['interview_question']->name;?></th>
                                                    <th>Score</th>
                                                </tr>
                                                <tr>
                                                    <th>Answer</th>
                                                    <td>
                                                        <?php echo $this->Form->hidden($key.'.candidate_id',array('value' => base64_decode($id)));?>
                                                        <?php echo $this->Form->hidden($key.'.question_id',array('value' => $question->id));?>
                                                        <?php
                                                            echo $this->Form->input($key.'.answer',
                                                                array(
                                                                    'class' => 'form-control',
                                                                    'div' => false,
                                                                    'label' => false,
                                                                    
                                                                    'type' => 'textarea',
                                                                    'rows' => 5
                                                                )
                                                            );
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                            echo $this->Form->input($key.'.score',
                                                                array(
                                                                    'class' => 'form-control score',
                                                                    'div' => false,
                                                                    'label' => false
                                                                )
                                                            );
                                                        ?>
                                                    </td>
                                                </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="pull-left">
                                   <label>Total : </label> <span class="total"></span>
                                </div>
                                <div class="pull-right">
                                    <?php
                                        echo $this->Form->button(__('Save'),[
                                            'class' => 'btn btn-primary',
                                            'type' => 'submit'
                                        ]);
                                    ?>
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $this->element('Candidate/note');?>
<?php echo $this->Html->script('User/candidate', ['block' => 'scriptBottom']); ?>