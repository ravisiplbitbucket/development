<?php use Cake\Core\Configure;?>
<div class="content-wrapper clearfix ">
<?php echo $this->Flash->render('positive');?>
    <div class="top-header clearfix">
        <div class="col-sm-6 col-xs-6 main-title">Candidates</div>
    </div>
    <div class="col-sm-12 setup-page ">
        <div class="filter-section clearfix">
            <?php
                echo $this->Form->create('search', [
                    'type' => 'get',
                    'url' => [
                        'controller' => 'Candidates',
                        'action' => 'index'
                    ]
                ]);
            ?>
            <div class="col-sm-12">
                <h3>Filters</h3>
            </div>
            <div class="col-sm-4 form-group"><label>Search By</label>
                <input type="text" name="tag_search" class="form-control" placeholder="Enter Candidate Name or Job #">
            </div>
            <div class="col-sm-12 form-group">
                <label>Refine BY</label>
            </div>
            <div class="col-sm-12 form-group">
                <div class="row">
                    <div class="col-sm-2 form-group">
                        <select  class="form-control" name="armed">
                            <option value>Select</option>
                            <option value="0">UnArmed</option>
                            <option value="1">Armed</option>
                            <option value>All</option>
                        </select>
                    </div>
                    <div class="col-sm-2 form-group">
                        <select class="form-control" name="job_type">
                            <option value>Select</option>
                            <option value="1">Full Time</option>
                            <option value="2">Part Time</option>
                            <option>All</option>
                        </select>
                    </div>
                    <div class="col-sm-2 form-group">
                        <?php
                            echo $this->Form->select('day', Configure::read('days'), [
                                'class' => 'form-control',
                                'empty' => 'Select Day',
                            ]);
                        ?>  
                    </div>
                    <div class="col-sm-2 form-group">
                        <?php
                            echo $this->Form->select('shift', $shift, [
                                'class' => 'form-control',
                                'empty' => 'Select shift',
                            ]);
                        ?>
                    </div>
                    <div class="col-sm-2 form-group">
                        <select class="form-control">
                            <option>Select</option>
                            <option>New</option>
                            <option>Phone Screened</option>
                            <option>Interviewed</option>
                            <option>Job Offered</option>
                            <option>Rejected</option>
                        </select>
                    </div>
                    <div class="col-sm-2"><button type='submit' class="btn btn-blue">Search</button></div>
                </div>
            </div>
            <!-- <?php echo $this->Form->end();?> -->
        </div>
        <div>
           <!--  <?php
                echo $this->Form->create('search', [
                    'type' => 'get',
                    'url' => [
                        'controller' => 'Candidates',
                        'action' => 'index'
                    ]
                ]);
            ?> -->
            <div class="col-sm-12 form-group">
                <label class="advanced-filters">Advanced Filters <i class="fa fa-angle-down" aria-hidden="true"></i></label>
            </div>
            <div class="col-sm-12 form-group  advanced-filter-from">
                <div class="row">
                    <div class="col-sm-2 form-group">
                         <div class="col-sm-7">
                        <label>Years of Exp</label>
                    </div>
                    <div class="col-sm-5">
                        <input name="experience" type="text" class="form-control">
                    </div>
                    </div>
                    <div class="col-sm-4 form-group">
                        <div class="col-sm-6">
                        <label>Valid Driver Licence</label>
                    </div>
                        <div class="col-sm-6">
                            <label class="radio-inline"><input type="radio" name="license" value="1">Yes</label>
                            <label class="radio-inline"><input type="radio" name="license" value="0">NO</label>
                        </div>
                    </div>
                    <div class="col-sm-4 form-group">
                          <div class="col-sm-6">
                        <label>Reliable Automobile</label>
                    </div>
                          <div class="col-sm-6">
                            <label class="radio-inline"><input type="radio" name="automobile" value="1">Yes</label>
                            <label class="radio-inline"><input type="radio" name="automobile" value="0">NO</label>
                        </div>
                    </div>
                    <div class="col-sm-2"><button type="submit" class="btn btn-blue">Search</button></div>
                </div>
            </div>
            <?php echo $this->Form->end();?>
        </div>
        <div class=" col-sm-12 table-responsive">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Date Applied</th>
                        <th>Name</th>
                        <th>Years of experience(in years)</th>
                        <th>Status</th>
                        <th>Valid Licence</th>
                        <th>Score</th>
                        <th>Job Number</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if(!empty($candidates->toArray())) {
                        foreach($candidates as $candidate) {
                        ?>
                        <tr>
                            <td><?php echo date('m-d-Y',strtotime($candidate->created));?></td>
                            <td>
                                <?php echo $this->Html->link($candidate->first_name.' '.$candidate->last_name,
                                '/Candidates/view/'.base64_encode($candidate->id)
                                );?> 
                            </td>
                            <td><?php echo $candidate->experience;?></td>
                            <?php if($candidate->status == 1) {
                                    $status = 'Hired';
                                } elseif ($candidate->status == 2) {
                                   $status = 'Rejected';
                                } elseif ($candidate->status == 3) {
                                    $status = 'Completed Application';
                                } elseif ($candidate->status == 4) {
                                    $status = 'Phone Screening';
                                } elseif ($candidate->status == 5) {
                                    $status = 'Interviewed';
                                } else {
                                    $status = 'New';
                                }
                            ?>
                            <td><?php echo $status ?></td>
                            <td><?php echo (($candidate->driving_licence) ==1) ?$candidate->licence_number : "No driving licence"?></td>
                            <td><?php echo $this->cell('Employee',['id' => $candidate->id])  ?></td>
                            <td>
                                <?php echo $this->Html->link($candidate['job']->job_number,
                                '/Candidates/jobView/'.base64_encode($candidate['job']->id),
                                array(
                                    'target'=> "_blank",
                                    'title' => 'view job'
                                    )
                                );?>
                            </td>
                        </tr>
                    <?php  } } else { ?>
                    <tr>
                        <td colspan="7" class="text-center">
                            No record found
                        </td>
                    </tr>
                       <?php } ?>
                </tbody>
            </table>
            <?php echo $this->element('pagination'); ?>
        </div>
    </div>
</div>
<?php
    echo $this->Html->script('User/candidate_search', [
        'block' => 'scriptBottom'
    ]);
?>