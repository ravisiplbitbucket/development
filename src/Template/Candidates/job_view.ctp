<div class="content-wrapper clearfix ">
    <div class="top-header clearfix">
        <div class="col-sm-6 col-xs-6 main-title">Job Description</div>
        <!-- <div class="col-sm-6 col-xs-6 text-right main-title"><button class="btn btn-blue small "><i class="fa fa-plus" aria-hidden="true"></i> Add New</button></div> -->
    </div>
    <div class="col-sm-8 setup-page">
        <div class="col-sm-12 form-group">
            <div class="col-sm-6">
                <label>Job#</label>
             </div>
            <div class="col-sm-6">   
                <div><?php echo $job->job_number;?></div>
            </div>
        </div>
        <div class="col-sm-12 form-group">
            <div class="col-sm-6">
                <label>Job Title</label>
            </div> 
            <div class="col-sm-6">   
                <div><?php echo $job->job_title;?></div>
            </div>
        </div> 
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-12 form-group">
                    <div class=" col-sm-6">
                        <label>Country</label>
                    </div>
                     <div class=" col-sm-6">
                        <div><?php echo $job['country']->name;?></div>
                    </div>
                </div>
                <div class="col-sm-12 form-group">
                    <div class="col-sm-6">
                        <label>Job State</label>
                    </div>
                    <div class="col-sm-6">    
                        <div><?php echo $job['state']->name;?></div>
                    </div> 
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-12 form-group">
                    <div class="col-sm-6">
                        <label>Job City</label>
                    </div>
                     <div class="col-sm-6">
                        <div><?php echo $job['city']->name;?></div>
                    </div>
                </div>
                <div class="col-sm-12 form-group">
                    <div class="col-sm-6">
                        <label>Job Site Zip Code</label>
                     </div> 
                     <div class="col-sm-6">  
                        <div><?php echo $job->zipcode;?></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-12 form-group">
                    <div class="col-sm-6">
                        <label>Job Type</label>
                    </div> 
                    <div class="col-sm-6">   
                        <div><?php echo $job['job_type']->job_name;?></div>
                    </div>
                </div>
                <div class="col-sm-12 form-group">
                    <div class="col-sm-6">
                        <label>Required Education</label>
                    </div> 
                     <div class="col-sm-6">   
                        <div><?php echo $job['education']->education_name?></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 form-group">
            <div class="col-sm-6">
                <label>Responsibilities  </label>
            </div> 
            <div class="col-sm-6">
               
                <div><?php echo $job->responsibility?></div>
            </div>  
        </div>
        <div class="col-sm-12 form-group">
            <div class="col-sm-6">
                <label>Qualifications</label> 
            </div> 
             <div class="col-sm-6">   
                <div><?php echo $job->qualification?></div>
            </div>
        </div>
        <div class="col-sm-12 form-group">
            <div class="col-sm-6">
                <label>Education and/or Experience</label>
            </div>  
            <div class="col-sm-6">   
                <div><?php echo $job->experience?></div>
            </div>
        </div>
        <div class="col-sm-12 form-group">
            <div class="col-sm-6">
                <label>Certificates/Licenses/Registrations</label>
            </div>
             
            <div class="col-sm-6">   
                <div><?php echo $job->certification?></div>
            </div>
        </div>
        <div class="col-sm-12 form-group">
            <div class="col-sm-6">
                <label>Language Skills</label>
            </div>
            <div class="col-sm-6">    
                <div><?php echo $job->language_skills?></div>
            </div>
        </div>

        <div class="col-sm-12 form-group">
            <div class=" col-sm-6">
                <label>Other Qualifications</label>
            </div> 
            <div class=" col-sm-6">   
                <div><?php echo $job->other_qualification?></div>
            </div>
        </div>
        <div class="col-sm-12 form-group">
            <div class="col-sm-6">
                <label>Physical Demands</label>
            </div>
             <div class="col-sm-6">    
                <div><?php echo $job->physical_demands?></div> 
            </div>
        </div>
        <div class="col-sm-12 form-group"> 

                <div class="col-sm-6">
                    <label>Salary & Benefits </label>
                </div>    
                    <div class="col-sm-6">
                        $<?php echo $job->salary_from?> - $<?php echo $job->salary_to ?> per month
                    </div>
            </div>
        </div> 
    </div>
</div> 