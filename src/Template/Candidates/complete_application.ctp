<div class="outer-bg">
    <div class="container employee-application">
        <div class="col-sm-10 col-sm-offset-1">
            <div class="row">
                <h1 class="">Employee Application </h1>
            </div>
        </div>
        <div class="wizard col-sm-10 col-sm-offset-1">
            <div class="wizard-inner">
                <div class="connecting-line"></div>
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Step 1">
                        <span class="round-tab">
                        Step 1
                        </span>
                        </a>
                    </li>
                    <li role="presentation" class="disabled">
                        <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="Step 2">
                        <span class="round-tab">
                        Step 2
                        </span>
                        </a>
                    </li>
                    <li role="presentation" class="disabled">
                        <a href="#step3" data-toggle="tab" aria-controls="step3" role="tab" title="Step 3">
                        <span class="round-tab">
                        Step 3
                        </span>
                        </a>
                    </li>
                </ul>
            </div>
                <div class="tab-content">
                        <?= $this->element('Candidate/first_step') ?>
                    <div class="tab-pane clearfix" role="tabpanel" id="step2">
                        <?= $this->element('Candidate/second_step') ?>
                    </div>
                    <div class="tab-pane clearfix" role="tabpanel" id="step3">
                        <?= $this->element('Candidate/third_step') ?>
                    </div>
                </div>
        </div>
    </div>
</div>
<?= $this->element('Candidate/history') ?>
<?= $this->Html->script('User/complete', ['block' => 'scriptBottom']) ?>