<div class="content-wrapper clearfix ">
    <div class="top-header clearfix">
        <div class="col-sm-9 col-xs-9 main-title">
            <a href="javascript:history.go(-1)">
            <i class="fa fa-arrow-circle-left" aria-hidden="true"></i>
            </a>
            <span>Employee Expiration</span>
        </div>
        <!-- <div class="col-sm-3 col-xs-3 text-right">
            <a href="javascript:history.go(-1)">
            <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
            </a>
        </div> -->
    </div>
    <div class="col-sm-12 setup-page candidates ">
        <div class="col-sm-12">
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            Expiry in 120 - 90 days
                            <i class="fa fa-angle-left"></i>
                            </a>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                            <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Last Name</th>
                                        <th>First Name</th>
                                        <th>Employee #</th>
                                        <th>Certification</th>
                                        <th>Expiration Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if(!empty($expirationDates120)) { 
                                            foreach($expirationDates120 as $expired120) {
                                        ?>
                                    <tr>
                                        <td><?php echo $expired120['candidate']->last_name?></td>
                                        <td><?php echo $expired120['candidate']->first_name?></td>
                                        <td><?php echo 'EMP0000'.$expired120['candidate']->id?></td>
                                        <td><?php echo $expired120['certification']->name?></td>
                                        <td><?php echo date('m-d-Y',strtotime($expired120->expiration_date));?></td>
                                    </tr>
                                    <?php } } else { ?>
                                        <tr>
                                            <td>No Record Found</td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTwo">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            Expiry in 89 - 60 days
                            <i class="fa fa-angle-left"></i>
                            </a>
                        </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
                        <div class="panel-body">
                              <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Last Name</th>
                                        <th>First Name</th>
                                        <th>Employee #</th>
                                        <th>Certification</th>
                                        <th>Expiration Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if(!empty($expirationDates89)) { 
                                            foreach($expirationDates89 as $expired89) {
                                        ?>
                                    <tr>
                                        <td><?php echo $expired89['candidate']->last_name?></td>
                                        <td><?php echo $expired89['candidate']->first_name?></td>
                                        <td><?php echo 'EMP0000'.$expired89['candidate']->id?></td>
                                        <td><?php echo $expired89['certification']->name?></td>
                                        <td><?php echo date('m-d-Y',strtotime($expired89->expiration_date));?></td>
                                    </tr>
                                    <?php } } else { ?>
                                        <tr>
                                            <td>No Record Found</td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingThree">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                            Expiry in 59 - 45 days
                            <i class="fa fa-angle-left"></i>
                            </a>
                        </h4>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingThree">
                        <div class="panel-body">
                              <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Last Name</th>
                                        <th>First Name</th>
                                        <th>Employee #</th>
                                        <th>Certification</th>
                                        <th>Expiration Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if(!empty($expirationDates59)) { 
                                            foreach($expirationDates59 as $expired59) {
                                        ?>
                                    <tr>
                                        <td><?php echo $expired59['candidate']->last_name?></td>
                                        <td><?php echo $expired59['candidate']->first_name?></td>
                                        <td><?php echo 'EMP0000'.$expired59['candidate']->id?></td>
                                        <td><?php echo $expired59['certification']->name?></td>
                                        <td><?php echo date('m-d-Y',strtotime($expired59->expiration_date));?></td>
                                    </tr>
                                    <?php } } else { ?>
                                        <tr>
                                            <td>No Record Found</td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingFour">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                            Expiry in 44 - 30 days
                            <i class="fa fa-angle-left"></i>
                            </a>
                        </h4>
                    </div>
                    <div id="collapseFour" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingFour">
                        <div class="panel-body">
                              <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Last Name</th>
                                        <th>First Name</th>
                                        <th>Employee #</th>
                                        <th>Certification</th>
                                        <th>Expiration Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if(!empty($expirationDates44)) { 
                                            foreach($expirationDates44 as $expired44) {
                                        ?>
                                    <tr>
                                        <td><?php echo $expired44['candidate']->last_name?></td>
                                        <td><?php echo $expired44['candidate']->first_name?></td>
                                        <td><?php echo 'EMP0000'.$expired44['candidate']->id?></td>
                                        <td><?php echo $expired44['certification']->name?></td>
                                        <td><?php echo date('m-d-Y',strtotime($expired44->expiration_date));?></td>
                                    </tr>
                                    <?php } } else { ?>
                                        <tr>
                                            <td>No Record Found</td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingFive">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                            Expiry in 29 or less days
                            <i class="fa fa-angle-left"></i>
                            </a>
                        </h4>
                    </div>
                    <div id="collapseFive" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingFive">
                        <div class="panel-body">
                              <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Last Name</th>
                                        <th>First Name</th>
                                        <th>Employee #</th>
                                        <th>Certification</th>
                                        <th>Expiration Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if(!empty($expirationDates29)) { 
                                            foreach($expirationDates29 as $expired29) {
                                        ?>
                                    <tr>
                                        <td><?php echo $expired29['candidate']->last_name?></td>
                                        <td><?php echo $expired29['candidate']->first_name?></td>
                                        <td><?php echo 'EMP0000'.$expired29['candidate']->id?></td>
                                        <td><?php echo $expired29['certification']->name?></td>
                                        <td><?php echo date('m-d-Y',strtotime($expired29->expiration_date));?></td>
                                    </tr>
                                    <?php } } else { ?>
                                        <tr>
                                            <td>No Record Found</td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>