<?php use Cake\Core\Configure;?>
<div class="content-wrapper clearfix ">
<?php echo $this->Flash->render('positive');?>
    <div class="top-header clearfix">
        <div class="col-sm-6 col-xs-6 main-title">Employees</div>
        <?php if(!in_array($this->request->session()->read('Auth.User.user_role_id'),[4,5])){ ?>
        <div class="col-sm-6 col-xs-6 main-title text-right pull-right">
        <?php echo $this->Html->link('Add New',
            '/Employees/add',
            array(
                'escape' => false
                )
        );?>
        </div>
        <?php } ?>
    </div>
    <div class="col-sm-12 setup-page ">
        <div class="filter-section clearfix">
            <?php
                echo $this->Form->create('search', [
                    'type' => 'get',
                    'url' => [
                        'controller' => 'Employees',
                        'action' => 'index'
                    ]
                ]);
            ?>
            <div class="col-sm-12">
                <h3>Filters</h3>
            </div>
            <div class="col-sm-4 form-group"><label>Search By</label>
                <input type="text" name="tag_search" class="form-control" placeholder="Enter Candidate Name or Job #">
            </div>
            <div class="col-sm-12 form-group">
                <label>Refine BY</label>
            </div>
            <div class="col-sm-12 form-group">
                <div class="row">
                    <div class="col-sm-2 form-group">
                        <select  class="form-control">
                            <option value>Select</option>
                            <option value="0">UnArmed</option>
                            <option value="1">Armed</option>
                            <option>All</option>
                        </select>
                    </div>
                    <div class="col-sm-2 form-group">
                        <select class="form-control" name="job_type">
                            <option value>Select</option>
                            <option value="1">Full Time</option>
                            <option value="2">Part Time</option>
                            <option>All</option>
                        </select>
                    </div>
                    <div class="col-sm-2 form-group">
                        <?php
                            echo $this->Form->select('day', Configure::read('days'), [
                                'class' => 'form-control',
                                'empty' => 'Select Day',
                            ]);
                        ?>  
                    </div>
                    <div class="col-sm-2 form-group">
                        <?php
                            echo $this->Form->select('shift', $shift, [
                                'class' => 'form-control',
                                'empty' => 'Select shift',
                            ]);
                        ?>
                    </div>
                    <div class="col-sm-2 form-group">
                        <select class="form-control">
                            <option>Select</option>
                            <option>New</option>
                            <option>Phone Screened</option>
                            <option>Interviewed</option>
                            <option>Job Offered</option>
                            <option>Rejected</option>
                        </select>
                    </div>
                    <div class="col-sm-1"><button type='submit' class="btn btn-blue">Search</button></div>
                    <div class="col-sm-1">
                    <?php echo $this->Html->link('Clear',
                        '/Employees/index',
                        array(
                            'class' => 'btn btn-blue'
                            )
                    );?>
                    </div>
                </div>
            </div>
            <!-- <?php echo $this->Form->end();?> -->
        </div>
        <div>
           <!--  <?php
                echo $this->Form->create('search', [
                    'type' => 'get',
                    'url' => [
                        'controller' => 'Employees',
                        'action' => 'index'
                    ]
                ]);
            ?> -->
            <div class="col-sm-12 form-group">
                <label class="advanced-filters">Advanced Filters <i class="fa fa-angle-down" aria-hidden="true"></i></label>
            </div>
            <div class="col-sm-12 form-group  advanced-filter-from">
                <div class="row">
                    <div class="col-sm-2 form-group">
                         <div class="col-sm-7">
                            <label>CPR/AED</label>
                        </div>
                        <div class="col-sm-5">
                            <?php
                                echo $this->Form->checkbox(__('cpr'));
                            ?>
                        </div>
                    </div>
                    <div class="col-sm-4 form-group">
                        <div class="col-sm-4">
                            <label>TWIC</label>
                        </div>
                        <div class="col-sm-8">
                            <?php
                                echo $this->Form->checkbox(__('twic'));
                            ?>
                        </div>
                    </div>                    
                    <div class="col-sm-2"><button type="submit" class="btn btn-blue">Search</button></div>
                </div>
            </div>
            <?php echo $this->Form->end();?>
        </div>
        <div class=" col-sm-12 table-responsive">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Last Name</th>
                        <th>First Name</th>
                        <th>Rank</th>
                        <th>Date of Hire</th>
                        <th>Status</th>
                        <?php if(!in_array($this->request->session()->read('Auth.User.user_role_id'),[4,5])){ ?><th>Actions</th><?php } ?>                        
                    </tr>
                </thead>
                <tbody>
                    <?php if(!empty($candidates->toArray())) {
                        foreach($candidates as $candidate) {
                        ?>
                        <tr>                            
                            <td>
                                <?php
                                    echo $this->Html->link(__($candidate->last_name), [
                                        'controller' => 'Employees',
                                        'action' => 'view',
                                        base64_encode($candidate->id)
                                    ]);
                                ?>                                
                            </td>
                            <td><?php echo $candidate->first_name;?></td>                            
                            <td><?php echo 'None';?></td>                            
                            <td><?php echo date('m-d-Y',strtotime($candidate->modified));?></td>
                            <td><?php echo ($candidate->status == 6) ? "Fired" : "Current Employee"?></td>
                            <?php if(!in_array($this->request->session()->read('Auth.User.user_role_id'),[4,5])){ ?>
                            <td>
                                <?php echo $this->Html->link(
                                    '<i class="fa fa-pencil-square" aria-hidden="true"></i>',
                                    '/Edits/edit/'.base64_encode($candidate->id),
                                    array(
                                        'escape' => false,
                                        'title' => 'edit'
                                        )
                                );?>
                                
                            </td>  
                            <?php } ?>                          
                        </tr>
                    <?php  } } else { ?>
                    <tr>
                        <td colspan="4" class="text-center">
                            No record found
                        </td>
                    </tr>
                       <?php } ?>
                </tbody>
            </table>
            <?php echo $this->element('pagination'); ?>
        </div>
    </div>
</div>
<?php
    echo $this->Html->script('User/candidate_search', [
        'block' => 'scriptBottom'
    ]);
?>