<?php
    echo $this->Html->css('candidate',[
        'block' => true
    ]);
?>

<div class="content-wrapper clearfix ">
<?php echo $this->Flash->render('positive');?>
    <div class="top-header clearfix">
        <div class="col-sm-9 col-xs-9 main-title">
            <a href="javascript:history.go(-1)">
                <i class="fa fa-arrow-circle-left" aria-hidden="true"></i>
            </a>
            <span>Employee Details</span>
        </div>
        <div class="col-sm-3 col-xs-3 text-right">
            <a href="javascript:history.go(-1)">
                <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
            </a>
        </div>
    </div>
    <div class="col-sm-12 setup-page candidates ">
        <div class="filter-section clearfix">
            <div class="col-sm-3">
                <?php if(!empty($candidates->image)) {
                    $image = '/candidate/candidates/image/'.$candidates->image_path.'/'.$candidates->image;
                } else{
                    $image = 'default.png';
                    } ?>
                <?php
                    echo $this->Html->image($image, [
                        'alt' => 'profile image',
                        'class' => 'img-responsive'
                    ]);
                ?>
            <?= $this->Form->create('',array('url' => array(
                    'controller' => 'Employees',
                    'action' => 'imageUpload',base64_encode($candidates->id)),
                    'type' => 'file',
                    'id' => 'status')
                ); 
            ?>
            <?php if(!in_array($this->request->session()->read('Auth.User.user_role_id'),[5,4,6])){ ?>
            <div class="col-sm-12">
                <label>Upload Image</label>
            </div>
            <div class="col-sm-12 form-group">
                <div class="row">
                    <div class="col-sm-12 form-group">
                        <div>
                            <?php echo $this->Form->control('image',array(
                                'class' => 'form-control',
                                'label' => false,
                                'type' => 'file'
                                )
                            );?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 form-group">
                <div class="row">
                    <div class="col-sm-4 form-group">
                        <div>
                            <input class="btn btn-primary" value = "Update" type = "submit">
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
            </form>
            </div>
            <div class="col-sm-9">
                <div class="col-sm-6">
                    <div class="form-group">
                        <span>
                            <b>Last Name:</b> <?php echo $candidates->last_name; ?>
                        </span>
                    </div>
                    <div class="form-group">
                        <span>
                            <b>First Name:</b> <?php echo $candidates->first_name; ?>
                        </span>
                    </div>
                    <div class="form-group">
                        <span>
                            <b>Employee #:</b>
                            <?php
                                echo 'EMP0000'.$candidates->id
                            ?>
                        </span>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <span>
                            <b>Email:</b> <?php echo $candidates->email; ?>
                        </span>
                        <span class="pull-right">
                            <?php 
                                    if($candidates->status != 6) {
                                    echo $this->Html->link(
                                        '<span class="btn btn-danger btn-lg">Fire</span>',
                                        '/Employees/fired/'.base64_encode($candidates->id),
                                        array(
                                            'escape' => false,
                                            'title' => 'active',
                                            'confirm' => 'you really want to fire this employee ?'
                                            )
                                    );  
                                }
                            ?>
                        </span>
                    </div>
                    <div class="form-group">
                        <span>
                            <b>status:</b> <?php 
                                if($candidates->status == 1) {
                                    echo "Current Employee";
                                } elseif ($candidates->status == 6) {
                                    echo "Fired";
                                }

                            ?>
                        </span>
                            
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <b>Address1:</b>
                        <?php echo $candidates->address1;?>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <b>Address2:</b>
                        <?php echo $candidates->address2;?>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <b>City:</b>
                        <?php echo $candidates->city['name'];?>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <b>State:</b>
                        <?php echo $candidates->state['name'];?>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <b>Zip:</b>
                        <?php echo $candidates->zipcode;?>
                    </div>
                </div>
                <?php if(!in_array($this->request->session()->read('Auth.User.user_role_id'),[4,5])) { ?>
                <div class="col-sm-6">
                    <div class="form-group">
                        <b>Social Security #:</b>
                        <?php
                            $socialSecurity = 'Not Available';
                            if (!empty($candidates->candidate_completes)) {
                                $socialSecurity = $candidates->candidate_completes[0]['social_security'];
                            }
                            echo $socialSecurity;
                        ?>
                    </div>
                </div>
                <?php } ?>
                <div class="col-sm-6">
                    <div class="form-group">
                        <b>Driver License #:</b>
                        <?php 
                            if ($candidates->driving_licence) {
                                echo ($candidates->licence_number);
                            } else {
                                echo "No driving Licence";
                            }
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <?php if(in_array($this->request->session()->read('Auth.User.user_role_id'),[2,3,5,6])){ ?>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                Training
                              <i class="fa fa-angle-left"></i>
                            </a>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="collapseOne">
                        <div class="panel-body">
                            <div class="col-sm-12">
                                <div class="clearfix form-group">
                                    <a href="#" data-toggle='modal' data-target="#addEmployeeTraining" class="btn btn-default pull-right">
                                        <span class="fa fa-plus"></span>
                                        <span id="addTraining">Add Training</span>
                                    </a>
                                </div>
                                <div class="table-responsive">
                                    <table class="table">
                                        <tbody>
                                            <thead>
                                                <th>Name</th>
                                                <th>Description</th>
                                                <th>Date</th>
                                                <th>Instructor</th>                                                
                                                <th>Hours</th>                                                
                                                <th>Job Code</th>                                                
                                            </thead>
                                        </tbody>
                                        <?php if(!empty($trainings)){
                                            foreach($trainings as $training) { ?>
                                                <tr>
                                                    <td><?php echo $training['training']->name?></td>
                                                    <td><?php echo $training['training']->description ?></td>
                                                    <td><?php echo date('m-d-Y',strtotime($training->training_date))?></td>
                                                    <td><?php echo $training->instructor ?></td>
                                                    <td><?php echo $training['training']->hours ?></td>
                                                    <td><?php echo (!empty($training['candidate']['job']))  ? $training['candidate']['job']->job_number : "-" ?></td>
                                                <tr>
                                        <?php } } else {?>
                                                <tr>
                                                    <td>
                                                        No record found
                                                    </td>
                                                <tr>
                                        <?php } ?> 
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <?php if(in_array($this->request->session()->read('Auth.User.user_role_id'),[2,3,6])){ ?>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTwo">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse"  data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                Certifications
                              <i class="fa fa-angle-left"></i>
                            </a>
                        </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="collapseTwo">
                        <div class="panel-body">
                            <div class="col-sm-12">
                                <div class="clearfix form-group">
                                    <a href="#" data-toggle='modal' data-target="#addEmployeeCertification" class="btn btn-default pull-right">
                                        <span class="fa fa-plus"></span>
                                        <span>Add Certifications</span>
                                    </a>
                                </div>
                                <div class="table-responsive">
                                    <table class="table">
                                        <tbody>
                                            <thead>
                                                <th>Name</th>
                                                <th>Description</th>
                                                <th>Expiration</th>
                                                <th>Image</th>                                                
                                            </thead>
                                        </tbody>
                                         <?php if(!empty($certificates)){
                                            foreach($certificates as $certificate) { ?>
                                                <tr>
                                                    <td><?php echo $certificate['certification']->name?></td>
                                                    <td><?php echo $certificate['certification']->description ?></td>
                                                    <td><?php echo date('m-d-Y',strtotime($certificate->expiration_date))?></td>
                                                    <td><?php echo $this->Html->image('/certificate/employeecertifications/image/'.$certificate->image_path.'/'.'square_'.$certificate->image) ?></td>
                                                <tr>
                                        <?php } } else {?>
                                                <tr>
                                                    <td>
                                                        No record found
                                                    </td>
                                                <tr>
                                        <?php } ?> 
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <?php if(in_array($this->request->session()->read('Auth.User.user_role_id'),[2,3,4,6])){ ?>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingThree">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                Uniform & Equipments
                              <i class="fa fa-angle-left"></i>
                            </a>
                        </h4>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                        <div class="panel-body">
                            <div class="col-sm-12">
                                <div class="clearfix form-group">
                                    <a href="#"  data-toggle='modal' data-target="#addEmployeeEquipment" class="btn btn-default pull-right">
                                        <span class="fa fa-plus"></span>
                                        <span>Add Item</span>
                                    </a>
                                </div>
                                 <div class="table-responsive">
                                    <table class="table">
                                        <tbody>
                                            <thead>
                                                <th>Item</th>
                                                <th>Description</th>
                                                <th>Quantity</th>
                                                <th>Condition</th>
                                                <th>Job Code</th>
                                            </thead>
                                        </tbody>
                                        <?php if(!empty($equipments)){
                                            foreach($equipments as $equipment) { ?>
                                                <tr>
                                                    <td><?php echo $equipment['equipment']->item?></td>
                                                    <td><?php echo $equipment['equipment']->description ?></td>
                                                    <td><?php echo $equipment->quantity ?></td>
                                                    <td><?php echo $equipment->equipment_condition ?></td>
                                                    <td><?php echo (!empty($training['candidate']['job']))  ? $equipment['candidate']['job']->job_number : "-" ?></td>
                                                <tr>
                                        <?php } } else {?>
                                                <tr>
                                                    <td>
                                                        No record found
                                                    </td>
                                                <tr>
                                        <?php } ?> 
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <?php if(in_array($this->request->session()->read('Auth.User.user_role_id'),[2,3,5,6])){ ?>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingFour">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                Commendations
                              <i class="fa fa-angle-left"></i>
                            </a>
                        </h4>

                    </div>
                    <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEight">
                        <div class="panel-body">
                            <div class="col-sm-12">
                                <div class="clearfix form-group">
                                    <a href="#"  data-toggle="modal" data-target="#addcommendation" class="btn btn-default pull-right">
                                        <span class="fa fa-plus"></span>
                                        <span>Add Commendations</span>
                                    </a>
                                </div>
                                 <div class="table-responsive">
                                    <table class="table">
                                        <tbody>
                                            <thead>
                                                <th>Title</th>
                                                <th>Issuer</th>
                                                <th>Date</th>
                                                <th>Details</th>
                                            </thead>
                                        </tbody>
                                        <?php if(!empty($commendations)){
                                            foreach($commendations as $commendation) { ?>
                                                <tr>
                                                    <td><?php echo $commendation->title?></td>
                                                    <td><?php echo $commendation->issuer?></td>
                                                    <td><?php echo date('m-d-Y',strtotime($commendation->commend_date))?></td>
                                                    <td><?php echo $commendation->detail?></td>
                                                <tr>
                                        <?php } } else {?>
                                                <tr>
                                                    <td>
                                                        No record found
                                                    </td>
                                                <tr>
                                        <?php } ?>        
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <?php if(in_array($this->request->session()->read('Auth.User.user_role_id'),[3,6])){ ?>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingNine">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                Disciplinary
                                <i class="fa fa-angle-left"></i>
                            </a>
                        </h4>

                    </div>
                    <div id="collapseNine" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingNine">
                        <div class="panel-body">
                            <div class="col-sm-12">
                                <div class="clearfix form-group">
                                    <a href="#"  data-toggle="modal" data-target="#adddiscip" class="btn btn-default pull-right">
                                        <span class="fa fa-plus"></span>
                                        <span>Add Disciplinary</span>
                                    </a>
                                </div>
                                <div class="table-responsive">
                                    <table class="table">
                                        <tbody>
                                            <thead>
                                                <th>Title</th>
                                                <th>Type</th>
                                                <th>Date</th>
                                                <th>Details</th>
                                                <th>Image</th>
                                            </thead>
                                        </tbody>
                                        <?php if(!empty($disciplinaries)){
                                            foreach($disciplinaries as $disciplinary) { ?>
                                                <tr>
                                                    <td><?php echo $disciplinary->title?></td>
                                                    <td><?php echo $disciplinary->type_discip?></td>
                                                    <td><?php echo $disciplinary->result?></td>
                                                    <td><?php echo date('m-d-Y',strtotime($disciplinary->discip_date))?></td>
                                                    <td><?php echo $this->Html->image('/disciplinary/disciplinaries/image/'.$disciplinary->image_path.'/'.'square_'.$disciplinary->image) ?></td>
                                                <tr>
                                        <?php } } else {?>
                                                <tr>
                                                    <td>
                                                        No record found
                                                    </td>
                                                <tr>
                                        <?php } ?> 
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>                
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<?php echo $this->element('Candidate/note');?>
<?php echo $this->element('Employee/training');?>
<?php echo $this->element('Employee/add_certification');?>
<?php echo $this->element('Employee/add_disciplinary');?>
<?php echo $this->element('Employee/add_commendation');?>
<?php echo $this->element('Employee/add_equipment');?>
<?php 
    echo $this->Html->script([
    'User/employee',
    ], [
    'block' => 'scriptBottom'
    ]); 
?>