<ol class="breadcrumb breadcrumb-custom">
    <li>
        <?php
            echo $this->Html->link(__('Back'), [
                'controller' => 'EmailTemplates',
                'action' => 'emailTemplateList',
                '?' => $this->request->getQuery()
            ]); 
        ?>
    <li class="active">
        <?php echo h(ucwords($emailTemplate['template_used_for']));?>
    </li>
</ol>
<section class="content-header clearfix">
    <header><?= __('Edit Email Template') ?></header>
</section>
<!-- Main content -->
<section class="content">
    <div class="box">
        <div class="col-lg-12">
            <div id="emails">
                <?= $this->Form->create($emailTemplate,array('class' => 'form-horizontal')) ?>
                    <div class="form-group">
                        <label class="control-label" for="exampleInputEmail1"><?php echo __('Template Name'); ?></label>
                        <?= $this->Form->input('template_used_for', ['class'=>'form-control', 'label' => false]); ?>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="exampleInputEmail1"><?php echo __('Subject'); ?></label>
                        <?= $this->Form->input('subject', ['class'=>'form-control', 'label' => false]); ?>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="exampleInputEmail1"><?php echo __('Body'); ?></label>
                        <?php
                            echo $this->CKEditor->loadJs();
                            echo $this->Form->textarea('mail_body', ['class'=>'form-control', 'label' => false]);
                            echo $this->CKEditor->replace('mail_body');
                        ?>
                    </div>
                    <div class="col-lg-8">
                        <?= $this->Form->button(__('Save'), ['class' => "btn btn-green ", 'type' => 'submit']); ?>
                    </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</section>