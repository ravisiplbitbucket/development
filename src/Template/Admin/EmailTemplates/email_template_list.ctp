<?php 
	use Cake\I18n\Time;
	use Cake\Utility\Inflector;

	$this->assign('title', Inflector::humanize(str_replace('-', '_', $page)));

	$this->Html->css('bootstrap-switch.min.css',[
		'block' => true
	]);
?>
<section>
	<header><?= __('Manage Email Templates') ?></header>
</section>
<section>
	<div class="filter clearfix">
		<div class="col-lg-6 pull-right text-right">
			<?php 
				echo $this->Form->create(null,[
					'type' => 'get',
					'id' => 'emailForm',
					'class' => 'form-inline',
					'url' => [
                        'controller' => 'EmailTemplates',
                        'action' => 'emailTemplateList'
                    ]
				]);

				// Add a template with the help placeholder.
				$this->Form->setTemplates([
				    'inputContainer' => '<div class="form-group">{{content}}</div>'
				]);

				// Generate an input and populate the help variable
				
				echo $this->Form->control('search', [
				    'label' => false,
				    'class' => 'form-control ',
				    'type' => 'text',
				    'placeholder' => 'Search by template name'
				]);
			?>

			<?php
				echo $this->Form->button('Search',[
					'type' => 'submit',
					'class' => 'btn btn-default btn-brown'
				]);

			?>

			<?php
				echo $this->Form->button('Clear',[
					'type' => 'button',
					'id' => 'clearForm',
					'class' => 'btn btn-default'
				]);

				$this->Form->end();
			?>
		</div>
	</div>

	<div class="table-responsive">
		<table class="table">
			<thead>
				<?=
					$this->Html->tableHeaders(['Template Name', 'Subject', 'Modified', 'Actions']);
				?>
			</thead>
			<tbody>
				<?php
					if ($emailTemplates->isEmpty()):
				?>
				<tr>
					<td colspan="5">
						<div class="text-center">
							<h2><?= __('NO RECORD FOUND'); ?></h2>
							<?= 
								$this->Html->image('no-record.png', [
									'alt' => 'no record found'
								]);
							?>
						</div>
					</td>
				</tr>
				<?php else: ?>
				<?php
					foreach ($emailTemplates as $key => $value) { 
						$status = $value['is_active']?'checked':'';
				?>
						<tr>
							<td>
								<span>
									<?= 
										h($value['template_used_for']); 
									?>
									<br>
									<small>
									</small>
								</span>
							</td>
							<td><?= h($value['subject']); ?></td>
							<td><?php echo h($value->modified->i18nFormat('MMMM dd, yyyy'));?></td>
							<td>
								<?php 
									echo $this->Html->link('<span class="fa fa-pencil-square-o icon-setting"></span>',[
										'controller' => 'EmailTemplates',
										'action' => 'editEmailTemplate',
										base64_encode($value->id),
										'?' => $this->request->query
									],[
										'escape' => false
									]);
									echo $this->Html->link(__('<span class="fa fa-trash-o icon-setting"></span>'),
		                                'javascript:void(0)', [
		                                'class' => 'delete-list',
		                                'data-id' => base64_encode($value->id),
		                                'escape' => false
                                	]);
                            	?>
							</td>
						</tr>
				<?php } ?>
			</tbody>
		</table>
		<?php echo $this->element('pagination'); ?>
	</div>
</section>
<?php
	endif;
?>
<?= $this->Html->script([
	'bootstrap-switch.min.js',
	'Admin/manageEmails'
	], [
	'block' => true
]) ?>
<?php $this->Html->scriptStart(['block' => true]); ?>
	$(function(){
		$("[name*='switch']").bootstrapSwitch();
	});
<?php $this->Html->scriptEnd(); ?>
