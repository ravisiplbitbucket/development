<?php 
	use Cake\I18n\Time;
	use Cake\Utility\Inflector;

	$this->assign('title', Inflector::humanize(str_replace('-', '_', $page)));

	$this->Html->css('bootstrap-switch.min.css',[
		'block' => true
	]);
?>
<section>
	<header><?= __('Manage Companies') ?></header>
</section>
<section>
	<div class="filter clearfix">
		<div class="col-lg-6 pull-right text-right">
			<?php 
				echo $this->Form->create(null,[
					'type' => 'get',
					'id' => 'companiesForm',
					'class' => 'form-inline',
					'url' => [
                        'controller' => 'Admins',
                        'action' => 'manageCompanies'
                    ]
				]);

				// Add a template with the help placeholder.
				$this->Form->setTemplates([
				    'inputContainer' => '<div class="form-group">{{content}}</div>'
				]);

				// Generate an input and populate the help variable
				
				echo $this->Form->control('search', [
				    'label' => false,
				    'class' => 'form-control search-bar',
				    'type' => 'text',
				    'placeholder' => 'Search by email, name or company name'
				]);
			?>

			<?php
				echo $this->Form->button('Search',[
					'type' => 'submit',
					'class' => 'btn btn-default btn-brown'
				]);

			?>

			<?php
				echo $this->Form->button('Clear',[
					'type' => 'button',
					'id' => 'clearForm',
					'class' => 'btn btn-default'
				]);

				$this->Form->end();
			?>
		</div>
	</div>

	<div class="table-responsive">
		<table class="table">
			<thead>
				<?=
					$this->Html->tableHeaders(['First Name', 'Last Name', 'Email Address', 'Company Name', 'Actions']);
				?>
			</thead>
			<tbody>
				<?php
					if ($companies->isEmpty()):
				?>
				<tr>
					<td colspan="5">
						<div class="text-center">
							<h2><?= __('NO RECORD FOUND'); ?></h2>
							<?= 
								$this->Html->image('no-record.png', [
									'alt' => 'no record found'
								]);
							?>
						</div>
					</td>
				</tr>
				<?php else: ?>
				<?php
					foreach ($companies as $key => $value) { 
						$status = $value['is_active']?'checked':'';
				?>
						<tr>
							<td>
								<span>
									<?= 
										h($value['first_name']); 
									?>
									<br>
									<small>
									</small>
								</span>
							</td>
							<td><?=__($value['last_name']);?></td>
							<td><?= h($value['user']['email']); ?></td>
							<td><?= h($value['company']); ?></td>
							<td>
								<?php 
									
									echo $this->Html->link(__('<span class="fa fa-eye icon-setting"></span>'), [
											'controller' => 'Admins',
											'action' => 'viewCompany',
											base64_encode($value->id),
											'?' => $this->request->query
										], [
			                                'escape' => false
                                	]);
                            	?>
								<input type="checkbox" class= 'switchCheckbox' data-id= <?= base64_encode($value->id)?> data-on-color="success" data-size="mini" data-off-color="danger" name="switch" <?=$status?>>
							</td>
						</tr>
				<?php } ?>
			</tbody>
		</table>
		<?php echo $this->element('pagination'); ?>
	</div>
</section>
<?php
	endif;
?>
<?= $this->Html->script([
	'bootstrap-switch.min.js',
	'Admin/manageCompanies'
	], [
	'block' => true
]) ?>
<?php $this->Html->scriptStart(['block' => true]); ?>
	$(function(){
		$("[name*='switch']").bootstrapSwitch();
	});
<?php $this->Html->scriptEnd(); ?>
