<?php 
	use Cake\I18n\Time;
	use Cake\Utility\Inflector;
	use Cake\Utility\Text;

	$this->assign('title', Inflector::humanize(str_replace('-', '_', $page)));
?>
<ol class="breadcrumb breadcrumb-custom">
	<li>
		<?php
			echo $this->Html->link(__('Back'), [
				'controller' => 'Admins',
				'action' => 'manageCompanies',
				'?' => $this->request->getQuery()
			]); 
		?>
	<li class="active">
		<?php echo h(ucwords($company['company']));?>
	</li>
</ol>
<section>
	<header class="clearfix">
		<?= h(ucwords($company['company']));?>
	</header>

</section>

<section class="content">
	<div class="row col-sm-12">
		<div class=" booking-info">
			<h3 class="heading"><?= __('Company Detail'); ?></h3>
			<div class="logo-admin">
				<?php
					$path = $this->Common->getCompanyImagePath($company['logo_path'], $company['website_logo']);
					$image = 'no-logo.png';
					if (file_exists(WWW_ROOT.$path)) {
						$image = '../'.$path;
					}
					echo $this->Html->image($image, [
						'alt' => 'company logo',
						'class' => 'img-responsive'
					]);
				?>
			</div>
			<table class="table table-info">
				<tbody>
					<tr>
						<td>
							<span class="fa fa-user"></span>
							<span><?= __('Full Name :'); ?></span>
						</td>
						<td>
							<span><?= h(__('{0} {1}', ucfirst($company['first_name']), $company['last_name'])); ?></span>
						</td>
					</tr>
					<tr>
						<td>
							<span class="fa fa-envelope"></span>
							<span><?= __('Email Address :'); ?></span>
						</td>
						<td>
							<span><?= h($company['user']['email']); ?></span>
						</td>
					</tr>
					<tr>
						<td>
							<span class="fa fa-home"></span>
							<span><?= __('Address :'); ?></span>
						</td>
						<td>
							<span>
								<?= 
									h(__('{0} , {1}, {2}, {3}, {4}', ucfirst($company['address1']), $company['city']['name'], $company['state']['name'], $company['country']['name'], 
										$company['zipcode'])); 
								?>
							</span>
						</td>
					</tr>
					<tr>
						<td>
							<span class="fa fa-phone-square"></span>
							<span><?= __('Phone Number :'); ?></span>
						</td>
						<td>
							<span><?= h($company['telephone']); ?></span>
						</td>
					</tr>
					<tr>
						<td>
							<span class="fa fa-external-link"></span>
							<span><?= __('Website Url :'); ?></span>
						</td>
						<td>
							<span>
								<?=
									$this->Html->link(h($company['website_url']), 'javascript:void(0)');
								?>
							</span>
						</td>
					</tr>
					<tr>
						<td>
							<span class="fa fa-globe"></span>
							<span><?= __('Website Name :'); ?></span>
						</td>
						<td>
							<?=
								h($company['website_name']);
							?>
						</td>
					</tr>
					<tr>
						<td>
							<span class="fa fa-info-circle"></span>
							<span><?= __('Company status :'); ?></span>
						</td>
						<td>
							<?php
								$class = 'danger';
								$label = 'Deactivate';
								if ($company['is_active']) {
									$class = 'success';
									$label = 'Active';
								}
							?>

							<span class="label label-status label-<?=$class?>"><?php echo $label ?></span>
						</td>
					</tr>
					<tr>
						<td>
							<span class="fa fa-file-text"></span>
							<span><?= __('Description :'); ?></span>
						</td>
						<td>
							<p>
								<?php
									$description = 'N/A';
									if (!empty($company['overview']))
										$description = $company['overview'];
									echo h($description);
								?>
							</p>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</section>