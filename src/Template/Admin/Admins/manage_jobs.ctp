<?php 
	use Cake\I18n\Time;
	use Cake\Utility\Inflector;

	$this->assign('title', Inflector::humanize(str_replace('-', '_', $page)));

	$this->Html->css('bootstrap-switch.min.css',[
		'block' => true
	]);
?>
<section>
	<header><?= __('Manage Jobs') ?></header>
</section>
<section>
	<div class="filter clearfix">
		<div class="col-lg-6 pull-right text-right">
			<?php 
				echo $this->Form->create(null,[
					'type' => 'get',
					'id' => 'jobForm',
					'class' => 'form-inline',
					'url' => [
                        'controller' => 'Admins',
                        'action' => 'manageJobs'
                    ]
				]);

				// Add a template with the help placeholder.
				$this->Form->setTemplates([
				    'inputContainer' => '<div class="form-group">{{content}}</div>'
				]);

				// Generate an input and populate the help variable
				
				echo $this->Form->control('search', [
				    'label' => false,
				    'class' => 'form-control',
				    'type' => 'text',
				    'placeholder' => 'Search by job title, number or date of posting'
				]);
			?>

			<?php
				echo $this->Form->button('Search',[
					'type' => 'submit',
					'class' => 'btn btn-default btn-brown'
				]);

			?>

			<?php
				echo $this->Form->button('Clear',[
					'type' => 'button',
					'class' => 'btn btn-default',
					'id' => 'clearForm'
				]);

				$this->Form->end();
			?>
		</div>
	</div>

	<div class="table-responsive">
		<table class="table">
			<thead>
				<?=
					$this->Html->tableHeaders(['Job #', 'Job title', 'Date Posted', 'Status', 'Actions']);
				?>
			</thead>
			<tbody>
				<?php
					if ($jobs->isEmpty()):
				?>
				<tr>
					<td colspan="5">
						<div class="text-center">
							<h2><?= __('NO RECORD FOUND'); ?></h2>
							<?= 
								$this->Html->image('no-record.png', [
									'alt' => 'no record found'
								]);
							?>
						</div>
					</td>
				</tr>
				<?php else: ?>
				<?php
					foreach ($jobs as $key => $value) {
						$status = $value['active']== 1?'checked':'';
				?>
						<tr>
							<td>
								<span>
									<?= 
										h($value->job_number); 
									?>
									<br>
									<small>
									</small>
								</span>
							</td>
							<td>
								<?= 
									h($value->job_title);
								?>
							</td>
							<td><?php echo date('M d, Y',strtotime($value->created))?></td>
							<td>
								<?php
									$class = 'danger';
									$label = 'closed';
									
									if ($value->active ==1) {
										$class = 'success';
										$label = 'open';
									}
								?>
								<span class="label label-status label-<?=$class?>"><?php echo $label ?></span>
							</td>
							<td>
								<input type="checkbox" class= 'switchJobs' data-id= <?= base64_encode($value->id)?> data-on-color="success" data-size="mini" data-off-color="danger" name="switch" <?=$status?>>
								<?php 
									echo $this->Html->link(__('<span class="fa fa-trash-o icon-setting"></span>'),
		                                'javascript:void(0)', [
		                                'class' => 'delete-list',
		                                'data-id' => base64_encode($value->id),
		                                'escape' => false
                                	]);
                            	?>
							</td>
						</tr>
				<?php } ?>
			</tbody>
		</table>
		<?php echo $this->element('pagination'); ?>
	</div>
</section>
<?php
	endif;
?>
<?= $this->Html->script([
	'bootstrap-switch.min.js',
	'Admin/manageJob'
	], [
	'block' => true
]) ?>
<?php $this->Html->scriptStart(['block' => true]); ?>
	$(function(){
		$("[name*='switch']").bootstrapSwitch();
	});
<?php $this->Html->scriptEnd(); ?>
