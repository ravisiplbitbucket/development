<?php 
	use Cake\I18n\Time;
	use Cake\Utility\Inflector;

	$this->assign('title', Inflector::humanize(str_replace('-', '_', $page)));

	$this->Html->css('bootstrap-switch.min.css',[
		'block' => true
	]);
?>
<section>
	<header><?= __('Job Applications') ?></header>
</section>
<section>
	<div class="filter clearfix">
	    <div class="col-lg-6 pull-left text-left">
	        <?php
				echo $this->Html->link('Download CSV',array(
                	'controller' => 'admins', 
                	'action' => 'export'
                ),[
					'escape' => false,
					'class' => 'btn btn-default btn-brown'
				]);

			?>
	    </div>
		<div class="col-lg-6 pull-right text-right">
			<?php 
				echo $this->Form->create(null,[
					'type' => 'get',
					'id' => 'companiesForm',
					'class' => 'form-inline',
					'url' => [
                        'controller' => 'Admins',
                        'action' => 'jobApplications'
                    ]
				]);

				// Add a template with the help placeholder.
				$this->Form->setTemplates([
				    'inputContainer' => '<div class="form-group">{{content}}</div>'
				]);

				// Generate an input and populate the help variable
				
				echo $this->Form->control('search', [
				    'label' => false,
				    'class' => 'form-control search-bar',
				    'type' => 'text',
				    'placeholder' => 'Search by Name'
				]);
			?>

			<?php
				echo $this->Form->button('Search',[
					'type' => 'submit',
					'class' => 'btn btn-default btn-brown'
				]);

			?>

			<?php
				echo $this->Form->button('Clear',[
					'type' => 'button',
					'id' => 'clearForm',
					'class' => 'btn btn-default'
				]);

				$this->Form->end();
			?>
		</div>
	</div>

	<div class="table-responsive">
		<table class="table">
			<thead>
				<?=
					$this->Html->tableHeaders(['Date Applied', 'Name', 'Year Of Experience(in years)', 'Status', 'Score','Job Number']);
				?>
			</thead>
			<tbody>
				<?php
					if ($candidates->isEmpty()):
				?>
				<tr>
					<td colspan="5">
						<div class="text-center">
							<h2><?= __('NO RECORD FOUND'); ?></h2>
							<?= 
								$this->Html->image('no-record.png', [
									'alt' => 'no record found'
								]);
							?>
						</div>
					</td>
				</tr>
				<?php else: ?>
				<?php
					foreach ($candidates as $key => $value) { 
						//$status = $value['is_active']?'checked':'';
				?>
						<tr>
							<td>
							    <?php echo date('m-d-Y',strtotime($value->created));?>
							</td>
							<td><?php echo $value->first_name.' '.$value->last_name
                                ;?></td>
							<td><?php echo $value->experience; ?></td>
							<?php if($value->status == 1) {
                                    $status = 'Hired';
                                } elseif ($value->status == 2) {
                                   $status = 'Rejected';
                                } elseif ($value->status == 3) {
                                    $status = 'Completed Application';
                                } elseif ($value->status == 4) {
                                    $status = 'Phone Screening';
                                } elseif ($value->status == 5) {
                                    $status = 'Interviewed';
                                } else {
                                    $status = 'New';
                                }
                            ?>
                            <td><?php echo $status ?></td>
                            <td><?php echo $this->cell('Employee',['id' => $value->id])  ?></td>
                            <td>
                                <?php echo $value['job']->job_number;?>
                            </td>
						</tr>
				<?php } ?>
			</tbody>
		</table>
		<?php echo $this->element('pagination'); ?>
	</div>
</section>
<?php
	endif;
?>
<?= $this->Html->script([
	'bootstrap-switch.min.js',
	'Admin/manageCompanies'
	], [
	'block' => true
]) ?>
<?php $this->Html->scriptStart(['block' => true]); ?>
	$(function(){
		$("[name*='switch']").bootstrapSwitch();
	});
<?php $this->Html->scriptEnd(); ?>
