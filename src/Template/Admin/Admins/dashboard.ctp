<?php 
	use Cake\Utility\Inflector;

	$this->assign('title', Inflector::humanize(str_replace('-', '_', $page)));
?>

<section>
	<header><?= __('Dashboard') ?></header>

	<div class="info-box">
		<div class="box-tb">
			<div class="box-cell text-center">
				<?=
					$this->Html->image('pic_customers.png',[
						'alt' => '',
						'url' => ''
					]);
				?>
				<div class="total">
					<?= $this->Number->format($publicEmployee['employee_count']); ?>
				</div>
				<div class="name">
					<?= __('TOTAL PUBLIC EMPLOYERS') ?>
				</div>
				<!-- <?=
					$this->Html->link(__('View More'),[
						'controller' => 'Pages',
						'action' => 'manage_customers'
					],[
						'class' => 'view-more'
					]);
				?> -->
			</div>
			<div class="box-cell text-center">
				<?=
					$this->Html->image('job_posted.png',[
						'alt' => '',
						'url' => ''
					]);
				?>
				<div class="total">
					<?= $this->Number->format($job['job_count']); ?>
				</div>
				<div class="name">
					<?= __('TOTAL JOB POSTED') ?>
				</div>
				<!-- <?=
					$this->Html->link(__('View More'),[
						'controller' => 'Pages',
						'action' => 'bookings'
					],[
						'class' => 'view-more'
					]);
				?> -->
			</div>
			<div class="box-cell text-center">
				<?=
					$this->Html->image('candidate.png',[
						'alt' => '',
						'url' => ''
					]);
				?>
				<div class="total">
					<?= $this->Number->format($candidates['count']); ?>
				</div>
				<div class="name">
					<?= __('TOTAL CANDIDATES') ?>
				</div>
				<!-- <?=
					$this->Html->link(__('View More'),[
						'controller' => 'Pages',
						'action' => 'rooms'
					],[
						'class' => 'view-more'
					]);
				?> -->
			</div>
		</div>
	</div>
</section>