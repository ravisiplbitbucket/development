<ol class="breadcrumb breadcrumb-custom">
    <li>
        <?php
            echo $this->Html->link(__('Back'), [
                'controller' => 'cms-pages',
                'action' => 'index',
                '?' => $this->request->getQuery()
            ]); 
        ?>
    <li class="active">
        <?php echo h(ucwords($cmsPage->page_name));?>
    </li>
</ol>
<section class="content-header clearfix">
    <header><?= __('Edit Cms Pages') ?></header>
</section>
<!-- Main content -->
<section class="content">
    <div class="box">
        <div class="col-lg-12">
            <div id="emails">
                <?= $this->Form->create($cmsPage,array('class' => 'form-horizontal')) ?>
                    <div class="form-group">
                        <label class="control-label" for="exampleInputEmail1"><?php echo __('Meta Title'); ?></label>
                        <?= $this->Form->input('meta_title', ['class'=>'form-control', 'label' => false]); ?>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="exampleInputEmail1"><?php echo __('Page Name'); ?></label>
                        <?= $this->Form->input('page_name', ['class'=>'form-control', 'label' => false]); ?>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="exampleInputEmail1"><?php echo __('Page Content'); ?></label>
                        <?php
                            echo $this->CKEditor->loadJs();
                            echo $this->Form->textarea('page_content', ['class'=>'form-control', 'label' => false]);
                            echo $this->CKEditor->replace('page_content');
                        ?>
                    </div>
                    <div class="col-lg-8">
                        <?= $this->Form->button(__('Save'), ['class' => "btn btn-green ", 'type' => 'submit']); ?>
                    </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</section>
<?= $this->Html->script('Admin/edit_cms', ['block' => 'scriptBottom']) ?>
<style type="text/css">
    .error {
        color:#ff0000;
    }
</style>