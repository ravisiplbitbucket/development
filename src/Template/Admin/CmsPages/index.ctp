<?php 
	use Cake\I18n\Time;
	use Cake\Utility\Inflector;

	$this->assign('title', Inflector::humanize(str_replace('-', '_', $page)));

	$this->Html->css('bootstrap-switch.min.css',[
		'block' => true
	]);
?>
<section>
	<header><?= __('Manage Cms Pages') ?></header>
</section>
<section>
	<div class="filter clearfix">
		<div class="col-lg-6 pull-right text-right">
			<?php 
				echo $this->Form->create(null,[
					'type' => 'get',
					'id' => 'emailForm',
					'class' => 'form-inline',
					'url' => [
                        'controller' => 'cms-pages',
                        'action' => 'index'
                    ]
				]);

				// Add a template with the help placeholder.
				$this->Form->setTemplates([
				    'inputContainer' => '<div class="form-group">{{content}}</div>'
				]);

				// Generate an input and populate the help variable
				
				echo $this->Form->control('search', [
				    'label' => false,
				    'class' => 'form-control ',
				    'type' => 'text',
				    'placeholder' => 'Search by template name'
				]);
			?>

			<?php
				echo $this->Form->button('Search',[
					'type' => 'submit',
					'class' => 'btn btn-default btn-brown'
				]);

			?>

			<?php
				echo $this->Form->button('Clear',[
					'type' => 'button',
					'id' => 'clearForm',
					'class' => 'btn btn-default'
				]);

				$this->Form->end();
			?>
		</div>
	</div>
	<div class="table-responsive">
		<table class="table">
			<thead>
				<?=
					$this->Html->tableHeaders(['Meta title', 'Page title', 'Modified', 'Actions']);
				?>
			</thead>
			<tbody>
				<?php if($cmsPages): ?>
					<?php foreach($cmsPages as $key => $page): ?>
						<tr>
							<td>
								<span>
									<?= 
										h($page['meta_title']); 
									?>
									<br>
									<small>
									</small>
								</span>
							</td>
							<td><?= h($page['page_name']); ?></td>
							<td><?php echo h($page->modified->i18nFormat('MMMM dd, yyyy'));?></td>
							<td>
								<?php 
									echo $this->Html->link('<span class="fa fa-pencil-square-o icon-setting"></span>',
										[
											'controller' => 'cms-pages',
											'action' => 'edit',
											base64_encode($page->id),
											'?' => $this->request->query
										],[
											'escape' => false
										]);
                            	?>
							</td>
						</tr>
					<?php endforeach;?>	
				<?php else: ?>
					<div class="text-center">
						<h2>NO RECORD FOUND</h2>
						<img src="/securityofficerhr/img/no-record.png" alt="no record found">
					</div>		
				<?php endif; ?>
			</tbody>
		</table>
		<?php echo $this->element('pagination'); ?>
	</div>
</section>

<?= $this->Html->script([
	'bootstrap-switch.min.js',
	'Admin/manageEmails'
	], [
	'block' => true
]) ?>
<?php $this->Html->scriptStart(['block' => true]); ?>
	$(function(){
		$("[name*='switch']").bootstrapSwitch();
	});
<?php $this->Html->scriptEnd(); ?>
