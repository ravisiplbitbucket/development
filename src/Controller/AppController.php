<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\Utility\Hash;
/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $controller = "Users";
        $prefix = null;
        $action = 'login';
        if(isset($this->request->prefix) && ($this->request->prefix == 'admin')){ 
            $action = 'index';
            $controller = "Admins";
            $prefix = "admin";
        }
        $this->loadComponent('Auth', [
            'authenticate' => [
            'Form' => [
                'fields' => ['username' => 'username', 'password' => 'password'],
                'finder' => 'auth'
                ]
            ],
            'loginAction' => [
                'controller' => $controller,
                'action' => $action,
                'prefix' => $prefix,
                'plugin' => false
            ],
            'loginRedirect' => [
                'controller' => 'Candidates',
                'action' => 'index'
            ],
            'logoutRedirect' => [
                'controller' => $controller,
                'action' => $action,
                'prefix' => $prefix
            ]
        ]);
        /*
         * Enable the following components for recommended CakePHP security settings.
         * see http://book.cakephp.org/3.0/en/controllers/components/security.html
         */
       // $this->loadComponent('Security', ['blackHoleCallback' => 'forceSSL']);
        //$this->loadComponent('Csrf');
    }

    
    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return \Cake\Network\Response|null|void
     */
    public function beforeRender(Event $event)
    {
        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }
    }

    /**
     * Before filter callback.
     *
     * @param \Cake\Event\Event $event The beforeFilter event.
     * @return \Cake\Network\Response|null|void
     */
    // public function beforeFilter(Event $event)
    // {
    //     $this->Auth->allow(['index', 'add']);
    // }

    protected function _sendEmailMessage($to = null, $email_body = null, $subject = null, $attachments = [])
    {
        $email = new Email('default');
        $email->from(['noreply@securityofficerhr.com'])
            ->to($to)
            ->emailFormat('html')
            ->subject($subject);
        if (!empty($attachments)) {
            $email->attachments($attachments);
        }
        if ($email->send($email_body)) {
            return true;
        }
        return false;
    }


    protected function _setValidationError($errors = array()) {
       $allErrors = Hash::flatten($errors);
       $allErrors = array_unique($allErrors);
       $str = null;
       if (!empty($allErrors)) {
           $str = '<ul>';
           foreach ($allErrors as $key => $val):
               $str.= '<li>'.$val.'</li>';
           endforeach;
           $str .= '</ul>';
       }
       return $str;
   }

    protected function _checkLoggedin() {
        $session = $this->request->session();

        if ($session->check('Auth.User') && isset($this->request->prefix) && ($this->request->prefix == 'admin') && in_array($session->read('Auth.User.user_role_id'),[ROLE_SOFTWAREOWNER])) {
            return $this->redirect(['controller' => 'Admins', 'action' => 'dashboard']);
        } elseif ($session->read('Auth.User.user_role_id') == 4) {
            return $this->redirect(['prefix' => false, 'controller' => 'Employees', 'action' => 'index']);
        }
        elseif ($session->check('Auth.User') && $session->read('Auth.User.user_role_id') != ROLE_SOFTWAREOWNER) {
            $this->Flash->error(__('You are not authorized to access.'), array('key' => 'positive'));
            return $this->redirect(['prefix' => false, 'controller' => 'Candidates', 'action' => 'index']);
        }
    }

}
