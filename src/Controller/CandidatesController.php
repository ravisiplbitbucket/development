<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Routing\Router;
use Cake\Utility\Hash;
use Cake\Console\ShellDispatcher;
/**
 * Candidates Controller
 *
 * @property \App\Model\Table\CandidatesTable $Candidates
 *
 * @method \App\Model\Entity\Candidate[] paginate($object = null, array $settings = [])
 */
class CandidatesController extends AppController
{

    public $paginate = [
        'limit' => 10,
        'order' => [
            'Candidates.id' => 'Desc'
        ]
    ];
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['completeApplication','setEducation','deleteEducation','setSession','setReference','deleteReference','getHistory','editHistory']);
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->viewBuilder()->layout('dashboard');
        $this->loadModel('shifts');
        try {        
            $shift = $this->shifts
                    ->find('list', [
                        'keyField' => 'id',
                        'valueField' => 'shift'
                    ]);

            if ($this->request->is('get')) {
                $condition = $this->_setCondition($this->request->query);                
                $query = $this->Candidates
                        ->find('all')
                        ->matching(
                            'WorkingShifts', function ($q) use($condition) {
                                return $q
                                    ->where($condition['SearchByDay']);
                            }
                        )
                        ->contain(['Jobs'])
                        ->where($condition['candidate'])
                        ->andWhere(['Candidates.company_id' => $this->Auth->user('company.id'),'Candidates.status <>' => 1])
                        ->group(['candidate_id']);               
                $candidates = $this->paginate($query);
            }
                //Earlier Query
                // $query = $this->Candidates->find('all')
                //    ->where(['Candidates.company_id' => $this->Auth->user('company.id')])
                //    ->contain(['Jobs']);
                // $candidates = $this->paginate($query);            
            
        } catch (\Exception $e) {        
           // redirecting to Last page if request page doesn't exist
            if(!empty($this->request->query['page'])) {
                $this->request->query['page'] = ($this->request->query['page'] -1 > 0) ? $this->request->query['page'] -1 : 1 ;
                return $this->redirect([
                       'controller' => $this->request->params['controller'],
                       'action' => $this->request->params['action']
                       
                   ]
                );
            }
            //redirecting to previous page Incase of exception
            return $this->redirect($this->referer());
        }
        $this->set(compact('candidates','shift'));
        $this->set('_serialize', ['candidates']);
    }

    /**
     * View method
     *
     * @param string|null $id Candidate id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
        
    public function view($id = null)
    {
        $this->viewBuilder()->layout('dashboard');
        $this->loadModel('Candidates');
        $this->loadModel('CandidateQuestions');
        $candidate = $this->Candidates->get(base64_decode($id), [
            'contain' => ['Jobs', 'Countries', 'States', 'Cities', 'JobTypes', 'CandidateHistories', 'WorkingShifts']
        ]);
        
        $this->loadModel('Notes');

        $this->loadModel('CandidateCompletes');
        $complete = $this->CandidateCompletes->find()
                    ->where(['CandidateCompletes.candidate_id' => base64_decode($id)])
                    ->first();
        $this->loadModel('CandidateEducations');
        $this->loadModel('CandidateMilitaries'); 
        $this->loadModel('CandidateReferences');
        if(!empty($complete)) {
            $education = $this->CandidateEducations->find()
                    ->where(['CandidateEducations.candidate_complete_id' => $complete->id])
                    ->contain(['Educations'])
                    ->first();
            $military = $this->CandidateMilitaries->find()
                    ->where(['CandidateMilitaries.candidate_complete_id' => $complete->id])
                    ->first();
            $reference = $this->CandidateReferences->find()
                    ->where(['CandidateReferences.candidate_complete_id' => $complete->id])
                    ->first();
        }
        $notes = $this->Notes->find()
                ->where(['Notes.candidate_id' => base64_decode($id)])
                ->toArray();
        $this->loadModel('CompanyDeleteQuestions');
        $questions = $this->CompanyDeleteQuestions->find()
                    ->where(['CompanyDeleteQuestions.company_id' => $this->Auth->user('company.id')])
                    ->contain(['InterviewQuestions']);
        $points = $this->Candidates->calculate(base64_decode($id));


        $candidateQuestion = $this->CandidateQuestions->find()
                    ->where(['CandidateQuestions.candidate_id' => base64_decode($id)])
                    ->toArray();


        $this->set(compact('candidate','id','notes','questions','education','military','reference','complete','points','candidateQuestion'));
        $this->set('_serialize', ['candidate']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $candidate = $this->Candidates->newEntity();
        if ($this->request->is('post')) {
            $candidate = $this->Candidates->patchEntity($candidate, $this->request->getData());
            if ($this->Candidates->save($candidate)) {
                $this->Flash->success(__('The candidate has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The candidate could not be saved. Please, try again.'));
        }
        $jobs = $this->Candidates->Jobs->find('list', ['limit' => 200]);
        $countries = $this->Candidates->Countries->find('list', ['limit' => 200]);
        $states = $this->Candidates->States->find('list', ['limit' => 200]);
        $cities = $this->Candidates->Cities->find('list', ['limit' => 200]);
        $jobTypes = $this->Candidates->JobTypes->find('list', ['limit' => 200]);
        $this->set(compact('candidate', 'jobs', 'countries', 'states', 'cities', 'jobTypes'));
        $this->set('_serialize', ['candidate']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Candidate id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $candidate = $this->Candidates->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $candidate = $this->Candidates->patchEntity($candidate, $this->request->getData());
            if ($this->Candidates->save($candidate)) {
                $this->Flash->success(__('The candidate has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The candidate could not be saved. Please, try again.'));
        }
        $jobs = $this->Candidates->Jobs->find('list', ['limit' => 200]);
        $countries = $this->Candidates->Countries->find('list', ['limit' => 200]);
        $states = $this->Candidates->States->find('list', ['limit' => 200]);
        $cities = $this->Candidates->Cities->find('list', ['limit' => 200]);
        $jobTypes = $this->Candidates->JobTypes->find('list', ['limit' => 200]);
        $this->set(compact('candidate', 'jobs', 'countries', 'states', 'cities', 'jobTypes'));
        $this->set('_serialize', ['candidate']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Candidate id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $candidate = $this->Candidates->get($id);
        if ($this->Candidates->delete($candidate)) {
            $this->Flash->success(__('The candidate has been deleted.'));
        } else {
            $this->Flash->error(__('The candidate could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

/**
 * addNote method - add note for candidate
 * @return \Cake\Http\Response|null redirects successfully back to candidate view page.
 */ 

    public function addNote() {
        $this->loadModel('Notes');
        $note = $this->Notes->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['note_by'] = $this->Auth->user('company.first_name').' '.$this->Auth->user('company.last_name');
            $note = $this->Notes->patchEntity($note, $this->request->data);
            if ($this->Notes->save($note)) {
                $this->Flash->success(__('Notes has been saved'), array('key' => 'positive'));
                return $this->redirect($this->referer());
            } else {
                $this->Flash->error(__('Notes has not been saved'), array('key' => 'positive'));
                return $this->redirect($this->referer());
            }
        }
    }

/**
 * candidateQuestion method - add score and answer for question asked to candidate
 * @return \Cake\Http\Response|null redirects successfully back to candidate view page.
 */ 

    public function candidateQuestion($id = null) {
        $this->loadModel('CandidateQuestions');
        
        if ($this->request->is(['post', 'put'])) { 
            if (!is_null($id)) {
                $this->CandidateQuestions->deleteAll(['candidate_id' => $id]);
            }
            $candidateQues = $this->CandidateQuestions->newEntities($this->request->data);
            
            if ($this->CandidateQuestions->saveMany($candidateQues)) {
                $this->Flash->success(__('Score has been saved'), array('key' => 'positive'));
                return $this->redirect($this->referer());
            } else {
                $this->Flash->error(__('Score has not been saved'), array('key' => 'positive'));
                return $this->redirect($this->referer());
            }
        }
    }

    /**
    * Parsing Data
    * @return Condition
    **/
    protected function _setCondition($request)
    {      
        $condition = [];
        $condition['candidate'] = [];
        $condition['SearchByDay'] = [];
        if (!empty($request['tag_search'])) {
            $tag_search = $request['tag_search'];
            $condition['candidate'][] = [
                'OR' => [
                    ['Candidates.first_name LIKE' => '%'.$tag_search.'%'],
                    ['Candidates.last_name LIKE' => '%'.$tag_search.'%'],
                    ['Candidates.middle_name LIKE' => '%'.$tag_search.'%'],
                    ['Candidates.job_id' => $tag_search]
                ]
            ];
        }        
        
        //For all status , no value should be pass
        if (isset($request['armed']) && $request['armed'] != '') {
            $condition['candidate'][] = ['Candidates.certified_armed' => $request['armed']];
        }

        if (isset($request['job_type']) && !empty($request['job_type'])) {
            $condition['candidate'][] = ['Candidates.job_type_id' => $request['job_type']];
        }
        $searchByDayCond = [];
        if (isset($request['day']) && !empty($request['day'])) {
            $searchByDayCond = ['WorkingShifts.day_id' => $request['day']];
            $condition['SearchByDay'][] = $searchByDayCond;
        }
        if (isset($request['shift']) && !empty($request['shift'])) {
            $condition['SearchByDay'][] = ['WorkingShifts.shift_id' => $request['shift']];
        }        

        if (isset($request['experience']) && !empty($request['experience'])) {
            $condition['candidate'][] = ['Candidates.experience' => $request['experience']];
        }

        if (isset($request['license']) && $request['license'] != '') {
            $condition['candidate'][] = ['Candidates.driving_licence' => $request['license']];
        }

        if (isset($request['automobile']) && $request['automobile'] != '') {
            $condition['candidate'][] = ['Candidates.dependable_automobile' => $request['automobile']];
        }        
        return $condition;
    }
/**
 * emailApplication  method : email send to candidate to complete its application form
 * @param $candId,$companyId   candidate ID, Company Id
 * @return \Cake\Http\Response|null redirects back to candidate view page
 * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
 */ 
    
    public function emailApplication($candId,$companyId) {
        $this->loadModel('Companies');
        $candidate = $this->Candidates->find()
                    ->where(['Candidates.id' => base64_decode($candId)])
                    ->first();
        $link = Router::url(
                    array(
                       'controller' => 'Candidates',
                       'action' => 'completeApplication',
                       $candId,$companyId
                    ),
                    true
                );
        $this->loadModel('EmailTemplates');
        $temp = $this->EmailTemplates->find()->where(['EmailTemplates.id' => 4])
                -> first();
        // replace #NAME with username and $LINK with forgot password link
        $logo = $this->Companies->find()
                ->where(['Companies.id' => base64_decode($companyId)])
                ->first();
         
        $image = Router::url('/logo/companies/website_logo/'.$logo->logo_path.'/'.$logo->website_logo,true);
        $temp['mail_body'] = str_replace(
            array('#LOGO','#NAME','#LINK'),
            array(
            $image,
            $candidate->first_name.' '.$candidate->last_name,
            $link),
            $temp['mail_body']
        );
       
        // sending forgot password mail
        $this->_sendEmailMessage($candidate->email, $temp['mail_body'], $temp['subject']);


        if ($candidate->is_email_sent == 1) {
            $this->Flash->error(__('Complete application link has been already sent'),array('key' => 'positive'));
            return $this->redirect(['action' => 'view',$candId]);
        }
        $candidate->is_email_sent = 1;
        $this->Candidates->save($candidate);
        $this->Flash->success(__('Complete application link has been sent to candidate'), array('key' => 'positive'));
        return $this->redirect(['action' => 'view',$candId]);
    }
/**
 * completeApplication  method
 * @param $candId,$companyId   candidate ID, Company Id
 * @return \Cake\Http\Response|null redirects back to job listing page
 * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
 */  

    public function completeApplication($candId,$companyId) {
        $this->viewBuilder()->layout('home');
        $this->loadModel('CandidateCompletes');
        $this->loadModel('Languages');
        $this->loadModel('Educations');
        $this->loadModel('CandidateHistories');
        $this->loadModel('Companies');
        $language = $this->Languages->find('list', [
            'keyField' => 'id',
            'valueField' => 'language'
        ]);
        $candidates = $this->Candidates->find()
                    ->where(['Candidates.id' => base64_decode($candId)])
                    ->contain(['Jobs'])
                    ->first();
        $educations = $this->Educations->find('list',[
            'keyField' => 'id',
            'valueField' => 'education_name'
        ]);
        $id = $url = $this->Companies->find()
            ->where(['Companies.id' => base64_decode($companyId)])
            ->toArray();
        $histories = $this->CandidateHistories->find('list',[
            'keyField' => 'id',
            'valueField' => 'company_name'
        ])->where(['CandidateHistories.candidate_id' => base64_decode($candId)]);
        $history = $this->CandidateHistories->newEntity();
        $candidatecomplete = $this->CandidateCompletes->newEntity();    
        if ($this->request->is('post')) {
            if(!empty($this->request->session()->read('CanditateComplete'))) {
                $tmp = $this->request->session()->read('CanditateComplete');
                $data = array_merge($tmp,$this->request->data);
            }
            $candidatecomplete = $this->CandidateCompletes->patchEntity(
            	$candidatecomplete, $data,['associated' => ['CandidateEducations','CandidateMilitaries', 'CandidateReferences','Candidates' => ['CandidateHistories']]]);
            $candidatecomplete->candidate_id=base64_decode($candId);
            $candidatecomplete->company_id = base64_decode($companyId);
            
            if ($this->CandidateCompletes->save($candidatecomplete)) {
                //Get Last saved Id and Saved It in Session So as To Use It in Previous Tab
                $this->request->session()->delete('CanditateComplete');
                $this->Flash->success(__('Your application has been submitted.We will Contact you soon'), array('key' => 'positive'));
                $url = $this->Companies->find()
                ->where(['Companies.id' => base64_decode($companyId)])
                ->first();
                
                $path = $url->website_url;

                return $this->redirect($path);
            // $this->Flash->error(__('The job is saved.'),array('key' => 'positive'));
            } else {
                
                $this->Flash->error(__('The job could not be saved. Please, try again.'),array('key' => 'positive'));
            }
        }
        $this->request->session()->delete('CanditateComplete');
        $this->set(compact('language','candId','companyId','candidates','educations','candidatecomplete','history','histories','id'));
    }

    public function setSession($candId, $companyId) {        
        if ($this->request->is('post')) { 
            $this->request->data['candidate_id'] = base64_decode($candId);
            $this->request->data['company_id'] = base64_decode($companyId);
            if($this->request->is('ajax')) {
                $this->viewBuilder()->layout('ajax');
                if(!empty($this->request->data['language_ids'])){
                	$this->request->data['language_ids'] = implode(',', $this->request->data['language_ids']);
                }
                $data = $this->request->data;
                if($this->request->session()->read('CanditateComplete')) {
                    $tmp = $this->request->session()->read('CanditateComplete');
                    $data = array_merge($tmp,$this->request->data);
                }
                $this->request->session()->write('CanditateComplete',$data);
                // pr($session);die;
                $response=[
                    'status' => 200,
                ];
                $this->set(compact('response'));                
            }
        } 
    }

    public function setEducation($candId, $companyId) {
    	$this->loadModel('CandidateEducations');
    	$this->loadModel('Educations');
    	$education= $this->CandidateEducations->newEntity();  
    	if($this->request->is('post')) {
            $education = $this->CandidateEducations->patchEntity($education, $this->request->getData());
    		$educations = $this->Educations->find()
    			->where(['id' => $this->request->data['education_id']])
    			->first();
            if($this->request->is('ajax')) {
	           	$this->viewBuilder()->layout('ajax');
	            $data = $this->request->data;
                if($this->request->session()->check('CanditateComplete.candidate_educations')){
	               $tmp['candidate_educations'] = $this->request->session()->read('CanditateComplete.candidate_educations');
                }
                $tmp['candidate_educations'][] = $this->request->data;

	            $this->request->session()->write('CanditateComplete.candidate_educations',$tmp['candidate_educations']);
                $educationIdInSession = count($tmp['candidate_educations'])-1;
                $this->viewBuilder()->layout('ajax');
                $this->set(compact('educationIdInSession', 'education', 'educations'));
                $this->render('/Element/Candidate/education_list');
	        }
    	}    	 
    }
    public function setReference($candId, $companyId) {
    	$this->loadModel('CandidateReferences');
    	$reference= $this->CandidateReferences->newEntity();  
    	if($this->request->is('post')) {
            $reference = $this->CandidateReferences->patchEntity($reference, $this->request->getData());
    		
            if($this->request->is('ajax')) {
	           	$this->viewBuilder()->layout('ajax');
	            $data = $this->request->data;

	            $tmp['candidate_references'] = $this->request->session()->read('CanditateComplete.candidate_references');
	            $tmp['candidate_references'][] = $this->request->data;

	            $this->request->session()->write('CanditateComplete.candidate_references',$tmp['candidate_references']);
                $referenceIdInSession = count($tmp['candidate_references'])-1;
                $this->viewBuilder()->layout('ajax');
                $this->set(compact('referenceIdInSession', 'reference'));
                $this->render('/Element/Candidate/reference_list');
       		}
    	}    	 
    }

    public function deleteReference($id) {
         if($this->request->is('ajax')) {
            $this->viewBuilder()->layout('ajax');
            if($this->request->session()->check('CanditateComplete.candidate_references')) {
                $reference = $this->request->session()->read('CanditateComplete.candidate_references');
                unset($reference[$id]);
                $this->request->session()->delete('CanditateComplete.candidate_references');
                if(!empty($reference)){
                    $this->request->session()->write('CanditateComplete.candidate_references', $reference);
                }
                $response=[
                    'status' => 200,
                ];
                $this->set(compact('response'));     
            }
        }
    }

    public function deleteEducation($id) {
         if($this->request->is('ajax')) {
            $this->viewBuilder()->layout('ajax');
            if($this->request->session()->check('CanditateComplete.candidate_educations')) {
                $education = $this->request->session()->read('CanditateComplete.candidate_educations');
                unset($education[$id]);
                $this->request->session()->write('CanditateComplete.candidate_educations', $education);
                $response=[
                    'status' => 200,
                ];
                $this->set(compact('response'));     
            }
        }    
    }

    public function getHistory($id){
        $this->loadModel('CandidateHistories');
        $history = $this->CandidateHistories->find()
            ->where(['CandidateHistories.id' => base64_decode($id)])
            ->first();
        if($this->request->is('ajax')) {
            $this->viewBuilder()->layout('ajax');
            $this->set(compact('history'));
            $this->render('/Element/Candidate/edit_history');
        }  
    }

    public function editHistory($id){
        if($this->request->is('ajax')) {
           $this->autoRender = false;
        } 
        $this->loadModel('CandidateHistories');
        $history = $this->CandidateHistories->find()
            ->where(['CandidateHistories.id' => base64_decode($id)])
            ->first();
        $response=[
            'status' => 400,
            'msg' => 'Something went wrong'
        ];
        if($this->request->is('post')) {
        	if(!empty($this->request->data['rounds_verified'])) {
        		$this->request->data['rounds_verified'] = implode(',', $this->request->data['rounds_verified']);
        	}
        	if(!empty($this->request->data['way_report_submitted'])) {
        		$this->request->data['way_report_submitted'] = implode(',', $this->request->data['way_report_submitted']);
        	}
        	if(!empty($this->request->data['shift_work'])) {
        		$this->request->data['shift_work'] = implode(',', $this->request->data['shift_work']);
        	}
        	if(!empty($this->request->data['equipment_issued'])) {
        		$this->request->data['equipment_issued'] = implode(',', $this->request->data['equipment_issued']);
        	}
        	if(!empty($this->request->data['vechicle_type'])) {
                $this->request->data['vechicle_type'] = implode(',', $this->request->data['vechicle_type']);
            }

            $history = $this->CandidateHistories->patchEntity($history, $this->request->getData());
            if($this->CandidateHistories->save($history)) {
                $response=[
                    'status' => 200,
                    'msg' => 'History updated'
                ];
            }
        } 
        $this->set(compact('response'));  
    }
/**
 * Hire  method
 * @param $candId   candidate ID
 * @return \Cake\Http\Response|null redirects back to candidate view page page
 * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
 */

    public function hire($candId) {
      $candidate = $this->Candidates->find()
                            ->where([
                                'Candidates.id' => base64_decode($candId)
                            ]);

        if ($candidate->isEmpty()) {
            $this->Flash->error(__('There is no candidate with this ID.'),array('key' => 'positive'));
            return $this->redirect('/');
        }
        $candidate = $candidate->first();
        $candidate->status = 1;
        
        if ($this->Candidates->save($candidate)) {
            $this->Flash->success(__('Candidate has been hired'), array('key' => 'positive'));
            return $this->redirect(['action' => 'view',$candId]);
        }
        $this->Flash->error(__('Some errors occurred while hiring candidate. Please try again.'), array('key' => 'positive'));
        return $this->redirect(['action' => 'view',$candId]);  
    }   
/**
 * Reject  method
 * @param $candId   candidate ID
 * @return \Cake\Http\Response|null redirects back to candidate view page page
 * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
 */

    public function reject($candId) {
      $candidate = $this->Candidates->find()
                            ->where([
                                'Candidates.id' => base64_decode($candId)
                            ]);

        if ($candidate->isEmpty()) {
            $this->Flash->error(__('There is no candidate with this ID.'),array('key' => 'positive'));
            return $this->redirect('/');
        }
        $candidate = $candidate->first();
        $candidate->status = 2;
        
        if ($this->Candidates->save($candidate)) {
            $this->Flash->success(__('Candidate has been rejected'), array('key' => 'positive'));
            return $this->redirect(['action' => 'view',$candId]);
        }
        $this->Flash->error(__('Some errors occurred while rejecting candidate. Please try again.'), array('key' => 'positive'));
        return $this->redirect(['action' => 'view',$candId]);  
    }
/**
 * statusUpdate  method
 * @param $candId   candidate ID
 * @return \Cake\Http\Response|null redirects back to candidate view page page
 * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
 */  

    public function statusUpdate($candId) {
        $candidate = $this->Candidates->find()
                    ->where([
                        'Candidates.id' => base64_decode($candId)
                    ]);
        if($this->request->is(['put','post']) ) {
           $candidate = $this->Candidates->newEntity();
           $candidate->id = base64_decode($candId);
           $candidate = $this->Candidates->patchEntity($candidate, $this->request->data);
           if ($this->Candidates->save($candidate)) {
               $this->Flash->success(__('
                  Candidate status has been updated successfully.'), array('key' => 'positive'));
               return $this->redirect(['action' => 'view',$candId]);
           }else{   
           $this->Flash->error(__('Unable to update candidate'), array('key' => 'positive'));
           }
           pr($candidate->errors());die;
        }
        $this->set(compact('candidate'));
    } 

    public function jobView($id) {
        $this->viewBuilder()->layout('dashboard');
        $this->loadModel('Jobs');
        $job = $this->Jobs->find()
                ->where(['Jobs.id' => base64_decode($id)])
                ->contain(['Educations','JobTypes','Cities','States','Countries'])
                ->first();
        
        $this->set(compact('job'));
    }

    public function printEmployee($id) {
        $this->viewBuilder()->layout('print');
        $this->loadModel('Languages');
        $this->loadModel('CompanyDeleteQuestions');
        $companyId = $this->Auth->user('company.id');
        $questions = $this->CompanyDeleteQuestions->findByCompanyId($companyId)->contain(['InterviewQuestions']);
        $candidate = $this->Candidates->get(base64_decode($id), [
            'contain' => ['CandidateCompletes' => ['CandidateReferences','CandidateEducations' => ['Educations'],'CandidateMilitaries'],'Jobs', 'Countries', 'States', 'Cities', 'JobTypes', 'CandidateHistories', 'WorkingShifts','CandidateQuestions' => ['CompanyDeleteQuestions' => ['InterviewQuestions']]]
        ]);
        if(!empty($candidate['candidate_completes'][0]->language_ids)) {
            $lang = explode(',', $candidate['candidate_completes'][0]->language_ids);
            $language = $this->Languages->find('list', [
                'keyField' => 'id',
                'valueField' => 'language'
            ])
            ->where(['Languages.id IN' => $lang])
            ->toArray();  
        }
        
        $this->set(compact('candidate','language', 'questions'));
    }

}
