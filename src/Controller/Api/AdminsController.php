<?php
namespace App\Controller\Api;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Controller\Component\RequestHandlerComponent;
use Cake\Network\Exception\NotFoundException;

/**
 * Api/Admins Controller
 *
 * 
 */
class AdminsController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
    }

    public function active()
    {
        if ($this->request->is('ajax')) {
            $this->loadModel('Companies');
            $id = base64_decode($this->request->query('id'));
            $state = $this->request->query('state');
            try {
                $Companies = $this->Companies->get($id);
                $stateMessage = 'deactivated';
                $status = 0;
                if ($state == 'true') {
                    $stateMessage = 'activated';
                    $status = 1;
                }
                $Companies['is_active'] = $status;
                if ($this->Companies->save($Companies)) {
                    $response = [
                        'status'=> 1,
                        'message' => __('Company has been {0} successfully', $stateMessage)
                    ];
                } else {
                    $response = [
                        'status'=> 0,
                        'message' => __('Unable To {0} the Company', $stateMessage)
                    ];
                }            
            } catch (\Exception $error) {
                $response = [
                    'status'=> 0,
                    'message' => __('Record Not Found')
                ];               
            }
        } else {
            $response = [
                'status' => 0,
                'message' => __('Invalid Format Request, only ajax request is allowed')
            ];
        }
        $this->set(compact('response'));
        $this->set('_serialize', ['response']);
        
    }

    public function changeJobStatus()
    {
        if ($this->request->is('ajax')) {
            $this->loadModel('Jobs');
            $id = base64_decode($this->request->query('id'));
            $state = $this->request->query('state');
            try {
                $Job = $this->Jobs->get($id);
                $stateMessage = 'deactivated';
                $status = 2;
                if ($state == 'true') {
                    $stateMessage = 'activated';
                    $status = 1;
                }
                $Job['active'] = $status;
                if ($this->Jobs->save($Job)) {
                    $response = [
                        'status'=> 1,
                        'message' => __('Job has been {0} successfully', $stateMessage)
                    ];
                } else {
                    $response = [
                        'status'=> 0,
                        'message' => __('Unable To {0} the Job', $stateMessage)
                    ];
                }            
            } catch (\Exception $error) {
                $response = [
                    'status'=> 0,
                    'message' => __('Record Not Found')
                ];               
            }
        } else {
            $response = [
                'status' => 0,
                'message' => __('Invalid Format Request, only ajax request is allowed')
            ];
        }
        $this->set(compact('response'));
        $this->set('_serialize', ['response']);
        
    }

    /**
   * delete candidate Details
   *
   *@return response status
   */
    public function deleteCandidate($id = null)
    {
        $this->loadModel('Candidates');
        if ($this->request->is('ajax')) {
            $id = base64_decode($this->request->query('id'));
            try {
                $candidate = $this->Candidates->get($id);
                if ($this->Candidates->save($candidate)) {
                    $response = [
                        'status'=> 1,
                        'message' => __('Candidate has been removed successfully')
                    ];
                } else {
                    $response = [
                        'status'=> 0,
                        'message' => __('Unable To delete the candidate')
                    ];
                }            
            } catch (\Exception $error) {
                $response = [
                    'status'=> 0,
                    'message' => __('Record Not Found')
                ];               
            }
        } else {
            $response = [
                'status' => 0,
                'message' => __('Invalid Format Request, only ajax request is allowed')
            ];
        }
        $this->set(compact('response'));
        $this->set('_serialize', ['response']);
    }

    /**
   * delete job Details
   *
   *@return response status
   */
    public function deleteJob($id = null)
    {
        $this->loadModel('Jobs');
        if ($this->request->is('ajax')) {
            $id = base64_decode($this->request->query('id'));
            try {
                $job = $this->Jobs->get($id);
                $job->is_delete = (int)true;
                if ($this->Jobs->save($job)) {
                    $this->_saveDeleteLog($id);
                    $response = [
                        'status'=> 1,
                        'message' => __('Job has been removed successfully')
                    ];
                } else {
                    $response = [
                        'status'=> 0,
                        'message' => __('Unable To delete the job')
                    ];
                }            
            } catch (\Exception $error) {
                $response = [
                    'status'=> 0,
                    'message' => __('Record Not Found')
                ];               
            }
        } else {
            $response = [
                'status' => 0,
                'message' => __('Invalid Format Request, only ajax request is allowed')
            ];
        }
        $this->set(compact('response'));
        $this->set('_serialize', ['response']);
    }

    /**
   * delete job Details
   *
   *@return response status
   */
    public function deleteEmail($id = null)
    {
        $emailTemplatesTable = TableRegistry::get('EmailTemplates');
        if ($this->request->is('ajax')) {
            $id = base64_decode($this->request->query('id'));
            try {
                $emailTemplate = $emailTemplatesTable->get($id);
                $emailTemplate->is_delete = (int)true;
                if ($emailTemplatesTable->save($emailTemplate)) {
                    $response = [
                        'status'=> 1,
                        'message' => __('Template has been removed successfully')
                    ];
                } else {
                    $response = [
                        'status'=> 0,
                        'message' => __('Unable To delete the template')
                    ];
                }            
            } catch (\Exception $error) {
                $response = [
                    'status'=> 0,
                    'message' => __('Record Not Found')
                ];               
            }
        } else {
            $response = [
                'status' => 0,
                'message' => __('Invalid Format Request, only ajax request is allowed')
            ];
        }
        $this->set(compact('response'));
        $this->set('_serialize', ['response']);
    }

    /**
    * Delete Job log
    *@return save status
    */
    protected function _saveDeleteLog($jobId)
    {
        $this->loadModel('JobDeleteLogs');
        $jobDeleteLogEntity = $this->JobDeleteLogs->newEntity();
        $jobDeleteLogEntity->user_id = $this->Auth->user('id');
        $jobDeleteLogEntity->job_id = $jobId;
        if ($this->JobDeleteLogs->save($jobDeleteLogEntity)) {
            return true;
        } else {
            return false;
        }
    }
}
