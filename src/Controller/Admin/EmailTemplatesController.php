<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;

/**
 * Settings Controller
 *
 */
class EmailTemplatesController extends AppController
{
    public $helpers = ['AkkaCKEditor.CKEditor'];
    public $limit = 20;

    /**
     * emailTemplateList method
     *
     * @return \Cake\Network\Response|null
     */
    public function emailTemplateList()
    {
        $this->viewBuilder()->layout('backend/admin');
        $page = __('Email Templates');
        try {
            $emailTemplatesTable = TableRegistry::get('EmailTemplates');
            if ($this->request->is('get')) {
                $condition = $this->_setEmailCondition($this->request->query);
                $emailTemplates = $emailTemplatesTable
                                ->find('all')
                                ->where(['is_delete' => (int)false, $condition['email_templates']]);
                $emailTemplates = $this->paginate($emailTemplates);
            }
        } catch (NotFoundException $e) {
            // redirecting to Last page if request page doesn't exist
            $this->request->query['page'] = $this->request->query['page'] - 1;
            return $this->redirect([
                   'controller' => $this->request->params['controller'],
                   'action' => $this->request->params['action'],
                ]
            );
        }
        $this->set(compact('emailTemplates', 'page'));
    }

    /**
     * editEmailTemplate method
     *
     * @return \Cake\Network\Response|null
     */
    public function editEmailTemplate($emailTemplateId = null) {
        $this->viewBuilder()->layout('backend/admin');
        $page = __('Email Templates');
        $title = __('Edit Email Templates');
        $emailTemplateId = base64_decode($emailTemplateId);
        $emailTemplatesTable = TableRegistry::get('EmailTemplates');
        $emailTemplate = $emailTemplatesTable->get($emailTemplateId);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $emailTemplate = $emailTemplatesTable->patchEntity($emailTemplate, $this->request->data);
            if ($emailTemplatesTable->save($emailTemplate)) {
                $this->Flash->success(__('The email template has been saved.'));
                return $this->redirect(['action' => 'emailTemplateList']);
            } else {
                $this->Flash->error(__('The email template could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('emailTemplate', 'title', 'page'));
    }

    protected function _setEmailCondition($request)
    {      
        $condition = [];
        $condition['email_templates'] = [];
        if (!empty($request['search'])) {
            $tag_search = trim($request['search']);
            $condition['email_templates'][] = [
                'OR' => [
                    ['template_used_for LIKE' => '%'.$tag_search.'%'],
                ]
            ];
        }
        return $condition;
    }

}
