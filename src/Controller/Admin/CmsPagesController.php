<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\ORM\TableRegistry;


/**
 * CmsPages Controller
 *
 *
 * @method \App\Model\Entity\CmsPage[] paginate($object = null, array $settings = [])
 */
class CmsPagesController extends AppController
{
    public $helpers = ['AkkaCKEditor.CKEditor'];
    public $limit = 20;

    public function initialize()
    {
        parent::initialize();  
        $this->viewBuilder()->layout('backend/admin');
    }

    protected function _setCondition($request)
    {      
        $condition = [];
        $condition['cms_pages'] = [];
        if (!empty($request['search'])) {
            $tag_search = trim($request['search']);
            $condition['cms_pages'][] = [
                'OR' => [
                    'meta_title LIKE' => '%'.$tag_search.'%',
                    'page_name LIKE' => '%'.$tag_search.'%',
                ]
            ];
        }
        return $condition;
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $cmsPages = $this->paginate($this->CmsPages);
        $page = __('Cms Pages');
        try {
            $cmsPagesTable = TableRegistry::get('CmsPages');
            if ($this->request->is('get')) {
                $condition = $this->_setCondition($this->request->query);
                $cmsPages = $cmsPagesTable
                                ->find('all')
                                ->where([$condition['cms_pages']]);
                $cmsPages = $this->paginate($cmsPages);
            }
        } catch (NotFoundException $e) {
            // redirecting to Last page if request page doesn't exist
            $this->request->query['page'] = $this->request->query['page'] - 1;
            return $this->redirect([
                   'controller' => $this->request->params['controller'],
                   'action' => $this->request->params['action'],
                ]
            );
        }
        $this->set(compact('cmsPages', 'page'));
        $this->set('_serialize', ['cmsPages', 'page']);
    }

    /**
     * View method
     *
     * @param string|null $id Cms Page id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $cmsPage = $this->CmsPages->get($id, [
            'contain' => []
        ]);

        $this->set('cmsPage', $cmsPage);
        $this->set('_serialize', ['cmsPage']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $cmsPage = $this->CmsPages->newEntity();
        if ($this->request->is('post')) {
            $cmsPage = $this->CmsPages->patchEntity($cmsPage, $this->request->getData());
            if ($this->CmsPages->save($cmsPage)) {
                $this->Flash->success(__('The cms page has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The cms page could not be saved. Please, try again.'));
        }
        $this->set(compact('cmsPage'));
        $this->set('_serialize', ['cmsPage']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Cms Page id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $page = __('Cms Pages');
        $title = __('Edit Cms Page');
        try {
            $cmsPage = $this->CmsPages->get(base64_decode($id));
        }catch (RecordNotFoundException $e) {
            $this->Flash->error(__('The cms page not found, try again.'));
            return $this->redirect($this->referer());
        }        
        if ($this->request->is(['patch', 'post', 'put'])) {
            $cmsPage = $this->CmsPages->patchEntity($cmsPage, $this->request->getData());
            if ($this->CmsPages->save($cmsPage)) {
                $this->Flash->success(__('The cms page has been updated.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The cms page could not be saved. Please, try again.'));
        }
        $this->set(compact('cmsPage', 'page' , 'title'));
        $this->set('_serialize', ['cmsPage','page' , 'title']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Cms Page id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);              

        if ($this->CmsPages->delete($cmsPage)) {
            $this->Flash->success(__('The cms page has been deleted.'));
        } else {
            $this->Flash->error(__('The cms page could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
