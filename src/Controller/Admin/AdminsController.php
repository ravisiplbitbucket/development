<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Utility\Text;
use Cake\Routing\Router;

/**
 * Admins Controller
 *
 * @property \App\Model\Table\AdminsTable $Admins
 */
class AdminsController extends AppController
{
    public $limit = 10;
    public $paginate = [
        'limit' => 10
    ];

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Paginator');
        $this->loadComponent('RequestHandler');
        $this->Auth->allow('index');
    }

/**
    * Index method
    *
    * @return \Cake\Network\Response|null
*/
    public function index()
    {
        $this->_checkLoggedin();
        $title = __('Administrator Login');
        $this->viewBuilder()->layout('backend/login');
        $user = TableRegistry::get('Users');
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {                
                if ($user['user_role_id'] == 1 && $user['active'] == 1) {     
                    $this->Auth->setUser($user);
                    return $this->redirect(['controller' => 'Admins', 'action' => 'dashboard']);                    
                } else {
                    $this->Flash->error(__('You are not authorized to access.'));
                }
            } else {
                $this->Flash->error(__('Invalid username or password, try again'));
            }
            return $this->redirect($this->referer());
        }
        $this->set(compact('user','title'));
    }

    public function dashboard()
    {
        $title = __('Administrator Dashboard');
        $page = __('dashboard');
        $this->viewBuilder()->layout('backend/admin');
        $this->loadModel('Users');
        $this->loadModel('Jobs');
        $this->loadModel('Candidates');

        //Get companines count 
        $publicEmployee = $this->Users->find();
        $publicEmployee = $publicEmployee->select([
                            'id',
                            'employee_count' => $publicEmployee->func()->count('Companies.id')
                        ])
                        ->contain([
                            'Companies'=> [
                                'fields' => [
                                    'id'
                                ]
                            ]
                        ])
                        ->Where([
                            'active' => (int)true
                        ])
                        ->first();

        //Get total job posted
        $job = $this->Jobs->find();
        $job =  $job->select([
                    'job_count' => $job->func()->count('id')
                ])
                ->first();

        //Get total candidates
        $candidates = $this->Candidates->find();
        $candidates =   $candidates->select([
                            'count' => $candidates->func()->count('id')
                        ])
                        ->first();
        $this->set(compact('title', 'page', 'publicEmployee', 'job', 'candidates'));
    }

    public function viewCompany($id = null)
    {
        $page = __('manage-companies');
        $this->viewBuilder()->layout('backend/admin');
        $this->loadModel('Companies');
        try {
            if ($this->request->is('get')) {
                $id = base64_decode($id);
                $company = $this->Companies->get($id, [
                    'contain' => [
                        'Users',
                        'Jobs',
                        'Countries',
                        'States',
                        'Cities'
                    ]
                ]);
                $this->set(compact('page', 'company'));
            }
        } catch (\Exception $e){
            $this->Flash->error(__('Record not found!'));
            return $this->redirect($this->referer());
        }
    }

    public function logout()
    {
        $this->Auth->logout();
        return $this->redirect('/admin');
    }

    public function manageJobs()
    {
        $page = __('manage-jobs');
        $this->viewBuilder()->layout('backend/admin');
        $this->loadModel('Jobs');
        try {
            if ($this->request->is('get')) {
                $condition = $this->_setJobCondition($this->request->query);
                $query = $this->Jobs
                        ->find('all')
                        ->contain([
                            'Candidates',
                            'Countries',
                            'States',
                            'Cities'
                        ])
                        ->where([$condition['job'], 'is_delete' => (int) false]);
                $jobs = $this->paginate($query);
            }
        } catch (\Exception $e) {        
           // redirecting to Last page if request page doesn't exist
            if(!empty($this->request->query['page'])) {
                $this->request->query['page'] = ($this->request->query['page'] -1 > 0) ? $this->request->query['page'] -1 : 1 ;
                return $this->redirect([
                       'controller' => $this->request->params['controller'],
                       'action' => $this->request->params['action']
                       
                   ]
                );
            }
            //redirecting to previous page Incase of exception
            return $this->redirect($this->referer());
        }
        $this->set(compact('title', 'page', 'jobs'));
        $this->set('_serialize', ['jobs']);
    }

    public function manageCompanies()
    {
        $page = __('manage-companies');
        $this->viewBuilder()->layout('backend/admin');
        $this->loadModel('Companies');
        try {
            if ($this->request->is('get')) {
                $condition = $this->_setCondition($this->request->query);
                $query = $this->Companies
                        ->find('all')
                        ->matching(
                            'Users', function ($q) use($condition) {
                                return $q
                                    ->where($condition['SearchByEmail']);
                            }
                        )
                        ->contain([
                            'Jobs',
                            'Users'
                        ])
                        ->where($condition['companies']);
                $companies = $this->paginate($query);
            }
        } catch (\Exception $e) {     
           // redirecting to Last page if request page doesn't exist
            if(!empty($this->request->query['page'])) {
                $this->request->query['page'] = ($this->request->query['page'] -1 > 0) ? $this->request->query['page'] -1 : 1 ;
                return $this->redirect([
                       'controller' => $this->request->params['controller'],
                       'action' => $this->request->params['action']
                       
                   ]
                );
            }
            //redirecting to previous page Incase of exception
            return $this->redirect($this->referer());
        }
        $this->set(compact('title', 'page', 'companies'));
        $this->set('_serialize', ['companies']);
    }

    /**
    * Parsing Data
    * @return Condition
    **/
    protected function _setCondition($request)
    {      
        $condition = [];
        $condition['companies'] = [];
        $condition['SearchByEmail'] = [];
        if (!empty($request['search'])) {
            $tag_search = trim($request['search']);
            if (filter_var($tag_search, FILTER_VALIDATE_EMAIL)) {
                $condition['SearchByEmail'][] = ['Users.email' => $tag_search];
            } else {
                $condition['companies'][] = [
                    'OR' => [
                        ['Companies.first_name LIKE' => '%'.$tag_search.'%'],
                        ['Companies.last_name LIKE' => '%'.$tag_search.'%'],
                        ['Companies.company LIKE' => '%'.$tag_search.'%'],
                    ]
                ];
            }
        }
        return $condition;
    }

    protected function _setJobCondition($request)
    {      
        $condition = [];
        $condition['job'] = [];
        if (!empty($request['search'])) {
            $tag_search = trim($request['search']);
            $condition['job'][] = [
                'OR' => [
                    ['Jobs.job_title LIKE' => '%'.$tag_search.'%'],
                    ['Jobs.job_number' => $tag_search],
                    // ['Jobs.created' => $tag_search],
                ]
            ];
        }
        return $condition;
    }
    
    public function jobApplications() {
        $page = __('job-applications');
        $this->viewBuilder()->layout('backend/admin'); 
        $this->loadModel('shifts');
        $this->loadModel('Candidates');
        try {        
            $shift = $this->shifts
                    ->find('list', [
                        'keyField' => 'id',
                        'valueField' => 'shift'
                    ]);

            if ($this->request->is('get')) {
                $condition = $this->_setConditioncandidate($this->request->query); 
                
                $query = $this->Candidates
                        ->find('all')
                        ->contain(['Jobs'])
                        ->where($condition['candidate'])
                        ->andWhere(['Candidates.status <>' => 1]);
                
                $candidates = $this->paginate($query);
                
            }
                //Earlier Query
                // $query = $this->Candidates->find('all')
                //    ->where(['Candidates.company_id' => $this->Auth->user('company.id')])
                //    ->contain(['Jobs']);
                // $candidates = $this->paginate($query);            
            
        } catch (\Exception $e) {        
           // redirecting to Last page if request page doesn't exist
            if(!empty($this->request->query['page'])) {
                $this->request->query['page'] = ($this->request->query['page'] -1 > 0) ? $this->request->query['page'] -1 : 1 ;
                return $this->redirect([
                       'controller' => $this->request->params['controller'],
                       'action' => $this->request->params['action']
                       
                   ]
                );
            }
            //redirecting to previous page Incase of exception
            return $this->redirect($this->referer());
        }
        $this->set(compact('candidates','shift','page'));
        $this->set('_serialize', ['candidates']);
        
    }
    
    protected function _setConditioncandidate($request)
    {      
        $condition = [];
        $condition['candidate'] = [];
        if (!empty($request['tag_search'])) {
            $tag_search = $request['tag_search'];
            $condition['candidate'][] = [
                'OR' => [
                    ['Candidates.first_name LIKE' => '%'.$tag_search.'%'],
                    ['Candidates.last_name LIKE' => '%'.$tag_search.'%'],
                    ['Candidates.middle_name LIKE' => '%'.$tag_search.'%'],
                    ['Candidates.job_id' => $tag_search]
                ]
            ];
        }        
        
       
        return $condition;
    }
    
    public function export() {
       // output headers so that the file is downloaded rather than displayed
        header('Content-type: text/csv');
        header('Content-Disposition: attachment; filename="demo.csv"');
        
        // do not cache the file
        header('Pragma: no-cache');
        header('Expires: 0');
        
        // create a file pointer connected to the output stream
        $file = fopen('php://output', 'w');
        
        // send the column headers
        fputcsv($file, array('Candidate Name','Candidate Email','Company','From','To','State','Job Title','Is security guard company or not?','Company that you worked at','Client Company address','Client Company Phone','Did you report directly to the client contact','Client Contact Name','May we contact this person as a reference?','Kind of site','Pay rate while at the site','Did you foot patrols at this site?','How many hours per shift did you spend waliking','Were you required to submit reports?','How were reports submitted','Which shift did you work at the site?','Was this site armed?','Please check all equipment that you were issued for this site','Did you ever have to use this equipment','Did you ever have to contact emergency services','Were you ever injured at this location','Were you required to operate any type of vehicle on site','Please check which ones','Apply Date'));
        
        $this->loadModel('CandidateHistories');
        $candidate = $this->CandidateHistories->find()
                ->contain(['Candidates'
        ])
        ->toArray();
        
        $data = array();
        foreach ($candidate as $job) {
            $candidate_name = !empty($job['candidate']['first_name']) ? $job['candidate']['first_name'].' '.$job['candidate']['last_name'] : '-';
            $candidate_email = !empty($job['candidate']['email']) ? $job['candidate']['email'] : '-';
            $company_name = !empty($job['company_name']) ? $job['company_name'] : '-';
            $from = !empty($job['from_work']) ? date('d M Y',strtotime($job['from_work'])) : '-';
            $to = !empty($job['to_work']) ? date('d M Y',strtotime($job['to_work'])) : '-';
            $state = !empty($job['where_employ']) ? $job['where_employ'] : '-';
            $job_title = !empty($job['designation']) ? $job['designation'] : '-';
            $is_security = !empty($job['security_company'] && $job['security_company'] == 1) ? 'Yes' : 'No';
            $company_worked_at = !empty($job['client_company']) ? $job['client_company'] : '-';
            $client_address = !empty($job['client_address']) ? $job['client_address'] : '-';
            $client_phone = !empty($job['client_number']) ? $job['client_number'] : '-';
            $report_client = !empty($job['report_client'] && $job['report_client'] == 1 ) ? 'Yes' : 'No';
            $client_name = !empty($job['client_name']) ? $job['client_name'] : '-';
            $may_contact = !empty($job['may_we_contact'] && $job['may_we_contact'] == 1) ? 'Yes' : 'No';
            $kind_site = !empty($job['kind_site']) ? $job['kind_site'] : '-';
            $pay_rate = !empty($job['rate_paid']) ? $job['rate_paid'] : '-';
            $foot_patrol = !empty($job['foot_patrol'] && $job['foot_patrol'] == 1) ? 'Yes' : 'No';
            $shift_hours = !empty($job['hours_per_shift']) ? $job['hours_per_shift'] : '-';
            $submit_report = !empty($job['required_submit_report'] && $job['required_submit_report'] == 1) ? 'Yes' : 'No'; 
            $way_submit_report = !empty($job['way_report_submitted']) ? $job['way_report_submitted'] : '-';
            $shift = !empty($job['shift_work']) ? $job['shift_work'] : '-';
            $site_armed = !empty($job['is_site_armed'] && $job['is_site_armed'] == 1) ? 'Yes' : 'No';
            $equipment_issued = !empty($job['equipment_issued']) ? $job['equipment_issued'] : '-';
            $use_equipment = !empty($job['used_equipment'] && $job['used_equipment'] == 1) ? 'Yes' : 'No';
            $contact_emergency = !empty($job['contact_emergency'] && $job['contact_emergency'] == 1) ? 'Yes' : 'No'; 
            $ever_injured = !empty($job['ever_injured'] && $job['ever_injured'] == 1) ? 'Yes' : 'No';
            $operate_vehicle = !empty($job['operate_vehicle'] && $job['operate_vehicle'] == 1) ? 'Yes' : 'No';
            $vehicle_name = !empty($job['vehicle_type']) ? $job['vehicle_type'] : '-';
            $date_applied = !empty($job['candidate']['created']) ? date('d M Y',strtotime($job['candidate']['created'])) : '-';
            $row = array(
                $candidate_name,
                $candidate_email,
                $company_name,
                $from,
                $to,
                $state,
                $job_title,
                $is_security,
                $company_worked_at,
                $client_address,
                $client_phone,
                $report_client,
                $client_name,
                $may_contact,
                $kind_site,
                $pay_rate,
                $foot_patrol,
                $shift_hours,
                $submit_report,
                $way_submit_report,
                $shift,
                $site_armed,
                $equipment_issued,
                $contact_emergency,
                $ever_injured,
                $operate_vehicle,
                $vehicle_name,
                $date_applied
            );
            
            array_push($data, $row);
            
        }
       
        // output each row of the data
        foreach ($data as $row)
        {
        fputcsv($file, $row);
        }
        
        exit();
    }
}
