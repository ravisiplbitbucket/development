<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Routing\Router;
use Cake\Utility\Text;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[] paginate($object = null, array $settings = [])
 */
class SearchController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Paginator');
    }

    public function index()
    {
    	$this->loadModel('Candidates');
    	try {    		
    		if ($this->request->is('get')) {
    			$condition = $this->_setCondition($this->request->query);            
                $query = $this->Candidates
                        ->find('all')
                        ->matching(
                            'WorkingShifts', function ($q) use($condition) {
                                return $q
                                    ->where($condition['SearchByDay']);
                            }
                        )
                        ->where($condition['candidate'])
                        ->andWhere(['Candidates.company_id' => $this->Auth->user('company.id')])
                        ->group(['candidate_id']);
                $query = $this->paginate($query);
                pr($query);die;
                $this->set(compact('query'));
    		}
    	} catch(\Exception $error) {
            return $this->redirect($this->referer());
    	}
    }

    protected function _setCondition($request)
    {      
        $condition = [];
        $condition['candidate'] = [];
        $condition['SearchByDay'] = [];
        if (!empty($request['name'])) {
            $name = $request['name'];
            $condition['candidate'][] = [
                'OR' => [
                    ['first_name LIKE' => '%'.$name.'%'],
                    ['last_name LIKE' => '%'.$name.'%'],
                    ['middle_name LIKE' => '%'.$name.'%'],
                    ['job_id' => $name]
                ]
            ];
        }        

        
        //For all status , no value should be pass
        if (isset($request['armed']) && $request['armed'] != '') {
            $condition['candidate'][] = ['certified_unarmed' => $request['armed']];
        }

        if (isset($request['job_type']) && !empty($request['job_type'])) {
            $condition['candidate'][] = ['job_type' => $request['job_type']];
        }
        $searchByDayCond = [];
        if (isset($request['day']) && !empty($request['day'])) {
            $searchByDayCond = ['WorkingShifts.day_id' => $request['day']];
            $condition['SearchByDay'][] = $searchByDayCond;
        }

        if (isset($request['shift'])) {
            $condition['SearchByDay'][] = ['WorkingShifts.shift_id' => $request['shift']];
        }

        if (isset($request['experience']) && !empty($request['experience'])) {
            $condition['candidate'][] = ['experience' => $request['experience']];
        }

        if (isset($request['license']) && $request['license'] != '') {
            $condition['candidate'][] = ['driving_licence' => $request['license']];
        }

        if (isset($request['automobile']) && $request['automobile'] != '') {
            $condition['candidate'][] = ['dependable_automobile' => $request['automobile']];
        }
        
        return $condition;
    }
}