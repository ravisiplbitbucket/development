<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Routing\Router;
use Cake\Utility\Hash;
/**
 * Candidates Controller
 *
 * @property \App\Model\Table\CandidatesTable $Candidates
 *
 * @method \App\Model\Entity\Candidate[] paginate($object = null, array $settings = [])
 */
class EditsController extends AppController
{

    public $paginate = [
        'limit' => 10,
        'order' => [
            'Candidates.id' => 'Desc'
        ]
    ];
    public function initialize()
    {
        parent::initialize();        
    }

    public function setSession() {    
        if ($this->request->is('post')) {
            $this->request->data['company_id'] = $this->Auth->user('company.id');
            if($this->request->is('ajax')) {
                $this->viewBuilder()->layout('ajax');
                if(!empty($this->request->data['step']) && ($this->request->data['step'] == 2 || $this->request->data['step'] == 4)) {
                    $data = $this->request->data;
                    if($this->request->session()->read('Candidate')) {
                        $tmp = $this->request->session()->read('Candidate');
                        $data = array_merge($tmp,$this->request->data);
                    }
                    $this->request->session()->write('Candidate',$data);
                    // pr($session);die;
                    $response=[
                        'status' => 200,
                    ];
                    $this->set(compact('response'));
                } elseif (!empty($this->request->data['step']) && $this->request->data['step'] == 3) {
                    $this->request->data = $this->__formatShiftData($this->request->data);
                    $tmp = $this->request->session()->read('Candidate');
                    $data = array_merge($tmp,$this->request->data);
                    $this->request->session()->write('Candidate',$data);
                    $response=[
                        'status' => 200,
                    ];
                    $this->set(compact('response'));
                } elseif (!empty($this->request->data['step']) && $this->request->data['step'] == 5) {
                    $this->request->data['candidate_completes']['language_ids'] = implode(',', $this->request->data['candidate_completes']['language_ids']);
                    $tmp = $this->request->session()->read('Candidate');
                    $complete = $this->request->session()->read('Candidate.candidate_completes.0');
                    if(!empty($complete)){
                        $tmp['candidate_completes'][0] = array_merge($complete,$this->request->data['candidate_completes']);
                    } else{
                        $tmp['candidate_completes'][0] = $this->request->data['candidate_completes'];
                    }                    
                    $this->request->session()->write('Candidate',$tmp);
                    $response=[
                        'status' => 200,
                    ];
                    $this->set(compact('response'));
                } elseif (!empty($this->request->data['step']) && $this->request->data['step'] == 6) {
                    $tmp = $this->request->session()->read('Candidate');
                    $tmp['candidate_completes'][0]['candidate_military'] = $this->request->data['candidate_military'];
                    $this->request->session()->write('Candidate',$tmp);
                    $response=[
                        'status' => 200,
                    ];
                    $this->set(compact('response'));
                }
            }
        } 
    }

/**
* saveCandidateHistory method
 *
 * @return \Cake\Http\Response|null redirects back to candidate apply page
 * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
 */
    public function saveCandidateHistory(){
        $this->loadModel('CandidateHistories');
        $history= $this->CandidateHistories->newEntity();  
        if($this->request->is('post')) {
            $response=['status' => 400];
            if(!empty($this->request->data['rounds_verified'])){
                $this->request->data['rounds_verified'] =implode(',',$this->request->data['rounds_verified']);
            }
            if(!empty($this->request->data['way_report_submitted'])){
                $this->request->data['way_report_submitted'] =implode(',',$this->request->data['way_report_submitted']);
            }
            if(!empty($this->request->data['shift_work'])){
                $this->request->data['shift_work'] =implode(',',$this->request->data['shift_work']);
            }
            if(!empty($this->request->data['equipment_issued'])){
                $this->request->data['equipment_issued'] =implode(',',$this->request->data['equipment_issued']);
            }
            if(!empty($this->request->data['vechicle_type'])){
                $this->request->data['vechicle_type'] =implode(',',$this->request->data['vechicle_type']);
            }
            $history = $this->CandidateHistories->patchEntity($history, $this->request->getData());
            $tmp['candidate_histories'] = $this->request->session()->read('Candidate.candidate_histories');
            $tmp['candidate_histories'][] = $this->request->data;

            $this->request->session()->write('Candidate.candidate_histories',$tmp['candidate_histories']);
            if($this->request->is('ajax')) {
                $historyIdInSession = count($tmp['candidate_histories'])-1;
                $this->viewBuilder()->layout('ajax');
                $this->set(compact('response', 'history','historyIdInSession'));
                $this->render('/Element/Job/history_detail_link');
            } 
        }
    }

    public function getHistory($id){
        $this->loadModel('CandidateHistories');
        $history = $this->CandidateHistories->find()
            ->where(['CandidateHistories.id' => base64_decode($id)])
            ->first();
        if($this->request->is('ajax')) {
            $this->viewBuilder()->layout('ajax');
            $this->set(compact('history'));
            $this->render('/Element/Edit/edit_history');
        }  
    }
    
    public function editHistory($id){
        if($this->request->is('ajax')) {
           $this->autoRender = false;
        }  
        $this->loadModel('CandidateHistories');
        $history = $this->CandidateHistories->find()
            ->where(['CandidateHistories.id' => base64_decode($id)])
            ->first();
        $response=[
            'status' => 400,
            'msg' => 'Something went wrong'
        ];
        if($this->request->is('post')) {
            if(!empty($this->request->data['rounds_verified'])) {
                $this->request->data['rounds_verified'] = implode(',', $this->request->data['rounds_verified']);
            }
            if(!empty($this->request->data['way_report_submitted'])) {
                $this->request->data['way_report_submitted'] = implode(',', $this->request->data['way_report_submitted']);
            }
            if(!empty($this->request->data['shift_work'])) {
                $this->request->data['shift_work'] = implode(',', $this->request->data['shift_work']);
            }
            if(!empty($this->request->data['equipment_issued'])) {
                $this->request->data['equipment_issued'] = implode(',', $this->request->data['equipment_issued']);
            }
            if(!empty($this->request->data['vechicle_type'])) {
                $this->request->data['vechicle_type'] = implode(',', $this->request->data['vechicle_type']);
            }

            $history = $this->CandidateHistories->patchEntity($history, $this->request->getData());
            if($this->CandidateHistories->save($history)) {
                $response=[
                    'status' => 200,
                    'msg' => 'History updated'
                ];
            }
        } 
        $this->set(compact('response'));  
    }

    protected function __formatShiftData($data) {
        $shift = [
            'step' => $data['step'],
            'date_begin_work' => $data['date_begin_work'],
            'hours_available' => $data['hours_available'],
            'working_shifts'  => []
        ];
        $i = 0;
        if(!empty($data['working_shifts'])) {            
            foreach($data['working_shifts'] as $key => $value) {
                foreach ($value as $index => $day) {  
                    $shift['working_shifts'][$i]['shift_id'] = $key;
                    $shift['working_shifts'][$i]['day_id'] = $day['day_id'];
                    $i++;
                }
            }
        }
        return $shift;
    }
    public function setEducation() {
        $this->loadModel('CandidateEducations');
        $this->loadModel('Educations');
        $education= $this->CandidateEducations->newEntity();  
        if($this->request->is('post')) {
            $education = $this->CandidateEducations->patchEntity($education, $this->request->getData());
            $educations = $this->Educations->find()
                ->where(['id' => $this->request->data['education_id']])
                ->first();
            if($this->request->is('ajax')) {
                $this->viewBuilder()->layout('ajax');
                $data = $this->request->data;
                if($this->request->session()->check('Candidate.candidate_completes.0.candidate_educations')){
                   $tmp['candidate_educations'] = $this->request->session()->read('Candidate.candidate_completes.0.candidate_educations');
                }
                $tmp['candidate_educations'][] = $this->request->data;

                $this->request->session()->write('Candidate.candidate_completes.0.candidate_educations',$tmp['candidate_educations']);
                $educationIdInSession = count($tmp['candidate_educations'])-1;
                $this->viewBuilder()->layout('ajax');
                $this->set(compact('educationIdInSession', 'education', 'educations'));
                $this->render('/Element/Employee/education_list');
            }
        }        
    }

    public function setReference() {
        $this->loadModel('CandidateReferences');
        $reference= $this->CandidateReferences->newEntity();  
        if($this->request->is('post')) {
            $reference = $this->CandidateReferences->patchEntity($reference, $this->request->getData());
            
            if($this->request->is('ajax')) {
                $this->viewBuilder()->layout('ajax');
                $data = $this->request->data;

                $tmp['candidate_references'] = $this->request->session()->read('Candidate.candidate_completes.0.candidate_references');
                $tmp['candidate_references'][] = $this->request->data;

                $this->request->session()->write('Candidate.candidate_completes.0.candidate_references',$tmp['candidate_references']);
                $referenceIdInSession = count($tmp['candidate_references'])-1;
                $this->viewBuilder()->layout('ajax');
                $this->set(compact('referenceIdInSession', 'reference'));
                $this->render('/Element/Employee/reference_list');
            }
        }        
    }

    public function deleteReference($id) {
         if($this->request->is('ajax')) {
            $this->viewBuilder()->layout('ajax');
            if($this->request->session()->check('Candidate.candidate_completes.0.candidate_references')) {
                $reference = $this->request->session()->read('Candidate.candidate_completes.0.candidate_references');
                unset($reference[$id]);
                $this->request->session()->delete('Candidate.candidate_completes.0.candidate_references');
                if(!empty($reference)){
                    $this->request->session()->write('Candidate.candidate_completes.0.candidate_references', $reference);
                }
                $response=[
                    'status' => 200,
                ];
                $this->set(compact('response'));     
            }
        }
    }

    public function deleteEducation($id) {
         if($this->request->is('ajax')) {
            $this->viewBuilder()->layout('ajax');
            if($this->request->session()->check('Candidate.candidate_completes.0.candidate_educations')) {
                $education = $this->request->session()->read('Candidate.candidate_completes.0.candidate_educations');
                unset($education[$id]);
                $this->request->session()->write('Candidate.candidate_completes.0.candidate_educations', $education);
                $response=[
                    'status' => 200,
                ];
                $this->set(compact('response'));     
            }
        }    
    }
    public function edit($id) {
        $this->viewBuilder()->layout('dashboard');
        $this->loadModel('Companies');
        $this->loadModel('Candidates');
        $this->loadModel('Educations');
        $this->loadModel('Countries');
        $this->loadModel('JobTypes');
        $this->loadModel('CandidateHistories');
        $this->loadModel('Languages');
        $this->loadModel('Cities');
        $this->loadModel('States');
        $this->loadModel('Jobs');
        $language = $this->Languages->find('list', [
            'keyField' => 'id',
            'valueField' => 'language'
        ]);
        $jobs = $this->Jobs->find('list', [
            'keyField' => 'id',
            'valueField' => 'job_title'
        ])
        ->where(['Jobs.company_id' => $this->Auth->user('company.id')]);
        $country = $this->Countries->find('list', [
            'keyField' => 'id',
            'valueField' => 'name'
        ])
        ->order('Countries.is_default DESC');


        $jobtypes = $this->JobTypes->find('list', [
            'keyField' => 'id',
            'valueField' => 'job_name'
        ]);
        $educations = $this->Educations->find('list',[
            'keyField' => 'id',
            'valueField' => 'education_name'
        ]);
         $histories = $this->CandidateHistories->find('list',[
            'keyField' => 'id',
            'valueField' => 'company_name'
        ])->where(['CandidateHistories.candidate_id' => base64_decode($id)]);
        $candidate = $this->Candidates->find()
                    ->where(['Candidates.id' => base64_decode($id)])
                    ->contain(['WorkingShifts','CandidateCompletes' => ['CandidateReferences','CandidateEducations' => ['Educations'],'CandidateMilitaries'],'CandidateHistories','Jobs'])
                    ->first();
        $states = $this->States->find('list', [
            'keyField' => 'id',
            'valueField' => 'name'
        ])
        ->where(['country_id' => $candidate->country_id]);
        
        $cities = $this->Cities->find('list', [
            'keyField' => 'id',
            'valueField' => 'name'
        ])
        ->where(['state_id' => $candidate->state_id]);
        if ($this->request->is('post')) {
            if(!empty($this->request->session()->read('Candidate'))) {
                $data = $this->request->session()->read('Candidate');
            }
            $completes = array_merge($data['candidate_completes'][0],$this->request->data['candidate_completes'][0]);
            $data = array_merge($data,$this->request->data);
            $data['candidate_completes'][0] = $completes;
            $candidate = $this->Candidates->patchEntity($candidate, $data,['associated' => ['WorkingShifts','CandidateHistories','CandidateCompletes' => ['associated' => ['CandidateEducations','CandidateReferences','CandidateMilitaries']]]]);
            if ($this->Candidates->save($candidate)) {

                //Get Last saved Id and Saved It in Session So as To Use It in Previous Tab
                $this->request->session()->delete('Candidate');
                $this->Flash->success(__('The Employee is saved.'), array('key' => 'positive'));
                return $this->redirect(['controller' => 'Employees','action' => 'index']);
            
            } else {
               pr($candidate->errors());die;
                $this->Flash->error(__('The candidate could not be saved. Please, try again.'),array('key' => 'positive'));
            }
        }
        $this->request->session()->delete('Candidate');
        $this->set(compact('candidate','country','jobtypes','educations','language','jobs','histories','states','cities'));
    }

    public function education($id) {
        $this->loadModel('CandidateEducations');
        $this->loadModel('Educations');
        $educations = $this->Educations->find('list',[
            'keyField' => 'id',
            'valueField' => 'education_name'
        ]);
        try {
           if ($id != null) {                
               $education = $this->CandidateEducations->find()
                   ->where(
                       [
                           'CandidateEducations.id' => base64_decode($id)
                       ]
                   )
                   ->first();
           } 
           
            if($this->request->is('ajax')){
                if($this->request->is('post')) {
                    $history = $this->CandidateEducations->patchEntity($education, $this->request->getData());
                    $this->CandidateEducations->save($history);
                }  else {                    
                    $this->autoRender = false;
                    $this->set(compact('education','educations'));
                    return $this->render('/Element/Edit/education');
                }        
            } 
       } catch (RecordNotFoundException $e) {
           $this->Flash->success('Candidate education not found please try agian');
           return $this->redirect($this->referer());
       }

    }

    public function reference($id) {
        $this->loadModel('CandidateReferences');
        try {
            if ($id != null) {                
               $reference = $this->CandidateReferences->find()
                   ->where(
                       [
                           'CandidateReferences.id' => base64_decode($id)
                       ]
                   )
                   ->first();
            } 
           
            if($this->request->is('ajax')){
                 if($this->request->is('post')) {
                    $history = $this->CandidateReferences->patchEntity($reference, $this->request->getData());
                    $this->CandidateReferences->save($history);
                }  else { 
                    $this->autoRender = false;
                    $this->set(compact('reference'));
                    return $this->render('/Element/Edit/reference');
                }
            }            
       } catch (RecordNotFoundException $e) {
           $this->Flash->success('Candidate reference not found please try agian');
           return $this->redirect($this->referer());
       }
    }
}
