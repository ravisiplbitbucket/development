<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Routing\Router;
use Cake\Utility\Text;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[] paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['getStates','getCities','activate','forgotPassword','recoverPassword','add']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        if(in_array($this->Auth->user('user_role_id'),[4,5])) {
            return $this->redirect($this->referer());
            $this->Flash->success(__('You are not authorised to this location'), array('key' => 'positive')); 
        }
        $this->viewBuilder()->layout('dashboard');
        $this->loadModel('Countries');
        $this->loadModel('States');
        $this->loadModel('Cities');
        $country = $this->Countries->find('list', [
            'keyField' => 'id',
            'valueField' => 'name'
        ])
        ->order(['Countries.is_default' => 'ASC','Countries.name' => 'ASC']);

        $states = $this->States->find('list', [
            'keyField' => 'id',
            'valueField' => 'name'
        ])
        ->where(['country_id' => $this->Auth->user('company.country_id')]);
        $cities = $this->Cities->find('list', [
            'keyField' => 'id',
            'valueField' => 'name'
        ])
        ->where(['state_id' => $this->Auth->user('company.state_id')]);

        $user = $this->Users->find()
                   ->where(
                       [
                           'Users.id' => $this->Auth->user('id')
                       ]
                   )
                   ->contain(['Companies'])
                   ->first();
        if($this->request->is(['patch', 'post', 'put']) ) {
            $this->request->data['id'] = $user->id;
            $this->request->data['company']['id'] = $user['company']->id;
            $user = $this->Users->patchEntity($user, $this->request->data,[
                'associated' => ['Companies']
                ]
            );

            if ($this->Users->save($user)) {
                // check whether the user is super admin or not
                if($this->Auth->user('user_role_id')  == 2 ) {
                    $this->Auth->setUser($user->toArray());
                } else {
                    $client = $this->Auth->user();
                    $client['company'] = $user->company;
                }
               $this->Flash->success(__('Company has been updated successfully.'), array('key' => 'positive'));
               return $this->redirect($this->referer());
            }
        pr($user->errors());die;
        $this->Flash->error(__('Unable to update Company'), array('key' => 'positive'));
        return $this->redirect($this->referer());
        }
        $this->set(compact('country','states', 'cities','user'));
    }

    /**
     * Login method
     *
     * @return \Cake\Http\Response|null Redirects on successful dashboard.
     */
    public function login() {
        $this->viewBuilder()->layout('frontend');
        $user = TableRegistry::get('Users');
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            
            if ($user) {
                $this->Auth->setUser($user);
                if(!empty($user['company_user'])) {
                    $this->loadModel('Companies');
                    $company = $this->Companies->find()
                                ->where(['user_id' => $user['company_user']['company_user_id']])
                                ->first()
                                ->toArray();
                    $this->request->session()->write('Auth.User.company',$company);
                }
                /** check the user role is super admin and company is active **/
                if (in_array($this->Auth->user('user_role_id'),[2,3,4,5,6]) && $this->Auth->user('active') == 1) {   
                    if(in_array($this->request->session()->read('Auth.User.user_role_id'),[4,5])) {
                        return $this->redirect(['controller' => 'Employees','action' => 'index']);
                    }  
                    return $this->redirect($this->Auth->redirectUrl());  
                }
            }
            $this->Flash->error(__('Invalid username or password, try again'),array('key' => 'positive'));
        }
    }

    /**
     * logout method
     *
     * 
     * @return \Cake\Http\Response|null redirects successfully bacjk to login page
     * 
     */
    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['UserRoles', 'Companies']
        ]);

        $this->set('user', $user);
        $this->set('_serialize', ['user']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->viewBuilder()->layout('frontend');
        $this->loadModel('Countries');
        $this->loadModel('States');
        $this->loadModel('Cities');
        $this->loadModel('WayKnows');
        $country = $this->Countries->find('list', [
            'keyField' => 'id',
            'valueField' => 'name'
        ])
        ->order(['Countries.is_default' => 'ASC','Countries.name' => 'ASC']);;

        $know = $this->WayKnows->find('list', [
            'keyField' => 'id',
            'valueField' => 'name'
        ]);
        $user = $this->Users->newEntity();
        
        if ($this->request->is('post')) {
            $this->request->data['user_role_id'] = 2;
            $this->request->data['uuid'] = Text::uuid();
            $this->request->data['company']['uuid'] = Text::uuid();
            // $this->request->data['company']['signup_path'] = preg_replace('/[^a-zA-Z0-9\']/', '-', $this->request->data['company']['website_name']);
            $this->request->data['divisions'][0]['name'] = 'default';
            $this->request->data['divisions'][0]['address1'] = $this->request->data['company']['address1'];
            $this->request->data['divisions'][0]['zip'] = $this->request->data['company']['zipcode'];
            $state = $this->States->find()
                    ->where(['States.id' => $this->request->data['company']['state_id']])
                    ->first();
            $this->request->data['divisions'][0]['state'] = $state->name;
            $city = $this->Cities->find()
                    ->where(['Cities.id' => $this->request->data['company']['city_id']])
                    ->first();
            $this->request->data['divisions'][0]['city'] = $city->name;
            $this->request->data['divisions'][0]['is_deleted'] = 0;
            
            $user = $this->Users->patchEntity($user, $this->request->getData(),array('associated' => ['Companies','Divisions']));
            if ($this->Users->save($user)) {
                $this->loadModel('CompanyDeleteQuestions');
                $this->CompanyDeleteQuestions->setDefault($user['company']->id);
                $this->loadModel('EmailTemplates');
                    $temp = $this->EmailTemplates->find()->where(['EmailTemplates.id' =>1])
                    -> first();
                      
                    $link = Router::url(
                        array('controller' => 'Users', 'action' => 'activate',$this->request->data['uuid']),
                        true
                    );
                    $temp['mail_body'] = str_replace(
                            array('#NAME','#LINK'),
                            array(
                                $this->request->data['company']['first_name'].' '.$this->request->data['company']['last_name'],
                                $link
                            ), 
                        $temp['mail_body']
                    );
                    $this->_sendEmailMessage($this->request->data['email'], $temp['mail_body'], $temp['subject']);
                $this->Flash->success(__('Email verification link has been sent to your email.Please verify your account and complete your registration'),array('key' => 'positive'));

                return $this->redirect(['action' => 'login']);
            }

            $errors = $this->_setValidationError($user->errors());                
            $this->Flash->error(__($errors),array('key' => 'positive'));
        }

        $userRoles = $this->Users->UserRoles->find('list', ['limit' => 200]);
        $this->set(compact('user', 'userRoles','country', 'states', 'cities','know'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Active method
     *
     * @param string $token UUID.
     * @return \Cake\Http\Response|null Redirects on successful activate.
     * 
     */
    public function activate($token) {
        $user = $this->Users->find()
                            ->where([
                                'Users.uuid' => $token
                            ]);

        if ($user->isEmpty()) {
            $this->Flash->error(__('This link has been expired. You have already used this link.'),array('key' => 'positive'));
            return $this->redirect('/');
        }
        $user = $user->first();
        $user->active = Configure::read('UserActivated.True');
        $user->uuid = Text::uuid();
        
        if ($this->Users->save($user)) {
            $this->Flash->success(__('Your account has been successfully verified. Please log-in and enjoy.'), array('key' => 'positive'));
            return $this->redirect(['action' => 'login']);
        }
        $this->Flash->error(__('Some errors occurred while verfiying your account. Please try again.'), array('key' => 'positive'));
        return $this->redirect(['action' => 'login']);
    }
    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $userRoles = $this->Users->UserRoles->find('list', ['limit' => 200]);
        $this->set(compact('user', 'userRoles'));
        $this->set('_serialize', ['user']);
    }

    /**
     * getStates method
     *
     * @param int $countryId id.
     * @return \Cake\Http\Response|null state name and phone code.
     */

    public function getStates($countryId) {
        $this->loadModel('States');
        $this->loadModel('Countries');       

        $states = $this->States->find('list', [
            'keyField' => 'id',
            'valueField' => 'name'
        ])->where(['country_id' => $countryId]);
        $countries = $this->Countries->find()->where([
            'id' => $countryId
        ])->first();

        $phoneCode = $countries['phonecode'];
        $this->set(compact('states', 'phoneCode'));
        $this->set('_serialize', ['states', 'phoneCode']);

        if ($this->request->is('ajax')) {
            $this->viewBuilder()->layout('ajax');
            $this->render('/Element/Signup/states');
        }
    }
    /**
     * getCities method
     *
     * @param int $stateId id.
     * @return \Cake\Http\Response|null city names.
     */
    public function getCities($stateId) {
        $this->loadModel('Cities');
        $cities = $this->Cities->find('list', [
            'keyField' => 'id',
            'valueField' => 'name'
        ])->where(['state_id' => $stateId]);

        $this->set(compact('cities'));
        $this->set('_serialize', ['cities']);

        if ($this->request->is('ajax')) {
            $this->viewBuilder()->layout('ajax');
            $this->render('/Element/Signup/cities');
        }
    }

    /**
     * forgotPassword method
     *
     *@throws \Cake\Network\Exception\NotFoundException When record not found.
     * @return \Cake\Http\Response|null redirects to forgot passowrd page.
     */
    public function forgotPassword() {
        $this->viewBuilder()->layout('frontend');
        $this->loadModel('Users');

        if ($this->request->is('post')) {
            if (empty($this->request->data)) {
                throw new NotFoundException;
            }
            
            $data = $this->request->data;
            
            if (!empty($data['email'])) {
                $user = $this->Users->find()->where(['Users.email' => $data['email']])
                    ->first();
                
                if (empty($user)) {
                    $this->Flash->error(__('Email does not exist. Please provide correct email or signup'), array('key' => 'positive')
                    ); 
                    $this->redirect($this->referer());
                }               
                    
                $key = $user['uuid'];
                $link = Router::url(
                    array(
                       'controller' => 'Users',
                       'action' => 'recoverPassword',
                       $key,
                    ),
                    true
                );
                $this->loadModel('EmailTemplates');
                $temp = $this->EmailTemplates->find()->where(['EmailTemplates.id' => 2])
                        -> first();
                // replace #NAME with username and $LINK with forgot password link
                $temp['mail_body'] = str_replace(
                    array('#NAME','#LINK'),
                    array($user['username']),
                    $link,
                    $temp['mail_body']
                );
                // sending forgot password mail
                $this->_sendEmailMessage($data['email'], $temp['mail_body'], $temp['subject']);
                $this->Flash->success(__('Forgot password link has been sent to your registered email account.'), array('key' => 'positive'));
                $this->redirect(['action'=>'login']);
            }
            
            $this->redirect(['action'=>'login']);
        } 
    }

    /**
     * recoverPassword method
     *
     * @param string $token uuid.
     * @return \Cake\Http\Response|null redirects successfully back to login after recovering password.
     */
    public function recoverPassword($token) {
        $this->viewBuilder()->layout('frontend');
        $this->loadModel('Users');
        if ($this->request->is('post')) {
            $user = $this->Users->find()
                    -> where(['Users.uuid' => $token])
                    ->first();
            if (empty($user)) {
                $this->Flash->error(__('Link has been expired please Try again.'), array('key' => 'positive')
                );
            return $this->redirect($this->referer());
            }
            
            if (!empty($this->request->data['password']) && $this->request->data['password'] == $this->request->data['new_password']) {
                $this->request->data['uuid'] = Text::uuid();
                $user = $this->Users->patchEntity($user, $this->request->data);
                $user->id = $user['id'];
                if ( $this->Users->save($user) ) {
                    $this->Flash->success(__('Password has been changed successfully. Login with your new password'),array('key' => 'positive')
                 );
                    $redirectUrl = array('controller'=> 'Users','action'=>'login');
                    $this->redirect($redirectUrl);
                }
            } else {  
                $this->Flash->error(__('Make sure that both passwords match.'), array('key' => 'positive')
                );
            }

            $redirectUrl = array('controller'=> 'Users','action'=>'login');
            $this->redirect($redirectUrl);
        }
    }

/**
 * Profile method
 *
 * @param string $id user_id.
 * @throws \Cake\Network\Exception\NotFoundException When record not found.
 * @return \Cake\Http\Response|null redirects successfully back to dashboard.
 */
    public function profile($id) {
        $this->loadModel('Users');
        try {
            if ($id != null) {                
               $user = $this->Users->find()
                   ->where(
                       [
                           'Users.id' => base64_decode($id)
                       ]
                   )
                   ->contain(['Companies'])
                   ->first();
            } 
            if($this->request->is('ajax')){
                $this->autoRender = false;
                $this->set(compact('user'));
                $this->render('/Element/Dashboard/profile');
            }            
        } catch (RecordNotFoundException $e) {
           $this->Flash->success('Company not found please try agian');
           return $this->redirect($this->referer());
        }

        if($this->request->is(['patch', 'post', 'put']) ) {
            $this->request->data['id'] = $user->id;
            $this->request->data['company']['id'] = $user['company']->id;
            $user = $this->Users->patchEntity($user, $this->request->data,[
                'associated' => ['Companies']
                ]
            );

            if ($this->Users->save($user)) {
                // check whether the user is super admin or not
                if($this->Auth->user('user_role_id')  == 2 ) {
                    $this->Auth->setUser($user->toArray());
                } else {
                    $client = $this->Auth->user();
                    $client['company'] = $user->company;
                }
               $this->Flash->success(__('Company has been updated successfully.'), array('key' => 'positive'));
               return $this->redirect($this->referer());
            }
        $this->Flash->error(__('Unable to update Company'), array('key' => 'positive'));
        return $this->redirect($this->referer());
        }
       $this->set(compact('user'));
    }

/**
 * Training method - add training for setup
 * @return \Cake\Http\Response|null redirects successfully back to setup page.
 */ 

    public function training() {
        $this->loadModel('Trainings');
        $training = $this->Trainings->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['user_id'] = $this->Auth->user('id');
            if($this->Auth->user('user_role_id') != 2) {
                $this->request->data['user_id'] = $this->Auth->user('company_user.company_user_id');
            }
            $training = $this->Trainings->patchEntity($training, $this->request->data);
            if ($this->Trainings->save($training)) {
                $this->Flash->success(__('Training has been saved'), array('key' => 'positive'));
                return $this->redirect($this->referer());
            } else {
                $this->Flash->error(__('Training has not been saved'), array('key' => 'positive'));
                return $this->redirect($this->referer());
            }
        }
    } 

/**
 * Certificate method - add training for setup
 * @return \Cake\Http\Response|null redirects successfully back to setup page.
 */
    
    public function certificate() {
        $this->loadModel('Certifications');
        $certificate = $this->Certifications->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['user_id'] = $this->Auth->user('id');
            if($this->Auth->user('user_role_id') != 2) {
                $this->request->data['user_id'] = $this->Auth->user('company_user.company_user_id');
            }
            $certificate = $this->Certifications->patchEntity($certificate, $this->request->data);
            if ($this->Certifications->save($certificate)) {
                $this->Flash->success(__('Certification has been saved'), array('key' => 'positive'));
                return $this->redirect($this->referer());
            } else {
                $this->Flash->error(__('Certification has not been saved'), array('key' => 'positive'));
                return $this->redirect($this->referer());
            }
        }
    }
/**
 * Certificate method - add training for setup
 * @return \Cake\Http\Response|null redirects successfully back to setup page.
 */

    public function equipment() {
        $this->loadModel('Equipments');
        $equipment = $this->Equipments->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['user_id'] = $this->Auth->user('id');
            if($this->Auth->user('user_role_id') != 2) {
                $this->request->data['user_id'] = $this->Auth->user('company_user.company_user_id');
            }
            $equipment = $this->Equipments->patchEntity($equipment, $this->request->data);
            if ($this->Equipments->save($equipment)) {
                $this->Flash->success(__('Uniform and Equipment has been saved'), array('key' => 'positive'));
                return $this->redirect($this->referer());
            } else {
                $this->Flash->error(__('Uniform and Equipment has not been saved'), array('key' => 'positive'));
                return $this->redirect($this->referer());
            }
        }
    }
/**
 * division method - add training for setup
 * @return \Cake\Http\Response|null redirects successfully back to setup page.
 */

    public function division() {
        $this->loadModel('Divisions');
        $division = $this->Divisions->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['user_id'] = $this->Auth->user('id');
            $division = $this->Divisions->patchEntity($division, $this->request->data);
            if ($this->Divisions->save($division)) {
                $this->Flash->success(__('Divisions has been saved'), array('key' => 'positive'));
                return $this->redirect($this->referer());
            } else {
                $this->Flash->error(__('Divisions has not been saved'), array('key' => 'positive'));
                return $this->redirect($this->referer());
            }
        }
    }
/**
 * interviewQuestion method - add training for setup
 * @return \Cake\Http\Response|null redirects successfully back to setup page.
 */

    public function interviewQuestion() {
        $this->loadModel('InterviewQuestions');
        $question = $this->InterviewQuestions->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['user_id'] = $this->Auth->user('id');
            if($this->Auth->user('user_role_id') != 2) {
                $this->request->data['user_id'] = $this->Auth->user('company_user.company_user_id');
            }
            $question = $this->InterviewQuestions->patchEntity($question, $this->request->data);
            if ($this->InterviewQuestions->save($question)) {
                $this->Flash->success(__('Interview Question has been saved'), array('key' => 'positive'));
                return $this->redirect($this->referer());
            } else {
                $this->Flash->error(__('Interview Question has not been saved'), array('key' => 'positive'));
                return $this->redirect($this->referer());
            }
        }
    }
/**
 * trainingListing method - Training listing page
 * @return \Cake\Http\Response|null redirects nothing.
 */
    
    public function trainingListing() {
        $this->viewBuilder()->layout('dashboard');
        $this->loadModel('Trainings');
        $trainings = $this->Trainings->find('all')
            ->where(['Trainings.user_id' => $this->Auth->user('id'),'Trainings.is_deleted' => 0])
            ->orWhere(['Trainings.user_id' => $this->Auth->user('company.id')]);
        $this->set(compact('trainings'));
    }
/**
 * certificateListing method - Certificate listing page
 * @return \Cake\Http\Response|null redirects nothing.
 */
    
    public function certificateListing() {
        $this->viewBuilder()->layout('dashboard');
        $this->loadModel('Certifications');
        $certificates = $this->Certifications->find('all')
            ->where(['Certifications.user_id' => $this->Auth->user('id'),'Certifications.is_deleted' => 0])
            ->orWhere(['Certifications.user_id' => $this->Auth->user('company.id')]);
        $this->set(compact('certificates'));
    }
/**
 * equipmentListing method - Uniform And Equipment listing page
 * @return \Cake\Http\Response|null redirects nothing.
 */
    
    public function equipmentListing() {
        $this->viewBuilder()->layout('dashboard');
        $this->loadModel('Equipments');
        $equipments = $this->Equipments->find('all')
            ->where(['Equipments.user_id' => $this->Auth->user('id'),'Equipments.is_deleted' => 0])
            ->orWhere(['Equipments.user_id' => $this->Auth->user('company.id')]);
        $this->set(compact('equipments'));
    }
/**
 * divisionListing method - Division listing page
 * @return \Cake\Http\Response|null redirects nothing.
 */
    
    public function divisionListing() {
        $this->viewBuilder()->layout('dashboard');
        $this->loadModel('Divisions');
        $divisions = $this->Divisions->find('all')
            ->where(['Divisions.user_id' => $this->Auth->user('id'),'Divisions.is_deleted' => 0])
            ->orWhere(['Divisions.user_id' => $this->Auth->user('company.id'),'Divisions.is_deleted' => 0]);
        $this->set(compact('divisions'));
    }

/**
 * questionListing method - Uniform And Equipment listing page
 * @return \Cake\Http\Response|null redirects nothing.
 */
    
    public function questionListing() {
        $this->viewBuilder()->layout('dashboard');
        $this->loadModel('InterviewQuestions');
        $questions = $this->InterviewQuestions->find('all')
                    ->select(['InterviewQuestions.user_id','CompanyDeleteQuestions.interview_question_id'])
                    ->where(['InterviewQuestions.user_id IS NULL'])
                    ->orWhere(['InterviewQuestions.user_id' => $this->Auth->user('id')])
                    ->leftJoinWith('CompanyDeleteQuestions',function($q){
                        return $q->where(['CompanyDeleteQuestions.company_id' => $this->Auth->user('company.id')]);
                    })
                    ->autoFields(true);

        $this->set(compact('questions'));
    }
/**
 * trainingDelete method
 *
 * @param string|null $id User id.
 * @return \Cake\Http\Response|null Redirects to trainingListing.
 * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
 */
    public function trainingDelete($id = null)
    {
        $this->loadModel('Trainings');
        $this->request->allowMethod(['GET', 'delete']);
        
        $training = $this->Trainings->get(base64_decode($id));
        $training->is_deleted = 1;
        if ($this->Trainings->save($training)) {
            $this->Flash->success(__('The Training has been deleted.'), array('key' => 'positive'));
        } else {
            $this->Flash->error(__('The Training could not be deleted. Please, try again.'), array('key' => 'positive'));
        }

        return $this->redirect(['action' => 'trainingListing']);
    }
/**
 * certificateDelete method
 *
 * @param string|null $id User id.
 * @return \Cake\Http\Response|null Redirects to certificateListing.
 * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
 */
    public function certificateDelete($id = null)
    {
        $this->loadModel('Certifications');
        $this->request->allowMethod(['GET', 'delete']);
        $certificate = $this->Certifications->get(base64_decode($id));
        $certificate->is_deleted = 1;
        if ($this->Certifications->save($certificate)) {
            $this->Flash->success(__('The Certification has been deleted.'),array('key' => 'positive'));
        } else {
            $this->Flash->error(__('The Certification could not be deleted. Please, try again.'),array('key' => 'positive'));
        }

        return $this->redirect(['action' => 'certificateListing']);
    }
/**
 * equipmentDelete method
 *
 * @param string|null $id User id.
 * @return \Cake\Http\Response|null Redirects to equipmentListing.
 * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
 */
    public function equipmentDelete($id = null)
    {
        $this->loadModel('Equipments');
        $this->request->allowMethod(['GET', 'delete']);
        $equipment = $this->Equipments->get(base64_decode($id));
        $equipment->is_deleted = 1;
        if ($this->Equipments->save($equipment)) {
            $this->Flash->success(__('The Equipment has been deleted.'),array('key' => 'positive'));
        } else {
            $this->Flash->error(__('The Equipment could not be deleted. Please, try again.'),array('key' => 'positive'));
        }

        return $this->redirect(['action' => 'equipmentListing']);
    }
/**
 * questionDelete method
 *
 * @param string|null $id User id.
 * @return \Cake\Http\Response|null Redirects to certificateListing.
 * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
 */
    public function questionDelete($id = null)
    {
        $this->loadModel('InterviewQuestions');
        $this->loadModel('CompanyDeleteQuestions');
        $this->request->allowMethod(['GET', 'delete']);
        $question = $this->InterviewQuestions->get(base64_decode($id));
        $question_delete = $this->CompanyDeleteQuestions->newEntity();
        $CompanyDeleteQuestions['interview_question_id'] = $question->id;
        $CompanyDeleteQuestions['company_id'] = $this->Auth->user('company.id');
        $CompanyDeleteQuestions['is_deleted'] = 1;
        $question_delete = $this->CompanyDeleteQuestions->patchEntity($question_delete, $CompanyDeleteQuestions);
        if ($this->CompanyDeleteQuestions->save($question_delete)) {
            $this->Flash->success(__('The Question has been deleted.'),array('key' => 'positive'));
        } else {
            $this->Flash->error(__('The Question could not be deleted. Please, try again.'),array('key' => 'positive'));
        }

        return $this->redirect(['action' => 'questionListing']);
    }
/**
 * divisionDelete method
 *
 * @param string|null $id User id.
 * @return \Cake\Http\Response|null Redirects to certificateListing.
 * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
 */
    public function divisionDelete($id = null)
    {
        $this->loadModel('Divisions');
        $this->request->allowMethod(['GET', 'delete']);
        $division = $this->Divisions->get(base64_decode($id));
        $division->is_deleted = 1;
        if ($this->Divisions->save($division)) {
            $this->Flash->success(__('The Division has been deleted.'),array('key' => 'positive'));
        } else {
            $this->Flash->error(__('The Division could not be deleted. Please, try again.'),array('key' => 'positive'));
        }

        return $this->redirect(['action' => 'divisionListing']);
    }
/**
 * trainingEdit method
 *
 * @param string|null $id User id.
 * @return \Cake\Http\Response|null Redirects to trainingListing.
 * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
 */
    public function trainingEdit($id = null) {
        $this->loadModel('Trainings');
        try {
           if ($id != null) {                
               $training = $this->Trainings->find()
                   ->where(
                       [
                           'Trainings.id' => base64_decode($id)
                       ]
                   )
                   ->first();
           } 
           
            if($this->request->is('ajax')){
                $this->autoRender = false;
                $this->set(compact('training'));
                return $this->render('/Element/Setup/training_edit');
            }            
       } catch (RecordNotFoundException $e) {
           $this->Flash->success('Training not found please try agian');
           return $this->redirect($this->referer());
       }
       if($this->request->is(['put','post']) ) {
           $training = $this->Trainings->newEntity();
           $training->id = base64_decode($id);
           $training = $this->Trainings->patchEntity($training, $this->request->data);
           if ($this->Trainings->save($training)) {
               $this->Flash->success(__('
                  Training has been updated successfully.'), array('key' => 'positive'));
               return $this->redirect(['action' => 'trainingListing']);
           }else{   
           $this->Flash->error(__('Unable to update Training'), array('key' => 'positive'));
           }
       }
       $this->set(compact('training'));
    }
/**
 * certificateEdit method
 *
 * @param string|null $id User id.
 * @return \Cake\Http\Response|null Redirects to certificateListing.
 * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
 */
    public function certificateEdit($id = null) {
        $this->loadModel('Certifications');
        try {
           if ($id != null) {                
               $certificate = $this->Certifications->find()
                   ->where(
                       [
                           'Certifications.id' => base64_decode($id)
                       ]
                   )
                   ->first();
           } 
           
            if($this->request->is('ajax')){
                $this->autoRender = false;
                $this->set(compact('certificate'));
                return $this->render('/Element/Setup/certificate_edit');
            }            
       } catch (RecordNotFoundException $e) {
           $this->Flash->success('certificate not found please try agian');
           return $this->redirect($this->referer());
       }

       if($this->request->is('put') ) {
           $certificate = $this->Certifications->newEntity();
           $certificate->id = base64_decode($id);
           $certificate = $this->Certifications->patchEntity($certificate, $this->request->data);
           if ($this->Certifications->save($certificate)) {
               $this->Flash->success(__('
                  Certification has been updated successfully.'), array('key' => 'positive'));
               
           }else{ 
                $this->Flash->error(__('Unable to update Certification'), array('key' => 'positive'));
           }
       }
       return $this->redirect(['action' => 'certificateListing']);
    }

/**
 * equipmentEdit method
 *
 * @param string|null $id User id.
 * @return \Cake\Http\Response|null Redirects to trainingListing.
 * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
 */
    public function equipmentEdit($id = null) {
        $this->loadModel('Equipments');
        try {
           if ($id != null) {                
               $equipment = $this->Equipments->find()
                   ->where(
                       [
                           'Equipments.id' => base64_decode($id)
                       ]
                   )
                   ->first();
           } 
            if($this->request->is('ajax')){
                $this->autoRender = false;
                $this->set(compact('equipment'));
                $this->render('/Element/Setup/equipment_edit');
            }            
       } catch (RecordNotFoundException $e) {
           $this->Flash->success('equipment not found please try agian');
           return $this->redirect($this->referer());
       }
       if($this->request->is('put') ) {
           $equipment = $this->Equipments->newEntity();
           $equipment->id = base64_decode($id);
           $equipment = $this->Equipments->patchEntity($equipment, $this->request->data);
           if ($this->Equipments->save($equipment)) {
               $this->Flash->success(__('
                  Equipment has been updated successfully.'), array('key' => 'positive'));
               return $this->redirect(['action' => 'equipmentListing']);
           }else{   
           $this->Flash->error(__('Unable to update Equipment'), array('key' => 'positive'));
           }
       }
       $this->set(compact('equipment'));
    }
/**
 * questionEdit method
 *
 * @param string|null $id User id.
 * @return \Cake\Http\Response|null Redirects to trainingListing.
 * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
 */
    public function questionEdit($id = null) {
        $this->loadModel('InterviewQuestions');
        try {
           if ($id != null) {                
               $question = $this->InterviewQuestions->find()
                   ->where(
                       [
                           'InterviewQuestions.id' => base64_decode($id)
                       ]
                   )
                   ->first();
           } 
            if($this->request->is('ajax')){
                $this->autoRender = false;
                $this->set(compact('question'));
                $this->render('/Element/Setup/question_edit');
            }            
       } catch (RecordNotFoundException $e) {
           $this->Flash->success('Question not found please try agian');
           return $this->redirect($this->referer());
       }
       if($this->request->is('put') ) {
           $question = $this->InterviewQuestions->newEntity();
           $question->id = base64_decode($id);
           $question = $this->InterviewQuestions->patchEntity($question, $this->request->data);
           if ($this->InterviewQuestions->save($question)) {
               $this->Flash->success(__('
                  Interview Question has been updated successfully.'), array('key' => 'positive'));
               return $this->redirect(['action' => 'questionListing']);
           }else{   
           $this->Flash->error(__('Unable to update Interview Question'), array('key' => 'positive'));
           }
       }
       $this->set(compact('question'));
    }
/**
 * divisionEdit method
 *
 * @param string|null $id User id.
 * @return \Cake\Http\Response|null Redirects to divisionListing.
 * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
 */
    public function divisionEdit($id = null) {
        $this->loadModel('Divisions');
        try {
           if ($id != null) {                
               $division = $this->Divisions->find()
                   ->where(
                       [
                           'Divisions.id' => base64_decode($id)
                       ]
                   )
                   ->first();
           } 
            if($this->request->is('ajax')){
                $this->autoRender = false;
                $this->set(compact('division'));
                $this->render('/Element/Setup/division_edit');
            }            
       } catch (RecordNotFoundException $e) {
           $this->Flash->success('Division not found please try agian');
           return $this->redirect($this->referer());
       }
       if($this->request->is('put') ) {
           $division = $this->Divisions->newEntity();
           $division->id = base64_decode($id);
           $division = $this->Divisions->patchEntity($division, $this->request->data);
           if ($this->Divisions->save($division)) {
               $this->Flash->success(__('
                  Interview Question has been updated successfully.'), array('key' => 'positive'));
               return $this->redirect(['action' => 'divisionListing']);
           }else{   
           $this->Flash->error(__('Unable to update Division'), array('key' => 'positive'));
           }
       }
       $this->set(compact('division'));
    }
/**
 * adminListing method
 *
 * @return \Cake\Http\Response|null .
 * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
 */ 
    public function adminListing() {
        $this->viewBuilder()->layout('dashboard');
        $this->loadModel('CompanyUsers');
        $this->loadModel('Divisions');
        if($this->Auth->user('user_role_id') == 2) {
            $where_division = ['Divisions.user_id' => $this->Auth->user('id'),'Divisions.is_deleted' => 0];
        } elseif($this->Auth->user('user_role_id') == 3) {
            $where_division = ['Divisions.user_id' => $this->Auth->user('company_user.company_user_id'),'Divisions.is_deleted' => 0];
        }
        $division = $this->Divisions->find('list', [
            'keyField' => 'id',
            'valueField' => 'name'
        ])
        ->where($where_division);
        $where = ['CompanyUsers.company_user_id' => $this->Auth->user('id')];
        switch ($this->Auth->user('user_role_id')) {
            case 3:
                $where = array_merge($where, ['Users.user_role_id IN' => [4, 5]]);
                break;
            
            default:
                # code...
                break;
        }
        $admins = $this->CompanyUsers->find('all')
            ->where($where)
            ->contain(['Users' => ['UserProfiles' => ['Divisions']]]);
        $this->set(compact('admins','division'));
    }

/** * addAdmin method - add additional admin for setup
* @return \Cake\Http\Response|null redirects successfully back to admin listing page.
*/    
    public function addAdmin() {
       $this->loadModel('Users');
      
       $user = $this->Users->newEntity();
       if ($this->request->is('post')) {
            $this->request->data['uuid'] = Text::uuid();
            $this->request->data['company_user']['company_user_id'] = $this->Auth->user('id');
            if($this->Auth->user('user_role_id') != 2) {
              $this->request->data['company_user']['company_user_id'] = $this->Auth->user('company_user.company_user_id');
            }
            $this->request->data['active'] = 1;
           $user = $this->Users->patchEntity(
               $user,
               $this->request->data,
               [
                   'associated' =>['CompanyUsers', 'UserProfiles']
               ]
           );
           if ($this->Users->save($user)) {

               $this->loadModel('EmailTemplates');
                $temp = $this->EmailTemplates->find()->where(['EmailTemplates.id' => 3])
                        -> first();
                
                if($this->request->data['user_role_id'] == 3) {
                    $admin = "Full Admin";
                } elseif ($this->request->data['user_role_id'] == 4) {
                   $admin = "Uniform Admin";
                } elseif ($this->request->data['user_role_id'] == 5) {
                    $admin = "Training admin";
                } elseif ($this->request->data['user_role_id'] == 6) {
                    $admin = "Supervisor";
                }
                $temp['mail_body'] = str_replace(
                    array('#NAME','#ADMIN','#USERNAME','#EMAIL','#PASSWORD'),
                    array($this->request->data['user_profile']['first_name'].' '.$this->request->data['user_profile']['last_name'],
                        $admin,
                        $this->request->data['username'],
                        $this->request->data['email'],
                        $this->request->data['password']
                        ),
                    $temp['mail_body']
                );
                // sending forgot password mail
               $this->_sendEmailMessage($this->request->data['email'], $temp['mail_body'], $temp['subject']);
               $this->Flash->success(__('Additional Admin has been saved'), array('key' => 'positive'));
               return $this->redirect($this->referer());
           } else {
                $errors = $this->_setValidationError($user->errors());                
                $this->Flash->error(__($errors),array('key' => 'positive'));
               return $this->redirect($this->referer());
           }
       }
   } 
/**
 * adminDelete method
 *
 * @param string|null $id User id.
 * @return \Cake\Http\Response|null Redirects to adminListing.
 * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
 */
    public function adminDelete($id = null)
    {
        $this->loadModel('Users');
        $this->request->allowMethod(['GET', 'delete']);
        $user = $this->Users->get(base64_decode($id));
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The Additional admin has been deleted.'),array('key' => 'positive'));
        } else {
            $this->Flash->error(__('The Additional admin could not be deleted. Please, try again.'),array('key' => 'positive'));
        }

        return $this->redirect(['action' => 'adminListing']);
    }

/**
 * adminEdit method
 *
 * @param string|null $id User id.
 * @return \Cake\Http\Response|null Redirects to adminListing.
 * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
 */
    public function adminEdit($id = null) {
        $this->loadModel('Users');
        $this->loadModel('Divisions');
        if($this->Auth->user('user_role_id') == 2) {
            $where = ['Divisions.user_id' => $this->Auth->user('id'),'Divisions.is_deleted' => 0];
        } elseif($this->Auth->user('user_role_id') == 3) {
            $where = ['Divisions.user_id' => $this->Auth->user('company_user.company_user_id'),'Divisions.is_deleted' => 0];
        }
        $division = $this->Divisions->find('list', [
            'keyField' => 'id',
            'valueField' => 'name'
        ])
        ->where($where);
        try {
           if ($id != null) {                
               $user = $this->Users->find()
                   ->where(
                       [
                           'Users.id' => base64_decode($id)
                       ]
                   )
                   ->contain(['UserProfiles'])
                   ->first();
           } 
            if($this->request->is('ajax')){
                $this->autoRender = false;
                $this->set(compact('user','division'));
                $this->render('/Element/Setup/admin_edit');
            }            
       } catch (RecordNotFoundException $e) {
           $this->Flash->success('User not found please try agian');
           return $this->redirect($this->referer());
       }
       if($this->request->is('put') ) {
           //$user = $this->Users->newEntity();
           $user->id = base64_decode($id);
           $users = $this->Users->patchEntity($user, $this->request->data,array('associated' => 'UserProfiles'));
           if ($this->Users->save($user)) {
               $this->Flash->success(__('
                  User has been updated successfully.'), array('key' => 'positive'));
               return $this->redirect(['action' => 'adminListing']);
           } else {   
           $this->Flash->error(__('Unable to update User'), array('key' => 'positive'));
           }
           pr($users->errors());die;
           return $this->redirect(['action' => 'adminListing']);
       }
    }

    public function addSelectedQuestions($id) {
        $this->loadModel('CompanyDeleteQuestions');
        $question = $this->CompanyDeleteQuestions->newEntity();
        $this->request->data['interview_question_id'] = $id;
        $this->request->data['company_id'] = $this->Auth->user('company.id');
        $question = $this->CompanyDeleteQuestions->patchEntity($question, $this->request->data);
            $this->CompanyDeleteQuestions->save($question);
            if($this->request->is('ajax')){
                $this->autoRender = false;
            }
    }

    public function deleteSelectedQuestions($id) {
        $this->loadModel('CompanyDeleteQuestions');
        $question = $this->CompanyDeleteQuestions->find()
                    ->where(['CompanyDeleteQuestions.interview_question_id' => $id])
                    ->first();
        $result = $this->CompanyDeleteQuestions->delete($question);
        if($this->request->is('ajax')){
            $this->autoRender = false;          
        }
    }
}
