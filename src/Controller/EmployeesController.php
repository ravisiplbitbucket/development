<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Routing\Router;
use Cake\Utility\Hash;
/**
 * Candidates Controller
 *
 * @property \App\Model\Table\CandidatesTable $Candidates
 *
 * @method \App\Model\Entity\Candidate[] paginate($object = null, array $settings = [])
 */
class EmployeesController extends AppController
{

    public $paginate = [
        'limit' => 10,
        'order' => [
            'Candidates.id' => 'Desc'
        ]
    ];
    public function initialize()
    {
        parent::initialize();        
    }

    public function index()
    {
        $myShell = new \App\Shell\ExpirationShell;
        $myShell->main();
        $this->viewBuilder()->layout('dashboard');
        $this->loadModel('shifts');
        $this->loadModel('Candidates');
        try {        
            $shift = $this->shifts
                    ->find('list', [
                        'keyField' => 'id',
                        'valueField' => 'shift'
                    ]);

            if ($this->request->is('get')) {                
                $condition = $this->_setCondition($this->request->query);
                
                $query = $this->Candidates
                        ->find('all')
                        //->contain(['Jobs'])
                        ->matching(
                            'WorkingShifts', function ($q) use($condition) {
                                return $q
                                    ->where($condition['SearchByDay']);
                            }
                        )
                        ->where($condition['candidate'])
                        ->andWhere([
                            'Candidates.company_id' => $this->Auth->user('company.id'),
                            'Candidates.status' => (int)true
                        ])
                        ->group(['Candidates.id']);      
                $candidates = $this->paginate($query);
               
            }            
        } catch (\Exception $e) {       
            pr($e);die;
           // redirecting to Last page if request page doesn't exist
            if(!empty($this->request->query['page'])) {
                $this->request->query['page'] = ($this->request->query['page'] -1 > 0) ? $this->request->query['page'] -1 : 1 ;
                return $this->redirect([
                       'controller' => $this->request->params['controller'],
                       'action' => $this->request->params['action']
                       
                   ]
                );
            }
            //redirecting to previous page Incase of exception
            return $this->redirect($this->referer());
        }
        $this->set(compact('candidates','shift'));
        $this->set('_serialize', ['candidates']);
    }

    public function view($id = null)
    {
        $this->viewBuilder()->layout('dashboard');
        $this->loadModel('Candidates');
        $this->loadModel('Trainings');
        $this->loadModel('Commendations');
        $this->loadModel('Disciplinaries');
        $this->loadModel('EmployeeTrainings');
        $this->loadModel('Certifications');
        $this->loadModel('EmployeeCertifications');
        $this->loadModel('Equipments');
        $this->loadModel('EmployeeEquipments');
        $candidates = $this->Candidates
                    ->get(base64_decode($id), [
                        'contain' => [
                            'Jobs',
                            'Countries',
                            'States',
                            'Cities',
                            'WorkingShifts',
                            'CandidateCompletes' => function ($q) {
                                return $q
                                ->contain([
                                    'CandidateEducations',
                                    'CandidateMilitaries',
                                    'CandidateReferences'
                                ])
                                ->limit(1);
                            }
                        ]
                    ]);
        //fetch training
        $trainingList = $this->Trainings
                        ->find('list', [
                            'keyField' => function ($q) {
                                return $q->id ;
                            },
                            'valueField' => 'name'
                        ])
                        ->where(['user_id' => $this->Auth->user('id')]);    
       

        $commendations = $this->Commendations->find('all')
                        ->where(['Commendations.employee_id' => base64_decode($id)])
                        ->toArray();
        $disciplinaries = $this->Disciplinaries->find('all')
                        ->where(['Disciplinaries.employee_id' => base64_decode($id)])
                        ->toArray();
        $trainings = $this->EmployeeTrainings->find()
                    ->where(['EmployeeTrainings.candidate_id' => base64_decode($id)])
                    ->contain(['Trainings','Candidates' => ['Jobs']])
                    ->toArray();
        $certificateList = $this->Certifications
                        ->find('list', [
                            'keyField' => function ($q) {
                                return $q->id ;
                            },
                            'valueField' => 'name'
                        ])
                        ->where(['user_id' => $this->Auth->user('id')]);
        $certificates = $this->EmployeeCertifications->find()
                    ->where(['EmployeeCertifications.candidate_id' => base64_decode($id)])
                    ->contain(['Certifications'])
                    ->toArray();
        $equipmentList = $this->Equipments
                        ->find('list', [
                            'keyField' => function ($q) {
                                return $q->id ;
                            },
                            'valueField' => 'item'
                        ])
                        ->where(['user_id' => $this->Auth->user('id')]);
        $equipments = $this->EmployeeEquipments->find()
                    ->where(['EmployeeEquipments.candidate_id' => base64_decode($id)])
                    ->contain(['Equipments','Candidates' => ['Jobs']])
                    ->toArray();
        $this->set(compact('candidates', 'trainingList','commendations','disciplinaries','trainings','certificateList','certificates','equipmentList','equipments'));
        $this->set('_serialize', ['candidates']);
    }

     /**
    * Parsing Data
    * @return Condition
    **/
    protected function _setCondition($request)
    {      
        $condition = [];
        $condition['candidate'] = [];
        $condition['SearchByDay'] = [];
        if (!empty($request['tag_search'])) {
            $tag_search = $request['tag_search'];
            $condition['candidate'][] = [
                'OR' => [
                    ['Candidates.first_name LIKE' => '%'.$tag_search.'%'],
                    ['Candidates.last_name LIKE' => '%'.$tag_search.'%'],
                    ['Candidates.middle_name LIKE' => '%'.$tag_search.'%'],
                    ['Candidates.job_id' => $tag_search]
                ]
            ];
        }        

        
        //For all status , no value should be pass
        if (isset($request['armed']) && $request['armed'] != '') {
            $condition['candidate'][] = ['Candidates.certified_unarmed' => $request['armed']];
        }

        if (isset($request['job_type']) && !empty($request['job_type'])) {
            $condition['candidate'][] = ['Candidates.job_type_id' => $request['job_type']];
        }
        $searchByDayCond = [];
        if (isset($request['day']) && !empty($request['day'])) {
            $searchByDayCond = ['WorkingShifts.day_id' => $request['day']];
            $condition['SearchByDay'][] = $searchByDayCond;
        }
        if (isset($request['shift']) && !empty($request['shift'])) {
            $condition['SearchByDay'][] = ['WorkingShifts.shift_id' => $request['shift']];
        }        

        if (isset($request['experience']) && !empty($request['experience'])) {
            $condition['candidate'][] = ['Candidates.experience' => $request['experience']];
        }

        if (isset($request['license']) && $request['license'] != '') {
            $condition['candidate'][] = ['Candidates.driving_licence' => $request['license']];
        }

        if (isset($request['automobile']) && $request['automobile'] != '') {
            $condition['candidate'][] = ['Candidates.dependable_automobile' => $request['automobile']];
        }        
        return $condition;
    }

    public function addDisciplinary() {
        $this->loadModel('Disciplinaries');
        $disciplinary = $this->Disciplinaries->newEntity();
        if ($this->request->is('post')) {
            $disciplinary = $this->Disciplinaries->patchEntity($disciplinary, $this->request->data);
            if ($this->Disciplinaries->save($disciplinary)) {
                $this->Flash->success(__('Disciplinaries has been saved'), array('key' => 'positive'));
                return $this->redirect($this->referer());
            } else {
                $this->Flash->error(__('Disciplinaries has not been saved'), array('key' => 'positive'));
                return $this->redirect($this->referer());
            }
            $this->set(compact('disciplinary'));
        }  
    }

    public function addCommendation() {
        $this->loadModel('Commendations');
        $commendation = $this->Commendations->newEntity();
        if ($this->request->is('post')) {
            $commendation = $this->Commendations->patchEntity($commendation, $this->request->data);
            if ($this->Commendations->save($commendation)) {
                $this->Flash->success(__('Commendations has been saved'), array('key' => 'positive'));
                return $this->redirect($this->referer());
            } else {
                $this->Flash->error(__('Commendations has not been saved'), array('key' => 'positive'));
                return $this->redirect($this->referer());
            }
            $this->set(compact('commendation'));
        }   
    }

    public function addTraining() {
        $this->loadModel('EmployeeTrainings');
        $training = $this->EmployeeTrainings->newEntity();
        if ($this->request->is('post')) {
            $training = $this->EmployeeTrainings->patchEntity($training, $this->request->data);
            if ($this->EmployeeTrainings->save($training)) {
                $this->Flash->success(__('Training has been assigned to employee'), array('key' => 'positive'));
                return $this->redirect($this->referer());
            } else {
                pr($training->errors());die;
                $this->Flash->error(__('Training has not been assigned'), array('key' => 'positive'));
                return $this->redirect($this->referer());
            }
            $this->set(compact('training'));
        }  
    }

    public function addCertification() {
        $this->loadModel('EmployeeCertifications');
        $certificate = $this->EmployeeCertifications->newEntity();
        if ($this->request->is('post')) {
            $certificate = $this->EmployeeCertifications->patchEntity($certificate, $this->request->data);
            if ($this->EmployeeCertifications->save($certificate)) {
                $this->Flash->success(__('Certification has been assigned to employee'), array('key' => 'positive'));
                return $this->redirect($this->referer());
            } else {
                $this->Flash->error(__('Certification has not been assigned'), array('key' => 'positive'));
                return $this->redirect($this->referer());
            }
            $this->set(compact('certificate'));
        }
    }

    public function addEquipment() {
        $this->loadModel('EmployeeEquipments');
        $equipment = $this->EmployeeEquipments->newEntity();
        if ($this->request->is('post')) {
            $equipment = $this->EmployeeEquipments->patchEntity($equipment, $this->request->data);
            if ($this->EmployeeEquipments->save($equipment)) {
                $this->Flash->success(__('Uniform And Equipment has been assigned to employee'), array('key' => 'positive'));
                return $this->redirect($this->referer());
            } else {
                $this->Flash->error(__('Uniform And Equipment has not been assigned'), array('key' => 'positive'));
                return $this->redirect($this->referer());
            }
            $this->set(compact('equipment'));
        }
    }

    public function getDescription($item) {
        $this->loadModel('Equipments');
        $description = $this->Equipments->find()
                        ->select(['description'])
                        ->where(['Equipments.id' => $item , 'Equipments.user_id' => $this->Auth->user('id')])
                        ->first();

        $this->set(compact('description'));
        $this->set('_serialize', ['description']);
    }

    public function expiration() {
        $this->viewBuilder()->layout('dashboard');
        $this->loadModel('EmployeeCertifications');
        $expirationDates29 = $this->EmployeeCertifications->find()
                         ->where(['(DATEDIFF(EmployeeCertifications.expiration_date,NOW())) BETWEEN 0 AND 29'])
                         ->matching('Candidates',function($q) {
                            return $q->where(['Candidates.company_id' => $this->Auth->user('company.id')]);
                         }) 
                         ->contain(['Candidates','Certifications']);

        $expirationDates44 = $this->EmployeeCertifications->find()
                         ->where(['(DATEDIFF(EmployeeCertifications.expiration_date,NOW())) BETWEEN 30 AND 44'])
                         ->matching('Candidates',function($q) {
                            return $q->where(['Candidates.company_id' => $this->Auth->user('company.id')]);
                         })
                         ->contain(['Candidates','Certifications']);

        $expirationDates59 = $this->EmployeeCertifications->find()
                         ->where(['(DATEDIFF(EmployeeCertifications.expiration_date,NOW())) BETWEEN 45 AND 59'])
                         ->matching('Candidates',function($q) {
                            return $q->where(['Candidates.company_id' => $this->Auth->user('company.id')]);
                         })
                         ->contain(['Candidates','Certifications']);

        $expirationDates89 = $this->EmployeeCertifications->find()
                         ->where(['(DATEDIFF(EmployeeCertifications.expiration_date,NOW())) BETWEEN 60 AND 89'])
                         ->matching('Candidates',function($q) {
                            return $q->where(['Candidates.company_id' => $this->Auth->user('company.id')]);
                         })
                         ->contain(['Candidates','Certifications']);
        
        $expirationDates120 = $this->EmployeeCertifications->find()
                         ->where(['(DATEDIFF(EmployeeCertifications.expiration_date,NOW())) BETWEEN 90 AND 120'])
                         ->matching('Candidates',function($q) {
                            return $q->where(['Candidates.company_id' => $this->Auth->user('company.id')]);
                         })
                         ->contain(['Candidates','Certifications']);

        $this->set(compact('expirationDates29','expirationDates44','expirationDates59','expirationDates89','expirationDates120'));
    }

    public function imageUpload($id) {
        $this->loadModel('Candidates');
        $candidate = $this->Candidates->find()
                    ->where([
                        'Candidates.id' => base64_decode($id)
                    ]);
        if($this->request->is(['put','post']) ) {
           $candidate = $this->Candidates->newEntity();
           $candidate->id = base64_decode($id);
           $candidate = $this->Candidates->patchEntity($candidate, $this->request->data);
           if ($this->Candidates->save($candidate)) {
               $this->Flash->success(__('
                  Employee Image has been updated successfully.'), array('key' => 'positive'));
               return $this->redirect(['Controller' => 'Employees','action' => 'view',$id]);
           } else{   
           $this->Flash->error(__('Unable to update candidate'), array('key' => 'positive'));
           }
           pr($candidate->errors());die;
        }
        $this->set(compact('candidate'));
    }

    public function add() {
        $this->viewBuilder()->layout('dashboard');
        $this->loadModel('Companies');
        $this->loadModel('Candidates');
        $this->loadModel('Educations');
        $this->loadModel('Countries');
        $this->loadModel('JobTypes');
        $this->loadModel('CandidateHistories');
        $this->loadModel('Languages');
        $this->loadModel('Jobs');
        $language = $this->Languages->find('list', [
            'keyField' => 'id',
            'valueField' => 'language'
        ]);
        $jobs = $this->Jobs->find('list', [
            'keyField' => 'id',
            'valueField' => 'job_title'
        ])
        ->where(['Jobs.company_id' => $this->Auth->user('company.id')]);
        $country = $this->Countries->find('list', [
            'keyField' => 'id',
            'valueField' => 'name'
        ])
        ->order(['Countries.is_default' => 'ASC','Countries.name' => 'ASC']);
        $jobtypes = $this->JobTypes->find('list', [
            'keyField' => 'id',
            'valueField' => 'job_name'
        ]);
        $educations = $this->Educations->find('list',[
            'keyField' => 'id',
            'valueField' => 'education_name'
        ]);
    
        $candidate = $this->Candidates->newEntity();    
        if ($this->request->is('post')) {
            $url = $this->Companies->find()
                ->where(['Companies.id' => $this->Auth->user('company.id')])
                ->first();
            $path = preg_replace('/[^a-zA-Z0-9\']/', '-',$url->company).'-'.$url->id;
            if(!empty($this->request->session()->read('Candidate'))) {
                $data = $this->request->session()->read('Candidate');
            }
            
            $data['candidate_completes'][0]['candidate_military'] = $this->request->data['candidate_military'];
            $candidate = $this->Candidates->patchEntity($candidate, $data,['associated' => ['WorkingShifts','CandidateHistories','CandidateCompletes' => ['associated' => ['CandidateEducations','CandidateReferences','CandidateMilitaries']]]]);
            $candidate->company_id = $this->Auth->user('company.id');
            $candidate->status = 1;
            $candidate['candidate_completes'][0]['company_id'] = $this->Auth->user('company.id');
            
            if ($this->Candidates->save($candidate)) {

                //Get Last saved Id and Saved It in Session So as To Use It in Previous Tab
                $this->request->session()->delete('Candidate');
                $this->Flash->success(__('The Employee is saved.'), array('key' => 'positive'));
                return $this->redirect(['action' => 'index']);
            
            } else {
                pr($candidate->errors());die;
                $errors = $this->_setValidationError($candidate->errors());                
                $this->Flash->error(__($errors),array('key' => 'positive'));
            }
        }
        $this->request->session()->delete('Candidate');
        $this->set(compact('candidate','country','jobtypes','educations','language','jobs'));
    }

    public function setSession() {        
        if ($this->request->is('post')) {
            $this->request->data['company_id'] = $this->Auth->user('company.id');
            if($this->request->is('ajax')) {
                $this->viewBuilder()->layout('ajax');
                if(!empty($this->request->data['step']) && ($this->request->data['step'] == 2 || $this->request->data['step'] == 4)) {
                    $data = $this->request->data;
                    
                    if($this->request->session()->read('Candidate')) {
                        $tmp = $this->request->session()->read('Candidate');
                        $data = array_merge($tmp,$this->request->data);
                    }
                    $this->request->session()->write('Candidate',$data);
                    // pr($session);die;
                    $response=[
                        'status' => 200,
                    ];
                    $this->set(compact('response'));
                } elseif (!empty($this->request->data['step']) && $this->request->data['step'] == 3) {
                    $this->request->data = $this->__formatShiftData($this->request->data);
                    $tmp = $this->request->session()->read('Candidate');
                    $data = array_merge($tmp,$this->request->data);
                    $this->request->session()->write('Candidate',$data);
                    $response=[
                        'status' => 200,
                    ];
                    $this->set(compact('response'));
                } elseif (!empty($this->request->data['step']) && $this->request->data['step'] == 5) {
                    
                    $this->request->data['candidate_completes']['language_ids'] = implode(',', $this->request->data['candidate_completes']['language_ids']);
                    $tmp = $this->request->session()->read('Candidate');
                    $complete = $this->request->session()->read('Candidate.candidate_completes.0');
                    $tmp['candidate_completes'][0] = array_merge($complete,$this->request->data['candidate_completes']);
                    $this->request->session()->write('Candidate',$tmp);
                    $response=[
                        'status' => 200,
                    ];
                    $this->set(compact('response'));
                }
                
            }
        } 
    }

/**
* saveCandidateHistory method
 *
 * @return \Cake\Http\Response|null redirects back to candidate apply page
 * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
 */
    public function saveCandidateHistory(){
        $this->loadModel('CandidateHistories');
        $history= $this->CandidateHistories->newEntity();  
        if($this->request->is('post')) {
            $response=['status' => 400];
            if(!empty($this->request->data['rounds_verified'])){
                $this->request->data['rounds_verified'] =implode(',',$this->request->data['rounds_verified']);
            }
            if(!empty($this->request->data['way_report_submitted'])){
                $this->request->data['way_report_submitted'] =implode(',',$this->request->data['way_report_submitted']);
            }
            if(!empty($this->request->data['shift_work'])){
                $this->request->data['shift_work'] =implode(',',$this->request->data['shift_work']);
            }
            if(!empty($this->request->data['equipment_issued'])){
                $this->request->data['equipment_issued'] =implode(',',$this->request->data['equipment_issued']);
            }
            if(!empty($this->request->data['vechicle_type'])){
                $this->request->data['vechicle_type'] =implode(',',$this->request->data['vechicle_type']);
            }
            $history = $this->CandidateHistories->patchEntity($history, $this->request->getData());
            $tmp['candidate_histories'] = $this->request->session()->read('Candidate.candidate_histories');
            $tmp['candidate_histories'][] = $this->request->data;

            $this->request->session()->write('Candidate.candidate_histories',$tmp['candidate_histories']);
            if($this->request->is('ajax')) {
                $historyIdInSession = count($tmp['candidate_histories'])-1;
                $this->viewBuilder()->layout('ajax');
                $this->set(compact('response', 'history','historyIdInSession'));
                $this->render('/Element/Job/history_detail_link');
            } 
        }
    }

    public function getHistory($id){
        $this->loadModel('CandidateHistories');
        $history = $this->CandidateHistories->find()
            ->where(['CandidateHistories.id' => base64_decode($id)])
            ->first();
        if($this->request->is('ajax')) {
            $this->viewBuilder()->layout('ajax');
            $this->set(compact('history'));
            $this->render('/Element/Candidate/edit_history');
        }  
    }
    
    public function editHistory($id){
        $this->loadModel('CandidateHistories');
        $history = $this->CandidateHistories->find()
            ->where(['CandidateHistories.id' => base64_decode($id)])
            ->first();
        $response=[
            'status' => 400,
            'msg' => 'Something went wrong'
        ];
        if($this->request->is('post')) {
            if(!empty($this->request->data['rounds_verified'])) {
                $this->request->data['rounds_verified'] = implode(',', $this->request->data['rounds_verified']);
            }
            if(!empty($this->request->data['way_report_submitted'])) {
                $this->request->data['way_report_submitted'] = implode(',', $this->request->data['way_report_submitted']);
            }
            if(!empty($this->request->data['shift_work'])) {
                $this->request->data['shift_work'] = implode(',', $this->request->data['shift_work']);
            }
            if(!empty($this->request->data['equipment_issued'])) {
                $this->request->data['equipment_issued'] = implode(',', $this->request->data['equipment_issued']);
            }
            if(!empty($this->request->data['vechicle_type'])) {
                $this->request->data['vechicle_type'] = implode(',', $this->request->data['vechicle_type']);
            }

            $history = $this->CandidateHistories->patchEntity($history, $this->request->getData());
            if($this->CandidateHistories->save($history)) {
                $response=[
                    'status' => 200,
                    'msg' => 'History updated'
                ];
            }
        } 
        $this->set(compact('response'));  
    }

    protected function __formatShiftData($data) {
        $shift = [
            'step' => $data['step'],
            'date_begin_work' => $data['date_begin_work'],
            'hours_available' => $data['hours_available'],
            'working_shifts'  => []
        ];
        $i = 0;
        if(!empty($data['working_shifts'])) {            
            foreach($data['working_shifts'] as $key => $value) {
                foreach ($value as $index => $day) {  
                    $shift['working_shifts'][$i]['shift_id'] = $key;
                    $shift['working_shifts'][$i]['day_id'] = $day['day_id'];
                    $i++;
                }
            }
        }
        return $shift;
    }
    public function setEducation() {
        $this->loadModel('CandidateEducations');
        $this->loadModel('Educations');
        $education= $this->CandidateEducations->newEntity();  
        if($this->request->is('post')) {
            $education = $this->CandidateEducations->patchEntity($education, $this->request->getData());
            $educations = $this->Educations->find()
                ->where(['id' => $this->request->data['education_id']])
                ->first();
            if($this->request->is('ajax')) {
                $this->viewBuilder()->layout('ajax');
                $data = $this->request->data;
                if($this->request->session()->check('Candidate.candidate_completes.0.candidate_educations')){
                   $tmp['candidate_educations'] = $this->request->session()->read('Candidate.candidate_completes.0.candidate_educations');
                }
                $tmp['candidate_educations'][] = $this->request->data;

                $this->request->session()->write('Candidate.candidate_completes.0.candidate_educations',$tmp['candidate_educations']);
                $educationIdInSession = count($tmp['candidate_educations'])-1;
                $this->viewBuilder()->layout('ajax');
                $this->set(compact('educationIdInSession', 'education', 'educations'));
                $this->render('/Element/Employee/education_list');
            }
        }        
    }

    public function setReference() {
        $this->loadModel('CandidateReferences');
        $reference= $this->CandidateReferences->newEntity();  
        if($this->request->is('post')) {
            $reference = $this->CandidateReferences->patchEntity($reference, $this->request->getData());
            
            if($this->request->is('ajax')) {
                $this->viewBuilder()->layout('ajax');
                $data = $this->request->data;

                $tmp['candidate_references'] = $this->request->session()->read('Candidate.candidate_completes.0.candidate_references');
                $tmp['candidate_references'][] = $this->request->data;

                $this->request->session()->write('Candidate.candidate_completes.0.candidate_references',$tmp['candidate_references']);
                $referenceIdInSession = count($tmp['candidate_references'])-1;
                $this->viewBuilder()->layout('ajax');
                $this->set(compact('referenceIdInSession', 'reference'));
                $this->render('/Element/Employee/reference_list');
            }
        }        
    }

    public function deleteReference($id) {
         if($this->request->is('ajax')) {
            $this->viewBuilder()->layout('ajax');
            if($this->request->session()->check('Candidate.candidate_completes.0.candidate_references')) {
                $reference = $this->request->session()->read('Candidate.candidate_completes.0.candidate_references');
                unset($reference[$id]);
                $this->request->session()->delete('Candidate.candidate_completes.0.candidate_references');
                if(!empty($reference)){
                    $this->request->session()->write('Candidate.candidate_completes.0.candidate_references', $reference);
                }
                $response=[
                    'status' => 200,
                ];
                $this->set(compact('response'));     
            }
        }
    }

    public function deleteEducation($id) {
         if($this->request->is('ajax')) {
            $this->viewBuilder()->layout('ajax');
            if($this->request->session()->check('Candidate.candidate_completes.0.candidate_educations')) {
                $education = $this->request->session()->read('Candidate.candidate_completes.0.candidate_educations');
                unset($education[$id]);
                $this->request->session()->write('Candidate.candidate_completes.0.candidate_educations', $education);
                $response=[
                    'status' => 200,
                ];
                $this->set(compact('response'));     
            }
        }    
    }
    public function edit($id) {
        $this->viewBuilder()->layout('dashboard');
        $this->loadModel('Companies');
        $this->loadModel('Candidates');
        $this->loadModel('Educations');
        $this->loadModel('Countries');
        $this->loadModel('JobTypes');
        $this->loadModel('CandidateHistories');
        $this->loadModel('Languages');
        $this->loadModel('Cities');
        $this->loadModel('States');
        $this->loadModel('Jobs');
        $language = $this->Languages->find('list', [
            'keyField' => 'id',
            'valueField' => 'language'
        ]);
        $jobs = $this->Jobs->find('list', [
            'keyField' => 'id',
            'valueField' => 'job_title'
        ])
        ->where(['Jobs.company_id' => $this->Auth->user('company.id')]);
        $country = $this->Countries->find('list', [
            'keyField' => 'id',
            'valueField' => 'name'
        ])
        ->order('Countries.is_default DESC');


        $jobtypes = $this->JobTypes->find('list', [
            'keyField' => 'id',
            'valueField' => 'job_name'
        ]);
        $educations = $this->Educations->find('list',[
            'keyField' => 'id',
            'valueField' => 'education_name'
        ]);
         $histories = $this->CandidateHistories->find('list',[
            'keyField' => 'id',
            'valueField' => 'company_name'
        ])->where(['CandidateHistories.candidate_id' => base64_decode($id)]);
        $candidate = $this->Candidates->find()
                    ->where(['Candidates.id' => base64_decode($id)])
                    ->contain(['WorkingShifts','CandidateCompletes' => ['CandidateReferences','CandidateEducations' => ['Educations'],'CandidateMilitaries'],'CandidateHistories','Jobs'])
                    ->first();
        $states = $this->States->find('list', [
            'keyField' => 'id',
            'valueField' => 'name'
        ])
        ->where(['country_id' => $candidate->country_id]);
        
        $cities = $this->Cities->find('list', [
            'keyField' => 'id',
            'valueField' => 'name'
        ])
        ->where(['state_id' => $candidate->state_id]);
        $this->set(compact('candidate','country','jobtypes','educations','language','jobs','histories','states','cities'));
    }

    public function education($id) {
        $this->loadModel('CandidateEducations');
        $this->loadModel('Educations');
        $educations = $this->Educations->find('list',[
            'keyField' => 'id',
            'valueField' => 'education_name'
        ]);
        try {
           if ($id != null) {                
               $education = $this->CandidateEducations->find()
                   ->where(
                       [
                           'CandidateEducations.id' => base64_decode($id)
                       ]
                   )
                   ->first();
           } 
           
            if($this->request->is('ajax')){
                $this->autoRender = false;
                $this->set(compact('education','educations'));
                return $this->render('/Element/Edit/education');
            }            
       } catch (RecordNotFoundException $e) {
           $this->Flash->success('Candidate education not found please try agian');
           return $this->redirect($this->referer());
       }

    }

    public function reference($id) {
        $this->loadModel('CandidateReferences');

        try {
           if ($id != null) {                
               $reference = $this->CandidateReferences->find()
                   ->where(
                       [
                           'CandidateReferences.id' => base64_decode($id)
                       ]
                   )
                   ->first();
           } 
           
            if($this->request->is('ajax')){
                $this->autoRender = false;
                $this->set(compact('reference'));
                return $this->render('/Element/Edit/reference');
            }            
       } catch (RecordNotFoundException $e) {
           $this->Flash->success('Candidate reference not found please try agian');
           return $this->redirect($this->referer());
       }
    }
    public function fired($id) {
        $fire = $this->Candidates->find()
                            ->where([
                                'Candidates.id' => base64_decode($id)
                            ]);
        $fire = $fire->first();
        $fire->status = 6;
        if ($this->Candidates->save($fire)) {
            $this->Flash->success(__('Employee has been Fired'), array('key' => 'positive'));
            return $this->redirect(['action' => 'index']);
        }
        $this->Flash->error(__('Some errors occurred. Please try again.'), array('key' => 'positive'));
        return $this->redirect(['action' => 'index']);
    }
}
