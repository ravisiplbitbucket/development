<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Utility\Text;
use Cake\Core\Configure;
use Cake\Routing\Router;
/**
 * Jobs Controller
 *
 * @property \App\Model\Table\JobsTable $Jobs
 *
 * @method \App\Model\Entity\Job[] paginate($object = null, array $settings = [])
 */
class JobsController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        $this->Auth->allow(['jobListing','apply','getStates','getCities','searchStates','searchCities','candidateApply', 'saveCandidateHistory', 'getHistory', 'setSession','getInsurances','editHistory', 'test']);
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->viewBuilder()->layout('dashboard');    
		try {
		   $query = $this->Jobs->find('all')
		           ->where(['Jobs.user_id' => $this->Auth->user('id')])
                   ->orWhere(['Jobs.company_id' => $this->Auth->user('company.id')])
		           ->contain(['Countries','States','Cities','Candidates'])
                   ->order(['Jobs.created' => 'DESC']);
		           
		   $jobs = $this->paginate($query);            
		} catch (\Exception $e) {
		   // redirecting to Last page if request page doesn't exist
            if(!empty($this->request->query['page'])) {
                $this->request->query['page'] = ($this->request->query['page'] -1 > 0) ? $this->request->query['page'] -1 : 1 ;
                return $this->redirect([
                       'controller' => $this->request->params['controller'],
                       'action' => $this->request->params['action']
                       
                   ]
                );
            }
		   
		}
		$this->set(compact('jobs'));
        $this->set('_serialize', ['jobs']);
    }

    /**
     * View method
     *
     * @param string|null $id Job id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->viewBuilder()->layout('dashboard');
        $job = $this->Jobs->find()
                ->where(['Jobs.id' => base64_decode($id)])
                ->contain(['Countries','States','Cities','JobTypes','Educations'])
                ->toArray();
        $this->set('job', $job);
        $this->set('_serialize', ['job']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add job post.
     */
    public function add()
    {
        $this->viewBuilder()->layout('dashboard');
        $this->loadModel('Countries');
        $this->loadModel('States');
        $this->loadModel('Cities');
        $this->loadModel('JobTypes');
        $this->loadModel('Educations');
        $this->loadModel('Jobs');
        $this->loadModel('Companies');
        $this->loadModel('Candidates');
        $candidate = $this->Candidates->find()
                    ->where(['Candidates.email_alerts' => 1]);
            
        $country = $this->Countries->find('list', [
            'keyField' => 'id',
            'valueField' => 'name'
        ])
        ->order(['Countries.is_default' => 'ASC','Countries.name' => 'ASC']);
        $timestamp = $this->Jobs->find('list',array(
            'keyField' => 'id',
            'valueField' => 'job_number'
            )
        );
        $company = $this->Companies->find()
                ->where(['Companies.user_id' => $this->Auth->user('id')])
                ->toArray();
        $this->Jobs->addBehavior('Geo.Geocoder', ['address' => 'zipcode']);
        $job = $this->Jobs->newEntity();
        if ($this->request->is('post')) {
            if(in_array($this->request->data['job_number'], $timestamp->toArray())) {
               $this->request->data['job_number'] = time();
            }
            $this->request->data['uuid'] = Text::uuid();
            $this->request->data['user_id'] = $this->Auth->user('id');
            $this->request->data['company_id'] = $this->Auth->user('company.id');
            $job = $this->Jobs->patchEntity($job, $this->request->getData());
            if ($this->Jobs->save($job)) {
                $jobs = $this->Jobs->find()
                        ->where(['Jobs.id' => $job->id])
                        ->contain(['Countries','States','Cities','JobTypes'])
                        ->first();
                $link = Router::url(['controller' => 'Jobs', 'action' => 'apply', base64_encode($jobs->id).'/'.base64_encode($jobs->company_id),'_full' => true]);
                $this->loadModel('EmailTemplates');
                $temp = $this->EmailTemplates->find()->where(['EmailTemplates.id' => 6])
                    -> first();    
                    $temp['mail_body'] = str_replace(
                            array('#JOB','#COUNTRY','#STATE','#CITY','#TYPE','#EDUCATION','#EXPERIENCE','#CERTIFICATION','#SKILL','#LINK'),
                            array(
                               $jobs->job_title,
                               $jobs['country']->name,
                               $jobs['state']->name,
                               $jobs['city']->name,
                               $jobs['job_type']->job_name,
                               $jobs->qualification,
                               $jobs->experience,
                               $jobs->certification,
                               $jobs->language_skills,
                               $link
                            ), 
                        $temp['mail_body']
                    );
                foreach($candidate as $cand) {
                    $this->_sendEmailMessage($cand['email'], $temp['mail_body'], $temp['subject']);
                }
                $this->Flash->success(__('The job has been saved.'),array('key' => 'positive'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The job could not be saved. Please, try again.'));
        } 
        
        $users = $this->Jobs->Users->find('list', ['limit' => 200]);
        $jobTypes = $this->JobTypes->find('list', ['keyField' => 'id',
            'valueField' => 'job_name']);
        $educations = $this->Educations->find('list', ['keyField' => 'id',
            'valueField' => 'education_name']);
        $this->set(compact('job', 'users', 'country', 'states', 'cities', 'jobTypes', 'educations','company'));
        $this->set('_serialize', ['job']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Job id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->loadModel('Countries');
        $this->viewBuilder()->layout('dashboard');
        $job = $this->Jobs->get(base64_decode($id), [
            'contain' => ['Countries','States','Cities','Educations','JobTypes']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $job = $this->Jobs->patchEntity($job, $this->request->getData());
            if ($this->Jobs->save($job)) {
                $this->Flash->success(__('The job has been updated.'),array('key' => 'positive'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The job could not be saved. Please, try again.'),array('key' => 'positive'));
        }
        $users = $this->Jobs->Users->find('list', ['limit' => 200]);
        $country = $this->Countries->find('list', [
            'keyField' => 'id',
            'valueField' => 'name'
        ]);
        $states = $this->Jobs->States->find('list')
                ->where(['country_id' => $job->country_id]);
        $cities = $this->Jobs->Cities->find('list', [
            'keyField' => 'id',
            'valueField' => 'name'
        ])
        ->where(['state_id' => $job->state_id]);
        $jobTypes = $this->Jobs->JobTypes->find('list',[
            'keyField' => 'id',
            'valueField' => 'job_name'
        ]);
        $educations = $this->Jobs->Educations->find('list',[
            'keyField' => 'id',
            'valueField' => 'education_name'
        ]);
        $this->set(compact('job', 'users', 'country', 'states', 'cities', 'jobTypes', 'educations'));
        $this->set('_serialize', ['job']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Job id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $job = $this->Jobs->get($id);
        if ($this->Jobs->delete($job)) {
            $this->Flash->success(__('The job has been deleted.'));
        } else {
            $this->Flash->error(__('The job could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * getStates method
     *
     * @param int $countryId id.
     * @return \Cake\Http\Response|null state name and phone code.
     */

    public function getStates($countryId) {
        $this->loadModel('States');
        $this->loadModel('Countries');       

        $states = $this->States->find('list', [
            'keyField' => 'id',
            'valueField' => 'name'
        ])->where(['country_id' => $countryId]);
        $countries = $this->Countries->find()->where([
            'id' => $countryId
        ])->first();
        $phoneCode = $countries['phonecode'];
        $this->set(compact('states', 'phoneCode'));
        $this->set('_serialize', ['states', 'phoneCode']);
        if($this->request->is('ajax')){
            $this->viewBuilder()->layout('ajax');
            $this->render('/Element/Job/states');
        }
    }

/**
 * getCities method
 *
 * @param int $stateId id.
 * @return \Cake\Http\Response|null city names.
 */
    public function getCities($stateId) {
        $this->loadModel('Cities');
        $cities = $this->Cities->find('list', [
            'keyField' => 'id',
            'valueField' => 'name'
        ])->where(['state_id' => $stateId]);
        $this->set(compact('cities'));
        $this->set('_serialize', ['cities']);
        if($this->request->is('ajax')){
            $this->viewBuilder()->layout('ajax');
            $this->render('/Element/Job/cities');
        }
    }

/**
 * getInsurances method
 *
 * @param int $stateId id.
 * @return \Cake\Http\Response|null city names.
 */
    public function getInsurances($countryId) {
        $this->loadModel('States');    

        $states = $this->States->find('list', [
            'keyField' => 'id',
            'valueField' => 'name'
        ])->where(['country_id' => $countryId]);

        $this->set(compact('states'));
        $this->set('_serialize', ['states']);
        if($this->request->is('ajax')){
            $this->viewBuilder()->layout('ajax');
            $this->render('/Element/Job/insurances');
        }
    }

 /**
     * searchStates method
     *
     * @param int $countryId id.
     * @return \Cake\Http\Response|null state name and phone code.
     */

    public function searchStates($countryId) {
        $this->loadModel('States');
        $this->loadModel('Countries');       

        $states = $this->States->find('list', [
            'keyField' => 'id',
            'valueField' => 'name'
        ])->where(['country_id' => $countryId]);
        $countries = $this->Countries->find()->where([
            'id' => $countryId
        ])->first();
        $phoneCode = $countries['phonecode'];
        $this->set(compact('states', 'phoneCode'));
        $this->set('_serialize', ['states', 'phoneCode']);
        if($this->request->is('ajax')){
            $this->viewBuilder()->layout('ajax');
            $this->render('/Element/Job/states_search');
        }
    }

/**
 * searchCities method
 *
 * @param int $stateId id.
 * @return \Cake\Http\Response|null city names.
 */
    public function searchCities($stateId) {
        $this->loadModel('Cities');
        $cities = $this->Cities->find('list', [
            'keyField' => 'id',
            'valueField' => 'name'
        ])->where(['state_id' => $stateId]);
        $this->set(compact('cities'));
        $this->set('_serialize', ['cities']);
        if($this->request->is('ajax')){
            $this->viewBuilder()->layout('ajax');
            $this->render('/Element/Job/cities_search');
        }
    }

/**
 * jobListing method
 * Params     $url custom website url
 * @return \Cake\Http\Response|null.
 */

    public function jobListing($url = null) {
        $this->viewBuilder()->layout('home');
        $this->loadModel('Cities');
        $this->loadModel('States');
        $this->loadModel('Countries');
        $this->loadModel('Companies');
        
        //Clear the candidate value from session
        $session = $this->request->session();
        if ($session->check('candidateId')) {
            $session->delete('candidateId');
        }

       	$states = [];
        $cities = [];
        $country = $this->Countries->find('list', [
            'keyField' => 'id',
            'valueField' => 'name'
        ])
        ->order(['Countries.is_default' => 'ASC','Countries.name' => 'ASC']);
        // pr($_SERVER);die;
        $id = $this->Companies->find()
            ->where(['Companies.website_url' => Configure::read('SiteUrl').'Jobs/jobListing/'.$url])
            ->toArray();
        if(empty($id)) {
            return $this->redirect($this->referer());
        }
        
        try {
	        $query = $this->Jobs->find('all')
                ->contain(['Countries','States','Cities','JobTypes'])
                ->where(['Jobs.company_id' => $id[0]->id,'Jobs.active' =>1]);
                
            if(!empty($this->request->query())) {
                if(!empty($this->request->query['country_id'])){
                    $query->andWhere(['Jobs.country_id'=>$this->request->query['country_id'],'Jobs.company_id' => $id[0]->id]);
                    $states = $this->States->find('list', [
                        'keyField' => 'id',
                        'valueField' => 'name'
                    ])->where(['country_id' =>$this->request->query['country_id']]);
                }
                if(!empty($this->request->query['state_id'])){
                    $query->andWhere(['Jobs.state_id' =>$this->request->query['state_id']]);
                    $cities = $this->Cities->find('list', [
                        'keyField' => 'id',
                        'valueField' => 'name'
                    ])->where(['state_id' =>$this->request->query['state_id']]);
                }
                if(!empty($this->request->query['city_id'])){
                    $query->andWhere(['Jobs.city_id' =>$this->request->query['city_id'],'Jobs.company_id' => $id[0]->id]);
                }
                
            }
            $jobs = $this->paginate($query);            
        } catch (NotFoundException $e) {
           // redirecting to Last page if request page doesn't exist
           $this->request->query['page'] = $this->request->query['page'] -1;
           return $this->redirect([
                   'controller' => $this->request->params['controller'],
                   'action' => $this->request->params['action']
                   
               ]
           );
       }
        $this->set(compact('jobs','states','cities','country','id'));
        $this->set('_serialize', ['jobs']);
    }

/**
* apply method
 *
 * @param string|null $id Job id $companyId  company id.
 * @return \Cake\Http\Response|null
 * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
 */
    public function apply($jobid = null,$companyId)
    {
        $this->viewBuilder()->layout('home');
        $this->loadModel('Companies');
        //Clear the candidate value from session
        $session = $this->request->session();
        if ($session->check('candidateId')) {
            $session->delete('candidateId');
        }
        $id = $url = $this->Companies->find()
            ->where(['Companies.id' => base64_decode($companyId)])
            ->toArray();
        $job = $this->Jobs->find()
                ->where(['Jobs.id' => base64_decode($jobid)])
                ->contain(['Countries','States','Cities','JobTypes','Educations','Companies'])
                ->toArray();
        
        $this->set(compact('job','url','id'));
        $this->set('_serialize', ['job']);
    }

    /**
* candidateApply method
 *
 * @param string|null $id job id, $companyId company id.
 * @return \Cake\Http\Response|null
 * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
 */
    public function candidateApply($jobid, $companyId, $candidateId = null) {
        $this->viewBuilder()->layout('home');
        $this->loadModel('Countries');
        $this->loadModel('States');
        $this->loadModel('Cities');
        $this->loadModel('JobTypes');
        $this->loadModel('Candidates');
        $this->loadModel('CandidateHistories');
        $this->loadModel('Companies');
        $country = $this->Countries->find('list', [
            'keyField' => 'id',
            'valueField' => 'name'
        ])
        ->order(['Countries.is_default' => 'ASC','Countries.name' => 'ASC']);

        $state = $this->States->find('list', [
            'keyField' => 'id',
            'valueField' => 'name'
        ]);
        $city = $this->Cities->find('list', [
            'keyField' => 'id',
            'valueField' => 'name'
        ]);
        $jobtypes = $this->JobTypes->find('list', [
            'keyField' => 'id',
            'valueField' => 'job_name'
        ]);
        $id = $url = $this->Companies->find()
            ->where(['Companies.id' => base64_decode($companyId)])
            ->toArray();
        $this->Candidates->addBehavior('Geo.Geocoder', ['address' => 'zipcode']);
        $candidate = $this->Candidates->newEntity();    
        if ($this->request->is('post')) {
	        $url = $this->Companies->find()
	            ->where(['Companies.id' => base64_decode($companyId)])
	            ->first();
	        $path = preg_replace('/[^a-zA-Z0-9\']/', '-',$url->company).'-'.$url->id;
            if(!empty($this->request->session()->read('Canditate'))) {
                $data = $this->request->session()->read('Canditate');
            }

            $data = array_merge($data,$this->request->data);
            $candidate = $this->Candidates->patchEntity($candidate, $data,['associated' => ['WorkingShifts','CandidateHistories']]);
            $candidate->job_id=base64_decode($jobid);
            $candidate->company_id = base64_decode($companyId);
           
            if ($this->Candidates->save($candidate)) {
            $this->loadModel('Companies');
            $company = $this->Companies->find()
                      ->where(['Companies.id' =>  base64_decode($companyId)])
                      ->contain(['Users'])
                      ->first();
            
            $this->loadModel('EmailTemplates');
                $temp = $this->EmailTemplates->find()->where(['EmailTemplates.id' => 7])
                    -> first();    
                    $temp['mail_body'] = str_replace(
                            array(),
                            array(
                               
                            ), 
                        $temp['mail_body']
                    );
                $this->_sendEmailMessage($company['user']->email, $temp['mail_body'], $temp['subject']);
                //Get Last saved Id and Saved It in Session So as To Use It in Previous Tab
                $this->request->session()->delete('Canditate');
                $this->Flash->success(__('The candidate is saved.'), array('key' => 'positive'));
                return $this->redirect([
                    'action' => 'jobListing',$path,
                    '?' => ['popup' => '@#SD$']]);
            // $this->Flash->error(__('The job is saved.'),array('key' => 'positive'));
            } else {
                $this->Flash->error(__('The candidate could not be saved. Please, try again.'),array('key' => 'positive'));
            }
        }
        $this->request->session()->delete('Canditate');
        $this->set(compact('country','state','city','jobtypes','candidate','jobid','history','companyId','candidateId','id'));
    }

    public function setSession($id, $companyId, $candidateId = null) {        
        if ($this->request->is('post')) {
            $this->request->data['job_id'] = base64_decode($id);            
            $this->request->data['company_id'] = base64_decode($companyId);
            if($this->request->is('ajax')) {
                $this->viewBuilder()->layout('ajax');
                if(!empty($this->request->data['step']) && $this->request->data['step'] == 2) {
                    $data = $this->request->data;
                    if($this->request->session()->read('Canditate')) {
                        $tmp = $this->request->session()->read('Canditate');
                        $data = array_merge($tmp,$this->request->data);
                    }
                    $this->request->session()->write('Canditate',$data);
                    // pr($session);die;
                    $response=[
                        'status' => 200,
                    ];
                    $this->set(compact('response'));
                } elseif (!empty($this->request->data['step']) && $this->request->data['step'] == 3) {
                    $this->request->data = $this->__formatShiftData($this->request->data);
                    $tmp = $this->request->session()->read('Canditate');
                    $data = array_merge($tmp,$this->request->data);
                    $this->request->session()->write('Canditate',$data);
                    $response=[
                        'status' => 200,
                    ];
                    $this->set(compact('response'));
                }
            }
        } 
    }

/**
* saveCandidateHistory method
 *
 * @return \Cake\Http\Response|null redirects back to candidate apply page
 * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
 */
    public function saveCandidateHistory(){
        $this->loadModel('CandidateHistories');
        $history= $this->CandidateHistories->newEntity();  
        if($this->request->is('post')) {
            $response=['status' => 400];
            if(!empty($this->request->data['rounds_verified'])){
                $this->request->data['rounds_verified'] =implode(',',$this->request->data['rounds_verified']);
            }
            if(!empty($this->request->data['way_report_submitted'])){
                $this->request->data['way_report_submitted'] =implode(',',$this->request->data['way_report_submitted']);
            }
            if(!empty($this->request->data['shift_work'])){
                $this->request->data['shift_work'] =implode(',',$this->request->data['shift_work']);
            }
            if(!empty($this->request->data['equipment_issued'])){
                $this->request->data['equipment_issued'] =implode(',',$this->request->data['equipment_issued']);
            }
            if(!empty($this->request->data['vechicle_type'])){
                $this->request->data['vechicle_type'] =implode(',',$this->request->data['vechicle_type']);
            }
            $history = $this->CandidateHistories->patchEntity($history, $this->request->getData());
            $tmp['candidate_histories'] = $this->request->session()->read('Canditate.candidate_histories');
            $tmp['candidate_histories'][] = $this->request->data;
            $this->request->session()->write('Canditate.candidate_histories',$tmp['candidate_histories']);
            if($this->request->is('ajax')) {
                $historyIdInSession = count($tmp['candidate_histories'])-1;
                $this->viewBuilder()->layout('ajax');
                $this->set(compact('response', 'history','historyIdInSession'));
                $this->render('/Element/Job/history_detail_link');
            } 
        }
    }

    public function getHistory($id) {
    	$this->loadModel('CandidateHistories');
        $historyDetail = $this->CandidateHistories->newEntity();
        $histories = $this->request->session()->read('Canditate.candidate_histories'); 
        if($histories) {
            $historyDetail = $this->CandidateHistories->patchEntity($historyDetail,$histories[base64_decode($id)]);
        };
    	if($this->request->is('ajax')) {
    		$this->viewBuilder()->layout('ajax');
    		$this->set(compact('historyDetail'));
            $this->render('/Element/Job/history_detail');
    	}
    }

    public function editHistory($id){
        $this->loadModel('CandidateHistories');
        $history = $this->CandidateHistories->newEntity();
        $histories = $this->request->session()->read('Canditate.candidate_histories'); 
        if($histories) {
            $history = $this->CandidateHistories->patchEntity($history,$histories[base64_decode($id)]);
        };
        $hist_id = base64_decode($id);
        if($this->request->is('ajax')) {
            $this->viewBuilder()->layout('ajax');
            $this->set(compact('history','hist_id'));
            $this->render('/Element/Job/edit_history');
        }
        $response=[
            'status' => 400,
            'msg' => 'Something went wrong'
        ];
        if($this->request->is('post')) {
            if(!empty($this->request->data['rounds_verified'])) {
                $this->request->data['rounds_verified'] = implode(',', $this->request->data['rounds_verified']);
            }
            if(!empty($this->request->data['way_report_submitted'])) {
                $this->request->data['way_report_submitted'] = implode(',', $this->request->data['way_report_submitted']);
            }
            if(!empty($this->request->data['shift_work'])) {
                $this->request->data['shift_work'] = implode(',', $this->request->data['shift_work']);
            }
            if(!empty($this->request->data['equipment_issued'])) {
                $this->request->data['equipment_issued'] = implode(',', $this->request->data['equipment_issued']);
            }
            if(!empty($this->request->data['vechicle_type'])) {
                $this->request->data['vechicle_type'] = implode(',', $this->request->data['vechicle_type']);
            }
            $tmp['candidate_histories'] = $this->request->session()->read('Canditate.candidate_histories');
            $tmp['candidate_histories'][base64_decode($id)] = $this->request->data;
            $this->request->session()->write('Canditate.candidate_histories',$tmp['candidate_histories']);
            
        } 
        
        $this->set(compact('response'));  
    }

    protected function __formatShiftData($data) {
        $shift = [
            'step' => $data['step'],
            'date_begin_work' => $data['date_begin_work'],
            'hours_available' => $data['hours_available'],
            'working_shifts'  => []
        ];
        $i = 0;
        if(!empty($data['working_shifts'])) {            
            foreach($data['working_shifts'] as $key => $value) {
                foreach ($value as $index => $day) {  
                    $shift['working_shifts'][$i]['shift_id'] = $key;
                    $shift['working_shifts'][$i]['day_id'] = $day['day_id'];
                    $i++;
                }
            }
        }
        return $shift;
    }

    public function deactivate($id) {
        $job = $this->Jobs->find()
                            ->where([
                                'Jobs.id' => base64_decode($id)
                            ]);
        $job = $job->first();
        $job->active = 2;
        if ($this->Jobs->save($job)) {
            $this->Flash->success(__('Job has been deactivated'), array('key' => 'positive'));
            return $this->redirect(['action' => 'index']);
        }
        $this->Flash->error(__('Some errors occurred while deactivating the job. Please try again.'), array('key' => 'positive'));
        return $this->redirect(['action' => 'index']);
    }

    public function activate($id) {
        $job = $this->Jobs->find()
                            ->where([
                                'Jobs.id' => base64_decode($id)
                            ]);
        $job = $job->first();
        $job->active = 1;
        if ($this->Jobs->save($job)) {
            $this->Flash->success(__('Job has been Activated'), array('key' => 'positive'));
            return $this->redirect(['action' => 'index']);
        }
        $this->Flash->error(__('Some errors occurred while activating the job. Please try again.'), array('key' => 'positive'));
        return $this->redirect(['action' => 'index']);
    }
    
    
    public function applySecurity() {
       $this->viewBuilder()->layout('apply'); 
    }

    
}
