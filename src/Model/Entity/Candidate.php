<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Candidate Entity
 *
 * @property int $id
 * @property int $job_id
 * @property string $first_name
 * @property string $last_name
 * @property string $middle_name
 * @property string $home_phone
 * @property string $mobile
 * @property string $email
 * @property string $address1
 * @property string $address2
 * @property int $country_id
 * @property int $state_id
 * @property int $city_id
 * @property string $zipcode
 * @property int $job_type_id
 * @property int $age_under_eighteen
 * @property int $worked_before
 * @property string $when_worked
 * @property string $where_worked
 * @property string $position
 * @property string $reason_leaving
 * @property int $convicted_crime
 * @property string $crime_reason
 * @property int $driving_licence
 * @property int $licence_number
 * @property int $dependable_automobile
 * @property string $automobile_make
 * @property string $automobile_model
 * @property string $automobile_year
 * @property \Cake\I18n\FrozenDate $date_begin_work
 * @property int $hours_available
 * @property int $certified_unarmed
 * @property int $email_alerts
 * @property string $writing_test
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Job $job
 * @property \App\Model\Entity\Country $country
 * @property \App\Model\Entity\State $state
 * @property \App\Model\Entity\City $city
 * @property \App\Model\Entity\JobType $job_type
 * @property \App\Model\Entity\CandidateHistory[] $candidate_histories
 * @property \App\Model\Entity\WorkingShift[] $working_shifts
 */
class Candidate extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
