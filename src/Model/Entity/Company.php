<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Company Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $first_name
 * @property string $last_name
 * @property string $company
 * @property int $address1
 * @property int $address2
 * @property string $country
 * @property string $state
 * @property string $city
 * @property string $zipcode
 * @property int $telephone
 * @property string $know_about_us
 * @property string $website_url
 * @property string $website_name
 * @property string $website_logo
 * @property string $logo_path
 * @property string $signup_path
 *
 * @property \App\Model\Entity\User $user
 */
class Company extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
