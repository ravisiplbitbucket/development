<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CandidateMilitary Entity
 *
 * @property int $id
 * @property int $candidate_complete_id
 * @property string $military_from_year
 * @property string $military_to_year
 * @property string $reserver_unit
 * @property \Cake\I18n\FrozenDate $meeting_date
 * @property string $skills
 * @property string $branch
 *
 * @property \App\Model\Entity\CandidateComplete $candidate_complete
 */
class CandidateMilitary extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
