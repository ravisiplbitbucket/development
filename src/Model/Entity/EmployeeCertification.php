<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * EmployeeCertification Entity
 *
 * @property int $id
 * @property int $candidate_id
 * @property int $certification_id
 * @property \Cake\I18n\FrozenDate $expiration_date
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Candidate $candidate
 * @property \App\Model\Entity\Certification $certification
 */
class EmployeeCertification extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
