<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CandidateComplete Entity
 *
 * @property int $id
 * @property int $candidate_id
 * @property int $company_id
 * @property string $social_security
 * @property string $relative_employed
 * @property string $language_ids
 * @property int $is_cpr_certified
 * @property int $is_twic_certified
 * @property string $unarmed_licence_number
 * @property \Cake\I18n\FrozenDate $unarmed_expiry_date
 * @property string $armed_licence_number
 * @property \Cake\I18n\FrozenDate $armed_expiry_date
 * @property \Cake\I18n\FrozenDate $cpr_expiry_date
 * @property \Cake\I18n\FrozenDate $twic_expiry_date
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Candidate $candidate
 * @property \App\Model\Entity\Company $company
 * @property \App\Model\Entity\CandidateEducation[] $candidate_educations
 * @property \App\Model\Entity\CandidateMilitary[] $candidate_militaries
 * @property \App\Model\Entity\CandidateReference[] $candidate_references
 */
class CandidateComplete extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
