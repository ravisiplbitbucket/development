<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CandidateEducation Entity
 *
 * @property int $id
 * @property int $candidate_complete_id
 * @property int $education_id
 * @property string $from_year
 * @property string $to_year
 * @property string $grade
 * @property string $degree
 *
 * @property \App\Model\Entity\CandidateComplete $candidate_complete
 * @property \App\Model\Entity\Education $education
 */
class CandidateEducation extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
