<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Job Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $job_number
 * @property string $job_title
 * @property int $country_id
 * @property int $state_id
 * @property int $city_id
 * @property string $zipcode
 * @property string $company_overview
 * @property string $responsibility
 * @property string $qualification
 * @property string $experience
 * @property string $certification
 * @property string $language_skills
 * @property string $other_qualification
 * @property string $physical_demands
 * @property int $miles
 * @property int $job_type_id
 * @property int $education_id
 * @property float $salary_from
 * @property float $salary_to
 * @property int $active
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Country $country
 * @property \App\Model\Entity\State $state
 * @property \App\Model\Entity\City $city
 * @property \App\Model\Entity\JobType $job_type
 * @property \App\Model\Entity\Education $education
 */
class Job extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
