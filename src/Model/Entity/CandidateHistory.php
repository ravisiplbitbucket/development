<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CandidateHistory Entity
 *
 * @property int $id
 * @property int $candidate_id
 * @property string $company_name
 * @property string $from_work
 * @property string $to_work
 * @property string $where_employ
 * @property string $designation
 * @property int $security_company
 * @property int $report_client
 * @property string $client_name
 * @property int $may_we_contact
 * @property string $kind_site
 * @property float $rate_paid
 * @property int $foot_patrol
 * @property int $hours_per_shift
 * @property int $perform_patrols
 * @property string $rounds_verified
 * @property int $required_submit_report
 * @property string $way_report_submitted
 * @property string $shift_work
 * @property int $is_site_armed
 * @property string $equipment_issued
 * @property int $used_equipment
 * @property int $contact_emergency
 * @property int $ever_injured
 * @property int $operate_vehicle
 * @property string $vechicle_type
 *
 * @property \App\Model\Entity\Candidate $candidate
 */
class CandidateHistory extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
