<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Users Model
 *
 * @property \App\Model\Table\UserRolesTable|\Cake\ORM\Association\BelongsTo $UserRoles
 * @property \App\Model\Table\CompaniesTable|\Cake\ORM\Association\HasMany $Companies
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UsersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('UserRoles', [
            'foreignKey' => 'user_role_id',
            'joinType' => 'INNER'
        ]);
        $this->hasOne('Companies', [
            'foreignKey' => 'user_id'
        ]);

        $this->hasOne('UserProfiles', [
            'foreignKey' => 'user_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);

        $this->hasOne('CompanyUsers', [
            'foreignKey' => 'user_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);
        $this->hasMany('Divisions', [
            'foreignKey' => 'user_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmpty('email');
            // ->add('email', 
            //     'unique', [
            //         'rule' => 'validateUnique',
            //         'provider' => 'table',
            //         'message' => __('Email already exists.')
            //     ]);

        // $validator->add('email', 'custom', [
        //     'rule' => function ($value, $context){
        //         $condition = [
        //                 'Users.email' => $value
                        
        //             ];

        //         if(!empty($context['data']['company_user'])) {
                   
        //             $condition['OR'] = [
        //             'CompanyUsers.company_user_id <>' => $context['data']['company_user']['company_user_id'],
        //             'Companies.user_id <>' => $context['data']['company_user']['company_user_id']

        //             ];
        //         }
                
        //         $data = $this->find('all',[
        //                 'conditions' => $condition,
        //                 'contain' => ['Companies','CompanyUsers']
        //             ]
        //         )->first();
                
        //         if(!empty($data)) {
        //             return false;
        //         }
               
        //         return true;
        //     },
        //     'message' => 'This Email already registed.',            
        // ]);

        $validator
            ->requirePresence('username', 'create')
            ->notEmpty('username')
            ->add('username', 
                'unique', [
                    'rule' => 'validateUnique',
                    'provider' => 'table',
                    'message' => __('Username already exists.')
                ]);;


        return $validator;
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationRelief(Validator $validator)
    {
        $validator = $this->validationDefault($validator);
        $validator->remove('email');
        $validator->remove('password');
        // $validator->remove('username');
        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        // $rules->add($rules->isUnique(['email']));
        // $rules->add($rules->isUnique(['username']));
        $rules->add($rules->existsIn(['user_role_id'], 'UserRoles'));

        return $rules;
    }

    public function findAuth(\Cake\ORM\Query $query, array $options)
    {
        $query
            ->where(['Users.active' => 1])
            ->contain(['Companies','CompanyUsers']);

        return $query;
    }
}
