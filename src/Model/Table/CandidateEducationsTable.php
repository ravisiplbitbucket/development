<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CandidateEducations Model
 *
 * @property \App\Model\Table\CandidateCompletesTable|\Cake\ORM\Association\BelongsTo $CandidateCompletes
 * @property \App\Model\Table\EducationsTable|\Cake\ORM\Association\BelongsTo $Educations
 *
 * @method \App\Model\Entity\CandidateEducation get($primaryKey, $options = [])
 * @method \App\Model\Entity\CandidateEducation newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CandidateEducation[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CandidateEducation|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CandidateEducation patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CandidateEducation[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CandidateEducation findOrCreate($search, callable $callback = null, $options = [])
 */
class CandidateEducationsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('candidate_educations');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('CandidateCompletes', [
            'foreignKey' => 'candidate_complete_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Educations', [
            'foreignKey' => 'education_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('from_year', 'create')
            ->notEmpty('from_year');

        $validator
            ->requirePresence('to_year', 'create')
            ->notEmpty('to_year');

        $validator
            ->requirePresence('grade', 'create')
            ->notEmpty('grade');

        $validator
            ->requirePresence('degree', 'create')
            ->notEmpty('degree');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['candidate_complete_id'], 'CandidateCompletes'));
        $rules->add($rules->existsIn(['education_id'], 'Educations'));

        return $rules;
    }
}
