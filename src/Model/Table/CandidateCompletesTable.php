<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CandidateCompletes Model
 *
 * @property \App\Model\Table\CandidatesTable|\Cake\ORM\Association\BelongsTo $Candidates
 * @property \App\Model\Table\CompaniesTable|\Cake\ORM\Association\BelongsTo $Companies
 * @property \App\Model\Table\CandidateEducationsTable|\Cake\ORM\Association\HasMany $CandidateEducations
 * @property \App\Model\Table\CandidateMilitariesTable|\Cake\ORM\Association\HasMany $CandidateMilitaries
 * @property \App\Model\Table\CandidateReferencesTable|\Cake\ORM\Association\HasMany $CandidateReferences
 *
 * @method \App\Model\Entity\CandidateComplete get($primaryKey, $options = [])
 * @method \App\Model\Entity\CandidateComplete newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CandidateComplete[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CandidateComplete|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CandidateComplete patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CandidateComplete[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CandidateComplete findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CandidateCompletesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('candidate_completes');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Candidates', [
            'foreignKey' => 'candidate_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Companies', [
            'foreignKey' => 'company_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('CandidateEducations', [
            'foreignKey' => 'candidate_complete_id'
        ]);
        $this->hasOne('CandidateMilitaries', [
            'foreignKey' => 'candidate_complete_id'
        ]);
        $this->hasMany('CandidateReferences', [
            'foreignKey' => 'candidate_complete_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        // $validator
        //     ->requirePresence('social_security', 'create')
        //     ->notEmpty('social_security');

        // $validator
        //     ->requirePresence('language_ids', 'create')
        //     ->notEmpty('language_ids');

        // $validator
        //     ->integer('is_cpr_certified')
        //     ->requirePresence('is_cpr_certified', 'create')
        //     ->notEmpty('is_cpr_certified');

        // $validator
        //     ->integer('is_twic_certified')
        //     ->requirePresence('is_twic_certified', 'create')
        //     ->notEmpty('is_twic_certified');

        // $validator
        //     ->notEmpty('unarmed_licence_number');

        // $validator
        //     ->notEmpty('unarmed_expiry_date');

        // $validator
        //     ->notEmpty('armed_licence_number');

        // $validator
        //     ->notEmpty('armed_expiry_date');

        // $validator
        //     ->notEmpty('cpr_expiry_date');

        // $validator
        //     ->notEmpty('twic_expiry_date');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['candidate_id'], 'Candidates'));
     

        return $rules;
    }

    public function beforeSave($event, $entity, $options) {
        if ($entity->has('unarmed_expiry_date') && !empty($entity->unarmed_expiry_date)) {
            $entity->unarmed_expiry_date = date('Y-m-d',strtotime(preg_replace('/\//', '-', $entity->unarmed_expiry_date)));           
        }
        if ($entity->has('armed_expiry_date') && !empty($entity->armed_expiry_date)) {
            $entity->armed_expiry_date = date('Y-m-d',strtotime(preg_replace('/\//', '-', $entity->armed_expiry_date)));           
        }
        if ($entity->has('cpr_expiry_date') && !empty($entity->cpr_expiry_date)) {
            $entity->cpr_expiry_date = date('Y-m-d',strtotime(preg_replace('/\//', '-', $entity->cpr_expiry_date)));           
        }
        if ($entity->has('twic_expiry_date') && !empty($entity->twic_expiry_date)) {
            $entity->twic_expiry_date = date('Y-m-d',strtotime(preg_replace('/\//', '-', $entity->twic_expiry_date)));           
        }
    }
}
