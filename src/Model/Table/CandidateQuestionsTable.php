<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CandidateMilitaries Model
 *
 * @property \App\Model\Table\CandidateCompletesTable|\Cake\ORM\Association\BelongsTo $CandidateCompletes
 *
 * @method \App\Model\Entity\CandidateMilitary get($primaryKey, $options = [])
 * @method \App\Model\Entity\CandidateMilitary newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CandidateMilitary[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CandidateMilitary|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CandidateMilitary patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CandidateMilitary[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CandidateMilitary findOrCreate($search, callable $callback = null, $options = [])
 */
class CandidateQuestionsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('candidate_questions');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('CompanyDeleteQuestions', [
            'foreignKey' => 'question_id'
        ]);
    }
}