<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CandidateMilitaries Model
 *
 * @property \App\Model\Table\CandidateCompletesTable|\Cake\ORM\Association\BelongsTo $CandidateCompletes
 *
 * @method \App\Model\Entity\CandidateMilitary get($primaryKey, $options = [])
 * @method \App\Model\Entity\CandidateMilitary newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CandidateMilitary[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CandidateMilitary|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CandidateMilitary patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CandidateMilitary[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CandidateMilitary findOrCreate($search, callable $callback = null, $options = [])
 */
class CandidateMilitariesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('candidate_militaries');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('CandidateCompletes', [
            'foreignKey' => 'candidate_complete_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        // $validator
        //     ->requirePresence('military_from_year', 'create')
        //     ->notEmpty('military_from_year');

        // $validator
        //     ->requirePresence('military_to_year', 'create')
        //     ->notEmpty('military_to_year');

        // $validator
        //     ->requirePresence('reserver_unit', 'create')
        //     ->notEmpty('reserver_unit');

        // $validator
        //     ->requirePresence('meeting_date', 'create')
        //     ->notEmpty('meeting_date');

        // $validator
        //     ->requirePresence('skills', 'create')
        //     ->notEmpty('skills');

        // $validator
        //     ->requirePresence('branch', 'create')
        //     ->notEmpty('branch');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['candidate_complete_id'], 'CandidateCompletes'));

        return $rules;
    }

    public function beforeSave($event, $entity, $options) {
        if ($entity->has('meeting_date') && !empty($entity->meeting_date)) {
            $entity->meeting_date = date('Y-m-d',strtotime(preg_replace('/\//', '-', $entity->meeting_date)));           
        }
    }
}
