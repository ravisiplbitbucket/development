<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CandidateHistories Model
 *
 * @property \App\Model\Table\CandidatesTable|\Cake\ORM\Association\BelongsTo $Candidates
 *
 * @method \App\Model\Entity\CandidateHistory get($primaryKey, $options = [])
 * @method \App\Model\Entity\CandidateHistory newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CandidateHistory[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CandidateHistory|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CandidateHistory patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CandidateHistory[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CandidateHistory findOrCreate($search, callable $callback = null, $options = [])
 */
class CandidateHistoriesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('candidate_histories');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Candidates', [
            'foreignKey' => 'candidate_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('company_name', 'create')
            ->notEmpty('company_name');

        $validator
            ->requirePresence('from_work', 'create')
            ->notEmpty('from_work');

        $validator
            ->requirePresence('to_work', 'create')
            ->notEmpty('to_work');

        $validator
            ->requirePresence('where_employ', 'create')
            ->notEmpty('where_employ');

        $validator
            ->requirePresence('designation', 'create')
            ->notEmpty('designation');

/*        $validator
            ->integer('security_company')
            ->requirePresence('security_company', 'create')
            ->notEmpty('security_company');

        $validator
            ->integer('report_client')
            ->requirePresence('report_client', 'create')
            ->notEmpty('report_client');

        $validator
            ->requirePresence('client_name', 'create')
            ->notEmpty('client_name');

        $validator
            ->integer('may_we_contact')
            ->requirePresence('may_we_contact', 'create')
            ->notEmpty('may_we_contact');

        $validator
            ->requirePresence('kind_site', 'create')
            ->notEmpty('kind_site');

        $validator
            ->decimal('rate_paid')
            ->requirePresence('rate_paid', 'create')
            ->notEmpty('rate_paid');

        $validator
            ->integer('foot_patrol')
            ->requirePresence('foot_patrol', 'create')
            ->notEmpty('foot_patrol');

        $validator
            ->integer('hours_per_shift')
            ->requirePresence('hours_per_shift', 'create')
            ->notEmpty('hours_per_shift');

        $validator
            ->integer('perform_patrols')
            ->requirePresence('perform_patrols', 'create')
            ->notEmpty('perform_patrols');

        $validator
            ->requirePresence('rounds_verified', 'create')
            ->notEmpty('rounds_verified');

        $validator
            ->integer('required_submit_report')
            ->requirePresence('required_submit_report', 'create')
            ->notEmpty('required_submit_report');

        $validator
            ->requirePresence('way_report_submitted', 'create')
            ->notEmpty('way_report_submitted');

        $validator
            ->requirePresence('shift_work', 'create')
            ->notEmpty('shift_work');

        $validator
            ->integer('is_site_armed')
            ->requirePresence('is_site_armed', 'create')
            ->notEmpty('is_site_armed');

        $validator
            ->requirePresence('equipment_issued', 'create')
            ->notEmpty('equipment_issued');

        $validator
            ->integer('used_equipment')
            ->requirePresence('used_equipment', 'create')
            ->notEmpty('used_equipment');

        $validator
            ->integer('contact_emergency')
            ->requirePresence('contact_emergency', 'create')
            ->notEmpty('contact_emergency');

        $validator
            ->integer('ever_injured')
            ->requirePresence('ever_injured', 'create')
            ->notEmpty('ever_injured');

        $validator
            ->integer('operate_vehicle')
            ->requirePresence('operate_vehicle', 'create')
            ->notEmpty('operate_vehicle');

        $validator
            ->requirePresence('vechicle_type', 'create')
            ->notEmpty('vechicle_type');
*/
        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['candidate_id'], 'Candidates'));

        return $rules;
    }

    public function beforeSave($event, $entity, $options) {
        if ($entity->has('date_started') && !empty($entity->date_started)) {
            $entity->date_started = date('Y-m-d',strtotime(preg_replace('/\//', '-', $entity->date_started)));
        }
        if ($entity->has('date_ended') && !empty($entity->date_ended)) {
            $entity->date_ended = date('Y-m-d',strtotime(preg_replace('/\//', '-', $entity->date_ended)));           
        }
    }
}
