<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
/**
 * Candidates Model
 *
 * @property \App\Model\Table\JobsTable|\Cake\ORM\Association\BelongsTo $Jobs
 * @property \App\Model\Table\CountriesTable|\Cake\ORM\Association\BelongsTo $Countries
 * @property \App\Model\Table\StatesTable|\Cake\ORM\Association\BelongsTo $States
 * @property \App\Model\Table\CitiesTable|\Cake\ORM\Association\BelongsTo $Cities
 * @property \App\Model\Table\JobTypesTable|\Cake\ORM\Association\BelongsTo $JobTypes
 * @property \App\Model\Table\CandidateHistoriesTable|\Cake\ORM\Association\HasMany $CandidateHistories
 * @property \App\Model\Table\WorkingShiftsTable|\Cake\ORM\Association\HasMany $WorkingShifts
 *
 * @method \App\Model\Entity\Candidate get($primaryKey, $options = [])
 * @method \App\Model\Entity\Candidate newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Candidate[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Candidate|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Candidate patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Candidate[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Candidate findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CandidatesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('candidates');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Jobs', [
            'foreignKey' => 'job_id'
        ]);
        $this->belongsTo('Countries', [
            'foreignKey' => 'country_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('States', [
            'foreignKey' => 'state_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Cities', [
            'foreignKey' => 'city_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('JobTypes', [
            'foreignKey' => 'job_type_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('CandidateHistories', [
            'foreignKey' => 'candidate_id'
        ]);
        $this->hasMany('WorkingShifts', [
            'foreignKey' => 'candidate_id',
            'saveStrategy' => 'replace'
        ]);
        $this->hasMany('CandidateHistories', [
            'foreignKey' => 'candidate_id'
        ]);
        $this->hasMany('CandidateCompletes', [
            'foreignKey' => 'candidate_id'
        ]);
        $this->hasMany('CandidateQuestions', [
            'foreignKey' => 'candidate_id',
        ]);
        $this->addBehavior('Proffer.Proffer', [
            'image' => [    // The name of your upload field
                'root' => WWW_ROOT . 'candidate', // Customise the root upload folder here, or omit to use the default
                'dir' => 'image_path',   // The name of the field to store the folder
                'thumbnailSizes' => [ // Declare your thumbnails
                    'square' => [   // Define the prefix of your thumbnail
                        'w' => 350, // Width
                        'h' => 107, // Height
                        'fit'  => true
                    ]
                ],
                'thumbnailMethod' => 'gd'   // Options are Imagick or Gd
            ]
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');
        $validator
            ->allowEmpty('image');
       /* $validator
            ->requirePresence('first_name', 'create')
            ->notEmpty('first_name');

        $validator
            ->requirePresence('last_name', 'create')
            ->notEmpty('last_name');

        $validator
            ->requirePresence('middle_name', 'create')
            ->notEmpty('middle_name');

        $validator
            ->requirePresence('home_phone', 'create')
            ->notEmpty('home_phone');

        $validator
            ->requirePresence('mobile', 'create')
            ->notEmpty('mobile');

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmpty('email');

        $validator
            ->requirePresence('address1', 'create')
            ->notEmpty('address1');

        $validator
            ->requirePresence('address2', 'create')
            ->notEmpty('address2');

        $validator
            ->requirePresence('zipcode', 'create')
            ->notEmpty('zipcode');

        $validator
            ->integer('age_under_eighteen')
            ->requirePresence('age_under_eighteen', 'create')
            ->notEmpty('age_under_eighteen');

        $validator
            ->integer('worked_before')
            ->requirePresence('worked_before', 'create')
            ->notEmpty('worked_before');

        $validator
            ->requirePresence('when_worked', 'create')
            ->notEmpty('when_worked');
  
        $validator
            ->requirePresence('where_worked', 'create')
            ->notEmpty('where_worked');

        $validator
            ->requirePresence('position', 'create')
            ->notEmpty('position');

        $validator
            ->requirePresence('reason_leaving', 'create')
            ->notEmpty('reason_leaving');

        $validator
            ->integer('convicted_crime')
            ->requirePresence('convicted_crime', 'create')
            ->notEmpty('convicted_crime');

        $validator
            ->requirePresence('crime_reason', 'create')
            ->notEmpty('crime_reason');

        $validator
            ->integer('driving_licence')
            ->requirePresence('driving_licence', 'create')
            ->notEmpty('driving_licence');

        $validator
            ->integer('licence_number')
            ->requirePresence('licence_number', 'create')
            ->notEmpty('licence_number');

        $validator
            ->integer('dependable_automobile')
            ->requirePresence('dependable_automobile', 'create')
            ->notEmpty('dependable_automobile');

        $validator
            ->requirePresence('automobile_make', 'create')
            ->notEmpty('automobile_make');

        $validator
            ->requirePresence('automobile_model', 'create')
            ->notEmpty('automobile_model');

        $validator
            ->requirePresence('automobile_year', 'create')
            ->notEmpty('automobile_year');

        $validator
            ->date('date_begin_work')
            ->requirePresence('date_begin_work', 'create')
            ->notEmpty('date_begin_work');

        $validator
            ->integer('hours_available')
            ->requirePresence('hours_available', 'create')
            ->notEmpty('hours_available');

        $validator
            ->integer('certified_unarmed')
            ->requirePresence('certified_unarmed', 'create')
            ->notEmpty('certified_unarmed');

        $validator
            ->integer('email_alerts')
            ->requirePresence('email_alerts', 'create')
            ->notEmpty('email_alerts');

        $validator
            ->requirePresence('writing_test', 'create')
            ->notEmpty('writing_test');
         */
        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
/*    public function buildRules(RulesChecker $rules)
    {
        //$rules->add($rules->isUnique(['email']));
        $rules->add($rules->existsIn(['job_id'], 'Jobs'));
        //$rules->add($rules->existsIn(['country_id'], 'Countries'));
       // $rules->add($rules->existsIn(['state_id'], 'States'));
        //$rules->add($rules->existsIn(['city_id'], 'Cities'));
        //$rules->add($rules->existsIn(['job_type_id'], 'JobTypes'));

        return $rules;
    }*/

    public function calculate($id) {
    $candidate = $this->find()
                ->where(['Candidates.id' => $id])
                ->contain(['CandidateCompletes' => ['CandidateEducations' => ['Educations'],'CandidateMilitaries'],'Jobs','JobTypes', 'CandidateHistories'])
                ->first();
    //pr($candidate);die;
    $experience = $armed = $age = $licence = $auto = $year = $education_score = $cpr_certified = $twic_certified = $writing = $job_type = $match_education = $injured = $accident = $year_experience = $benefits = $salary_benefits = $year_diff = 0;

        if($candidate->experience < 3) {
            $experience = 3.33;
        } elseif ($candidate->experience >= 3 && $candidate->experience <= 7) {
            $experience = 6.67;
        } elseif ($candidate->experience < 7) {
            $experience = 10;
        }

        if($candidate->certified_armed == 1) {
            $armed = 5;
        } else {
            $armed = 0;
        }

        if($candidate->age_under_eighteen == 1) {
            $age = 2;
        } else {
            $age = 0;
        }

        if($candidate->driving_licence == 1) {
            $licence = 2;
        }else {
            $licence = 0;
        }

        if($candidate->dependable_automobile ==1) {
            $auto = 2;
        }else {
            $auto = 0;
        }

        if(!empty($candidate['candidate_completes'])) {
            if(!empty($candidate['candidate_completes'][0]['candidate_military'])) {
                if(!empty($candidate['candidate_completes'][0]['candidate_military']->military_to_year)) {
                    $year_to = explode('/', $candidate['candidate_completes'][0]['candidate_military']->military_to_year);
                }
                
                if(!empty($candidate['candidate_completes'][0]['candidate_military']->military_from_year)) {
                    $year_from = explode('/', $candidate['candidate_completes'][0]['candidate_military']->military_from_year);
                }
                
                if(!empty($year_to[1]) && !empty($year_from[1])) {
                    $year_diff = $year_to[1] - $year_from[1];
                }
                
                if($year_diff >= 10) {
                    $year = 5;
                }else {
                    $year = 0;
                } 
            }
            

            foreach($candidate['candidate_completes'][0]['candidate_educations'] as $education) {
                    $candidate_education[] = $education->education_id;
            }
            
            if(empty($candidate_education)) {
                $education_score = 0;
            }elseif (max($candidate_education) == 1) {
                $education_score = 5;
            } elseif (max($candidate_education) >= 2) {
                $education_score = 10 ;
            }

            if($candidate['candidate_completes'][0]->is_cpr_certified == 1) {
                $cpr_certified = 1;
            } 

            if($candidate['candidate_completes'][0]->is_twic_certified == 1) {
                $twic_certified = 1;
            }

            if(!empty($candidate_education)) {
                if(in_array($candidate['job']->education_id,$candidate_education)) {
                $match_education = 10;
                } 
            }
           
        }
        

        if(!empty($candidate->writing_test)) {
            $writing = 15;
        } 

        if($candidate->job_type_id == $candidate['job']->id) {
            $job_type = 10;
        } 

        
        foreach($candidate['candidate_histories'] as $history) {
            if($history->ever_injured) {
                $injured = -2;
            }
            if($history->accident_vehicle) {
                $accident = -2;
            }

            $diff = $this->dateDifference($history->from_work,$history->to_work,'%y').'.'.$this->dateDifference($history->from_work,$history->to_work,'%m');
            $year_experience = $year_experience + (float)$diff;

            $benefits = $benefits + $history->rate_paid;
        }

        $jobs = TableRegistry::get('Jobs');
        $job = $jobs->find()
                ->where(['Jobs.id' => $candidate->job_id])
                ->first();

        if(!empty($candidate['candidate_histories'])){
            $salary = $benefits/count($candidate['candidate_histories']);
            $salary_from = $job->salary_from;
            $salary_to = $job->salary_to;

            if($salary >= $salary_from && $salary < $salary_to) {
                $salary_benefits = 10;
            } elseif ($salary == $salary_to) {
                $salary_benefits = 5;
            } else {
                $salary_benefits = 0;
            }
        }   
        
       
        

        if($year_experience <= 3) {
            $security_experience = 3.33;
        } elseif ($year_experience >= 3 && $year_experience <= 7) {
            $security_experience = 6.67;
        } elseif ($year_experience > 7) {
           $security_experience = 10;
        }
        $total_point = $experience + $armed + $age + $licence + $auto + $year + $education_score + $cpr_certified + $twic_certified + $writing + $job_type + $match_education + $injured + $accident + $security_experience + $salary_benefits ;

        return $total_point;
    }

    function dateDifference($date_1 , $date_2 , $differenceFormat = '%y' )
    {
       $datetime1 = date_create($date_1);
       $datetime2 = date_create($date_2);
     
       $interval = date_diff($datetime1, $datetime2);
     
       return $interval->format($differenceFormat);
     
    }
}
