<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
/**
 * CompanyDeleteQuestions Model
 *
 * @property \App\Model\Table\CompaniesTable|\Cake\ORM\Association\BelongsTo $Companies
 * @property \App\Model\Table\InterviewQuestionsTable|\Cake\ORM\Association\BelongsTo $InterviewQuestions
 *
 * @method \App\Model\Entity\CompanyDeleteQuestion get($primaryKey, $options = [])
 * @method \App\Model\Entity\CompanyDeleteQuestion newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CompanyDeleteQuestion[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CompanyDeleteQuestion|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CompanyDeleteQuestion patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CompanyDeleteQuestion[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CompanyDeleteQuestion findOrCreate($search, callable $callback = null, $options = [])
 */
class CompanyDeleteQuestionsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('company_delete_questions');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Companies', [
            'foreignKey' => 'company_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('InterviewQuestions', [
            'foreignKey' => 'interview_question_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');


        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['company_id'], 'Companies'));
        $rules->add($rules->existsIn(['interview_question_id'], 'InterviewQuestions'));

        return $rules;
    }
    
    public function setDefault($id) {
        $table = TableRegistry::get('InterviewQuestions');
        $questions = $table->find('list', [
            'keyField' => 'id',
            'valueField' => 'id'
        ])
        ->limit(15);
        foreach($questions as $key => $question) {
            $question_id[] = [
            'company_id' => $id,
            'interview_question_id' => $key
            ];
        }
        
        if(!empty($question_id)) {
           $entities = $this->newEntities($question_id);
           if($this->saveMany($entities)){
               return true;
           }
       }
       return false;
        
    }
}
